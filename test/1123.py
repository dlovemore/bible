>>> from bible import *
>>> [int('1123',x) for x in span(4,36)]
[91, 163, 267, 409, 595, 831, 1123, 1477, 1899, 2395, 2971, 3633, 4387, 5239, 6195, 7261, 8443, 9747, 11179, 12745, 14451, 16303, 18307, 20469, 22795, 25291, 27963, 30817, 33859, 37095, 40531, 44173, 48027]
>>> Sel(b,ix=_[:28])
Genesis 4:12;7:4;11:1;17:12;24:4;30:1;38:4;49:4;Exodus 14:10;30:13;Leviticus 9:18;Numbers 1:29;22:12;Deuteronomy 11:31;Joshua 14:8;1 Samuel 2:21;2 Samuel 16:17;2 Kings 8:20;1 Chronicles 29:15;Esther 2:21;Psalms 37:1;143:10;Isaiah 34:4;Ezekiel 1:5;Zephaniah 1:8;Luke 8:46;Romans 2:1;Revelation 7:7 (28 verses)
>>> s=_
>>> s.ix
[91, 163, 267, 409, 595, 831, 1123, 1477, 1899, 2395, 2971, 3633, 4387, 5239, 6195, 7261, 8443, 9747, 11179, 12745, 14451, 16303, 18307, 20469, 22795, 25291, 27963, 30817]
>>> s&p
Genesis 4:12 When thou tillest the ground, it shall not henceforth yield unto thee her strength; a fugitive and a vagabond shalt thou be in the earth.
Genesis 7:4 For yet seven days, and I will cause it to rain upon the earth forty days and forty nights; and every living substance that I have made will I destroy from off the face of the earth.
Genesis 11:1 And the whole earth was of one language, and of one speech.
Genesis 17:12 And he that is eight days old shall be circumcised among you, every man child in your generations, he that is born in the house, or bought with money of any stranger, which is not of thy seed.
Genesis 24:4 But thou shalt go unto my country, and to my kindred, and take a wife unto my son Isaac.
Genesis 30:1 And when Rachel saw that she bare Jacob no children, Rachel envied her sister; and said unto Jacob, Give me children, or else I die.
Genesis 38:4 And she conceived again, and bare a son; and she called his name Onan.
Genesis 49:4 Unstable as water, thou shalt not excel; because thou wentest up to thy father's bed; then defiledst thou it: he went up to my couch.
Exodus 14:10 And when Pharaoh drew nigh, the children of Israel lifted up their eyes, and, behold, the Egyptians marched after them; and they were sore afraid: and the children of Israel cried out unto the LORD.
Exodus 30:13 This they shall give, every one that passeth among them that are numbered, half a shekel after the shekel of the sanctuary: (a shekel is twenty gerahs:) an half shekel shall be the offering of the LORD.
Leviticus 9:18 He slew also the bullock and the ram for a sacrifice of peace offerings, which was for the people: and Aaron's sons presented unto him the blood, which he sprinkled upon the altar round about,
Numbers 1:29 Those that were numbered of them, even of the tribe of Issachar, were fifty and four thousand and four hundred.
Numbers 22:12 And God said unto Balaam, Thou shalt not go with them; thou shalt not curse the people: for they are blessed.
Deuteronomy 11:31 For ye shall pass over Jordan to go in to possess the land which the LORD your God giveth you, and ye shall possess it, and dwell therein.
Joshua 14:8 Nevertheless my brethren that went up with me made the heart of the people melt: but I wholly followed the LORD my God.
1 Samuel 2:21 And the LORD visited Hannah, so that she conceived, and bare three sons and two daughters. And the child Samuel grew before the LORD.
2 Samuel 16:17 And Absalom said to Hushai, Is this thy kindness to thy friend? why wentest thou not with thy friend?
2 Kings 8:20 In his days Edom revolted from under the hand of Judah, and made a king over themselves.
1 Chronicles 29:15 For we are strangers before thee, and sojourners, as were all our fathers: our days on the earth are as a shadow, and there is none abiding.
Esther 2:21 In those days, while Mordecai sat in the king's gate, two of the king's chamberlains, Bigthan and Teresh, of those which kept the door, were wroth, and sought to lay hands on the king Ahasuerus.
Psalms 37:1 Fret not thyself because of evildoers, neither be thou envious against the workers of iniquity.
Psalms 143:10 Teach me to do thy will; for thou art my God: thy spirit is good; lead me into the land of uprightness.
Isaiah 34:4 And all the host of heaven shall be dissolved, and the heavens shall be rolled together as a scroll: and all their host shall fall down, as the leaf falleth off from the vine, and as a falling fig from the fig tree.
Ezekiel 1:5 Also out of the midst thereof came the likeness of four living creatures. And this was their appearance; they had the likeness of a man.
Zephaniah 1:8 And it shall come to pass in the day of the LORD's sacrifice, that I will punish the princes, and the king's children, and all such as are clothed with strange apparel.
Luke 8:46 And Jesus said, Somebody hath touched me: for I perceive that virtue is gone out of me.
Romans 2:1 Therefore thou art inexcusable, O man, whosoever thou art that judgest: for wherein thou judgest another, thou condemnest thyself; for thou that judgest doest the same things.
Revelation 7:7 Of the tribe of Simeon were sealed twelve thousand. Of the tribe of Levi were sealed twelve thousand. Of the tribe of Issachar were sealed twelve thousand.
>>> 
>>> 
