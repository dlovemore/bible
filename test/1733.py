>>> from bible import *
>>> tells("JESUS OF NAZARETH THE KING OF THE JEWS")
JESUS OF NAZARETH THE KING OF THE JEWS   =
  5    2     8     3    4   2  3    4   31
  74  21    93     33  41  21  33  57   373
 515  66   1155   213  86  66 213  615 2929
>>> tells("Authorised King James Version")
Authorised King James Version   =
    10       4    5      7     26
    120     41    48    102    311
    777     86   156    714   1733
>>> 1733*ns
[1733] [270]
>>> 
>>> tells("ὁ λόγος")
 ὁ λόγος  =
 1   5    6
16   67   83
70  373  443
>>> 1733*np
270
>>> tells("MENE MENE TEKEL PERES")
MENE MENE TEKEL PERES  =
  4    4    5     5    18
 37   37    53    63  190
 100  100  260   270  730
>>> ὁ λόγος373*pn
2549
>>> 
