# >>> from bible import *
# >>> IJohn[5:7:8]
# 1 John 5:7 For there are three that bear record in heaven, the Father, the Word, and the Holy Ghost: and these three are one.
# 1 John 5:8 And there are three that bear witness in earth, the Spirit, and the water, and the blood: and these three agree in one.
# >>> 
# >>> from collections import Counter
# >>> a=Counter('the father the word and the holy ghost')
# >>> b=Counter('the spirit and the water and the blood')
# >>> a&b
# Counter({' ': 7, 't': 5, 'e': 4, 'h': 3, 'a': 2, 'r': 2, 'o': 2, 'd': 2, 'w': 1, 'n': 1, 'l': 1, 's': 1})
# >>> a+b
# Counter({' ': 14, 't': 10, 'h': 9, 'e': 8, 'a': 5, 'o': 5, 'd': 5, 'r': 4, 'n': 3, 'w': 2, 'l': 2, 's': 2, 'i': 2, 'f': 1, 'y': 1, 'g': 1, 'p': 1, 'b': 1})
# >>> a-b
# Counter({'h': 3, 'f': 1, 'o': 1, 'y': 1, 'g': 1})
# >>> b-a
# Counter({'i': 2, 'p': 1, 'a': 1, 'n': 1, 'd': 1, 'b': 1})
# >>> 
