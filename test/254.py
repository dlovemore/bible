>>> from bible import *
>>> tells('light')
 l i g h  t   =
 1 1 1 1  1   5
12 9 7 8  20  56
30 9 7 8 200 254
>>> 
>>> c/254
1180285.2677165354
>>> fr
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'fr' is not defined
>>> fr
frac(       fractions   from        frozenset(  
>>> fr
frac(       fractions   from        frozenset(  
>>> frac(c,254)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: frac() takes 1 positional argument but 2 were given
>>> frac
<function frac at 0xb64bb348>
>>> fr=fractions.Fraction
>>> fr(c,254)
Fraction(149896229, 127)
>>> c/254
1180285.2677165354
>>> 913/pi
290.6169260858009
>>> 913/pi/137
2.1212914312832183
>>> psi*82
-50.67878707749138
>>> psi**2
0.3819660112501052
>>> psi**2*360
137.50776405003788
>>> 913/_
6.639625088135149
>>> 913/_/pi
43.7700807241551
>>> 913/(psi**2*pi)
760.8449901986426
>>> 913/(psi**2*360*pi)
2.1134583061073404
>>> 913/(psi**2*360)
6.639625088135149
>>> 913*(psi**2*360)
125544.58857768458
>>> 913*(psi**2*360)/pi
39962.083701153606
>>> 913*(psi**2*360)/pi/24
1665.0868208814002
>>> 913*(psi**2*360)/pi/24/(c/254/1000)
1.4107494742375262
>>> 2**.5
1.4142135623730951
>>> psi**2*360
137.50776405003788
>>> 913*(psi**2*360)/pi/24/(c/254/1000)*+math.atan
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: bad operand type for unary +: 'builtin_function_or_method'
>>> 913*(psi**2*360)/pi/24/(c/254/1000)*+F(math.atan)
1.4107494742375262 0.9541600339074254
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)
0.7088430782797025
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.atan)
0.6166362928874712
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)
0.7829396438656995
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos*180/pi)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for *: 'builtin_function_or_method' and 'int'
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)*180/pi
44.859137206980314
>>> 1.4*(c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)*180/pi
7.077541834837765
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)*180/pi
44.859137206980314
>>> (c/254/1000)/(913*(psi**2*360*0+137)/pi/24)*F(math.acos)*180/pi
44.6453333497524
>>> (c/254/1000)/(913*(psi**2*360*0)/pi/24)*F(math.acos)*180/pi
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: float division by zero
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)*180/pi
44.859137206980314
>>> 45-_
0.14086279301968574
>>> 45-pi
41.8584073464102
>>> 45-pi+3
44.8584073464102
>>> exp(pi)
23.140692632779267
>>> 68-exp(pi)
44.85930736722074
>>> (c/254/1000)/(913*(psi**2*360)/pi/24)*F(math.acos)*180/pi
44.859137206980314
>>> _-44|I*60
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for *: 'Same' and 'int'
>>> _-44|F(I)*60
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for *: 'Func' and 'int'
>>> (_-44)*60
51.548232418818856
>>> (_-51)*60
32.89394512913134
>>> 44 51 33
  File "<stdin>", line 1
    44 51 33
        ^
SyntaxError: invalid syntax
>>> 

