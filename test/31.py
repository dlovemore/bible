>>> from bible import *
>>> IJohn[5:7:8].tells()
For there are three that bear record in heaven, the Father, the Word, and the Holy Ghost: and these three are one.   =
 3    5    3    5     4    4     6    2    6     3     6     3    4    3   3    4     5    3    5     5    3    3   88
 39   56   24   56   49   26    63   23    55    33    58    33   60   19  33  60    69    19   57    56   24  34   946
156  308   96  308   409  98    252  59   469   213   310   213  654   55 213  798   375   55  318   308   96  115 5878
And there are three that bear witness in earth, the Spirit, and the water, and the blood: and these three agree in one.   =
 3    5    3    5     4    4     7     2    5    3     6     3   3     5    3   3     5    3    5     5     5    2   3   92
 19   56   24   56   49   26    109   23   52    33    91    19  33   67    19  33   48    19   57    56    36  23  34   982
 55  308   96  308   409  98    964   59   304  213   478    55 213   796   55 213   156   55  318   308   108  59  115 5743
>>> tellkbd("the Father, the Word and the Holy Ghost",lsum)
<kbd>the<br>3</kbd>
<kbd>Father,<br>6</kbd>
<kbd>the<br>3</kbd>
<kbd>Word<br>4</kbd>
<kbd>and<br>3</kbd>
<kbd>the<br>3</kbd>
<kbd>Holy<br>4</kbd>
<kbd>Ghost<br>5</kbd>
<kbd>= 31</kbd>
>>> tellkbd("the Spirit, and the water, and the blood",lsum)
<kbd>the<br>3</kbd>
<kbd>Spirit,<br>6</kbd>
<kbd>and<br>3</kbd>
<kbd>the<br>3</kbd>
<kbd>water,<br>5</kbd>
<kbd>and<br>3</kbd>
<kbd>the<br>3</kbd>
<kbd>blood<br>5</kbd>
<kbd>= 31</kbd>
>>> kbdtable(alphatable(ab))*p
<kbd>a<br>1</kbd>
<kbd>b<br>2</kbd>
<kbd>c<br>3</kbd>
<kbd>d<br>4</kbd>
<kbd>e<br>5</kbd>
<kbd>f<br>6</kbd>
<kbd>g<br>7</kbd>
<kbd>h<br>8</kbd>
<kbd>i<br>9</kbd>
<kbd> j<br>10</kbd>
<kbd> k<br>11</kbd>
<kbd> l<br>12</kbd>
<kbd> m<br>13</kbd>
<kbd> n<br>14</kbd>
<kbd> o<br>15</kbd>
<kbd> p<br>16</kbd>
<kbd> q<br>17</kbd>
<kbd> r<br>18</kbd>
<kbd> s<br>19</kbd>
<kbd> t<br>20</kbd>
<kbd> u<br>21</kbd>
<kbd> v<br>22</kbd>
<kbd> w<br>23</kbd>
<kbd> x<br>24</kbd>
<kbd> y<br>25</kbd>
<kbd> z<br>26</kbd>
>>> "the Father Word Holy Ghost and Spirit water blood".split()@tellkbd
<kbd>t<br>20</kbd>
<kbd>h<br>8</kbd>
<kbd>e<br>5</kbd>
<kbd>= 33</kbd>
<kbd>F<br>6</kbd>
<kbd>a<br>1</kbd>
<kbd>t<br>20</kbd>
<kbd>h<br>8</kbd>
<kbd>e<br>5</kbd>
<kbd>r<br>18</kbd>
<kbd>= 58</kbd>
<kbd>W<br>23</kbd>
<kbd>o<br>15</kbd>
<kbd>r<br>18</kbd>
<kbd>d<br>4</kbd>
<kbd>= 60</kbd>
<kbd>H<br>8</kbd>
<kbd>o<br>15</kbd>
<kbd>l<br>12</kbd>
<kbd>y<br>25</kbd>
<kbd>= 60</kbd>
<kbd>G<br>7</kbd>
<kbd>h<br>8</kbd>
<kbd>o<br>15</kbd>
<kbd>s<br>19</kbd>
<kbd>t<br>20</kbd>
<kbd>= 69</kbd>
<kbd>a<br>1</kbd>
<kbd>n<br>14</kbd>
<kbd>d<br>4</kbd>
<kbd>= 19</kbd>
<kbd>S<br>19</kbd>
<kbd>p<br>16</kbd>
<kbd>i<br>9</kbd>
<kbd>r<br>18</kbd>
<kbd>i<br>9</kbd>
<kbd>t<br>20</kbd>
<kbd>= 91</kbd>
<kbd>w<br>23</kbd>
<kbd>a<br>1</kbd>
<kbd>t<br>20</kbd>
<kbd>e<br>5</kbd>
<kbd>r<br>18</kbd>
<kbd>= 67</kbd>
<kbd>b<br>2</kbd>
<kbd>l<br>12</kbd>
<kbd>o<br>15</kbd>
<kbd>o<br>15</kbd>
<kbd>d<br>4</kbd>
<kbd>= 48</kbd>
[None, None, None, None, None, None, None, None, None]
>>> tellkbd("the Father, the Word, & the Holy Ghost")
<kbd>the<br>33</kbd>
<kbd>Father,<br>58</kbd>
<kbd>the<br>33</kbd>
<kbd>Word,<br>60</kbd>
<kbd>&<br>0</kbd>
<kbd>the<br>33</kbd>
<kbd>Holy<br>60</kbd>
<kbd>Ghost<br>69</kbd>
<kbd>= 346</kbd>
>>> tellkbd("the Spirit, and the water, and the blood")
<kbd>the<br>33</kbd>
<kbd>Spirit,<br>91</kbd>
<kbd>and<br>19</kbd>
<kbd>the<br>33</kbd>
<kbd>water,<br>67</kbd>
<kbd>and<br>19</kbd>
<kbd>the<br>33</kbd>
<kbd>blood<br>48</kbd>
<kbd>= 343</kbd>
>>> 
