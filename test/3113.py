>>> from bible import *
>>> j11="ἐν ἀρχῇ ἦν ὁ λόγος καὶ ὁ λόγος ἦν πρὸς τὸν θεόν καὶ θεὸς ἦν ὁ λόγος"
>>> j11*tells
ἐν ἀρχῇ ἦν  ὁ λόγος καὶ  ὁ λόγος ἦν πρὸς τὸν θεόν καὶ θεὸς ἦν  ὁ λόγος   =
 2   5   2  1   5    3   1   5    2   4   3    4   3    4   2  1   5    52
19  62  22 16   67   22 16   67  22  72   51  44   22  50  22 16   67   657
55  719 58 70  373   31 70  373  58  450 420  134  31  284 58 70  373  3627
>>> 3627*ns
[3, 3, 13, 31] [2, 2, 6, 11]
>>> 3*31*13*3
3627
>>> 311342/pi
99103.23658423376
>>> divmod(.9792458*3600,60)
(58.0, 45.28488000000016)
>>> 
>>> divmod(.1342*3600,60)
(8.0, 3.1200000000000614)
>>> Luke[13:32].tells()
And he said unto them, Go ye, and tell that fox, Behold, I cast out devils, and I do cures  to day and  to morrow, and the third day I shall be perfected.   =
 3   2   4    4    4    2  2   3    4    4    3     6    1   4   3     6     3  1  2   5    2   3   3   2     6     3   3    5    3  1   5    2      9      113
 19 13  33   70    46  22  30  19  49   49   45     46   9  43   56    71    19 9 19   66   35  30  19  35   102    19  33   59   30 9   52   7     82     1245
 55 13  114  610  253  67 705  55  265  409  666   109   9  304 560   548    55 9 64  498  260 705  55 260   840    55 213  311  705 9  169   7     388    9345
>>> "first second third fourth fifth sixth"*tells
first second third fourth fifth sixth   =
  5      6     5      6     5     5    32
  72    60     59    88     49    80   408
 405    222   311    664   229   917  2748
>>> 
>>> 
>>> Luke[13:31].vn()
25550
>>> 
>>> 
>>> 
>>> b/'tale'
Exodus 5:8,18;25:39;37:24;38:24-25,27,29;Leviticus 19:16;1 Samuel 18:27;2 Samuel 12:30;1 Kings 9:14,28;10:10,14;16:24;20:39;2 Kings 5:5,22-23;15:19;18:14;23:33;1 Chronicles 9:28;19:6;20:2;22:14;29:4,7;2 Chronicles 3:8;8:18;9:9,13;25:6,9;27:5;36:3;Ezra 7:22;8:26;Esther 3:9;Psalms 90:9;Proverbs 11:13;18:8;20:19;26:20,22;Ezekiel 22:9;Zechariah 5:7;Matthew 18:24;25:15-16,20,22,24-25,28;Luke 24:11;Revelation 16:21 (58 verses)
>>> b/'tale'/'told'
Psalms 90:9 For all our days are passed away in thy wrath: we spend our years as a tale that is told.
>>> tells('thy wrath')
thy wrath   =
 3    5     8
 53   70   123
908  799  1707
>>> b/'tell'/'them'
Genesis 15:5;40:8;Exodus 10:2;Numbers 14:14;21:1;1 Kings 1:20;2 Kings 22:15;1 Chronicles 21:10;2 Chronicles 34:23;Job 1:15-17;Psalms 147:4;Isaiah 19:12;42:9;45:21;Jeremiah 15:2;23:32;28:13;33:13;Ezekiel 3:11;12:23;17:12;Matthew 8:4;17:9;18:17;21:24,27;22:4;28:9-10;Mark 5:19;7:36;8:30;9:9;10:32;11:29,33;Luke 5:14;7:22,42;8:56;9:21;10:24;13:32;18:8;19:40;20:8;22:67;John 8:14 (50 verses)
>>> b/'stars'
Genesis 1:16;15:5;22:17;26:4;37:9;Exodus 32:13;Deuteronomy 1:10;4:19;10:22;28:62;Judges 5:20;1 Chronicles 27:23;Nehemiah 4:21;9:23;Job 3:9;9:7;22:12;25:5;38:7;Psalms 8:3;136:9;147:4;148:3;Ecclesiastes 12:2;Isaiah 13:10;14:13;Jeremiah 31:35;Ezekiel 32:7;Daniel 8:10;12:3;Joel 2:10;3:15;Amos 5:8;Obadiah 1:4;Nahum 3:16;Matthew 24:29;Mark 13:25;Luke 21:25;Acts 27:20;1 Corinthians 15:41;Hebrews 11:12;Jude 1:13;Revelation 1:16,20-2:1;3:1;6:13;8:12;12:1,4 (50 verses)
>>> b/'the stars'
Genesis 1:16;15:5;22:17;26:4;Exodus 32:13;Deuteronomy 1:10;4:19;10:22;28:62;Judges 5:20;1 Chronicles 27:23;Nehemiah 4:21;9:23;Job 3:9;9:7;22:12;25:5;Psalms 8:3;147:4;Ecclesiastes 12:2;Isaiah 13:10;14:13;Jeremiah 31:35;Ezekiel 32:7;Daniel 8:10;12:3;Joel 2:10;3:15;Obadiah 1:4;Nahum 3:16;Matthew 24:29;Mark 13:25;Luke 21:25;1 Corinthians 15:41;Hebrews 11:12;Revelation 6:13;8:12;12:4 (38 verses)
>>> tell('Arcturus')
A  r c  t  u  r  u  s  =
1 18 3 20 21 18 21 19 121
>>> 
