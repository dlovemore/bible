>>> from bible import *
>>> tell('TEKEL UPHARSIN')
TEKEL UPHARSIN  =
  53    106    159
>>> 
>>> tell('sin',ssum)
 s  i n   =
100 9 50 159
>>> 
>>> 159*2
318
>>> b/'double'/'sin'
Isaiah 40:2;Jeremiah 16:18;James 4:8 (3 verses)
>>> p(_)
Isaiah 40:2 Speak ye comfortably to Jerusalem, and cry unto her, that her warfare is accomplished, that her iniquity is pardoned: for she hath received of the LORD's hand double for all her sins.
Jeremiah 16:18 And first I will recompense their iniquity and their sin double; because they have defiled my land, they have filled mine inheritance with the carcases of their detestable and abominable things.
James 4:8 Draw nigh to God, and he will draw nigh to you. Cleanse your hands, ye sinners; and purify your hearts, ye double minded.
>>> Deuteronomy[1:1].tells()
These be the words which Moses spake unto all Israel  on this side Jordan in the wilderness, in the plain over against the Red sea, between Paran, and Tophel, and Laban, and Hazeroth, and Dizahab.   =
  5   2   3    5     5     5     5    4    3    6     2   4    4     6    2   3       10     2   3    5    4      7     3   3   3      7      5     3     6     3    5     3      8      3     7      154
  57  7   33   79    51    71    52   70   25   64    29  56   37    62   23  33     128     23  33   52   60     71    33  27  25     74     50    19    76    19   30    19    101     19    51     1659
 318  7  213  754   528   305   196  610   61  235   110 317  118   215   59 213     893     59 213  160  555    368   213  99 106    767    212    55   373    55   84    55    1172    55   825    10578
>>> tells(q.comfort)
c o  m  f o  r   t   =
1 1  1  1 1  1   1   7
3 15 13 6 15 18  20  90
3 60 40 6 60 90 200 459
>>> Jeremiah[16:16].tell(ssum)
Behold, I will send for many fishers, saith the LORD, and they shall fish them; and after will I send for many hunters, and they shall hunt them from every mountain, and from every hill, and out of the holes of the rocks.   =
  109   9 569  159  156 791    318     318  213  184   55 913   169  123   253   55  302  569  9 159  156 791    753     55 913   169  558  253  196   1200    710     55 196   1200   77   55 560 66 213  203  66 213  273   14364
>>> # Note the two consecutive 318
>>> tell("fishers",ssum)
f i  s  h e r   s   =
6 9 100 8 5 90 100 318
>>> tell("saith",ssum)
 s  a i  t  h  =
100 1 9 200 8 318
>>> (b.words()@ssum*pairs*L)/F(op.eq([318,318],...))
[[318, 318]]
>>> # This means this is unique to the verse.
>>> 
>>> 
