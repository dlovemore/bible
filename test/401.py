>>> from bible import *
>>> tell('את')
א  ת  =
1 22 23
>>> tell(ssum,'את')
א  ת   =
1 400 401
>>> tell(ssum,'Lord Jesus Christ')
Lord Jesus Christ   =
 184  515    410  1109
>>> tells("ὁ κύριος ἡμῶν ἰησοῦς χριστὸς")
 ὁ κύριος ἡμῶν ἰησοῦς χριστὸς   =
 1    6     4     6      7     24
16   98    61    96     130    401
70   800   898   888    1480  4136
>>> 
