>>> from bible import *
>>> b/666
1 Kings 10:14;2 Chronicles 9:13;Ezra 2:13;Revelation 13:18 (4 verses)
>>> p(_)
1 Kings 10:14 Now the weight of gold that came to Solomon in one year was six hundred threescore and six talents of gold,
2 Chronicles 9:13 Now the weight of gold that came to Solomon in one year was six hundred and threescore and six talents of gold;
Ezra 2:13 The children of Adonikam, six hundred sixty and six.
Revelation 13:18 Here is wisdom. Let him that hath understanding count the number of the beast: for it is the number of a man; and his number is Six hundred threescore and six.
>>> b/'Adonikam'
Ezra 2:13;8:13;Nehemiah 7:18 (3 verses)
>>> p(_)
Ezra 2:13 The children of Adonikam, six hundred sixty and six.
Ezra 8:13 And of the last sons of Adonikam, whose names are these, Eliphelet, Jeiel, and Shemaiah, and with them threescore males.
Nehemiah 7:18 The children of Adonikam, six hundred threescore and seven.
>>> 
>>> b/777
Genesis 5:31 And all the days of Lamech were seven hundred seventy and seven years: and he died.
>>> _.tell(lsum,osum,ssum)
And all the days of Lamech were seven hundred seventy and seven years: and he died.   =
 3   3   3    4   2    6     4    5      7       7     3    5      5    3   2   4    66
 19  25  33  49  21   42    51    65     74     110    19   65    68    19 13   22   695
 55  61 213  805 66   87    600  560    461     1460   55  560    896   55 13   22  5969
>>> tell('seventy times seven')
seventy times seven  =
  110     66    65  241
>>> tell('fold')
f  o  l d  =
6 15 12 4 37
>>> 
>>> from bible import *
>>> tell=tellmd
>>> Row(['כיסר','ורנסס'])@partial(tell,ssum)
|כ|י|ס|ר|=
|-|-|-|-|-
|20|10|60|200|290
|ו|ר|נ|ס|ס|=
|-|-|-|-|-|-
|6|200|50|60|60|376
None None
>>> 
>>> 
>>> 
>>> tell('Καισαροσ φρανσι',ssum)
|Καισαροσ|φρανσι|=
|-|-|-
|602|861|1463
>>> 
>>> tell('Nero Caesar',ssum)
|Nero|Caesar|=
|-|-|-
|205|200|405
>>> tell('Jorge Mario Bergoglio',ssum)
|Jorge|Mario|Bergoglio|=
|-|-|-|-
|172|200|270|642
>>> tell('Franciscus',ssum)
|F|r|a|n|c|i|s|c|u|s|=
|-|-|-|-|-|-|-|-|-|-|-
|6|90|1|50|3|9|100|3|300|100|662
>>> tell('BERGOGLIO',ord)
|B|E|R|G|O|G|L|I|O|=
|-|-|-|-|-|-|-|-|-|-
|66|69|82|71|79|71|76|73|79|666
>>> tell('λατεινος',ssum)
|λ|α|τ|ε|ι|ν|ο|ς|=
|-|-|-|-|-|-|-|-|-
|30|1|300|5|10|50|70|200|666
>>> def asc(x): return sum(map(ord,x))
... 
>>> tell('zoom.us')
|z|o|o|m.|u|s|=
|-|-|-|-|-|-|-
|26|15|15|13|21|19|109
>>> 512+109
621
>>> ord('.')*3
138
>>> 138+512
650
>>> tell('BILL GATES',asc)
|BILL|GATES|=
|-|-|-
|291|372|663
>>> tellkbd('BERGOGLIO',asc)
<kbd>B<br>66</kbd>
<kbd>E<br>69</kbd>
<kbd>R<br>82</kbd>
<kbd>G<br>71</kbd>
<kbd>O<br>79</kbd>
<kbd>G<br>71</kbd>
<kbd>L<br>76</kbd>
<kbd>I<br>73</kbd>
<kbd>O<br>79</kbd>
<kbd>= 666</kbd>
>>> tellkbd('λατεινος',ssum)
<kbd>λ<br>30</kbd>
<kbd>α<br>1</kbd>
<kbd>τ<br>300</kbd>
<kbd>ε<br>5</kbd>
<kbd>ι<br>10</kbd>
<kbd>ν<br>50</kbd>
<kbd>ο<br>70</kbd>
<kbd>ς<br>200</kbd>
<kbd>= 666</kbd>
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> from dna import *
>>> AY
3088286401
>>> AY**.5
55572.35284743665
>>> AY**(1/3)
1456.260902161315
>>> 6**3
216
>>> sums('fox')
(3, 45, 666)
>>> b/'fox'
Judges 15:4;Nehemiah 4:3;Psalms 63:10;Song of Solomon 2:15;Lamentations 5:18;Ezekiel 13:4;Matthew 8:20;Luke 9:58;13:32 (9 verses)
>>> p(_)
Judges 15:4 And Samson went and caught three hundred foxes, and took firebrands, and turned tail to tail, and put a firebrand in the midst between two tails.
Nehemiah 4:3 Now Tobiah the Ammonite was by him, and he said, Even that which they build, if a fox go up, he shall even break down their stone wall.
Psalms 63:10 They shall fall by the sword: they shall be a portion for foxes.
Song of Solomon 2:15 Take us the foxes, the little foxes, that spoil the vines: for our vines have tender grapes.
Lamentations 5:18 Because of the mountain of Zion, which is desolate, the foxes walk upon it.
Ezekiel 13:4 O Israel, thy prophets are like the foxes in the deserts.
Matthew 8:20 And Jesus saith unto him, The foxes have holes, and the birds of the air have nests; but the Son of man hath not where to lay his head.
Luke 9:58 And Jesus said unto him, Foxes have holes, and birds of the air have nests; but the Son of man hath not where to lay his head.
Luke 13:32 And he said unto them, Go ye, and tell that fox, Behold, I cast out devils, and I do cures to day and to morrow, and the third day I shall be perfected.
>>> Luke[13:31:32]
Luke 13:31 The same day there came certain of the Pharisees, saying unto him, Get thee out, and depart hence: for Herod will kill thee.
Luke 13:32 And he said unto them, Go ye, and tell that fox, Behold, I cast out devils, and I do cures to day and to morrow, and the third day I shall be perfected.
>>> b/'Herod'
Matthew 2:1,3,7,12-13,15-16,19,22;14:1,3,6;22:16;Mark 3:6;6:14,16-22;8:15;12:13;Luke 1:5;3:1,19;8:3;9:7,9;13:31;23:7-8,11-12,15;Acts 4:27;12:1,6,11,19-21;13:1;23:35;Romans 16:11 (46 verses)
>>> Acts[12:19]-(12,23)
Acts 12:19-23 (5 verses)
>>> p(_)
Acts 12
19 And when Herod had sought for him, and found him not, he examined the keepers, and commanded that they should be put to death. And he went down from Judaea to Caesarea, and there abode.
20 And Herod was highly displeased with them of Tyre and Sidon: but they came with one accord to him, and, having made Blastus the king's chamberlain their friend, desired peace; because their country was nourished by the king's country.
21 And upon a set day Herod, arrayed in royal apparel, sat upon his throne, and made an oration unto them.
22 And the people gave a shout, saying, It is the voice of a god, and not of a man.
23 And immediately the angel of the Lord smote him, because he gave not God the glory: and he was eaten of worms, and gave up the ghost.
>>> b/'sitteth'/'temple'
2 Thessalonians 2:4 Who opposeth and exalteth himself above all that is called God, or that is worshipped; so that he as God sitteth in the temple of God, shewing himself that he is God.
Revelation 7:15 Therefore are they before the throne of God, and serve him day and night in his temple: and he that sitteth on the throne shall dwell among them.
>>> Genesis[13:16].tells()
And I will make thy seed  as the dust of the earth:  so that if a man can number the dust of the earth, then shall thy seed also be numbered.   =
 3  1   4    4   3    4   2   3    4   2  3     5    2    4   2 1  3   3     6    3    4   2  3     5     4    5    3    4    4   2     8      106
 19 9  56   30   53  33   20  33  64  21  33   52    34  49  15 1  28  18   73    33  64  21  33   52    47    52   53  33   47   7     82    1165
 55 9  569  66  908  114 101 213  604 66 213   304  160  409 15 1  91  54   487  213  604 66 213   304   263  169  908  114  191  7    496    7987
>>> 670+517
1187
>>> tells("dust of the earth")
dust of the earth   =
  4   2  3    5    14
 64  21  33   52   170
 604 66 213  304  1187
>>> 
>>> "ושמתי את זרעך כעפר הארץ אשר אם יוכל איש למנות את עפר הארץ גם זרעך ימנה"*tells
ושמתי  את זרעך כעפר הארץ אשר אם יוכל איש למנות  את עפר הארץ גם זרעך ימנה   =
  5    2    4    4    4   3   2   4   3    5    2   3    4   2   4    4   55
  72   23  54   64   44   42 14  39   32   67   23  53  44  16  54   42   683
 756  401  297  370  296 501 41  66  311  526  401 350  296 43  297  105 5057
>>> 
>>> "ושמתי את זרעך כעפר הארץ אשר אם יוכל איש למנות את עפר הארץ גם זרעך ימנה"[::-1]
'הנמי ךערז םג ץראה רפע תא תונמל שיא לכוי םא רשא ץראה רפעכ ךערז תא יתמשו'
>>> "כעפר הארץ"*tells
כעפר הארץ  =
  4    4   8
 64   44  108
 370  296 666
>>> 
>>> 
>>> 
>>> 
