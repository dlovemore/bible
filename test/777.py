>>> from bible import *
>>> Genesis[1:1]-Genesis[2:3]
Genesis 1:1-2:3 (34 verses)
>>> _.vc()
34
>>> tells("one")
 o  n e  =
 1  1 1  3
15 14 5  34
60 50 5 115
>>> 34*factors
[2, 17]
>>> 34*tri
595
>>> b.ch(tri(34))
Psalms 117:1 O praise the LORD, all ye nations: praise him, all ye people.
Psalms 117:2 For his merciful kindness is great toward us: and the truth of the LORD endureth for ever. Praise ye the LORD.
>>> _.wc()
33
>>> 
>>> 117*ns
[3, 3, 13] [2, 2, 6]
>>> 119*ns
[7, 17] [4, 7]
>>> Leviticus/14
Leviticus 23:5 In the fourteenth day of the first month at even is the LORD's passover.
>>> Leviticus/15
Leviticus 23:6,34,39;27:7 (4 verses)
>>> p(_)
Leviticus 23
6 And on the fifteenth day of the same month is the feast of unleavened bread unto the LORD: seven days ye must eat unleavened bread.
34 Speak unto the children of Israel, saying, The fifteenth day of this seventh month shall be the feast of tabernacles for seven days unto the LORD.
39 Also in the fifteenth day of the seventh month, when ye have gathered in the fruit of the land, ye shall keep a feast unto the LORD seven days: on the first day shall be a sabbath, and on the eighth day shall be a sabbath.
Leviticus 27:7 And if it be from sixty years old and above; if it be a male, then thy estimation shall be fifteen shekels, and for the female ten shekels.
>>> b.chapters()/(I*method.wc*)
invalid syntax (<15>, line 1)
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
