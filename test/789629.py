    >>> from bible import *
Body Word count (excluding superscriptions & colophons)
    >>> bwc=b.wc()
    >>> bwc
    789629

Use an approximation of π

    >>> π=decimal.Decimal("3.14159265358979323844384626443")
    >>> π
    Decimal('3.14159265358979323844384626443')

We're using decimal as 64 bit floats don't give us enough digits

    >>> 789629/pi
    251346.71711742046

    >>> π*p # apply p
    3.14159265358979323844384626443
    >>> p # p is just print
    Func(<built-in function print>)
    >>> π*p # print π
    3.14159265358979323844384626443

Divide the word count by pi. Why? God told me to do it. Will find the verse later.

    >>> bwc
    789629
    >>> _ # _ is just previous result
    789629
    >>> q=_/π
    >>> q*p
    251346.7171174204441771997194
    >>> bwc/pi
    251346.71711742046
Maybe 64 bit floats were enough.

    >>> 

Looking at the integer part this is just an anagram of 123456

    >>> int(q)
    251346

We can prove this by sorting the digits.

    >>> join=F(''.join)
    >>> ['a','z']*join
    'az'
    >>> inorder=str*sort*join*int
    >>> inorder(31415)
    11345
    >>> b/'in order'
    Genesis 22:9;Exodus 26:17;39:37;40:4,23;Leviticus 1:7-8,12;6:12;24:8;Joshua 2:6;2 Samuel 17:23;1 Kings 18:33;2 Kings 20:1;2 Chronicles 13:11;29:35;Job 33:5;Psalms 40:5;50:21;Ecclesiastes 12:9;Isaiah 38:1;44:7;Ezekiel 41:6;Luke 1:1,3;Acts 18:23;1 Corinthians 11:34;14:40;Titus 1:5 (29 verses)
    >>> Genesis[22:9]
    Genesis 22:9 And they came to the place which God had told him of; and Abraham built an altar there, and laid the wood in order, and bound Isaac his son, and laid him on the altar upon the wood.
    >>> Leviticus[1:7]
    Leviticus 1:7 And the sons of Aaron the priest shall put fire upon the altar, and lay the wood in order upon the fire:
    >>> Luke[1:1:3]*p
    Luke 1
    1 Forasmuch as many have taken in hand to set forth in order a declaration of those things which are most surely believed among us,
    2 Even as they delivered them unto us, which from the beginning were eyewitnesses, and ministers of the word;
    3 It seemed good to me also, having had perfect understanding of all things from the very first, to write unto thee in order, most excellent Theophilus,

    >>> q
    Decimal('251346.7171174204441771997194')
    >>> int(q)
    251346
    >>> _*inorder
    123456
    >>> bwc
    789629

Seen this pattern before?

The total old testament verse count 23145 is an anagram of digits up to 5:

    >>> ot.vc()*+inorder
    23145 12345
    >>> 


789 gives us the remaing decimal digits after 123456

[789 also relates to the shCherback symmetries, which is a DNA thing.

https://arxiv.org/pdf/1303.6739&ved=2ahUKEwiS54e135bfAhV6URUIHTNyDnUQFjAAegQIBxAB&usg=AOvVaw23Ytzb1btcZvc80sK_f5Ku
See figure 8(b)

shCherbak, Vladimir I.  and Makukov, Maxim A.
The “Wow! signal” of the terrestrial genetic code.
Icarus 224.1 (2013): 228-242.]

But more simply we have six digits corresponding to six days.
And then the seventh following the decimal point.

    >>> q*p
    251346.7171174204441771997194

So we have a reordering of the six plus a sabbath following a . which is itself a rest.

    >>> b/'six days'/'rest'*p
    Exodus 20:11 For in six days the LORD made heaven and earth, the sea, and all that in them is, and rested the seventh day: wherefore the LORD blessed the sabbath day, and hallowed it.
    Exodus 23:12 Six days thou shalt do thy work, and on the seventh day thou shalt rest: that thine ox and thine ass may rest, and the son of thy handmaid, and the stranger, may be refreshed.
    Exodus 31:15 Six days may work be done; but in the seventh is the sabbath of rest, holy to the LORD: whosoever doeth any work in the sabbath day, he shall surely be put to death.
    Exodus 31:17 It is a sign between me and the children of Israel for ever: for in six days the LORD made heaven and earth, and on the seventh day he rested, and was refreshed.
    Exodus 34:21 Six days thou shalt work, but on the seventh day thou shalt rest: in earing time and in harvest thou shalt rest.
    Exodus 35:2 Six days shall work be done, but on the seventh day there shall be to you an holy day, a sabbath of rest to the LORD: whosoever doeth work therein shall be put to death.
    Leviticus 23:3 Six days shall work be done: but the seventh day is the sabbath of rest, an holy convocation; ye shall do no work therein: it is the sabbath of the LORD in all your dwellings.
    >>> Genesis[2:2]
    Genesis 2:2 And on the seventh day God ended his work which he had made; and he rested on the seventh day from all his work which he had made.

Wow, relates 22 and 77. See also 'Authorised' and "Authorised Version"
    >>> Genesis[2:2].count('seventh')
    2

Ahh, so the 122 pattern relates to book chapter and verse 1 2 2

    >>> tells("Authorised Version")
    Authorised Version   =
        10        7     17
        120      102    222
        777      714   1491
    >>> 

So 1 and 7 are the first and the last of a seven

    >>> b/'first shall be last'
    Matthew 19:30 But many that are first shall be last; and the last shall be first.
    Mark 10:31 But many that are first shall be last; and the last first.
If we take 1 and 7 to be the first and last,
    >>> b/'first'/'last'
    1 Chronicles 29:29;2 Chronicles 9:29;12:15;16:11;20:34;25:26;26:22;28:26;35:27;Nehemiah 8:18;Isaiah 41:4;44:6;48:12;Jeremiah 50:17;Matthew 12:45;19:30;20:8,16;27:64;Mark 9:35;10:31;Luke 11:26;13:30;1 Corinthians 15:45;2 Peter 3:3;Revelation 1:11,17;2:8,19;22:13 (30 verses)
    >>> Isaiah[41:4]
    Isaiah 41:4 Who hath wrought and done it, calling the generations from the beginning? I the LORD, the first, and with the last; I am he.

717117 can be anything from 111111 to 777777

In handwriting it's a visual pun, and there are many people who write 1's like 7's and 7's like 1's

[In Hebrew a ו looks like a 1, ו is a VAU, which is 6

The corresponding letter in greek is digamma ϝ, not nowadays used in writing, but numerically

digamma, ϝ = 6 which is twice (di-) gamma, γ, the Greek g, which = 3

in English g is 7, γ (gamma) is 3 in Greek

GIMEL, ג is the third Hebrew letter 3 by number and sound like g

The word camel is גמל, GIMEL MEM LAMED in Hebrew

In Greek camel is καμήλος, kappa alpha mu eta lambda omicron (final) sigma

Greek kappa, κ, corresponds numberically to 11 k 20

GIMEL ג of GML becomes greek κ here, which then becomes a hard c in camel

visually though a Hebrew ג, GIMEL looks like a greek LAMBDA λ=12

Like many letters some are soft and some are hard and these vary.]

    >>> q*p
    251346.7171174204441771997194
    >>> 

717 and 117 follow the point

117 is the shortest Psalm
119 is the longest

    >>> 119*factors
    [7, 17]

    >>> 7*17,117
    (119, 117)
    >>> 

7/17 and 1/17 are important dates related to feasts.

This is perhaps a longer discussion or already understood.

Here's a primer

    >>> b/'keep'/'feast'
    Exodus 12:14;23:14-15;34:18;Leviticus 23:39,41;Numbers 29:12;Deuteronomy 16:10,15;2 Chronicles 30:13;Nahum 1:15;Zechariah 14:16,18-19;Acts 18:21;1 Corinthians 5:8 (16 verses)
    >>> 

But what about the 789629?

Another way of reading this is 7 is the sabbath. 89 now refers to 1189

Compare 789, 1189

    >>> b.ch(629)
    Proverbs 1:1-33 (33 verses)

629 Marks the first chapter after the end of Psalms

    >>> 629-1
    628
    >>> b.ch(628)
    Psalms 150:1-6 (6 verses)
    >>> 314*2==628 # by the way
    True

Why not 628 or 479? rather than 628
    >>> b.chapter(479,628)
    Psalms 1:1-150:6 (2461 verses)

    >>> q*p
    251346.7171174204441771997194

The following 42 confirms the last chapter of Job, the chapter preceeding Psalm 1

    >>> Psalm[1].chn()-1
    478
    >>> b.chapter(_).ref()
    'Job 42:1-17'

That's more beautiful than I remembered.


    >>> _/method.isdigit
    '42117'

These are the digits 117 and 42 from our quotient
    >>> q
    Decimal('251346.7171174204441771997194')
