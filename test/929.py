>>> from bible import *
>>> b/'the sum'
Exodus 30:12;38:21;Numbers 1:2,49;4:2,22;26:2,4;31:26,49;2 Samuel 24:9;1 Chronicles 21:5;Esther 4:7;Psalms 139:17;Proverbs 6:8;30:25;Isaiah 28:4;Jeremiah 8:20;Ezekiel 28:12;Daniel 2:35;7:1;Amos 3:15;Micah 7:1;Hebrews 8:1 (24 verses)
>>> p(_)
Exodus 30:12 When thou takest the sum of the children of Israel after their number, then shall they give every man a ransom for his soul unto the LORD, when thou numberest them; that there be no plague among them, when thou numberest them.
Exodus 38:21 This is the sum of the tabernacle, even of the tabernacle of testimony, as it was counted, according to the commandment of Moses, for the service of the Levites, by the hand of Ithamar, son to Aaron the priest.
Numbers 1:2 Take ye the sum of all the congregation of the children of Israel, after their families, by the house of their fathers, with the number of their names, every male by their polls;
Numbers 1:49 Only thou shalt not number the tribe of Levi, neither take the sum of them among the children of Israel:
Numbers 4:2 Take the sum of the sons of Kohath from among the sons of Levi, after their families, by the house of their fathers,
Numbers 4:22 Take also the sum of the sons of Gershon, throughout the houses of their fathers, by their families;
Numbers 26:2 Take the sum of all the congregation of the children of Israel, from twenty years old and upward, throughout their fathers' house, all that are able to go to war in Israel.
Numbers 26:4 Take the sum of the people, from twenty years old and upward; as the LORD commanded Moses and the children of Israel, which went forth out of the land of Egypt.
Numbers 31:26 Take the sum of the prey that was taken, both of man and of beast, thou, and Eleazar the priest, and the chief fathers of the congregation:
Numbers 31:49 And they said unto Moses, Thy servants have taken the sum of the men of war which are under our charge, and there lacketh not one man of us.
2 Samuel 24:9 And Joab gave up the sum of the number of the people unto the king: and there were in Israel eight hundred thousand valiant men that drew the sword; and the men of Judah were five hundred thousand men.
1 Chronicles 21:5 And Joab gave the sum of the number of the people unto David. And all they of Israel were a thousand thousand and an hundred thousand men that drew sword: and Judah was four hundred threescore and ten thousand men that drew sword.
Esther 4:7 And Mordecai told him of all that had happened unto him, and of the sum of the money that Haman had promised to pay to the king's treasuries for the Jews, to destroy them.
Psalms 139:17 How precious also are thy thoughts unto me, O God! how great is the sum of them!
Proverbs 6:8 Provideth her meat in the summer, and gathereth her food in the harvest.
Proverbs 30:25 The ants are a people not strong, yet they prepare their meat in the summer;
Isaiah 28:4 And the glorious beauty, which is on the head of the fat valley, shall be a fading flower, and as the hasty fruit before the summer; which when he that looketh upon it seeth, while it is yet in his hand he eateth it up.
Jeremiah 8:20 The harvest is past, the summer is ended, and we are not saved.
Ezekiel 28:12 Son of man, take up a lamentation upon the king of Tyrus, and say unto him, Thus saith the Lord GOD; Thou sealest up the sum, full of wisdom, and perfect in beauty.
Daniel 2:35 Then was the iron, the clay, the brass, the silver, and the gold, broken to pieces together, and became like the chaff of the summer threshingfloors; and the wind carried them away, that no place was found for them: and the stone that smote the image became a great mountain, and filled the whole earth.
Daniel 7:1 In the first year of Belshazzar king of Babylon Daniel had a dream and visions of his head upon his bed: then he wrote the dream, and told the sum of the matters.
Amos 3:15 And I will smite the winter house with the summer house; and the houses of ivory shall perish, and the great houses shall have an end, saith the LORD.
Micah 7:1 Woe is me! for I am as when they have gathered the summer fruits, as the grapegleanings of the vintage: there is no cluster to eat: my soul desired the firstripe fruit.
Hebrews 8:1 Now of the things which we have spoken this is the sum: We have such an high priest, who is set on the right hand of the throne of the Majesty in the heavens;
>>> Hebrews.bookn()
58
>>> 
