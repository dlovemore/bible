>>> from bible import *
>>> b.chapter(1)@method.words@X[0]
'In' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'And' 'So' 'And' 'And' 'And' 'And'
>>> _@eq*"And"
False True True True True True True True True True True True True True True True True True True True True True True True True True False True True True True
>>> def avg(l):
...     l=list(l)
...     return sum(l)/len(l)
... 
>>> avg=F(avg)
>>> 
>>> andp=F(lambda x: x@method.words@X[0]@eq*'And'*avg)
>>> Genesis[1]*andp
0.9354838709677419
>>> Genesis.chapters()@+andp*sort(...,key=X[1])
[Genesis 49:1-33 (33 verses) 0.12121212121212122, Genesis 6:1-22 (22 verses) 0.5, Genesis 13:1-18 (18 verses) 0.5555555555555556, Genesis 20:1-18 (18 verses) 0.5555555555555556, Genesis 31:1-55 (55 verses) 0.5818181818181818, Genesis 7:1-24 (24 verses) 0.5833333333333334, Genesis 45:1-28 (28 verses) 0.6071428571428571, Genesis 44:1-34 (34 verses) 0.6176470588235294, Genesis 3:1-24 (24 verses) 0.625, Genesis 36:1-43 (43 verses) 0.627906976744186, Genesis 18:1-33 (33 verses) 0.6666666666666666, Genesis 8:1-22 (22 verses) 0.6818181818181818, Genesis 19:1-38 (38 verses) 0.6842105263157895, Genesis 10:1-32 (32 verses) 0.6875, Genesis 40:1-23 (23 verses) 0.6956521739130435, Genesis 12:1-20 (20 verses) 0.7, Genesis 23:1-20 (20 verses) 0.7, Genesis 34:1-31 (31 verses) 0.7096774193548387, Genesis 28:1-22 (22 verses) 0.7272727272727273, Genesis 46:1-34 (34 verses) 0.7352941176470589, Genesis 41:1-57 (57 verses) 0.7368421052631579, Genesis 42:1-38 (38 verses) 0.7368421052631579, Genesis 17:1-27 (27 verses) 0.7407407407407407, Genesis 47:1-31 (31 verses) 0.7419354838709677, Genesis 14:1-24 (24 verses) 0.75, Genesis 32:1-32 (32 verses) 0.75, Genesis 2:1-25 (25 verses) 0.76, Genesis 15:1-21 (21 verses) 0.7619047619047619, Genesis 25:1-34 (34 verses) 0.7647058823529411, Genesis 50:1-26 (26 verses) 0.7692307692307693, Genesis 26:1-35 (35 verses) 0.7714285714285715, Genesis 39:1-23 (23 verses) 0.782608695652174, Genesis 9:1-29 (29 verses) 0.7931034482758621, Genesis 33:1-20 (20 verses) 0.8, Genesis 27:1-46 (46 verses) 0.8043478260869565, Genesis 4:1-26 (26 verses) 0.8076923076923077, Genesis 11:1-32 (32 verses) 0.8125, Genesis 16:1-16 (16 verses) 0.8125, Genesis 21:1-34 (34 verses) 0.8235294117647058, Genesis 35:1-29 (29 verses) 0.8275862068965517, Genesis 22:1-24 (24 verses) 0.8333333333333334, Genesis 37:1-36 (36 verses) 0.8333333333333334, Genesis 24:1-67 (67 verses) 0.8507462686567164, Genesis 43:1-34 (34 verses) 0.8529411764705882, Genesis 30:1-43 (43 verses) 0.8604651162790697, Genesis 48:1-22 (22 verses) 0.8636363636363636, Genesis 38:1-30 (30 verses) 0.8666666666666667, Genesis 29:1-35 (35 verses) 0.9142857142857143, Genesis 1:1-31 (31 verses) 0.9354838709677419, Genesis 5:1-32 (32 verses) 0.9375]
>>> b.books()@+andp*sort(...,key=X[1])
[3 John 1:1-14 (14 verses) 0.0, Song of Solomon 1:1-8:14 (117 verses) 0.008547008547008548, Proverbs 1:1-31:31 (915 verses) 0.009836065573770493, Titus 1:1-3:15 (46 verses) 0.021739130434782608, Lamentations 1:1-5:22 (154 verses) 0.025974025974025976, Psalms 1:1-150:6 (2461 verses) 0.03413246647704185, Philemon 1:1-25 (25 verses) 0.04, Job 1:1-42:17 (1070 verses) 0.05046728971962617, 1 Peter 1:1-5:14 (105 verses) 0.05714285714285714, Ecclesiastes 1:1-12:14 (222 verses) 0.06306306306306306, James 1:1-5:20 (108 verses) 0.06481481481481481, Habakkuk 1:1-3:19 (56 verses) 0.07142857142857142, 1 Timothy 1:1-6:21 (113 verses) 0.07964601769911504, Nahum 1:1-3:19 (47 verses) 0.0851063829787234, Philippians 1:1-4:23 (104 verses) 0.08653846153846154, 1 Thessalonians 1:1-5:28 (89 verses) 0.10112359550561797, Romans 1:1-16:27 (433 verses) 0.10623556581986143, 2 Timothy 1:1-4:22 (83 verses) 0.10843373493975904, 2 Corinthians 1:1-13:14 (257 verses) 0.11673151750972763, Galatians 1:1-6:18 (149 verses) 0.12080536912751678, 1 Corinthians 1:1-16:24 (437 verses) 0.12814645308924486, Ephesians 1:1-6:24 (155 verses) 0.12903225806451613, Hebrews 1:1-13:25 (303 verses) 0.1419141914191419, 2 John 1:1-13 (13 verses) 0.15384615384615385, Hosea 1:1-14:9 (197 verses) 0.15736040609137056, Haggai 1:1-2:23 (38 verses) 0.15789473684210525, Jude 1:1-25 (25 verses) 0.16, Colossians 1:1-4:18 (95 verses) 0.16842105263157894, Micah 1:1-7:20 (105 verses) 0.17142857142857143, Joel 1:1-3:21 (73 verses) 0.1780821917808219, Zephaniah 1:1-3:20 (53 verses) 0.18867924528301888, Jeremiah 1:1-52:34 (1364 verses) 0.18914956011730205, 2 Thessalonians 1:1-3:18 (47 verses) 0.19148936170212766, Isaiah 1:1-66:24 (1292 verses) 0.19427244582043343, 2 Peter 1:1-3:18 (61 verses) 0.19672131147540983, John 1:1-21:25 (879 verses) 0.19681456200227532, Amos 1:1-9:15 (146 verses) 0.19863013698630136, 1 John 1:1-5:21 (105 verses) 0.20952380952380953, Obadiah 1:1-21 (21 verses) 0.23809523809523808, Ezra 1:1-10:44 (280 verses) 0.2857142857142857, Malachi 1:1-4:6 (55 verses) 0.2909090909090909, Jonah 1:1-4:11 (48 verses) 0.2916666666666667, Daniel 1:1-12:13 (357 verses) 0.29971988795518206, Deuteronomy 1:1-34:12 (959 verses) 0.32742440041710114, Nehemiah 1:1-13:31 (406 verses) 0.33004926108374383, Ezekiel 1:1-48:35 (1273 verses) 0.33699921445404557, Matthew 1:1-28:20 (1071 verses) 0.3520074696545285, Esther 1:1-10:3 (167 verses) 0.38922155688622756, Zechariah 1:1-14:21 (211 verses) 0.4075829383886256, Acts 1:1-28:31 (1007 verses) 0.48361469712015887, 1 Chronicles 1:1-29:30 (942 verses) 0.4968152866242038, 2 Chronicles 1:1-36:23 (822 verses) 0.5012165450121655, Luke 1:1-24:53 (1151 verses) 0.5438748913987836, 2 Samuel 1:1-24:25 (695 verses) 0.5640287769784172, Numbers 1:1-36:13 (1288 verses) 0.5652173913043478, Leviticus 1:1-27:34 (859 verses) 0.5739231664726426, 2 Kings 1:1-25:30 (719 verses) 0.5841446453407511, Joshua 1:1-24:33 (658 verses) 0.6124620060790273, 1 Kings 1:1-22:53 (816 verses) 0.6151960784313726, Ruth 1:1-4:22 (85 verses) 0.6235294117647059, 1 Samuel 1:1-31:13 (810 verses) 0.6283950617283951, Judges 1:1-21:25 (618 verses) 0.6666666666666666, Exodus 1:1-40:38 (1213 verses) 0.6760098928277, Revelation 1:1-22:21 (404 verses) 0.6782178217821783, Mark 1:1-16:20 (678 verses) 0.6799410029498525, Genesis 1:1-50:26 (1533 verses) 0.7377690802348337]
>>> _@F(repr)@p
3 John 1:1-14 (14 verses) 0.0
Song of Solomon 1:1-8:14 (117 verses) 0.008547008547008548
Proverbs 1:1-31:31 (915 verses) 0.009836065573770493
Titus 1:1-3:15 (46 verses) 0.021739130434782608
Lamentations 1:1-5:22 (154 verses) 0.025974025974025976
Psalms 1:1-150:6 (2461 verses) 0.03413246647704185
Philemon 1:1-25 (25 verses) 0.04
Job 1:1-42:17 (1070 verses) 0.05046728971962617
1 Peter 1:1-5:14 (105 verses) 0.05714285714285714
Ecclesiastes 1:1-12:14 (222 verses) 0.06306306306306306
James 1:1-5:20 (108 verses) 0.06481481481481481
Habakkuk 1:1-3:19 (56 verses) 0.07142857142857142
1 Timothy 1:1-6:21 (113 verses) 0.07964601769911504
Nahum 1:1-3:19 (47 verses) 0.0851063829787234
Philippians 1:1-4:23 (104 verses) 0.08653846153846154
1 Thessalonians 1:1-5:28 (89 verses) 0.10112359550561797
Romans 1:1-16:27 (433 verses) 0.10623556581986143
2 Timothy 1:1-4:22 (83 verses) 0.10843373493975904
2 Corinthians 1:1-13:14 (257 verses) 0.11673151750972763
Galatians 1:1-6:18 (149 verses) 0.12080536912751678
1 Corinthians 1:1-16:24 (437 verses) 0.12814645308924486
Ephesians 1:1-6:24 (155 verses) 0.12903225806451613
Hebrews 1:1-13:25 (303 verses) 0.1419141914191419
2 John 1:1-13 (13 verses) 0.15384615384615385
Hosea 1:1-14:9 (197 verses) 0.15736040609137056
Haggai 1:1-2:23 (38 verses) 0.15789473684210525
Jude 1:1-25 (25 verses) 0.16
Colossians 1:1-4:18 (95 verses) 0.16842105263157894
Micah 1:1-7:20 (105 verses) 0.17142857142857143
Joel 1:1-3:21 (73 verses) 0.1780821917808219
Zephaniah 1:1-3:20 (53 verses) 0.18867924528301888
Jeremiah 1:1-52:34 (1364 verses) 0.18914956011730205
2 Thessalonians 1:1-3:18 (47 verses) 0.19148936170212766
Isaiah 1:1-66:24 (1292 verses) 0.19427244582043343
2 Peter 1:1-3:18 (61 verses) 0.19672131147540983
John 1:1-21:25 (879 verses) 0.19681456200227532
Amos 1:1-9:15 (146 verses) 0.19863013698630136
1 John 1:1-5:21 (105 verses) 0.20952380952380953
Obadiah 1:1-21 (21 verses) 0.23809523809523808
Ezra 1:1-10:44 (280 verses) 0.2857142857142857
Malachi 1:1-4:6 (55 verses) 0.2909090909090909
Jonah 1:1-4:11 (48 verses) 0.2916666666666667
Daniel 1:1-12:13 (357 verses) 0.29971988795518206
Deuteronomy 1:1-34:12 (959 verses) 0.32742440041710114
Nehemiah 1:1-13:31 (406 verses) 0.33004926108374383
Ezekiel 1:1-48:35 (1273 verses) 0.33699921445404557
Matthew 1:1-28:20 (1071 verses) 0.3520074696545285
Esther 1:1-10:3 (167 verses) 0.38922155688622756
Zechariah 1:1-14:21 (211 verses) 0.4075829383886256
Acts 1:1-28:31 (1007 verses) 0.48361469712015887
1 Chronicles 1:1-29:30 (942 verses) 0.4968152866242038
2 Chronicles 1:1-36:23 (822 verses) 0.5012165450121655
Luke 1:1-24:53 (1151 verses) 0.5438748913987836
2 Samuel 1:1-24:25 (695 verses) 0.5640287769784172
Numbers 1:1-36:13 (1288 verses) 0.5652173913043478
Leviticus 1:1-27:34 (859 verses) 0.5739231664726426
2 Kings 1:1-25:30 (719 verses) 0.5841446453407511
Joshua 1:1-24:33 (658 verses) 0.6124620060790273
1 Kings 1:1-22:53 (816 verses) 0.6151960784313726
Ruth 1:1-4:22 (85 verses) 0.6235294117647059
1 Samuel 1:1-31:13 (810 verses) 0.6283950617283951
Judges 1:1-21:25 (618 verses) 0.6666666666666666
Exodus 1:1-40:38 (1213 verses) 0.6760098928277
Revelation 1:1-22:21 (404 verses) 0.6782178217821783
Mark 1:1-16:20 (678 verses) 0.6799410029498525
Genesis 1:1-50:26 (1533 verses) 0.7377690802348337
[None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]
>>> 
>>> sorted(_,key=X[1])
<13>:1: AttributeError: 'NoneType' object has no attribute '__getitem__'
/home/pi/python/parle/func.py:24: AttributeError: 'NoneType' object has no attribute '__getitem__'
  __call__(
    self=Func(<function meth.<locals>.callmeth.<locals>.callmeth at 0...
  )
    args=(None,)
    kwargs={}
/home/pi/python/parle/func.py:463: AttributeError: 'NoneType' object has no attribute '__getitem__'
  callmeth(object=None)
    args=(1,)
    kwargs={}
    name=__getitem__
/home/pi/python/parle/func.py:451: AttributeError: 'NoneType' object has no attribute '__getitem__'
  callmethod(name=__getitem__, object=None)
    args=(1,)
    kwargs={}
>>> sorted(_,key=X[1])
<14>:1: AttributeError: 'NoneType' object has no attribute '__getitem__'
/home/pi/python/parle/func.py:24: AttributeError: 'NoneType' object has no attribute '__getitem__'
  __call__(
    self=Func(<function meth.<locals>.callmeth.<locals>.callmeth at 0...
  )
    args=(None,)
    kwargs={}
/home/pi/python/parle/func.py:463: AttributeError: 'NoneType' object has no attribute '__getitem__'
  callmeth(object=None)
    args=(1,)
    kwargs={}
    name=__getitem__
/home/pi/python/parle/func.py:451: AttributeError: 'NoneType' object has no attribute '__getitem__'
  callmethod(name=__getitem__, object=None)
    args=(1,)
    kwargs={}
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> `
invalid syntax (<25>, line 1)
>>> 
>>> 
>>> Genesis[1].count('and')
97
>>> Genesis[1].wc()
797
>>> 97/797
0.12170639899623588
>>> Genesis[1].midword()
['saw']
>>> (Genesis[1]-Genesis[2:3]).midword()
['may', 'fly']
>>> Genesis[1].count('saw')
7
>>> datetime.datetime.no()
<34>:1: AttributeError: type object 'datetime.datetime' has no attribute 'no'
>>> datetime.datetime.now()
datetime.datetime(2022, 12, 17, 11, 42, 12, 419790)
>>> 
>>> 
