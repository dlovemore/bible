>>> from bible import *
>>> ssum("מאור")
247
>>> ssum("rib")
101
>>> "God"*count*pn
101
>>> tells("Isaac")
I  s  a a c  =
1  1  1 1 1  5
9  19 1 1 3  33
9 100 1 1 3 114
>>> tells("Israel")
I  s   r a e  l  =
1  1   1 1 1  1  6
9  19 18 1 5 12  64
9 100 90 1 5 30 235
>>> 
