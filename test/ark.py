>>> from bible import *
>>> b/'ark'/'shittim'
Exodus 25:10;37:1;Deuteronomy 10:3 (3 verses)
>>> p(_)
Exodus 25:10 And they shall make an ark of shittim wood: two cubits and a half shall be the length thereof, and a cubit and a half the breadth thereof, and a cubit and a half the height thereof.
Exodus 37:1 And Bezaleel made the ark of shittim wood: two cubits and a half was the length of it, and a cubit and a half the breadth of it, and a cubit and a half the height of it:
Deuteronomy 10:3 And I made an ark of shittim wood, and hewed two tables of stone like unto the first, and went up into the mount, having the two tables in mine hand.
>>> 2.5*1.5*1.5
5.625
>>> _*71**3/4**3
31457.021484375
>>> _/64
491.5159606933594
>>> pi
3.141592653589793
>>> 
>>> 
>>> 2.5*1.5*71*71/16
1181.484375
>>> 1.5*1.5*71*71/16
708.890625
>>> 1181*4+708*2
6140
>>> 31457-31102
355
>>> (2.5**2+2*1.5**2)**.5
3.278719262151
>>> _*71/4
58.19726690318025
>>> _/4
14.549316725795062
>>> 71/4
17.75
>>> _/4
4.4375
>>> 31102/5.625
5529.2444444444445
>>> _**(1/3)
17.682972169763662
>>> 
>>> (71/4)**3
5592.359375
>>> 
>>> 
