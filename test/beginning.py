>>> from bible import *
>>> tell('the',osum)
 t h e  =
20 8 5 33
>>> tell('beginning',osum)
b e g i  n  n i  n g  =
2 5 7 9 14 14 9 14 7 81
>>> tell('the beginning')
the beginning  =
 33     81    114
>>> 33+81*49
4002
>>> 
>>> 
>>> tell('the',ssum)
 t  h e  =
200 8 5 213
>>> tell('beginning',ssum)
b e g i  n  n i  n g  =
2 5 7 9 50 50 9 50 7 189
>>> tell('the beginning',ssum)
the beginning  =
213    189    402
