>>> from bible import *
>>> b/'book of the lord'
Isaiah 34:16 Seek ye out of the book of the LORD, and read: no one of these shall fail, none shall want her mate: for my mouth it hath commanded, and his spirit it hath gathered them.
>>> p(_.vn())
18320
>>> p(_.tells())
Seek  ye out of the book of the LORD, and read:  no one of these shall fail, none shall want her mate: for  my mouth  it hath commanded, and his spirit  it hath gathered them.   =
  4   2   3   2  3    4   2  3    4    3    4    2   3   2   5     5     4     4    5     4   3    4    3   2    5    2    4       9      3   3     6    2    4      8      4    130
 40   30  56 21  33  43  21  33   49   19   28   29  34 21   57    52    28   48    52   58   31   39   39  38   77   29  37      72      19  36   91    29  37     68      46  1440
 130 705 560 66 213  142 66 213  184   55  100  110 115 66  318   169    46   165  169   751 103  246  156 740  608  209  217     207     55 117   478  209  217    320    253  8478
None
>>> tells("Seek ye out of the book of the LORD")
Seek  ye out of the book of the LORD   =
  4   2   3   2  3    4   2  3    4   27
 40   30  56 21  33  43  21  33  49   326
 130 705 560 66 213  142 66 213  184 2279
>>> 
>>> 
>>>
