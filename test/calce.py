f=lambda n,p=1,q=1,k=1:k<n+9and f(n,p*k+1,q*k,k+1) or str(p*100**n//q)[:n]

>>> from parle import *
>>> def edigits(n):
...     p,q=1,1
...     for k in range(1,n+9):
...         p,q=p*k+1,q*k
...     return str(p*100**n//q)[:n]
... 
>>> ed10000=edigits(10000)
>>> 
>>> # File("edigits10000").lines=[ed10000[n:n+100] for n in range(0,10000,100)]
>>> 
>>> 
