>>> from bible import *
>>> from pprint import pprint as pp
>>> sorted(bible.pattern('[A-Z][A-Z ,]*[A-Z][A-Z]'))
['A GOOD', 'BRANCH', 'GOD', 'HOLINESS TO THE LORD', 'HOLINESS UNTO THE LORD', 'I AM', 'I AM THAT I AM', 'I, O LORD', 'JAH', 'JEHOVAH', 'JESUS', 'JESUS OF NAZARETH THE KING OF THE JEWS', 'KING OF KINGS, AND LORD OF LORDS', 'LORD', 'LORD GOD', 'LORD JEHOVAH', 'MENE', 'MENE, MENE, TEKEL, UPHARSIN', 'MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH', 'O GOD', 'O LORD', 'PERES', 'TEKEL', 'THE KING OF THE JEWS', 'THE LORD OUR RIGHTEOUSNESS', 'THE LORD THY GOD', 'THERE', 'THIS IS JESUS THE KING OF THE JEWS', 'THIS IS THE KING OF THE JEWS', 'TO THE UNKNOWN GOD']
>>> pp(sorted([(count(s),psum(s),lsum(s),s) for s in _]))
[(19, 19, 3, 'JAH'),
 (23, 50, 3, 'I AM'),
 (26, 71, 3, 'GOD'),
 (37, 100, 4, 'MENE'),
 (41, 131, 4, 'O GOD'),
 (42, 132, 5, 'A GOOD'),
 (46, 154, 6, 'BRANCH'),
 (49, 184, 4, 'LORD'),
 (53, 260, 5, 'TEKEL'),
 (56, 308, 5, 'THERE'),
 (63, 270, 5, 'PERES'),
 (64, 244, 5, 'O LORD'),
 (69, 492, 7, 'JEHOVAH'),
 (73, 253, 6, 'I, O LORD'),
 (74, 515, 5, 'JESUS'),
 (75, 255, 7, 'LORD GOD'),
 (95, 509, 10, 'I AM THAT I AM'),
 (118, 676, 11, 'LORD JEHOVAH'),
 (161, 1376, 13, 'THE LORD THY GOD'),
 (185, 1193, 16, 'THE KING OF THE JEWS'),
 (206, 1574, 15, 'TO THE UNKNOWN GOD'),
 (218, 1019, 17, 'HOLINESS TO THE LORD'),
 (233, 1088, 21, 'MENE, MENE, TEKEL, UPHARSIN'),
 (253, 1369, 19, 'HOLINESS UNTO THE LORD'),
 (269, 1619, 22, 'THIS IS THE KING OF THE JEWS'),
 (279, 927, 25, 'KING OF KINGS, AND LORD OF LORDS'),
 (315, 1881, 23, 'THE LORD OUR RIGHTEOUSNESS'),
 (343, 2134, 27, 'THIS IS JESUS THE KING OF THE JEWS'),
 (373, 2929, 31, 'JESUS OF NAZARETH THE KING OF THE JEWS'),
 (763,
  5587,
  65,
  'MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE '
  'EARTH')]
>>> sums('Messiah')
(7, 74, 263)
>>> osum('dnA')
19
>>> osum('את'),osum('I AM')
(23, 23)
>>> osum('begin')
37
>>> osum('King')
41
>>> ssum('Adam')
46
>>> osum('XY')
49
>>> osum('ישוע')
53
>>> 63*63,49*81,49*osum('beginning')
(3969, 3969, 3969)
>>> 2**6,osum('God')
(64, 26)
>>> 3*osum('I AM')
69
>>> osum('Joshua'),osum('Messiah'),osum('MENE MENE')
(74, 74, 74)
>>> 
>>> b/"JESUS OF NAZARETH THE KING"
John 19:19 And Pilate wrote a title, and put it on the cross. And the writing was JESUS OF NAZARETH THE KING OF THE JEWS.
>>> _.vn(),_.chn()
(26845, 1016)
>>> ot.vc()
23145
>>> 26845-23145
3700
>>> 1016-929
87
>>> b[:163].tells()
I hate and abhor lying: but thy law do I love.   =
1   4   3    5      5    3   3   3   2 1   4    34
9  34   19   44    67    43  53  36 19 9   54   387
9  214  55  161    796  502 908 531 64 9  495  3744
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> pp(sorted([(ssum(s),s) for s in _]))
<31>:1: TypeError: 'int' object is not iterable
>>> 26**2
676
>>> 
>>> fs(75),fs(255)
([3, 5, 5], [3, 5, 17])
>>> fs(5587)
[37, 151]
>>> fs(763)
[7, 109]
>>> tell('THE BRANCH')
THE BRANCH  =
 33   46   79
>>> tell('THE BRANCH',ssum)
THE BRANCH  =
213   154  367
>>> 26*26
676
>>> tell('
EOL while scanning string literal (<40>, line 1)
>>> pf(492)
Counter({2: 2, 3: 1, 41: 1})
>>> 
>>> 
>>> np(373)
74
>>> npp(373)
13
>>> fs(676)
[2, 2, 13, 13]
>>> fs(2929)
[29, 101]
>>> npf(2929)
{29: 10, 101: 26}
>>> sums('rib')
(3, 29, 101)
>>> 29*101
2929
>>> tell('JESUS OF NAZARETH KING OF THE JEWS',ssum)
JESUS OF NAZARETH KING OF THE JEWS   =
 515  66   1155    86  66 213  615 2716
>>> b.pattern('AND LORD OF')
{'AND LORD OF'}
>>> b/'AND LORD OF LORDS'
Deuteronomy 10:17;1 Timothy 6:15;Revelation 19:16 (3 verses)
>>> p(_)
Deuteronomy 10:17 For the LORD your God is God of gods, and Lord of lords, a great God, a mighty, and a terrible, which regardeth not persons, nor taketh reward:
1 Timothy 6:15 Which in his times he shall shew, who is the blessed and only Potentate, the King of kings, and Lord of lords;
Revelation 19:16 And he hath on his vesture and on his thigh a name written, KING OF KINGS, AND LORD OF LORDS.
>>> sums('KING OF KINGS')
(11, 122, 338)
>>> sums('LORD OF LORDS')
(11, 138, 534)
>>> 
>>> tell('THIS IS JESUS THE KING OF THE JEWS')
THIS IS JESUS THE KING OF THE JEWS  =
 56  28   74   33  41  21  33  57  343
>>> tell('JESUS OF NAZARETH THE KING OF THE JEWS')
JESUS OF NAZARETH THE KING OF THE JEWS  =
  74  21    93     33  41  21  33  57  373
>>> prime(74)
373
>>> fs(269)
[269]
>>> np(269)
57
>>> pf(1376)
Counter({2: 5, 43: 1})
>>> 32*43
1376
>>> ssum('TEKEL')
260
>>> (Matthew-Revelation).chaptercount()
260
>>> tell('TEKEL UPHARSIN')
TEKEL UPHARSIN  =
  53     106   159
>>> fs(159)
[3, 53]
>>> 
>>> tell('TEKEL UPHARSIN',ssum)
TEKEL UPHARSIN  =
 260     628   888
>>> sums('MENE MENE')
(8, 74, 200)
>>> b.chapter(628)
Psalms 150:1-6 (6 verses)
>>> sums('PERES')
(5, 63, 270)
>>> sums('Jehoshuah')
(9, 95, 500)
>>> sums('I AM THAT I AM')
(10, 95, 509)
>>> int('368',12)
512
>>> 41*29
1189
>>> 
>>> tells('MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH')
MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH   =
    7       7     3     5    3     6    2    7     3       12       2  3    5    65
   125      71    33   51    33   79   21    93    19      132     21  33   52   763
  1835     845   213   303  213   403  66   489    55      582     66 213  304  5587
>>> 763*ns
[7, 109] [4, 29]
>>> b.ch(763)
Jeremiah 18:1-23 (23 verses)
>>> p(_)
Jeremiah 18
1 The word which came to Jeremiah from the LORD, saying,
2 Arise, and go down to the potter's house, and there I will cause thee to hear my words.
3 Then I went down to the potter's house, and, behold, he wrought a work on the wheels.
4 And the vessel that he made of clay was marred in the hand of the potter: so he made it again another vessel, as seemed good to the potter to make it.
5 Then the word of the LORD came to me, saying,
6 O house of Israel, cannot I do with you as this potter? saith the LORD. Behold, as the clay is in the potter's hand, so are ye in mine hand, O house of Israel.
7 At what instant I shall speak concerning a nation, and concerning a kingdom, to pluck up, and to pull down, and to destroy it;
8 If that nation, against whom I have pronounced, turn from their evil, I will repent of the evil that I thought to do unto them.
9 And at what instant I shall speak concerning a nation, and concerning a kingdom, to build and to plant it;
10 If it do evil in my sight, that it obey not my voice, then I will repent of the good, wherewith I said I would benefit them.
11 Now therefore go to, speak to the men of Judah, and to the inhabitants of Jerusalem, saying, Thus saith the LORD; Behold, I frame evil against you, and devise a device against you: return ye now every one from his evil way, and make your ways and your doings good.
12 And they said, There is no hope: but we will walk after our own devices, and we will every one do the imagination of his evil heart.
13 Therefore thus saith the LORD; Ask ye now among the heathen, who hath heard such things: the virgin of Israel hath done a very horrible thing.
14 Will a man leave the snow of Lebanon which cometh from the rock of the field? or shall the cold flowing waters that come from another place be forsaken?
15 Because my people hath forgotten me, they have burned incense to vanity, and they have caused them to stumble in their ways from the ancient paths, to walk in paths, in a way not cast up;
16 To make their land desolate, and a perpetual hissing; every one that passeth thereby shall be astonished, and wag his head.
17 I will scatter them as with an east wind before the enemy; I will shew them the back, and not the face, in the day of their calamity.
18 Then said they, Come and let us devise devices against Jeremiah; for the law shall not perish from the priest, nor counsel from the wise, nor the word from the prophet. Come, and let us smite him with the tongue, and let us not give heed to any of his words.
19 Give heed to me, O LORD, and hearken to the voice of them that contend with me.
20 Shall evil be recompensed for good? for they have digged a pit for my soul. Remember that I stood before thee to speak good for them, and to turn away thy wrath from them.
21 Therefore deliver up their children to the famine, and pour out their blood by the force of the sword; and let their wives be bereaved of their children, and be widows; and let their men be put to death; let their young men be slain by the sword in battle.
22 Let a cry be heard from their houses, when thou shalt bring a troop suddenly upon them: for they have digged a pit to take me, and hid snares for my feet.
23 Yet, LORD, thou knowest all their counsel against me to slay me: forgive not their iniquity, neither blot out their sin from thy sight, but let them be overthrown before thee; deal thus with them in the time of thine anger.
>>> b/'KING OF THE JEWS'
Matthew 2:2;27:11,29,37;Mark 15:2,9,12,18,26;Luke 23:3,37-38;John 18:33,39;19:3,19,21 (17 verses)
>>> (Matthew+John)/'King of the jews'
Matthew 2:2;27:11,29,37;John 18:33,39;19:3,19,21 (9 verses)
>>> p(_)
Matthew 2:2 Saying, Where is he that is born King of the Jews? for we have seen his star in the east, and are come to worship him.
Matthew 27
11 And Jesus stood before the governor: and the governor asked him, saying, Art thou the King of the Jews? And Jesus said unto him, Thou sayest.
29 And when they had platted a crown of thorns, they put it upon his head, and a reed in his right hand: and they bowed the knee before him, and mocked him, saying, Hail, King of the Jews!
37 And set up over his head his accusation written, THIS IS JESUS THE KING OF THE JEWS.
John 18:33 Then Pilate entered into the judgment hall again, and called Jesus, and said unto him, Art thou the King of the Jews?
John 18:39 But ye have a custom, that I should release unto you one at the passover: will ye therefore that I release unto you the King of the Jews?
John 19
3 And said, Hail, King of the Jews! and they smote him with their hands.
19 And Pilate wrote a title, and put it on the cross. And the writing was JESUS OF NAZARETH THE KING OF THE JEWS.
21 Then said the chief priests of the Jews to Pilate, Write not, The King of the Jews; but that he said, I am King of the Jews.
>>> 
