>>> from bible import *
>>> 789629*10**100//31415926535897932384626433832795
251346717117420444175695834376150517111313379174638374162939827506738075132
>>> 789629*10**100//31415926535897932384626433832795028841971693993
251346717117420444175695834376150286357816671218790697533474
>>> b.vi(2314)
Exodus 28:20 And the fourth row a beryl, and an onyx, and a jasper: they shall be set in gold in their inclosings.
>>> tells('beryl','onyx','jasper')
b e  r  y   l  =
1 1  1  1   1  5
2 5 18  25 12  62
2 5 90 700 30 827
>>> tells('beryl onyx jasper')
beryl onyx jasper   =
  5     4     6    15
  62   78    69    209
 827  1410   276  2513
>>> 2513-209
2304
>>> b.vi(2313)
Exodus 28:19 And the third row a ligure, an agate, and an amethyst.
>>> b.vi(2312)
Exodus 28:18 And the second row shall be an emerald, a sapphire, and a diamond.
>>> b.vi(2315)
Exodus 28:21 And the stones shall be with the names of the children of Israel, twelve, according to their names, like the engravings of a signet; every one with his name shall they be according to the twelve tribes.
>>> tells("thy truth")
thy truth   =
 3    5     8
 53   87   140
908  798  1706
>>> tells("thy word")
thy word   =
 3    4    7
 53  60   113
908  654 1562
>>> tells("messias")
 m e  s   s  i a  s   =
 1 1  1   1  1 1  1   7
13 5  19  19 9 1  19  85
40 5 100 100 9 1 100 355
>>> 113/85
1.3294117647058823
>>> 355/113
3.1415929203539825
>>> 85/113
0.7522123893805309
>>> 1562/85
18.376470588235293
>>> 1562/355
4.4
>>> b[4:4]
Genesis 4:4;Exodus 4:4;Leviticus 4:4;Numbers 4:4;Deuteronomy 4:4;Joshua 4:4;Judges 4:4;Ruth 4:4;1 Samuel 4:4;2 Samuel 4:4;1 Kings 4:4;2 Kings 4:4;1 Chronicles 4:4;2 Chronicles 4:4;Ezra 4:4;Nehemiah 4:4;Esther 4:4;Job 4:4;Psalms 4:4;Proverbs 4:4;Ecclesiastes 4:4;Song of Solomon 4:4;Isaiah 4:4;Jeremiah 4:4;Lamentations 4:4;Ezekiel 4:4;Daniel 4:4;Hosea 4:4;Amos 4:4;Jonah 4:4;Micah 4:4;Zechariah 4:4;Malachi 4:4;Matthew 4:4;Mark 4:4;Luke 4:4;John 4:4;Acts 4:4;Romans 4:4;1 Corinthians 4:4;2 Corinthians 4:4;Galatians 4:4;Ephesians 4:4;Philippians 4:4;Colossians 4:4;1 Thessalonians 4:4;1 Timothy 4:4;2 Timothy 4:4;Hebrews 4:4;James 4:4;1 Peter 4:4;1 John 4:4;Revelation 4:4 (53 verses)
>>> b[4:4].text()*sums
(5410, 62326, 437734)
>>> b[4:4].text()*sums@truediv*4.4
1229.5454545454545 14164.999999999998 99484.99999999999
>>> b[1:2]*sums
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 31, in __rmul__
    return Fun(left) and compose(left,self) or self(left)
  File "/home/pi/python/parle/func.py", line 24, in __call__
    return self.f(*args,**kwargs)
TypeError: unhashable type: 'Sel'
>>> b[1:2].text()*sums
(6202, 70356, 473070)
>>> b[1:1].text()*sums
(6677, 76580, 511559)
>>> 5557*2
11114
>>> 1111*4
4444
>>> 4444/2
2222.0
>>> "profane and vain babblings")
  File "<stdin>", line 1
    "profane and vain babblings")
                                ^
SyntaxError: invalid syntax
>>> "profane and vain babblings"*tells
profane and vain babblings   =
   7     3    4      9      23
   75    19  46      68     208
  282    55  460    203    1000
>>> niav
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'niav' is not defined
>>> tells("New International Version")
New International Version   =
 3        13         7     23
 42      152        102    296
555      755        714   2024
>>> 2024-150
1874
>>> tells("eumjm")
e  u   m  j  m  =
1  1   1  1  1  5
5  21 13 10 13  62
5 300 40 10 40 395
>>> tells("eart")
e a  r  t   =
1 1  1  1   4
5 1 18  20  44
5 1 90 200 296
>>> tells("av eumjm fav")
 av eumjm fav   =
 2    5    3   10
 23   62   29  114
401  395  407 1203
>>> tells("King James")
King James  =
  4    5    9
 41    48   89
 86   156  242
>>> tells("King James Version")
King James Version  =
  4    5      7     16
 41    48    102   191
 86   156    714   956
>>> tells("King James Bible")
King James Bible  =
  4    5     5    14
 41    48    30  119
 86   156    48  290
>>> tells("Authorised King James Bible")
Authorised King James Bible   =
    10       4    5     5    24
    120     41    48    30   239
    777     86   156    48  1067
>>> tells("Authorised King James Version")
Authorised King James Version   =
    10       4    5      7     26
    120     41    48    102    311
    777     86   156    714   1733
>>>
