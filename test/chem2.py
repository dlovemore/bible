>>> from bible import *
>>> [chapter.chapter() for chapter in b.chapters() if chapter.vc()==24]*reduce(add,...)
777
>>> tri(103)-tri(88)
1440
>>> tri(71)-tri(56)
960
>>> 1440+960
2400
>>> tells("darkness")
d a  r  k  n e  s   s   =
1 1  1  1  1 1  1   1   8
4 1 18 11 14 5  19  19  91
4 1 90 20 50 5 100 100 370
>>> Genesis[1:2].tells()
And the earth was without form, and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters.   =
 3   3    5    3     7      4    3    4    3      8     3    4   3    4   2  3    4    3   3     6    2  3    5     4   3    4   2  3     6     110
 19  33   52   43   116     52   19   50   19    91     43  66   33  15  21  33   30   19  33   91   21  26   59   66   33  15  21  33    86   1238
 55 213  304  601   1277   196   55  473   55    370   601  480 213  15  66 213   84   55 213   478  66  71  509   480 213  15  66 213   896   8546
>>> tells("tennessine")
 t  e  n  n e  s   s  i  n e  =
 1  1  1  1 1  1   1  1  1 1  10
 20 5 14 14 5  19  19 9 14 5 124
200 5 50 50 5 100 100 9 50 5 574
>>> 5*74
370
>>> tells("uuh")
 u   u  h  =
 1   1  1  3
 21  21 8  50
300 300 8 608
>>> tells("uuo")
 u   u   o  =
 1   1   1  3
 21  21 15  57
300 300 60 660
>>> tells("oxygen")
 o  x   y  g e  n   =
 1  1   1  1 1  1   6
15  24  25 7 5 14  90
60 600 700 7 5 50 1422
>>> tells("Oganesson")
 O g a  n e  s   s   o  n  =
 1 1 1  1 1  1   1   1  1  9
15 7 1 14 5  19  19 15 14 109
60 7 1 50 5 100 100 60 50 433
>>> 1422*2
2844
>>> tells("hydrogen")
h  y  d  r  o g e  n  =
1  1  1  1  1 1 1  1  8
8  25 4 18 15 7 5 14  96
8 700 4 90 60 7 5 50 924
>>> 96*2
192
>>> 96+90
186
>>> 192+90
282
>>> 1422+924
2346
>>> 192+90*2
372
>>> 1422+924*2
3270
>>> tells("gen")
g e  n  =
1 1  1  3
7 5 14 26
7 5 50 62
>>> tells("gen esis")
gen esis  =
 3    4   7
 26  52   78
 62  214 276
>>> tells("gen es is")
gen  es  is  =
 3   2   2   7
 26  24  28  78
 62 105 109 276
>>> tells("gen esis")
gen esis  =
 3    4   7
 26  52   78
 62  214 276
>>> tells("god create")
god create  =
 3     6    9
 26   52    78
 71   304  375
>>> 117+118
235
>>> 294+117
411
>>> 294+118
412
>>> 4**2+1+1
18
>>> 4**2+11**2
137
>>> 2701+3627*2
9955
>>> tells("God Almighty")
God Almighty   =
 3      8     11
 26    95     121
 71    995   1066
>>> tells("Revelation")
 R e  v  e  l a  t  i  o  n  =
 1 1  1  1  1 1  1  1  1  1  10
18 5  22 5 12 1  20 9 15 14 121
90 5 400 5 30 1 200 9 60 50 850
>>> tells("Lord God Almighty")
Lord God Almighty   =
  4   3      8     15
 49   26    95     170
 184  71    995   1250
>>> tells("Holy, Holy, Holy, Lord God Almighty")
Holy, Holy, Holy, Lord God Almighty   =
  4     4     4     4   3      8     27
  60    60    60   49   26    95     350
 798   798   798   184  71    995   3644
>>> 3644+2400
6044
>>> 
>>> "La Ce Pr Nd Pm Sm Eu Gd Tb Dy Ho Er Tm Yb Lu"*tells
La Ce  Pr Nd  Pm  Sm  Eu Gd  Tb  Dy Ho Er  Tm  Yb  Lu   =
 2  2  2   2  2   2   2   2  2   2   2  2  2   2   2   30
13  8  34 18  29  32  26 11  22  29 23 23  33  27  33  361
31  8 160 54 110 140 305 11 202 704 68 95 240 702 330 3160
>>> "Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No Lr"*tells
Ac  Th Pa  U   Np  Pu Am Cm Bk Cf  Es Fm Md  No  Lr   =
 2  2   2  1   2   2   2  2  2  2  2   2  2  2   2   29
 4  28 17  21  30  37 14 16 13  9  24 19 17  29  30  308
 4 208 71 300 120 370 41 43 22  9 105 46 44 110 120 1613
>>> 1613+3160
4773
>>> 1613+361
1974
>>> tells("lu lr")
 lu  lr  =
 2   2   4
 33  30  63
330 120 450
>>> 4773-450
4323
>>> 
