>>> from bible import *
>>> Esther
Esther 1:1-10:3 (167 verses)
>>> Esther[8:9]
Esther 8:9 Then were the king's scribes called at that time in the third month, that is, the month Sivan, on the three and twentieth day thereof; and it was written according to all that Mordecai commanded unto the Jews, and to the lieutenants, and the deputies and rulers of the provinces which are from India unto Ethiopia, an hundred twenty and seven provinces, unto every province according to the writing thereof, and unto every people after their language, and to the Jews according to their writing, and according to their language.
>>> Esther[8:9].wc()
90
>>> vsinorder=sorted(b*L,key=method.vc)
>>> vsinorder[:7]
[Genesis 1:1 In the beginning God created the heaven and the earth., Genesis 1:2 And the earth was without form, and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters., Genesis 1:3 And God said, Let there be light: and there was light., Genesis 1:4 And God saw the light, that it was good: and God divided the light from the darkness., Genesis 1:5 And God called the light Day, and the darkness he called Night. And the evening and the morning were the first day., Genesis 1:6 And God said, Let there be a firmament in the midst of the waters, and let it divide the waters from the waters., Genesis 1:7 And God made the firmament, and divided the waters which were under the firmament from the waters which were above the firmament: and it was so.]
>>> 
>>> 
>>> 
