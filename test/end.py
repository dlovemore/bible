>>> from bible import *
>>> b/'decl'/'end'/'beginning'
Isaiah 46:10 Declaring the end from the beginning, and from ancient times the things that are not yet done, saying, My counsel shall stand, and I will do all my pleasure:
>>> _.tells()
Declaring the end from the beginning, and from ancient times the things that are not yet done, saying,  My counsel shall stand, and I will do all  my pleasure:   =
    9      3   3    4   3       9      3    4     7      5    3     6     4   3   3   3    4      6     2     7      5      5    3  1   4   2  3   2      8      124
    73     33  23  52   33     81      19  52     66     66   33   77    49   24  49  50   38     75    38    89     52    58    19 9  56  19  25  38     97    1393
   199    213  59  196 213     189     55  196   318    354  213   374   409  96 310 905  119    867   740   548    169    355   55 9  569 64  61 740    601    9196
>>> Isaiah[46:10].vn()
18597
>>> Isaiah.vn()
17656
>>> 
>>> 18597-17656+1
942
>>> _*ns
[2, 3, 157] [1, 2, 37]
>>> 33+81
114
>>> ln(114)
4.736198448394496
>>> e=1.602176634e-19
>>> ln(e)
-43.27775366589175
>>> ln(e/3)
-44.37636595455986
>>> 5366*ns
[2, 2683] [1, 389]
>>> 2777*ns
[2777] [404]
>>> Revelation.vc()
404
>>> tell('Authorised Version',psum)
Authorised Version   =
    777      714   1491
>>> tell('Authorized Version',psum)
Authorized Version   =
   1477      714   2191
>>> tells('Authorised King James Version')
Authorised King James Version   =
    10       4    5      7     26
    120     41    48    102    311
    777     86   156    714   1733
>>> tells('Authorized King James Version')
Authorized King James Version   =
    10       4    5      7     26
    127     41    48    102    318
   1477     86   156    714   2433
>>> tell('Authorised Version')
Authorised Version  =
    120      102   222
>>> tells('Authorised King James Version')
Authorised King James Version   =
    10       4    5      7     26
    120     41    48    102    311
    777     86   156    714   1733
>>> tells('the stars')
the stars  =
 3    5    8
 33   77  110
213  491  704
>>> tells("Jesus Christ")
Jesus Christ  =
  5      6    11
  74    77   151
 515    410  925
>>> tells("Lord Jesus")
Lord Jesus  =
  4    5    9
 49    74  123
 184  515  699
>>> tells("Lord Jesus Christ")
Lord Jesus Christ   =
  4    5      6    15
 49    74    77    200
 184  515    410  1109
>>> Revelation[11:9]
Revelation 11:9 And they of the people and kindreds and tongues and nations shall see their dead bodies three days and an half, and shall not suffer their dead bodies to be put in graves.
>>> _.vn()
30882
>>> 
>>> 
>>> tells("t'")
 t'  =
 1   1
 20  20
200 200
>>> 1491*ns
[3, 7, 71] [2, 4, 20]
>>> pp(20)
929
>>> 
>>> 
