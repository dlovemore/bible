# >>> from bible import *
# >>> from decimal import *
# >>> from math import *
# >>> import decimal
# >>> setcontext(Context(100))
# >>> 
# >>> 
# >>> pi=Decimal('3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214')
# >>> pi
# Decimal('3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214')
# >>> 
# >>> epi=Decimal('23.140692632779269005729086367948547380266106242600211993445046409524342350690')
# >>> print(epi)
# 23.140692632779269005729086367948547380266106242600211993445046409524342350690
# >>> print(pi)
# 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214
# >>> tell("Gelfond's constant")
# Gelfond's constant  =
#     82       106   188
# >>> factors(9773262)
# [2, 3, 3, 229, 2371]
## >>> factors(692623377969)
## [3, 230874459323]
## >>> 
# >>> factors(int(str(692623377969)[::-1]))
# [2, 2, 2, 3, 19, 2126695891]
# >>> epimpi=Decimal('19.99909997918947576726644298466904449606893684322510617247010181721652594440')
# >>> pimepi=Decimal('20.00000000000000000000000000000000000000000000000000000000000000000000000000')-epimpi
# >>> pimepi
# Decimal('0.00090002081052423273355701533095550393106315677489382752989818278347405560')
# >>> fbase(23,pi)
# <generator object fbase at 0xb5774170>
# >>> fbase(23,pimepi)
# <generator object fbase at 0xb5774230>
# >>> list(itertools.islice(_,19))
# [0, 0, 0, 10, 21, 19, 19, 8, 17, 10, 5, 16, 2, 1, 20, 17, 8, 16, 2]
# >>> 
# >>> list(itertools.islice(fbase(7,epimpi),19))
# [19, 6, 6, 6, 4, 5, 6, 0, 5, 3, 6, 2, 5, 3, 2, 1, 5, 4, 3]
# >>> 
# >>> math.log(pi)
# 1.1447298858494002
# >>> 
# >>> 
# >>> log(2)
# 0.6931471805599453
# >>> 
# >>> 
# >>> 2081-1611
# 470
# >>> pf(208)
# Counter({2: 4, 13: 1})
# >>> 
# >>> sums('επι')
# (3, 32, 95)
# >>> tell('επι',osum,ssum)
# ε  π  ι  =
# 5 17 10 32
# 5 80 10 95
# >>> 5**27
# 7450580596923828125
# >>> 
# >>> 5**32
# 23283064365386962890625
# >>> 5**23
# 11920928955078125
# >>> 
# >>> 789629/pi
# Decimal('251346.7171174204441756958343761502863578166712127817703363436344437652388382333293533429439290273160')
# >>> Job[42]
# Job 42:1-17 (17 verses)
# >>> 
# >>> epi
# Decimal('23.140692632779269005729086367948547380266106242600211993445046409524342350690')
# >>> root163=Decimal(163)**Decimal('0.5')
# >>> root163
# Decimal('12.76714533480370466171095200978089234738236378030125885121260298384872617289023925955942348386753187')
# >>> epi**root163
# Decimal('262537412640768743.9999999999992500725971981856888793538563373369908627075373447940057585380261507254')
# >>> int(_)+1
# 262537412640768744
# >>> _//24
# 10939058860032031
# >>> 939*ns
# [3, 313] [2, 65]
# >>> 
# >>> ln(23)
# 3.1354942159291497
# >>> 
# >>> 
# >>> 
# >>> b.book(2)[3][14].ws()[5:7]
# ['I', 'AM']
# >>> b.vi(23140).ws()[5]
# 'that'
# >>> b.book(3)[14:15].ws()[8:11]
# ['log', 'of', 'oil']
# >>> b.vi(3141).ws()[4:9]
# ['the', 'oil', 'that', 'is', 'in']
# >>> b.book(23)[14:6]
# Isaiah 14:6 He who smote the people in wrath with a continual stroke, he that ruled the nations in anger, is persecuted, and none hindereth.
# >>> b.vi(23146)
# Matthew 1:1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.
# >>> John[17]/'thee'
# John 17:1,3-5,7-8,11,13,21,25 (10 verses)
# >>> p(_)
# John 17
# 1 These words spake Jesus, and lifted up his eyes to heaven, and said, Father, the hour is come; glorify thy Son, that thy Son also may glorify thee:
# 3 And this is life eternal, that they might know thee the only true God, and Jesus Christ, whom thou hast sent.
# 4 I have glorified thee on the earth: I have finished the work which thou gavest me to do.
# 5 And now, O Father, glorify thou me with thine own self with the glory which I had with thee before the world was.
# 7 Now they have known that all things whatsoever thou hast given me are of thee.
# 8 For I have given unto them the words which thou gavest me; and they have received them, and have known surely that I came out from thee, and they have believed that thou didst send me.
# 11 And now I am no more in the world, but these are in the world, and I come to thee. Holy Father, keep through thine own name those whom thou hast given me, that they may be one, as we are.
# 13 And now come I to thee; and these things I speak in the world, that they might have my joy fulfilled in themselves.
# 21 That they all may be one; as thou, Father, art in me, and I in thee, that they also may be one in us: that the world may believe that thou hast sent me.
# 25 O righteous Father, the world hath not known thee: but I have known thee, and these have known that thou hast sent me.
# >>> _.vns()
# [26761, 26763, 26764, 26765, 26767, 26768, 26771, 26773, 26781, 26785]
# >>> exp(pi)
# 23.140692632779267
# >>> b.book(2)[31][4]
# Exodus 31:4 To devise cunning works, to work in gold, and in silver, and in brass,
# >>> b/'divided by'
# Numbers 26:55;1 Chronicles 24:5;Amos 7:17 (3 verses)
# >>> Amos[7:17].ws()[29:]
# ['be', 'divided', 'by', 'line', 'and', 'thou', 'shalt', 'die', 'in', 'a', 'polluted', 'land', 'and', 'Israel', 'shall', 'surely', 'go', 'into', 'captivity', 'forth', 'of', 'his', 'land']
# >>> Genesis/'over'
# Genesis 1:18,26,28;3:16;4:7;8:1;9:14,19;19:21,25,29;21:16;24:2;25:25;27:29;31:21,23,25,52;32:10,16,21-23,31;33:3,13-14;36:31;37:8;39:4-5;41:33-34,40-41,43,45,56;42:6;43:12;44:4,6;45:26;47:6,20,26;49:19,22 (49 verses)
# >>> Genesis[1:18]
# Genesis 1:18 And to rule over the day and over the night, and to divide the light from the darkness: and God saw that it was good.
# >>> Genesis[1:1:17].wc()
# 378
# >>> Genesis[1:14]
# Genesis 1:14 And God said, Let there be lights in the firmament of the heaven to divide the day from the night; and let them be for signs, and for seasons, and for days, and years:
# >>> Genesis[1:1:13].wc()
# 280
# >>> b/'by the moon'
# Deuteronomy 33:14 And for the precious fruits brought forth by the sun, and for the precious things put forth by the moon,
# >>> Deuteronomy[33:14].vn()
# 5825
# >>> 
# >>> 
# >>> 
# >>> 
# >>>
