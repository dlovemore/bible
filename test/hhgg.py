>>> from bible import *
>>> tell('Douglas Noel Adams')
Douglas Noel Adams  =
   79    46    38  163
>>> tell('In the beginning God')
In the beginning God  =
23  33     81     26 163
>>> tell('created the heaven and')
created the heaven and  =
   56    33   55    19 163
>>> tell('DNA')
D  N A  =
4 14 1 19
>>> tell('Adam')
A d a  m  =
1 4 1 13 19
>>> tell(extc,'Adam')
A d a  m  =
1 4 1 40 46
>>> count('Noel')
46
>>> count('Douglas')
79
>>> 1978*ns
[2, 23, 43] [1, 9, 14]
>>> count('was')*count('Noel')
1978
>>> lettercount("Hitch Hiker's Guide to the Galaxy")
27
>>> 
>>> tells('Douglas Noel Adams')
Douglas Noel Adams  =
   7      4    5    16
   79    46    38  163
  502    145  146  793
>>> 793*ns
[13, 61] [6, 18]
>>> tells('number')
 n  u   m b e  r  =
 1  1   1 1 1  1  6
14  21 13 2 5 18  73
50 300 40 2 5 90 487
>>> 
>>> 'Douglas Noel Adams'[::-1]
'smadA leoN salguoD'
>>> tells("The Hitchhiker's Guide to the Galaxy Primary and Secondary Phases")
The Hitchhiker's Guide  to the Galaxy Primary and Secondary Phases   =
 3       11        5    2   3     6      7     3      9        6    55
 33      118       46   35  33   70     100    19    104      68    626
213      460      325  260 213  1339    1000   55    1013     284  5162
>>> 626*ns
[2, 313] [1, 65]
>>> tells('six hundred threescore and six')
six hundred threescore and six   =
 3     7        10      3   3   26
 52    74       116     19  52  313
709   461       566     55 709 2500
>>> 
