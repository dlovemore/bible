>>> from bible import *
>>> b[3][14][15]
Leviticus 14:15 And the priest shall take some of the log of oil, and pour it into the palm of his own left hand:
>>> _.vn(),_.chn(),Leviticus[1]-_
(3127, 104, Leviticus 1:1-14:15 (381 verses))
>>> sof(411)
552
>>> sof(66)
144
>>> sof(1189)
1260
>>> 6**6+3
46659
>>> from bible.pi import pistr
>>> pistr[242424]
'4'
>>> pistr[242424-5:242424+8]
'1324242424201'
>>> # [(n,pistr[max(0,n-6):n+6]) for n in span(999999) if str(n) in pistr[n-len(str(n)):n+len(str(n))]]
>>> [(1, '3141592'), (5, '31415926535'), (315, '660631558817'), (360, '259036001133'), (384, '652138414695'), (875, '865875332083'), (1614, '001614524919'), (9218, '889218654364'), (16470, '048758164703'), (44899, '459447448990'), (47696, '005247696884'), (62843, '320628432065'), (242424, '213242424242'), (247826, '247826096808'), (271070, '625102710706'), (496498, '080749649824')]
[(1, '3141592'), (5, '31415926535'), (315, '660631558817'), (360, '259036001133'), (384, '652138414695'), (875, '865875332083'), (1614, '001614524919'), (9218, '889218654364'), (16470, '048758164703'), (44899, '459447448990'), (47696, '005247696884'), (62843, '320628432065'), (242424, '213242424242'), (247826, '247826096808'), (271070, '625102710706'), (496498, '080749649824')]
>>> _@I[0]
[1, 5, 315, 360, 384, 875, 1614, 9218, 16470, 44899, 47696, 62843, 242424, 247826, 271070, 496498]
>>> pistr.find('16470')
1602
>>> 16470*ns
[2, 3, 3, 3, 5, 61] [1, 2, 2, 2, 3, 18]
>>> 
>>> b.vi(1746)
Exodus 9:3 Behold, the hand of the LORD is upon thy cattle which is in the field, upon the horses, upon the asses, upon the camels, upon the oxen, and upon the sheep: there shall be a very grievous murrain.
>>> Exodus/'locust'
Exodus 10:4,12-14,19 (5 verses)
>>> td=datetime.timedelta
>>> td(hours=5,minutes=17,seconds=46)
datetime.timedelta(seconds=19066)
>>> 19066
19066
>>> 1746*phi
2825.0873443573164
>>> ln(11302928)/ln(2)
23.43019321241801
>>> bin(11302928)
'0b101011000111100000010000'
>>> 43<<18|15<<11|16
11302928
>>> base(22,11302928)
[2, 4, 5, 11, 3, 10]
>>> int('245b3a',22)
11302928
>>> base(23,11302928)
[1, 17, 8, 22, 13, 15]
>>> 
>>> 
>>> Revelation/'was'/'is'/'come'
Revelation 1:4,8;4:8 (3 verses)
>>> Revelation[4:8].tells()
And the four beasts had each of them six wings about him; and they were full of eyes within: and they rest not day and night, saying, Holy, holy, holy, LORD God Almighty, which was, and is, and  is  to come.   =
 3   3    4     6    3    4   2   4   3    5     5     3   3    4    4    4   2   4     6     3    4    4   3   3   3     5      6      4     4     4     4   3      8       5     3   3   2   3   2   2    4    154
 19  33  60    66    13  17  21  46   52   72    59   30   19  58   51   51  21  54     83    19  58   62   49  30  19   58      75     60    60    60   49   26     95      51   43   19  28  19  28  35   36   1804
 55 213  456   408   13  17  66  253 709  666   563   57   55  913  600  366 66  810   776    55  913  395 310 705  55   274    867    798   798   798   184  71    995     528   601  55 109  55 109 260  108  16105
>>> _.vns(),_.chn()
([30702, 30706, 30777], 1168)
>>> Revelation[4].chn()
1171
>>> 
>>> 
>>> 
>>> 
>>> bin(3088286401)
'0b10111000000100111000001011000001'
>>> 23<<27|39<<15|11<<6|1
3088286401
>>> tells('was and is and is to come')
was and  is and  is  to come   =
 3   3   2   3   2   2    4   19
 43  19  28  19  28  35  36   208
601  55 109  55 109 260  108 1297
>>> 
>>> 1080/666
1.6216216216216217
>>> phi
1.618033988749895
>>> 1746/1080
1.6166666666666667
>>> 1746+1080
2826
>>> Fibonacci(6,666,1080)
11970
>>> 1080-666
414
>>> 666-414
252
>>> 414-252
162
>>> 252-162
90
>>> 162-90
72
>>> 90-72
18
>>> 72-18
54
>>> span(30)@Fn(...,f0=54,f1=18)
18 72 90 162 252 414 666 1080 1746 2826 4572 7398 11970 19368 31338 50706 82044 132750 214794 347544 562338 909882 1472220 2382102 3854322 6236424 10090746 16327170 26417916 42745086
>>> 16470/phi
10179.019794710768
>>> 16470-10179
6291
>>> 10179-6291
3888
>>> 6291-3888
2403
>>> 3888-2403
1485
>>> 2403-1485
918
>>> 1485-918
567
>>> 918-567
351
>>> 567-351
216
>>> 351-216
135
>>> 216-135
81
>>> 135-81
54
>>> 81-54
27
>>> Fn(14,27,27)
16470
>>> Fn(15)*27
16470
>>> Fn(15)
610
>>> 16470/phi
10179.019794710768
>>> 10179*ns
[3, 3, 3, 13, 29] [2, 2, 2, 6, 10]
>>> 10179/27
377.0
>>> b[16][4][7]
Nehemiah 4:7 But it came to pass, that when Sanballat, and Tobiah, and the Arabians, and the Ammonites, and the Ashdodites, heard that the walls of Jerusalem were made up, and that the breaches began to be stopped, then they were very wroth,
>>> b.vi(1647)
Exodus 5:14 And the officers of the children of Israel, which Pharaoh's taskmasters had set over them, were beaten, and demanded, Wherefore have ye not fulfilled your task in making brick both yesterday and to day, as heretofore?
>>> b.vi(2020)
Exodus 18:20 And thou shalt teach them ordinances and laws, and shalt shew them the way wherein they must walk, and the work that they must do.
>>> _.tells()
And thou shalt teach them ordinances and laws, and shalt shew them the  way wherein they must walk, and the work that they must do.   =
 3    4    5     5     4      10      3    4    3    5     4    4   3    3     7      4    4    4    3   3    4    4    4    4   2   103
 19  64    60    37   46      102     19   55   19   60   55   46   33  49     82    58   73    47   19  33  67   49   58   73   19  1242
 55  568  339   217   253     372     55  631   55  339   613  253 213 1201   667    913  640  551   55 213  670  409  913  640  64 10899
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> b.ch(1079)
2 Corinthians 1:1-24 (24 verses)
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
