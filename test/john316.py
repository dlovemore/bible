>>> from bible import *
>>> John[3:14]
John 3:14 And as Moses lifted up the serpent in the wilderness, even so must the Son of man be lifted up:
>>> John[3:14:16]
John 3:14-16 (3 verses)
>>> p(_)
John 3
14 And as Moses lifted up the serpent in the wilderness, even so must the Son of man be lifted up:
15 That whosoever believeth in him should not perish, but have eternal life.
16 For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.
>>> _.vns()
[26135, 26136, 26137]
>>> John[3:16].tells()
For God  so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.   =
 3   3   2    5    3     5     4   2   4   3    4      8      3    4      9         9      2  3     6    3     6     3    4       11       4    113
 39  26  34   58   33   72    49  13  35   36  66     88     48   49     130        88    23  30   79    49    75    43  36      132       32  1363
156  71 160  499  213   684   409 13  413 117  840    529    210  409    1228      664    59  57   502  310   282   502  414     897       50  9688
>>> len(John[3:16].ws())
25
>>> 66+88
154
>>> 9688*ns
[2, 2, 2, 7, 173] [1, 1, 1, 4, 40]
>>> 9688*nsof
[8983]
>>> b.vi(8963)
1 Kings 7:28 And the work of the bases was on this manner: they had borders, and the borders were between the ledges:
>>> _[0].vn()
<12>:1: AttributeError: 'str' object has no attribute 'vn'
>>> IIKings[3:22].chn()
316
>>> 1363*ns
[29, 47] [10, 15]
>>> 
>>> [v for v in b if v.count(ssum)==9688]
[Leviticus 8:16 And he took all the fat that was upon the inwards, and the caul above the liver, and the two kidneys, and their fat, and Moses burned it upon the altar., 1 Samuel 6:2 And the Philistines called for the priests and the diviners, saying, What shall we do to the ark of the LORD? tell us wherewith we shall send it to his place., Esther 1:20 And when the king's decree which he shall make shall be published throughout all his empire, (for it is great,) all the wives shall give to their husbands honour, both to great and small., John 3:16 For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life., John 10:32 Jesus answered them, Many good works have I shewed you from my Father; for which of those works do ye stone me?]
>>> _@(method.chn+method.vn+method.count)
[98 2934 1327, 242 7334 1453, 427 12723 1642, 1000 26137 1363, 1007 26514 1111]
>>> 
>>> [v for v in b if v.count(ssum)==8983]
