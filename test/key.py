>>> from bible import *
>>> b/'key of David'
Revelation 3:7 And to the angel of the church in Philadelphia write; These things saith he that is holy, he that is true, he that hath the key of David, he that openeth, and no man shutteth; and shutteth, and no man openeth;
>>> b/'key'/'David'
Isaiah 22:22 And the key of the house of David will I lay upon his shoulder; so he shall open, and none shall shut; and he shall shut, and none shall open.
Revelation 3:7 And to the angel of the church in Philadelphia write; These things saith he that is holy, he that is true, he that hath the key of David, he that openeth, and no man shutteth; and shutteth, and no man openeth;
>>> _.vns(),[v.chn() for v in _]
([18075, 30754], [701, 1170])
>>> Revelation[3:7].tells()
And  to the angel of the church in Philadelphia write; These things saith he that  is holy, he that  is true, he that hath the key of David, he that openeth, and  no man shutteth; and shutteth, and  no man openeth;   =
 3   2   3    5    2  3     6    2      12         5     5      6     5    2   4   2    4    2   4   2    4    2   4    4   3   3   2    5    2   4      7     3   2   3      8      3      8      3   2   3      7     161
 19  35  33   39  21  33   61   23      101       75     57    77     57  13  49   28   60  13  49   28   64  13  49   37   33  41 21   40   13  49     83     19  29  28    121     19    121     19  29  28    83     1810
 55 260 213   93  66 213   412  59      245       804   318    374   318  13  409 109  798  13  409 109  595  13  409  217 213 725 66   418  13  409    398    55 110  91    1021    55    1021    55 110  91    398   11773
>>> 1170*ns
[2, 3, 3, 5, 13] [1, 2, 2, 3, 6]
>>> 11773*ns
[61, 193] [18, 44]
>>> tells('son of David')
son of David  =
 3   2   5    10
 48 21   40  109
210 66  418  694
>>> 277*ns
[277] [59]
>>> 
>>> 
