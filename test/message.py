>>> from bible import *
>>> c='Who sends a message in the number of characters in a book?'
>>> tell(lsum,c)
Who sends a message in the number of characters in a book?  =
 3    5   1    7     2  3     6    2     10      2 1   4   46
>>> tell(lsum,"1:1 In the beginning God created the heaven and the earth.")
1:1 In the beginning God created the heaven and the earth.  =
 2   2  3      9      3     7     3     6    3   3     5   46
>>> tell(lsum,osum,Isaiah[41:26].text())
Who hath declared from the beginning, that we may know? and beforetime, that we may say, He is righteous? yea, there is none that sheweth, yea, there is none that declareth, yea, there is none that heareth your words.   =
 3    4      8      4   3       9       4   2  3    4    3       10       4   2  3    3   2  2      9       3    5    2   4    4      7      3    5    2   4    4       9       3    5    2   4    4     7      4     5    168
 46  37     52     52   33     81      49  28  39   63   19      98      49  28  39  45  13 28     122     31    56  28  48   49     88     31    56  28  48   49      76      31    56  28  48   49     65    79    79   1944
>>> tell(lsum,osum,Isaiah[46:10].text())
Declaring the end from the beginning, and from ancient times the things that are not yet done, saying, My counsel shall stand, and I will do all my pleasure:   =
    9      3   3    4   3       9      3    4     7      5    3     6     4   3   3   3    4      6     2    7      5      5    3  1   4   2  3   2     8      124
    73     33  23  52   33     81      19  52     66     66   33   77    49   24  49  50   38     75   38    89     52    58    19 9  56  19  25 38     97    1393
>>> tell(lsum,osum,Isaiah[48:3].text())
I have declared the former things from the beginning; and they went forth out of my mouth, and I shewed them; I did them suddenly, and they came to pass.   =
1   4      8     3     6      6     4   3       9      3    4    4    5    3   2  2    5    3  1    6     4   1  3    4      8      3    4    4   2   4    119
9  36     52     33   75     77    52   33     81      19  58   62    67   56 21 38   77    19 9   64     46  9  17  46     104     19  58   22  35   55  1349
>>> tell(lsum,osum,Isaiah[48:5].text())
I have even from the beginning declared it to thee; before it came to pass I shewed it thee: lest thou shouldest say, Mine idol hath done them, and my graven image, and my molten image, hath commanded them.   =
1   4    4    4   3      9         8     2  2   4      6    2   4   2   4  1    6    2   4     4    4      9       3    4    4    4    4    4    3   2    6      5    3   2    6      5     4      9       4    161
9  36   46   52   33     81       52    29 35   38    51   29  22  35  55  9   64   29   38   56   64     123     45   41   40   37   38    46   19 38   67     35    19 38   79     35    37      72      46  1718
>>> Isaiah[41:26]|Isaiah[48:5]
Isaiah 41:26 Who hath declared from the beginning, that we may know? and beforetime, that we may say, He is righteous? yea, there is none that sheweth, yea, there is none that declareth, yea, there is none that heareth your words.
Isaiah 48:5 I have even from the beginning declared it to thee; before it came to pass I shewed it thee: lest thou shouldest say, Mine idol hath done them, and my graven image, and my molten image, hath commanded them.
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> lsum(Genesis[1:1])
<18>:1: TypeError: unhashable type: 'Sel'
/home/pi/python/parle/func.py:12: TypeError: unhashable type: 'Sel'
  __call__(self=Func(<functools._lru_cache_wrapper object at 0xb63c3d...
    args=(Genesis 1:1 In the beginning God created the heaven and the...
    kwargs={}
>>> 
>>> base(22,103)
[4, 15]
>>> tell(lsum,osum,ssum,'Or in the base of the code in the book?')
 Or in the base of the code in the book?   =
 2   2  3    4   2  3    4   2  3    4    29
 33 23  33  27  21  33  27  23  33   43   296
150 59 213  108 66 213  72  59 213  142  1295
>>> tell(lsum,'The contents of the book are the message.')
The contents of the book are the message.  =
 3      8     2  3    4   3   3      7    33
>>> base(33,1189)
[1, 3, 1]
>>> IJohn[5:7:8]
1 John 5:7 For there are three that bear record in heaven, the Father, the Word, and the Holy Ghost: and these three are one.
1 John 5:8 And there are three that bear witness in earth, the Spirit, and the water, and the blood: and these three agree in one.
>>> tell(lsum,'the Father, the Word, and the Holy Ghost')
the Father, the Word, and the Holy Ghost  =
 3     6     3    4    3   3    4    5   31
>>> tell(lsum,'the Spirit, and the water, and the blood')
the Spirit, and the water, and the blood  =
 3     6     3   3     5    3   3    5   31
>>> base(22,3088286401)
[1, 5, 5, 5, 8, 9, 0, 13]
>>> base(23,3088286401)
[20, 19, 18, 19, 18, 11, 18]
>>> sum(_)
123
>>> tell('rib')
 r i b  =
18 9 2 29
>>> tell('Lord Jesus')
Lord Jesus  =
 49    74  123
>>> base(33,1189)
[1, 3, 1]
>>> int('1555890d',base=22)
3088286401
>>> chr(55)
'7'
>>> 
>>> bible.vi(15558)
Psalms 103:8 The LORD is merciful and gracious, slow to anger, and plenteous in mercy.
>>> 3088286401/3
1029428800.3333334
>>> int(_)
1029428800
>>> p(base(22,_))
[9, 1, 16, 10, 3, 0, 4]
>>> p(base(23,_))
[6, 21, 21, 14, 6, 3, 21]
>>> int('6JJD63J',23)
1015984263
>>> int('KJIJIBI',23)
3088286401
>>> int('1555890d',22) 
3088286401
>>> int('15558',22) 
290034
>>> p(Psalm[103])
Psalms 103
1 Bless the LORD, O my soul: and all that is within me, bless his holy name.
2 Bless the LORD, O my soul, and forget not all his benefits:
3 Who forgiveth all thine iniquities; who healeth all thy diseases;
4 Who redeemeth thy life from destruction; who crowneth thee with lovingkindness and tender mercies;
5 Who satisfieth thy mouth with good things; so that thy youth is renewed like the eagle's.
6 The LORD executeth righteousness and judgment for all that are oppressed.
7 He made known his ways unto Moses, his acts unto the children of Israel.
8 The LORD is merciful and gracious, slow to anger, and plenteous in mercy.
9 He will not always chide: neither will he keep his anger for ever.
10 He hath not dealt with us after our sins; nor rewarded us according to our iniquities.
11 For as the heaven is high above the earth, so great is his mercy toward them that fear him.
12 As far as the east is from the west, so far hath he removed our transgressions from us.
13 Like as a father pitieth his children, so the LORD pitieth them that fear him.
14 For he knoweth our frame; he remembereth that we are dust.
15 As for man, his days are as grass: as a flower of the field, so he flourisheth.
16 For the wind passeth over it, and it is gone; and the place thereof shall know it no more.
17 But the mercy of the LORD is from everlasting to everlasting upon them that fear him, and his righteousness unto children's children;
18 To such as keep his covenant, and to those that remember his commandments to do them.
19 The LORD hath prepared his throne in the heavens; and his kingdom ruleth over all.
20 Bless the LORD, ye his angels, that excel in strength, that do his commandments, hearkening unto the voice of his word.
21 Bless ye the LORD, all ye his hosts; ye ministers of his, that do his pleasure.
22 Bless the LORD, all his works in all places of his dominion: bless the LORD, O my soul.
>>> tell(lsum,osum,ssum,'Bless bless who who who the he the he he for as like for as for but to the bless bless bless')
Bless bless who who who the he the he he for  as like for  as for but  to the bless bless bless   =
  5     5    3   3   3   3   2  3   2  2  3   2    4   3   2   3   3   2   3    5     5     5    71
  57    57   46  46  46  33 13  33 13 13  39  20  37   39  20  39  43  35  33   57    57    57   833
 237   237  568 568 568 213 13 213 13 13 156 101  64  156 101 156 502 260 213  237   237   237  5063
>>> b.v(15558)
'The LORD is merciful and gracious, slow to anger, and plenteous in mercy.'
>>> 
>>> int('15551189',22)
3088208165
>>> pf(15558)
Counter({2: 1, 3: 1, 2593: 1})
>>> int('15558',22)
290034
>>> pf(_)
Counter({3: 3, 2: 1, 41: 1, 131: 1})
>>> pf(3088286401)
Counter({6619: 1, 466579: 1})
>>> pf(_)
<54>:1: TypeError: 'Counter' object cannot be interpreted as an integer
/home/pi/python/parle/func.py:12: TypeError: 'Counter' object cannot be interpreted as an integer
  __call__(self=Func(<function pf at 0xb631d6a8>))
    args=(Counter({6619: 1, 466579: 1}),)
    kwargs={}
/home/pi/python/parle/primes.py:83: TypeError: 'Counter' object cannot be interpreted as an integer
  pf(n=Counter({6619: 1, 466579: 1}))
/home/pi/python/parle/func.py:12: TypeError: 'Counter' object cannot be interpreted as an integer
  __call__(self=Func(<function factors at 0xb631d660>))
    args=(Counter({6619: 1, 466579: 1}),)
    kwargs={}
/home/pi/python/parle/primes.py:69: TypeError: 'Counter' object cannot be interpreted as an integer
  factors(n=Counter({6619: 1, 466579: 1}))
    fs=[]
    k=Counter({6619: 1, 466579: 1})
    p=Func(<built-in function print>)
>>> [b.name() for b in b.books()]
['Genesis', 'Exodus', 'Leviticus', 'Numbers', 'Deuteronomy', 'Joshua', 'Judges', 'Ruth', '1 Samuel', '2 Samuel', '1 Kings', '2 Kings', '1 Chronicles', '2 Chronicles', 'Ezra', 'Nehemiah', 'Esther', 'Job', 'Psalms', 'Proverbs', 'Ecclesiastes', 'Song of Solomon', 'Isaiah', 'Jeremiah', 'Lamentations', 'Ezekiel', 'Daniel', 'Hosea', 'Joel', 'Amos', 'Obadiah', 'Jonah', 'Micah', 'Nahum', 'Habakkuk', 'Zephaniah', 'Haggai', 'Zechariah', 'Malachi', 'Matthew', 'Mark', 'Luke', 'John', 'Acts', 'Romans', '1 Corinthians', '2 Corinthians', 'Galatians', 'Ephesians', 'Philippians', 'Colossians', '1 Thessalonians', '2 Thessalonians', '1 Timothy', '2 Timothy', 'Titus', 'Philemon', 'Hebrews', 'James', '1 Peter', '2 Peter', '1 John', '2 John', '3 John', 'Jude', 'Revelation']
>>> ' '.join(_)
'Genesis Exodus Leviticus Numbers Deuteronomy Joshua Judges Ruth 1 Samuel 2 Samuel 1 Kings 2 Kings 1 Chronicles 2 Chronicles Ezra Nehemiah Esther Job Psalms Proverbs Ecclesiastes Song of Solomon Isaiah Jeremiah Lamentations Ezekiel Daniel Hosea Joel Amos Obadiah Jonah Micah Nahum Habakkuk Zephaniah Haggai Zechariah Malachi Matthew Mark Luke John Acts Romans 1 Corinthians 2 Corinthians Galatians Ephesians Philippians Colossians 1 Thessalonians 2 Thessalonians 1 Timothy 2 Timothy Titus Philemon Hebrews James 1 Peter 2 Peter 1 John 2 John 3 John Jude Revelation'
>>> tell(lsum,_)
Genesis Exodus Leviticus Numbers Deuteronomy Joshua Judges Ruth 1 Samuel 2 Samuel 1 Kings 2 Kings 1 Chronicles 2 Chronicles Ezra Nehemiah Esther Job Psalms Proverbs Ecclesiastes Song of Solomon Isaiah Jeremiah Lamentations Ezekiel Daniel Hosea Joel Amos Obadiah Jonah Micah Nahum Habakkuk Zephaniah Haggai Zechariah Malachi Matthew Mark Luke John Acts Romans 1 Corinthians 2 Corinthians Galatians Ephesians Philippians Colossians 1 Thessalonians 2 Thessalonians 1 Timothy 2 Timothy Titus Philemon Hebrews James 1 Peter 2 Peter 1 John 2 John 3 John Jude Revelation  =
   7       6       9        7         11        6      6     4  1    6   1    6   1   5   1   5   1     10     1     10       4      8       6    3     6       8         12        4   2    7       6       8         12         7       6     5     4    4     7      5     5     5       8        9        6       9        7       7      4    4    4    4     6   1      11     1      11         9         9          11         10     1       13      1       13      1    7    1    7      5       8       7      5   1   5   1   5   1   4  1   4  1   4    4      10     479
>>> nF(51)
(9, 34, -17, 51, 4, 55, 10)
>>> nF(55)
10
>>> nF(15)
(7, 13, -2, 15, 6, 21, 8)
>>> nF(1)
2
>>> nF(5)
5
>>> nF(55)
10
>>> nF(89)
11
>>> int('90d',base=22)
4369
>>> 
>>> int('90d',base=17)
2614
>>> nF(_)
(18, 2584, -30, 2614, 1567, 4181, 19)
>>> [int('90d',b) for b in span(14,33)]
[1777, 2038, 2317, 2614, 2929, 3262, 3613, 3982, 4369, 4774, 5197, 5638, 6097, 6574, 7069, 7582, 8113, 8662, 9229, 9814]
>>> hex(2317)
'0x90d'
>>> chr(2317)
'ऍ'
>>> 
>>> 3088286401/299792.458
10301.414590623224
>>> math.sqrt(_)
101.49588459944188
>>> 588+599
1187
>>> b.chapter(588)
Psalms 110:1-7 (7 verses)
>>> b.chapter(599)
Psalms 121:1-8 (8 verses)
>>> sof(3088)
6014
>>> sof(3088286401)
3088759600
>>> p(bin(_))
0b10111000000110101011101100110000
>>> p(base(22,_))
[1, 5, 5, 7, 8, 18, 15, 14]
>>> p(base(23,_))
[20, 19, 20, 12, 16, 0, 15]
>>> pf(_)
Counter({2: 4, 5: 2, 41: 1, 331: 1, 569: 1})
>>> pf(3088)
Counter({2: 4, 193: 1})
>>> math.sqrt(3088)
55.569775957799216
>>> 6**3
216
>>> math.sqrt(3088286401)
55572.35284743665
>>> 
>>> 
>>> from dna import *
>>> AV,W,X,Y
(2875001522, 16569, 156040895, 57227415)
>>> AV+W+X
3031058986
>>> math.sqrt(_)
55055.054136745704
>>> _,_*math.sqrt(2)
(55055.054136745704, 77859.60423737075)
>>> AV+W+X+Y
3088286401
>>> math.sqrt(_), _/299792.458
(55572.35284743665, 10301.414590623224)
>>> b.Deut[5].wc()
921
>>> pf(921)
Counter({3: 1, 307: 1})
>>> pn(pn(21))
367
>>> pn(74)
373
>>> 
>>> 
>>> 
>>> Deuteronomy[5].vn()
5055
>>> b.vi(30882)
Revelation 11:9 And they of the people and kindreds and tongues and nations shall see their dead bodies three days and an half, and shall not suffer their dead bodies to be put in graves.
>>> ns(30882)
[2, 3, 5147] [1, 2, 686]
>>> 3*5147
15441
>>> b.vi(15441,15442)
Psalms 94:9 He that planted the ear, shall he not hear? he that formed the eye, shall he not see?
Psalms 94:10 He that chastiseth the heathen, shall not he correct? he that teacheth man knowledge, shall not he know?
>>> b.ch(309)
1 Kings 18:1-46 (46 verses)
>>> p(_)
1 Kings 18
1 And it came to pass after many days, that the word of the LORD came to Elijah in the third year, saying, Go, shew thyself unto Ahab; and I will send rain upon the earth.
2 And Elijah went to shew himself unto Ahab. And there was a sore famine in Samaria.
3 And Ahab called Obadiah, which was the governor of his house. (Now Obadiah feared the LORD greatly:
4 For it was so, when Jezebel cut off the prophets of the LORD, that Obadiah took an hundred prophets, and hid them by fifty in a cave, and fed them with bread and water.)
5 And Ahab said unto Obadiah, Go into the land, unto all fountains of water, and unto all brooks: peradventure we may find grass to save the horses and mules alive, that we lose not all the beasts.
6 So they divided the land between them to pass throughout it: Ahab went one way by himself, and Obadiah went another way by himself.
7 And as Obadiah was in the way, behold, Elijah met him: and he knew him, and fell on his face, and said, Art thou that my lord Elijah?
8 And he answered him, I am: go, tell thy lord, Behold, Elijah is here.
9 And he said, What have I sinned, that thou wouldest deliver thy servant into the hand of Ahab, to slay me?
10 As the LORD thy God liveth, there is no nation or kingdom, whither my lord hath not sent to seek thee: and when they said, He is not there; he took an oath of the kingdom and nation, that they found thee not.
11 And now thou sayest, Go, tell thy lord, Behold, Elijah is here.
12 And it shall come to pass, as soon as I am gone from thee, that the Spirit of the LORD shall carry thee whither I know not; and so when I come and tell Ahab, and he cannot find thee, he shall slay me: but I thy servant fear the LORD from my youth.
13 Was it not told my lord what I did when Jezebel slew the prophets of the LORD, how I hid an hundred men of the LORD's prophets by fifty in a cave, and fed them with bread and water?
14 And now thou sayest, Go, tell thy lord, Behold, Elijah is here: and he shall slay me.
15 And Elijah said, As the LORD of hosts liveth, before whom I stand, I will surely shew myself unto him to day.
16 So Obadiah went to meet Ahab, and told him: and Ahab went to meet Elijah.
17 And it came to pass, when Ahab saw Elijah, that Ahab said unto him, Art thou he that troubleth Israel?
18 And he answered, I have not troubled Israel; but thou, and thy father's house, in that ye have forsaken the commandments of the LORD, and thou hast followed Baalim.
19 Now therefore send, and gather to me all Israel unto mount Carmel, and the prophets of Baal four hundred and fifty, and the prophets of the groves four hundred, which eat at Jezebel's table.
20 So Ahab sent unto all the children of Israel, and gathered the prophets together unto mount Carmel.
21 And Elijah came unto all the people, and said, How long halt ye between two opinions? if the LORD be God, follow him: but if Baal, then follow him. And the people answered him not a word.
22 Then said Elijah unto the people, I, even I only, remain a prophet of the LORD; but Baal's prophets are four hundred and fifty men.
23 Let them therefore give us two bullocks; and let them choose one bullock for themselves, and cut it in pieces, and lay it on wood, and put no fire under: and I will dress the other bullock, and lay it on wood, and put no fire under:
24 And call ye on the name of your gods, and I will call on the name of the LORD: and the God that answereth by fire, let him be God. And all the people answered and said, It is well spoken.
25 And Elijah said unto the prophets of Baal, Choose you one bullock for yourselves, and dress it first; for ye are many; and call on the name of your gods, but put no fire under.
26 And they took the bullock which was given them, and they dressed it, and called on the name of Baal from morning even until noon, saying, O Baal, hear us. But there was no voice, nor any that answered. And they leaped upon the altar which was made.
27 And it came to pass at noon, that Elijah mocked them, and said, Cry aloud: for he is a god; either he is talking, or he is pursuing, or he is in a journey, or peradventure he sleepeth, and must be awaked.
28 And they cried aloud, and cut themselves after their manner with knives and lancets, till the blood gushed out upon them.
29 And it came to pass, when midday was past, and they prophesied until the time of the offering of the evening sacrifice, that there was neither voice, nor any to answer, nor any that regarded.
30 And Elijah said unto all the people, Come near unto me. And all the people came near unto him. And he repaired the altar of the LORD that was broken down.
31 And Elijah took twelve stones, according to the number of the tribes of the sons of Jacob, unto whom the word of the LORD came, saying, Israel shall be thy name:
32 And with the stones he built an altar in the name of the LORD: and he made a trench about the altar, as great as would contain two measures of seed.
33 And he put the wood in order, and cut the bullock in pieces, and laid him on the wood, and said, Fill four barrels with water, and pour it on the burnt sacrifice, and on the wood.
34 And he said, Do it the second time. And they did it the second time. And he said, Do it the third time. And they did it the third time.
35 And the water ran round about the altar; and he filled the trench also with water.
36 And it came to pass at the time of the offering of the evening sacrifice, that Elijah the prophet came near, and said, LORD God of Abraham, Isaac, and of Israel, let it be known this day that thou art God in Israel, and that I am thy servant, and that I have done all these things at thy word.
37 Hear me, O LORD, hear me, that this people may know that thou art the LORD God, and that thou hast turned their heart back again.
38 Then the fire of the LORD fell, and consumed the burnt sacrifice, and the wood, and the stones, and the dust, and licked up the water that was in the trench.
39 And when all the people saw it, they fell on their faces: and they said, The LORD, he is the God; the LORD, he is the God.
40 And Elijah said unto them, Take the prophets of Baal; let not one of them escape. And they took them: and Elijah brought them down to the brook Kishon, and slew them there.
41 And Elijah said unto Ahab, Get thee up, eat and drink; for there is a sound of abundance of rain.
42 So Ahab went up to eat and to drink. And Elijah went up to the top of Carmel; and he cast himself down upon the earth, and put his face between his knees,
43 And said to his servant, Go up now, look toward the sea. And he went up, and looked, and said, There is nothing. And he said, Go again seven times.
44 And it came to pass at the seventh time, that he said, Behold, there ariseth a little cloud out of the sea, like a man's hand. And he said, Go up, say unto Ahab, Prepare thy chariot, and get thee down that the rain stop thee not.
45 And it came to pass in the mean while, that the heaven was black with clouds and wind, and there was a great rain. And Ahab rode, and went to Jezreel.
46 And the hand of the LORD was on Elijah; and he girded up his loins, and ran before Ahab to the entrance of Jezreel.
>>> 
>>> 
>>> pf(55055)
Counter({11: 2, 5: 1, 7: 1, 13: 1})
>>> factors(55055)
[5, 7, 11, 11, 13]
>>> sof(55055)
89376
>>> 55055
55055
>>> b.book(55)
2 Timothy 1:1-4:22 (83 verses)
>>> b.book(15)
Ezra 1:1-10:44 (280 verses)
>>> b.book(51)
Colossians 1:1-4:18 (95 verses)
>>> tell('the spirit')
the spirit
 33+  91  =124
>>> 
