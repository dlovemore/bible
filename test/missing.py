# >>> from bible import *
# >>> Matthew[17:21]+Matthew[18:11]+Matthew[23:14]+Mark[7:16]+Mark[9:44]+Mark[9:46]+Mark[15:28]+Mark[16:9:20]+Luke[17:36]+Luke[23:17]+John[5:3:4]+(John[7:53]-John[8:11])+Acts[8:37]+Acts[15:34]+Acts[24:7]+Acts[28:29]+Romans[16:24]
# Matthew 17:21;18:11;23:14;Mark 7:16;9:44,46;15:28;16:9-20;Luke 17:36;23:17;John 5:3-4;7:53-8:11;Acts 8:37;15:34;24:7;28:29;Romans 16:24 (40 verses)
# >>> p(_)
# Matthew 17:21 Howbeit this kind goeth not out but by prayer and fasting.
# Matthew 18:11 For the Son of man is come to save that which was lost.
# Matthew 23:14 Woe unto you, scribes and Pharisees, hypocrites! for ye devour widows' houses, and for a pretence make long prayer: therefore ye shall receive the greater damnation.
# Mark 7:16 If any man have ears to hear, let him hear.
# Mark 9:44 Where their worm dieth not, and the fire is not quenched.
# Mark 9:46 Where their worm dieth not, and the fire is not quenched.
# Mark 15:28 And the scripture was fulfilled, which saith, And he was numbered with the transgressors.
# Mark 16
# 9 Now when Jesus was risen early the first day of the week, he appeared first to Mary Magdalene, out of whom he had cast seven devils.
# 10 And she went and told them that had been with him, as they mourned and wept.
# 11 And they, when they had heard that he was alive, and had been seen of her, believed not.
# 12 After that he appeared in another form unto two of them, as they walked, and went into the country.
# 13 And they went and told it unto the residue: neither believed they them.
# 14 Afterward he appeared unto the eleven as they sat at meat, and upbraided them with their unbelief and hardness of heart, because they believed not them which had seen him after he was risen.
# 15 And he said unto them, Go ye into all the world, and preach the gospel to every creature.
# 16 He that believeth and is baptized shall be saved; but he that believeth not shall be damned.
# 17 And these signs shall follow them that believe; In my name shall they cast out devils; they shall speak with new tongues;
# 18 They shall take up serpents; and if they drink any deadly thing, it shall not hurt them; they shall lay hands on the sick, and they shall recover.
# 19 So then after the Lord had spoken unto them, he was received up into heaven, and sat on the right hand of God.
# 20 And they went forth, and preached every where, the Lord working with them, and confirming the word with signs following. Amen.
# Luke 17:36 Two men shall be in the field; the one shall be taken, and the other left.
# Luke 23:17 (For of necessity he must release one unto them at the feast.)
# John 5:3 In these lay a great multitude of impotent folk, of blind, halt, withered, waiting for the moving of the water.
# John 5:4 For an angel went down at a certain season into the pool, and troubled the water: whosoever then first after the troubling of the water stepped in was made whole of whatsoever disease he had.
# John 7:53 And every man went unto his own house.
# John 8
# 1 Jesus went unto the mount of Olives.
# 2 And early in the morning he came again into the temple, and all the people came unto him; and he sat down, and taught them.
# 3 And the scribes and Pharisees brought unto him a woman taken in adultery; and when they had set her in the midst,
# 4 They say unto him, Master, this woman was taken in adultery, in the very act.
# 5 Now Moses in the law commanded us, that such should be stoned: but what sayest thou?
# 6 This they said, tempting him, that they might have to accuse him. But Jesus stooped down, and with his finger wrote on the ground, as though he heard them not.
# 7 So when they continued asking him, he lifted up himself, and said unto them, He that is without sin among you, let him first cast a stone at her.
# 8 And again he stooped down, and wrote on the ground.
# 9 And they which heard it, being convicted by their own conscience, went out one by one, beginning at the eldest, even unto the last: and Jesus was left alone, and the woman standing in the midst.
# 10 When Jesus had lifted up himself, and saw none but the woman, he said unto her, Woman, where are those thine accusers? hath no man condemned thee?
# 11 She said, No man, Lord. And Jesus said unto her, Neither do I condemn thee: go, and sin no more.
# Acts 8:37 And Philip said, If thou believest with all thine heart, thou mayest. And he answered and said, I believe that Jesus Christ is the Son of God.
# Acts 15:34 Notwithstanding it pleased Silas to abide there still.
# Acts 24:7 But the chief captain Lysias came upon us, and with great violence took him away out of our hands,
# Acts 28:29 And when he had said these words, the Jews departed, and had great reasoning among themselves.
# Romans 16:24 The grace of our Lord Jesus Christ be with you all. Amen.
# >>> 


