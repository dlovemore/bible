>>> from bible import *
>>> ws=b.words()@meth.strip(",.;:?'-()!")*L*F(set)
>>> len(ws)
13758
>>> pws=[w for w in ws if w.lower()==w.lower()[::-1]]
>>> len(pws)
30
>>> pws
['peep', 'A', 'Did', 'ere', 'Abba', 'Asa', 'Eye', 'Aziza', 'deed', 'Halah', 'Eve', 'Ara', 'Aha', 'noon', 'Gog', 'Ziz', 'I', 'ewe', 'Ono', 'aha', 'Non', 'Anna', 'Iri', 'eye', 'a', 'Hannah', 'O', 'Ava', 'Nun', 'did']
>>> pws@(I+F(sums))
['peep' (4, 42, 150), 'A' (1, 1, 1), 'Did' (3, 17, 17), 'ere' (3, 28, 100), 'Abba' (4, 6, 6), 'Asa' (3, 21, 102), 'Eye' (3, 35, 710), 'Aziza' (5, 63, 1611), 'deed' (4, 18, 18), 'Halah' (5, 30, 48), 'Eve' (3, 32, 410), 'Ara' (3, 20, 92), 'Aha' (3, 10, 10), 'noon' (4, 58, 220), 'Gog' (3, 29, 74), 'Ziz' (3, 61, 1609), 'I' (1, 9, 9), 'ewe' (3, 33, 510), 'Ono' (3, 44, 170), 'aha' (3, 10, 10), 'Non' (3, 43, 160), 'Anna' (4, 30, 102), 'Iri' (3, 36, 108), 'eye' (3, 35, 710), 'a' (1, 1, 1), 'Hannah' (6, 46, 118), 'O' (1, 15, 60), 'Ava' (3, 24, 402), 'Nun' (3, 49, 400), 'did' (3, 17, 17)]
>>> pws@method.lower*L*F(set)*F(len)
26
>>> 
>>> 
>>> 
