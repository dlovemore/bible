>>> from bible import *
>>> b/'perfect'/'generations'
Genesis 6:9 These are the generations of Noah: Noah was a just man and perfect in his generations, and Noah walked with God.
>>> b/'perfect'
Genesis 6:9;17:1;Leviticus 22:21;Deuteronomy 18:13;25:15;32:4;1 Samuel 14:41;2 Samuel 22:31,33;1 Kings 8:61;11:4;15:3,14;2 Kings 20:3;1 Chronicles 12:38;28:9;29:9,19;2 Chronicles 4:21;8:16;15:17;16:9;19:9;24:13;25:2;Ezra 7:12;Job 1:1,8;2:3;8:20;9:20-22;11:7;15:29;22:3;28:3;36:4;37:16;Psalms 18:30,32;19:7;37:37;50:2;64:4;101:2,6;119:96;138:8;139:22;Proverbs 2:21;4:18;11:5;Isaiah 18:5;26:3;38:3;42:19;47:9;Jeremiah 23:20;Lamentations 2:15;Ezekiel 16:14;27:3-4,11;28:12,15;Matthew 5:48;14:36;19:21;21:16;Luke 1:3;6:40;8:14;13:32;John 17:23;Acts 3:16;18:26;22:3;23:15,20;24:22;Romans 12:2;1 Corinthians 1:10;2:6;13:10;2 Corinthians 7:1;12:9;13:9,11;Galatians 3:3;Ephesians 4:12-13;Philippians 3:12,15;Colossians 1:28;3:14;4:12;1 Thessalonians 3:10;5:2;2 Timothy 3:17;Hebrews 2:10;5:9;6:1;7:11,19;9:9,11;10:1,14;11:40;12:23;13:21;James 1:4,17,25;2:22;3:2;1 Peter 5:10;1 John 2:5;4:12,17-18;Revelation 3:2 (123 verses)
>>> allfacors(496)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'allfacors' is not defined
>>> allfactors(496)
[1, 2, 4, 8, 16, 31, 62, 124, 248, 496]
>>> fs(496)
[2, 2, 2, 2, 31]
>>> tri(496)
123256
>>> b.ws()[123250:123260]
['a', 'thousand', 'throughout', 'all', 'the', 'tribes', 'of', 'Israel', 'shall', 'ye']
>>> b.ws()[123250:123262]
['a', 'thousand', 'throughout', 'all', 'the', 'tribes', 'of', 'Israel', 'shall', 'ye', 'send', 'to']
>>> b.ws()[123240:123272]
['Midianites', 'and', 'avenge', 'the', 'LORD', 'of', 'Midian', 'Of', 'every', 'tribe', 'a', 'thousand', 'throughout', 'all', 'the', 'tribes', 'of', 'Israel', 'shall', 'ye', 'send', 'to', 'the', 'war', 'So', 'there', 'were', 'delivered', 'out', 'of', 'the', 'thousands']
>>> b/'avenge the LORD of Midian'
Numbers 31:3 And Moses spake unto the people, saying, Arm some of yourselves unto the war, and let them go against the Midianites, and avenge the LORD of Midian.
>>> np(197)
45
>>> b.vi(8128)
2 Samuel 4:7 For when they came into the house, he lay on his bed in his bedchamber, and they smote him, and slew him, and beheaded him, and took his head, and gat them away through the plain all night.
>>> b/'plain'
Genesis 11:2;12:6;13:10-12,18;14:13;18:1;19:17,25,28-29;25:27;Exodus 21:5;Numbers 22:1;26:3,63;31:12;33:48-50;35:1;36:13-Deuteronomy 1:1,7;2:8;3:10,17;4:43,49;11:30;27:8;34:1,3,8;Joshua 3:16;4:13;5:10;8:14;11:2,16;12:1,3,8;13:9,16-17,21,32;20:8;Judges 4:11;9:6,37;11:33;1 Samuel 2:27;10:3,16;23:24;2 Samuel 2:29;4:7;15:28;17:16;18:23;1 Kings 7:46;20:23,25;2 Kings 14:25;25:4-5;1 Chronicles 27:28;2 Chronicles 4:17;9:27;26:10;Ezra 4:18;Nehemiah 3:22;6:2;12:28;Psalms 27:11;Proverbs 8:9;15:19;Isaiah 28:25;32:4;40:4;Jeremiah 17:26;21:13;39:4-5;48:8,21;52:7-8;Ezekiel 3:22-23;8:4;Daniel 3:1;Amos 1:5;Obadiah 1:19;Habakkuk 2:2;Zechariah 4:7;7:7;14:10;Mark 7:35;Luke 6:17;John 10:24;11:14;16:25,29;2 Corinthians 3:12;Hebrews 11:14 (109 verses)
>>> b.vi(8128).tells()
For when they came into the house, he lay  on his bed in his bedchamber, and they smote him, and slew him, and beheaded him, and took his head, and gat them away through the plain all night.   =
 3    4    4    4    4   3     5    2  3   2   3   3   2  3       10      3    4    5     3   3    4    3   3      8      3   3    4   3    4    3   3    4    4     7     3    5    3     5    145
 39  50   58   22   58   33   68   13  38  29  36  11 23  36      61      19  58    72   30   19  59   30   19    34     30   19  61   36   18   19  28  46   50     97    33   52   25   58   1487
156  563  913  49   319 213   473  13 731 110 117  11 59 117     160      55  913  405   57   55  635  57   55    34     57   55  340 117   18   55 208  253 1202   673   213  160   61   274  9956
>>> allfactors(8128)
[1, 2, 4, 8, 16, 32, 64, 127, 254, 508, 1016, 2032, 4064, 8128]
>>> fs(8128)
[2, 2, 2, 2, 2, 2, 127]
>>> tri(172)
14878
>>> tri(127)
8128
>>> tri(31)
496
>>> tri(7)
28
>>> tri(3)
6
>>> b.vi(127)
Genesis 5:21 And Enoch lived sixty and five years, and begat Methuselah:
>>> b/698

>>> b/689

>>> b/'Methuselah'
Genesis 5:21-22,25-27;1 Chronicles 1:3 (6 verses)
>>> _.vns()
[127, 128, 131, 132, 133, 10256]
>>> b.vi(128)
Genesis 5:22 And Enoch walked with God after he begat Methuselah three hundred years, and begat sons and daughters:
>>> b.vi(133)
Genesis 5:27 And all the days of Methuselah were nine hundred sixty and nine years: and he died.
>>> 969*ns
[3, 17, 19] [2, 7, 8]
>>> 19*3
57
>>> 17*3
51
>>> IChronicles[1:3]
1 Chronicles 1:3 Henoch, Methuselah, Lamech,
>>> tri(31)
496
>>> sum(span(31)
... )
496
>>> sum(span(6))
21
>>> b/'very good'
Genesis 1:31;Judges 18:9;1 Samuel 19:4;25:15;1 Kings 1:6;Jeremiah 24:2-3 (7 verses)
>>> 
