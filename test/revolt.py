    >>> "rist rest"*words@tells
     r i  s   t   =
     1 1  1   1   4
    18 9  19  20  66
    90 9 100 200 399
     r e  s   t   =
     1 1  1   1   4
    18 5  19  20  62
    90 5 100 200 395
    [None, None]
    >>> tell('Ch ri st')
    Ch ri st  =
    11 27 39 77
    >>> tell('Chrest')
    C h  r e  s  t  =
    3 8 18 5 19 20 73
    >>> tell('number')
     n  u  m b e  r  =
    14 21 13 2 5 18 73
    >>> tell('Jesus')
     J e  s  u  s  =
    10 5 19 21 19 74
    >>> tell('Jesus Chrest')
    Jesus Chrest  =
      74    73   147
    >>> 3*7*7
    147
    >>> tell('Lord')
     L  o  r d  =
    12 15 18 4 49
    >>> tells('Lord Jesus Christ')
    Lord Jesus Christ   =
      4    5      6    15
     49    74    77    200
     184  515    410  1109
    >>> tell('Lord Jesus Chrest')
    Lord Jesus Chrest  =
     49    74    73   196
    >>> 7*7+7*7+7*7+7*7
    196
    >>> 2*7*7*2
    196
    >>> tell("Lord Jesus Chrest",ssum)
    Lord Jesus Chrest   =
     184  515    406  1105
    >>> Revelation[11:5]
    Revelation 11:5 And if any man will hurt them, fire proceedeth out of their mouth, and devoureth their enemies: and if any man will hurt them, he must in this manner be killed.
    >>> (_-Revelation[22:21]).vc()
    225
    >>> 15**2
    225
    >>> b/'tell no man'
    Matthew 8:4;16:20;Mark 7:36;8:30;9:9;Luke 5:14;8:56;9:21;Acts 23:22 (9 verses)
    >>> Matthew[16:20]
    Matthew 16:20 Then charged he his disciples that they should tell no man that he was Jesus the Christ.
    >>> tells('Lord Jesus Christ')
    Lord Jesus Christ   =
      4    5      6    15
     49    74    77    200
     184  515    410  1109
    >>> Revelation[11:9]
    Revelation 11:9 And they of the people and kindreds and tongues and nations shall see their dead bodies three days and an half, and shall not suffer their dead bodies to be put in graves.
    >>> (_-Revelation[22:21]).vc()
    221
    >>> tells("David Love More")
    David Love More   =
      5     4    4   13
      40   54   51   145
     418   495  195 1108
    >>> Revelation[11:8]
    Revelation 11:8 And their dead bodies shall lie in the street of the great city, which spiritually is called Sodom and Egypt, where also our Lord was crucified.
    >>> 
    >>> (_-Revelation[22:21]).vc()

Let these sink in:
    >>> b/'sink in'
    Psalms 69:2 I sink in deep mire, where there is no standing: I am come into deep waters, where the floods overflow me.
    >>> ssum('God')
    71
    >>> count('el') # from Hebrew אל, Bethel house of god, Eli my god
    17
    >>> count('Ch')
    11
    >>> count('Christ')
    77
    >>> tell('Ch ri st')
    Ch ri st  =
    11 27 39 77
    >>> b/'give you rest'
    Matthew 11:28 Come unto me, all ye that labour and are heavy laden, and I will give you rest.
    >>> b/'This is the work of God'*p
    John 6:29 Jesus answered and said unto them, This is the work of God, that ye believe on him whom he hath sent.
    >>> b/'Seek ye first'
    Matthew 6:33 But seek ye first the kingdom of God, and his righteousness; and all these things shall be added unto you.
    >>> 

It was the Romans that crowned Jesus with a crown of thorns.
    >>> b/'thorn'/'head'*p
    Matthew 27:29 And when they had platted a crown of thorns, they put it upon his head, and a reed in his right hand: and they bowed the knee before him, and mocked him, saying, Hail, King of the Jews!
    Mark 15:17 And they clothed him with purple, and platted a crown of thorns, and put it about his head,
    John 19:2 And the soldiers platted a crown of thorns, and put it on his head, and they put on him a purple robe,
    >>> Numbers[14]
    Numbers 14:1-45 (45 verses)
    >>> b/'faith cometh by hearing'
    Romans 10:17 So then faith cometh by hearing, and hearing by the word of God.

As a prefix to the next part, A thorn in English is the name for a letter that used to look like a y and pronounced th. There used to be old pub signs saying Ye Olde, but is that true? In fact I thought it looked more like a greek psi, Ψwhich is 25 700, and 1 before ω Ω=26 800.

    >>> b/'crown of thorns'*p
    Matthew 27:29 And when they had platted a crown of thorns, they put it upon his head, and a reed in his right hand: and they bowed the knee before him, and mocked him, saying, Hail, King of the Jews!
    Mark 15:17 And they clothed him with purple, and platted a crown of thorns, and put it about his head,
    John 19:2 And the soldiers platted a crown of thorns, and put it on his head, and they put on him a purple robe,
    John 19:5 Then came Jesus forth, wearing the crown of thorns, and the purple robe. And Pilate saith unto them, Behold the man!
    >>> John[19:1:6]*p
    John 19
    1 Then Pilate therefore took Jesus, and scourged him.
    2 And the soldiers platted a crown of thorns, and put it on his head, and they put on him a purple robe,
    3 And said, Hail, King of the Jews! and they smote him with their hands.
    4 Pilate therefore went forth again, and saith unto them, Behold, I bring him forth to you, that ye may know that I find no fault in him.
    5 Then came Jesus forth, wearing the crown of thorns, and the purple robe. And Pilate saith unto them, Behold the man!
    6 When the chief priests therefore and officers saw him, they cried out, saying, Crucify him, crucify him. Pilate saith unto them, Take ye him, and crucify him: for I find no fault in him.
    >>> b/'manner'/'king'
    Judges 8:18;11:17;1 Samuel 8:9,11;10:25;2 Samuel 14:3;15:6;2 Kings 11:14;17:26-27;1 Chronicles 18:10;2 Chronicles 18:19;32:15;Esther 1:13;2:12;Daniel 6:23;Matthew 4:23 (17 verses)
    >>> _*p
    Judges 8:18 Then said he unto Zebah and Zalmunna, What manner of men were they whom ye slew at Tabor? And they answered, As thou art, so were they; each one resembled the children of a king.
    Judges 11:17 Then Israel sent messengers unto the king of Edom, saying, Let me, I pray thee, pass through thy land: but the king of Edom would not hearken thereto. And in like manner they sent unto the king of Moab: but he would not consent: and Israel abode in Kadesh.
    1 Samuel 8:9 Now therefore hearken unto their voice: howbeit yet protest solemnly unto them, and shew them the manner of the king that shall reign over them.
    1 Samuel 8:11 And he said, This will be the manner of the king that shall reign over you: He will take your sons, and appoint them for himself, for his chariots, and to be his horsemen; and some shall run before his chariots.
    1 Samuel 10:25 Then Samuel told the people the manner of the kingdom, and wrote it in a book, and laid it up before the LORD. And Samuel sent all the people away, every man to his house.
    2 Samuel 14:3 And come to the king, and speak on this manner unto him. So Joab put the words in her mouth.
    2 Samuel 15:6 And on this manner did Absalom to all Israel that came to the king for judgment: so Absalom stole the hearts of the men of Israel.
    2 Kings 11:14 And when she looked, behold, the king stood by a pillar, as the manner was, and the princes and the trumpeters by the king, and all the people of the land rejoiced, and blew with trumpets: and Athaliah rent her clothes, and cried, Treason, Treason.
    2 Kings 17:26 Wherefore they spake to the king of Assyria, saying, The nations which thou hast removed, and placed in the cities of Samaria, know not the manner of the God of the land: therefore he hath sent lions among them, and, behold, they slay them, because they know not the manner of the God of the land.
    2 Kings 17:27 Then the king of Assyria commanded, saying, Carry thither one of the priests whom ye brought from thence; and let them go and dwell there, and let him teach them the manner of the God of the land.
    1 Chronicles 18:10 He sent Hadoram his son to king David, to enquire of his welfare, and to congratulate him, because he had fought against Hadarezer, and smitten him; (for Hadarezer had war with Tou;) and with him all manner of vessels of gold and silver and brass.
    2 Chronicles 18:19 And the LORD said, Who shall entice Ahab king of Israel, that he may go up and fall at Ramothgilead? And one spake saying after this manner, and another saying after that manner.
    2 Chronicles 32:15 Now therefore let not Hezekiah deceive you, nor persuade you on this manner, neither yet believe him: for no god of any nation or kingdom was able to deliver his people out of mine hand, and out of the hand of my fathers: how much less shall your God deliver you out of mine hand?
    Esther 1:13 Then the king said to the wise men, which knew the times, (for so was the king's manner toward all that knew law and judgment:
    Esther 2:12 Now when every maid's turn was come to go in to king Ahasuerus, after that she had been twelve months, according to the manner of the women, (for so were the days of their purifications accomplished, to wit, six months with oil of myrrh, and six months with sweet odours, and with other things for the purifying of the women;)
    Daniel 6:23 Then was the king exceedingly glad for him, and commanded that they should take Daniel up out of the den. So Daniel was taken up out of the den, and no manner of hurt was found upon him, because he believed in his God.
    Matthew 4:23 And Jesus went about all Galilee, teaching in their synagogues, and preaching the gospel of the kingdom, and healing all manner of sickness and all manner of disease among the people.
    >>> ISamuel[8:1:21]*p
    1 Samuel 8
    1 And it came to pass, when Samuel was old, that he made his sons judges over Israel.
    2 Now the name of his firstborn was Joel; and the name of his second, Abiah: they were judges in Beersheba.
    3 And his sons walked not in his ways, but turned aside after lucre, and took bribes, and perverted judgment.
    4 Then all the elders of Israel gathered themselves together, and came to Samuel unto Ramah,
    5 And said unto him, Behold, thou art old, and thy sons walk not in thy ways: now make us a king to judge us like all the nations.
    6 But the thing displeased Samuel, when they said, Give us a king to judge us. And Samuel prayed unto the LORD.
    7 And the LORD said unto Samuel, Hearken unto the voice of the people in all that they say unto thee: for they have not rejected thee, but they have rejected me, that I should not reign over them.
    8 According to all the works which they have done since the day that I brought them up out of Egypt even unto this day, wherewith they have forsaken me, and served other gods, so do they also unto thee.
    9 Now therefore hearken unto their voice: howbeit yet protest solemnly unto them, and shew them the manner of the king that shall reign over them.
    10 And Samuel told all the words of the LORD unto the people that asked of him a king.
    11 And he said, This will be the manner of the king that shall reign over you: He will take your sons, and appoint them for himself, for his chariots, and to be his horsemen; and some shall run before his chariots.
    12 And he will appoint him captains over thousands, and captains over fifties; and will set them to ear his ground, and to reap his harvest, and to make his instruments of war, and instruments of his chariots.
    13 And he will take your daughters to be confectionaries, and to be cooks, and to be bakers.
    14 And he will take your fields, and your vineyards, and your oliveyards, even the best of them, and give them to his servants.
    15 And he will take the tenth of your seed, and of your vineyards, and give to his officers, and to his servants.
    16 And he will take your menservants, and your maidservants, and your goodliest young men, and your asses, and put them to his work.
    17 He will take the tenth of your sheep: and ye shall be his servants.
    18 And ye shall cry out in that day because of your king which ye shall have chosen you; and the LORD will not hear you in that day.
    19 Nevertheless the people refused to obey the voice of Samuel; and they said, Nay; but we will have a king over us;
    20 That we also may be like all the nations; and that our king may judge us, and go out before us, and fight our battles.
    21 And Samuel heard all the words of the people, and he rehearsed them in the ears of the LORD.
    >>> ISamuel[8:6:9]*p
    1 Samuel 8
    6 But the thing displeased Samuel, when they said, Give us a king to judge us. And Samuel prayed unto the LORD.
    7 And the LORD said unto Samuel, Hearken unto the voice of the people in all that they say unto thee: for they have not rejected thee, but they have rejected me, that I should not reign over them.
    8 According to all the works which they have done since the day that I brought them up out of Egypt even unto this day, wherewith they have forsaken me, and served other gods, so do they also unto thee.
    9 Now therefore hearken unto their voice: howbeit yet protest solemnly unto them, and shew them the manner of the king that shall reign over them.
    >>> 

    >>> John[19:6]
    John 19:6 When the chief priests therefore and officers saw him, they cried out, saying, Crucify him, crucify him. Pilate saith unto them, Take ye him, and crucify him: for I find no fault in him.
    >>> _.count('him')
    6

But that's not what is written in the printed King James Bible.

This is the first time that I remember seeing the italics making a real
difference here.

John 19:5 Then came Jesus forth, wearing the crown of thorns, and the purple robe. And [Pilate] saith unto them, Behold the man!
John 19:6 When the chief priests therefore and officers saw [him], they cried out, saying, Crucify [him], crucify [him]. Pilate saith unto them, Take ye him, and crucify him: for I find no fault in him.

Now, I feel we are on dangerous ground here, but the Word is my Light.

Thought. If Y is TH then Ye is THe and You is Thou and if "The" is The word

This is just like how DNA works, there are things called introns and exons.

Introns are the sequences that are removed in the reading process and exons are the sequences that are expressed. The italics are indicating which words were added in translation.

Is this is a work of deception that labels some words as though they shouldn't be there? or is this a work of extreme honesty, in which it is made clear which words came by inspiration and are not translations of the received text?

John 19
 1 Then Pilate therefore took Jesus, and scourged [him].
 2 And the soldiers platted a crown of thorns, and put [it] on his head, and they put on him a purple robe,
 3 And said, Hail, King of the Jews! and they smote him with their hands.
 4 Pilate therefore went forth again, and saith unto them, Behold, I bring him forth to you, that ye may know that I find no fault in him.
 5 Then came Jesus forth, wearing the crown of thorns, and the purple robe.
And Pilate saith unto them, Behold the man!
 6 When the chief priests therefore and officers saw him, they cried out, saying, Crucify [him], crucify [him].
Pilate saith unto them, Take ye him, and crucify him: for I find no fault in him.
 7 The Jews answered him, We have a law, and by our law he ought to die, because he made himself the Son of God.
 8 ¶ When Pilate therefore heard that saying, he was the more afraid;
 9 And went again into the judgment hall, and saith unto Jesus, Whence art thou?
But Jesus gave him no answer.
 10 Then saith Pilate unto him, Speakest thou not unto me? knowest thou not that I have power to crucify thee, and have power to release thee?
 11 Jesus answered, Thou couldest have no power [at all] against me, except it were given thee from above:
    therefore he that delivered me unto thee hath the greater sin.
 12 And from thenceforth Pilate sought to release him:
    but the Jews cried out, saying, If thou let this man go, thou art not Caesar's friend:
    whosoever maketh himself a king speaketh against Caesar.
 13 ¶ When Pilate therefore heard that saying, he brought Jesus forth, and sat down in the judgment seat in a place that is called the Pavement, but in the Hebrew, Gabbatha.
 14 And it was the preparation of the passover, and about the sixth hour:
    and he saith unto the Jews, Behold your King!
 15 But they cried out, Away with [him], away with [him], crucify him.
     Pilate saith unto them, Shall I crucify your King?
     The chief priests answered, We have no king but Caesar.
 16 Then delivered he him therefore unto them to be crucified. And they took Jesus, and led him away.
 17 And he bearing his cross went forth into a place called the place of a skull, which is called in the Hebrew Golgotha:
 18 Where they crucified him, and two other with him, on either side one, and Jesus in the midst.
 19 And Pilate wrote a title, and put it on the cross. And the writing was JESUS OF NAZARETH THE KING OF THE JEWS.
 20 This title then read many of the Jews: for the place where Jesus was crucified was nigh to the city: and it was written in Hebrew, and Greek, and Latin.
 21 Then said the chief priests of the Jews to Pilate, Write not, The King of the Jews; but that he said, I am King of the Jews.
 22 Pilate answered, What I have written I have written.
 23 Then the soldiers, when they had crucified Jesus, took his garments, and made four parts, to every soldier a part; and also his coat: now the coat was without seam, woven from the top throughout.
 24 They said therefore among themselves, Let us not rend it, but cast lots for it, whose it shall be: that the scripture might be fulfilled, which saith, They parted my raiment among them, and for my vesture they did cast lots. These things therefore the soldiers did.
 25 Now there stood by the cross of Jesus his mother, and his mother's sister, Mary the wife of Cleophas, and Mary Magdalene.
 26 When Jesus therefore saw his mother, and the disciple standing by, whom he loved, he saith unto his mother, Woman, behold thy son!
 27 Then saith he to the disciple, Behold thy mother! And from that hour that disciple took her unto his own home.
 28 After this, Jesus knowing that all things were now accomplished, that the scripture might be fulfilled, saith, I thirst.
 29 Now there was set a vessel full of vinegar: and they filled a spunge with vinegar, and put it upon hyssop, and put it to his mouth.
 30 When Jesus therefore had received the vinegar, he said, It is finished: and he bowed his head, and gave up the ghost.
 31 The Jews therefore, because it was the preparation, that the bodies should not remain upon the cross on the sabbath day, (for that sabbath day was an high day,) besought Pilate that their legs might be broken, and that they might be taken away.
 32 Then came the soldiers, and brake the legs of the first, and of the other which was crucified with him.
 33 But when they came to Jesus, and saw that he was dead already, they brake not his legs:
 34 But one of the soldiers with a spear pierced his side, and forthwith came there out blood and water.
 35 And he that saw it bare record, and his record is true: and he knoweth that he saith true, that ye might believe.
 36 For these things were done, that the scripture should be fulfilled, A bone of him shall not be broken.
 37 And again another scripture saith, They shall look on him whom they pierced.
 38 And after this Joseph of Arimathaea, being a disciple of Jesus, but secretly for fear of the Jews, besought Pilate that he might take away the body of Jesus: and Pilate gave him leave. He came therefore, and took the body of Jesus.
 39 And there came also Nicodemus, which at the first came to Jesus by night, and brought a mixture of myrrh and aloes, about an hundred pound weight.
 40 Then took they the body of Jesus, and wound it in linen clothes with the spices, as the manner of the Jews is to bury.
 41 Now in the place where he was crucified there was a garden; and in the garden a new sepulchre, wherein was never man yet laid.
 42 There laid they Jesus therefore because of the Jews' preparation day; for the sepulchre was nigh at hand.

>>> 
>>> 
>>> 
>>>

