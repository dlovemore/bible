>>> from bible import *
>>> ws=b.ws()@method.lower*F(set)*sort
>>> [w for w in ws if str(ssum(w))==str(count(w))[::-1] and ssum(w)!=count(w)]
['abijam', 'abode', 'ahban', 'ahlab', 'akan', 'am', 'amal', 'amam', 'an', 'anak', 'angel', 'angle', 'apiece', 'ar', 'baalim', 'baked', 'balah', 'bamah', 'bani', 'bedan', 'been', 'begin', 'being', 'blade', 'blossomed', 'called', 'candle', 'cilicia', 'communicate', 'confectionaries', 'consecration', 'deck', 'deemed', 'despiseth', 'diadem', 'diversities', 'elealeh', 'eliah', 'elidad', 'ensamples', 'ephai', 'fame', 'feeble', 'fellowdisciples', 'fenced', 'filled', 'flea', 'gebim', 'gilgal', 'glad', 'glean', 'gloriest', 'goad', 'hadlai', 'hammothdor', 'healed', 'heaped', 'hell', 'helm', 'henadad', 'hiddekel', 'hilkiah', 'hod', 'ibneiah', 'idalah', 'india', 'jaakan', 'jaalam', 'jachan', 'jahdo', 'jediael', 'jehovahshalom', 'job', 'kelaiah', 'kind', 'kohathites', 'lacked', 'lama', 'leaf', 'leg', 'naam', 'nathanmelech', 'needed', 'netophathi', 'off', 'shelomoth', 'tottering']
>>> 
>>> rhm=_
>>> len(rhm)
87
>>> rhm@(+count+ssum)
['abijam' 36 63, 'abode' 27 72, 'ahban' 26 62, 'ahlab' 24 42, 'akan' 27 72, 'am' 14 41, 'amal' 27 72, 'amam' 28 82, 'an' 15 51, 'anak' 27 72, 'angel' 39 93, 'angle' 39 93, 'apiece' 39 93, 'ar' 19 91, 'baalim' 38 83, 'baked' 23 32, 'balah' 24 42, 'bamah' 25 52, 'bani' 26 62, 'bedan' 26 62, 'been' 26 62, 'begin' 37 73, 'being' 37 73, 'blade' 24 42, 'blossomed' 104 401, 'called' 37 73, 'candle' 39 93, 'cilicia' 46 64, 'communicate' 117 711, 'confectionaries' 156 651, 'consecration' 136 631, 'deck' 23 32, 'deemed' 36 63, 'despiseth' 105 501, 'diadem' 36 63, 'diversities' 139 931, 'elealeh' 48 84, 'eliah' 35 53, 'elidad' 35 53, 'ensamples' 104 401, 'ephai' 39 93, 'fame' 25 52, 'feeble' 35 53, 'fellowdisciples' 169 961, 'fenced' 37 73, 'filled' 48 84, 'flea' 24 42, 'gebim' 36 63, 'gilgal' 48 84, 'glad' 24 42, 'glean' 39 93, 'gloriest' 105 501, 'goad' 27 72, 'hadlai' 35 53, 'hammothdor' 115 511, 'healed' 35 53, 'heaped' 39 93, 'hell' 37 73, 'helm' 38 83, 'henadad' 37 73, 'hiddekel' 58 85, 'hilkiah' 58 85, 'hod' 27 72, 'ibneiah' 48 84, 'idalah' 35 53, 'india' 37 73, 'jaakan' 38 83, 'jaalam' 38 83, 'jachan' 37 73, 'jahdo' 38 83, 'jediael' 46 64, 'jehovahshalom' 137 731, 'job' 27 72, 'kelaiah' 47 74, 'kind' 38 83, 'kohathites' 116 611, 'lacked' 36 63, 'lama' 27 72, 'leaf' 24 42, 'leg' 24 42, 'naam' 29 92, 'nathanmelech' 104 401, 'needed' 37 73, 'netophathi' 116 611, 'off' 27 72, 'shelomoth' 115 511, 'tottering' 128 821]
>>> sort(_,key=X[1])
['am' 14 41, 'an' 15 51, 'ar' 19 91, 'baked' 23 32, 'deck' 23 32, 'ahlab' 24 42, 'balah' 24 42, 'blade' 24 42, 'flea' 24 42, 'glad' 24 42, 'leaf' 24 42, 'leg' 24 42, 'bamah' 25 52, 'fame' 25 52, 'ahban' 26 62, 'bani' 26 62, 'bedan' 26 62, 'been' 26 62, 'abode' 27 72, 'akan' 27 72, 'amal' 27 72, 'anak' 27 72, 'goad' 27 72, 'hod' 27 72, 'job' 27 72, 'lama' 27 72, 'off' 27 72, 'amam' 28 82, 'naam' 29 92, 'eliah' 35 53, 'elidad' 35 53, 'feeble' 35 53, 'hadlai' 35 53, 'healed' 35 53, 'idalah' 35 53, 'abijam' 36 63, 'deemed' 36 63, 'diadem' 36 63, 'gebim' 36 63, 'lacked' 36 63, 'begin' 37 73, 'being' 37 73, 'called' 37 73, 'fenced' 37 73, 'hell' 37 73, 'henadad' 37 73, 'india' 37 73, 'jachan' 37 73, 'needed' 37 73, 'baalim' 38 83, 'helm' 38 83, 'jaakan' 38 83, 'jaalam' 38 83, 'jahdo' 38 83, 'kind' 38 83, 'angel' 39 93, 'angle' 39 93, 'apiece' 39 93, 'candle' 39 93, 'ephai' 39 93, 'glean' 39 93, 'heaped' 39 93, 'cilicia' 46 64, 'jediael' 46 64, 'kelaiah' 47 74, 'elealeh' 48 84, 'filled' 48 84, 'gilgal' 48 84, 'ibneiah' 48 84, 'hiddekel' 58 85, 'hilkiah' 58 85, 'blossomed' 104 401, 'ensamples' 104 401, 'nathanmelech' 104 401, 'despiseth' 105 501, 'gloriest' 105 501, 'hammothdor' 115 511, 'shelomoth' 115 511, 'kohathites' 116 611, 'netophathi' 116 611, 'communicate' 117 711, 'tottering' 128 821, 'consecration' 136 631, 'jehovahshalom' 137 731, 'diversities' 139 931, 'confectionaries' 156 651, 'fellowdisciples' 169 961]
>>> b/'gather'/'wing'
Isaiah 10:14;Matthew 23:37;Luke 13:34 (3 verses)
>>> Matthew[23:37]
Matthew 23:37 O Jerusalem, Jerusalem, thou that killest the prophets, and stonest them which are sent unto thee, how often would I have gathered thy children together, even as a hen gathereth her chickens under her wings, and ye would not!
>>> b/'henadad'
Ezra 3:9;Nehemiah 3:18,24;10:9 (4 verses)
>>> Matthew[23:37].chn()
952
>>> Matthew[23:37].vn()
23956
>>> 23956-23145
811
>>> b/'reed'
1 Kings 14:15;2 Kings 18:21;Job 40:21;Isaiah 19:6-7;35:7;36:6;42:3;Jeremiah 51:32;Ezekiel 29:6;40:3,5-8;41:8;42:16-20;45:1;48:8;Matthew 11:7;12:20;27:29-30,48;Mark 15:19,36;Luke 7:24;Revelation 11:1;21:15-16 (34 verses)
>>> p(_)
1 Kings 14:15 For the LORD shall smite Israel, as a reed is shaken in the water, and he shall root up Israel out of this good land, which he gave to their fathers, and shall scatter them beyond the river, because they have made their groves, provoking the LORD to anger.
2 Kings 18:21 Now, behold, thou trustest upon the staff of this bruised reed, even upon Egypt, on which if a man lean, it will go into his hand, and pierce it: so is Pharaoh king of Egypt unto all that trust on him.
Job 40:21 He lieth under the shady trees, in the covert of the reed, and fens.
Isaiah 19:6 And they shall turn the rivers far away; and the brooks of defence shall be emptied and dried up: the reeds and flags shall wither.
Isaiah 19:7 The paper reeds by the brooks, by the mouth of the brooks, and every thing sown by the brooks, shall wither, be driven away, and be no more.
Isaiah 35:7 And the parched ground shall become a pool, and the thirsty land springs of water: in the habitation of dragons, where each lay, shall be grass with reeds and rushes.
Isaiah 36:6 Lo, thou trustest in the staff of this broken reed, on Egypt; whereon if a man lean, it will go into his hand, and pierce it: so is Pharaoh king of Egypt to all that trust in him.
Isaiah 42:3 A bruised reed shall he not break, and the smoking flax shall he not quench: he shall bring forth judgment unto truth.
Jeremiah 51:32 And that the passages are stopped, and the reeds they have burned with fire, and the men of war are affrighted.
Ezekiel 29:6 And all the inhabitants of Egypt shall know that I am the LORD, because they have been a staff of reed to the house of Israel.
Ezekiel 40
3 And he brought me thither, and, behold, there was a man, whose appearance was like the appearance of brass, with a line of flax in his hand, and a measuring reed; and he stood in the gate.
5 And behold a wall on the outside of the house round about, and in the man's hand a measuring reed of six cubits long by the cubit and an hand breadth: so he measured the breadth of the building, one reed; and the height, one reed.
6 Then came he unto the gate which looketh toward the east, and went up the stairs thereof, and measured the threshold of the gate, which was one reed broad; and the other threshold of the gate, which was one reed broad.
7 And every little chamber was one reed long, and one reed broad; and between the little chambers were five cubits; and the threshold of the gate by the porch of the gate within was one reed.
8 He measured also the porch of the gate within, one reed.
Ezekiel 41:8 I saw also the height of the house round about: the foundations of the side chambers were a full reed of six great cubits.
Ezekiel 42
16 He measured the east side with the measuring reed, five hundred reeds, with the measuring reed round about.
17 He measured the north side, five hundred reeds, with the measuring reed round about.
18 He measured the south side, five hundred reeds, with the measuring reed.
19 He turned about to the west side, and measured five hundred reeds with the measuring reed.
20 He measured it by the four sides: it had a wall round about, five hundred reeds long, and five hundred broad, to make a separation between the sanctuary and the profane place.
Ezekiel 45:1 Moreover, when ye shall divide by lot the land for inheritance, ye shall offer an oblation unto the LORD, an holy portion of the land: the length shall be the length of five and twenty thousand reeds, and the breadth shall be ten thousand. This shall be holy in all the borders thereof round about.
Ezekiel 48:8 And by the border of Judah, from the east side unto the west side, shall be the offering which ye shall offer of five and twenty thousand reeds in breadth, and in length as one of the other parts, from the east side unto the west side: and the sanctuary shall be in the midst of it.
Matthew 11:7 And as they departed, Jesus began to say unto the multitudes concerning John, What went ye out into the wilderness to see? A reed shaken with the wind?
Matthew 12:20 A bruised reed shall he not break, and smoking flax shall he not quench, till he send forth judgment unto victory.
Matthew 27
29 And when they had platted a crown of thorns, they put it upon his head, and a reed in his right hand: and they bowed the knee before him, and mocked him, saying, Hail, King of the Jews!
30 And they spit upon him, and took the reed, and smote him on the head.
48 And straightway one of them ran, and took a spunge, and filled it with vinegar, and put it on a reed, and gave him to drink.
Mark 15:19 And they smote him on the head with a reed, and did spit upon him, and bowing their knees worshipped him.
Mark 15:36 And one ran and filled a spunge full of vinegar, and put it on a reed, and gave him to drink, saying, Let alone; let us see whether Elias will come to take him down.
Luke 7:24 And when the messengers of John were departed, he began to speak unto the people concerning John, What went ye out into the wilderness for to see? A reed shaken with the wind?
Revelation 11:1 And there was given me a reed like unto a rod: and the angel stood, saying, Rise, and measure the temple of God, and the altar, and them that worship therein.
Revelation 21:15 And he that talked with me had a golden reed to measure the city, and the gates thereof, and the wall thereof.
Revelation 21:16 And the city lieth foursquare, and the length is as large as the breadth: and he measured the city with the reed, twelve thousand furlongs. The length and the breadth and the height of it are equal.
>>> 
>>> 
>>> 
