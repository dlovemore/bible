>>> from bible import *
>>> Genesis[13:16].tells()
And I will make thy seed  as the dust of the earth:  so that if a man can number the dust of the earth, then shall thy seed also be numbered.   =
 3  1   4    4   3    4   2   3    4   2  3     5    2    4   2 1  3   3     6    3    4   2  3     5     4    5    3    4    4   2     8      106
 19 9  56   30   53  33   20  33  64  21  33   52    34  49  15 1  28  18   73    33  64  21  33   52    47    52   53  33   47   7     82    1165
 55 9  569  66  908  114 101 213  604 66 213   304  160  409 15 1  91  54   487  213  604 66 213   304   263  169  908  114  191  7    496    7987
>>> tells("dust of the earth")
dust of the earth   =
  4   2  3    5    14
 64  21  33   52   170
 604 66 213  304  1187
>>> "Lord God Almighty"*tells
Lord God Almighty   =
  4   3      8     15
 49   26    95     170
 184  71    995   1250
>>> b/'Lord God Almighty'
Revelation 4:8;11:17;15:3;16:7;21:22 (5 verses)
>>> _*p
Revelation 4:8 And the four beasts had each of them six wings about him; and they were full of eyes within: and they rest not day and night, saying, Holy, holy, holy, LORD God Almighty, which was, and is, and is to come.
Revelation 11:17 Saying, We give thee thanks, O LORD God Almighty, which art, and wast, and art to come; because thou hast taken to thee thy great power, and hast reigned.
Revelation 15:3 And they sing the song of Moses the servant of God, and the song of the Lamb, saying, Great and marvellous are thy works, Lord God Almighty; just and true are thy ways, thou King of saints.
Revelation 16:7 And I heard another out of the altar say, Even so, Lord God Almighty, true and righteous are thy judgments.
Revelation 21:22 And I saw no temple therein: for the Lord God Almighty and the Lamb are the temple of it.
>>> 
>>> b.ch(1187)
Revelation 20:1-15 (15 verses)
>>> tells("sand of the sea")
sand of the sea  =
  4   2  3   3   12
 38  21  33  25 117
 155 66 213 106 540
>>> IIIJohn.vc()
14
>>> IIIJohn.wc()
294
>>> tells("clay")
c  l a  y   =
1  1 1  1   4
3 12 1  25  41
3 30 1 700 734
