>>> from bible import *
>>> Matthew[9:13:14]
Matthew 9:13 But go ye and learn what that meaneth, I will have mercy, and not sacrifice: for I am not come to call the righteous, but sinners to repentance.
Matthew 9:14 Then came to him the disciples of John, saying, Why do we and the Pharisees fast oft, but thy disciples fast not?
>>> _.vn()*p
23393
>>> c # speed of light
299792458
>>> c//1022
293339
>>> 
>>> 
>>> 
>>> Matthew[5:13:14]
Matthew 5:13 Ye are the salt of the earth: but if the salt have lost his savour, wherewith shall it be salted? it is thenceforth good for nothing, but to be cast out, and to be trodden under foot of men.
Matthew 5:14 Ye are the light of the world. A city that is set on an hill cannot be hid.
>>> p(_.vn())
23248
>>> b.vi(23339)
Matthew 7:22 Many will say to me in that day, Lord, Lord, have we not prophesied in thy name? and in thy name have cast out devils? and in thy name done many wonderful works?
>>> b.vi(923)
Genesis 31:49 And Mizpah; for he said, The LORD watch between me and thee, when we are absent one from another.
>>> Matthew[1]+Matthew[7]
Matthew 1:1-25;7:1-29 (54 verses)
>>> p(_.wc(),_.text()*sums)
1099 (4558, 50895, 361386)
>>> 
>>> 
>>> c # speed of light
299792458
>>> 29979+1123
31102
>>> # Sodium is Na Z=11 A=23
>>> Matthew[5:13:14]*p
Matthew 5:13 Ye are the salt of the earth: but if the salt have lost his savour, wherewith shall it be salted? it is thenceforth good for nothing, but to be cast out, and to be trodden under foot of men.
Matthew 5:14 Ye are the light of the world. A city that is set on an hill cannot be hid.
>>> 
