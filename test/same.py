>>> from bible import *
>>> ss=reduce(orr,([v+w for v,w in pairs(b) if v.ws()[:6]==w.ws()[:6]]))
>>> ss
Genesis 3:13-14;16:9-11;29:33-35;36:17-18;45:3-4;Exodus 4:8-9;12:25-26;19:9-10;28:22-23;40:5-6;Leviticus 13:5;14:14-15,19-20;18:12-17;Numbers 8:9-10;9:20-21;10:15-16,19-20,23-24,26-27;21:12-13;34:24-28;35:16-17;Deuteronomy 27:20-23;30:5-7;Joshua 17:1-2;22:31-32;Judges 8:23-24;1 Samuel 30:28-31;2 Samuel 15:16-17;1 Kings 21:15-16;22:32-33;1 Chronicles 16:41-42;Nehemiah 7:70-71;Job 1:7-8,16-18;2:2-3;Psalms 29:3-4;103:1-2;118:8-9;124:1-2;129:1;135:19-20;Isaiah 2:7-8;7:21-23;27:12-13;40:7-8;Jeremiah 11:21-22;12:8-9,15-16;Ezekiel 3:3-4;5:7-8;11:16-17;47:9-10,22-23;Micah 5:7-8;Zechariah 8:6-7,19-20;13:2-4;Matthew 11:8-9;13:44-45;23:14-15;Luke 7:25-26;John 5:24-25;1 Corinthians 15:6-7;Hebrews 11:23-24 (149 verses)
>>> 
>>> ss.vns()
[69, 70, 391, 392, 393, 829, 830, 831, 1058, 1059, 1362, 1363, 1610, 1611, 1842, 1843, 2036, 2037, 2316, 2317, 2713, 2714, 3058, 3059, 3126, 3127, 3131, 3132, 3264, 3265, 3266, 3267, 3268, 3269, 3949, 3950, 3986, 3987, 4004, 4005, 4008, 4009, 4012, 4013, 4015, 4016, 4353, 4354, 4841, 4842, 4843, 4844, 4845, 4862, 4863, 5606, 5607, 5608, 5609, 5714, 5715, 5716, 6277, 6278, 6458, 6459, 6743, 6744, 8007, 8008, 8009, 8010, 8406, 8407, 9467, 9468, 9513, 9514, 10862, 10863, 12491, 12492, 12877, 12878, 12886, 12887, 12888, 12894, 12895, 14312, 14313, 15551, 15552, 15878, 15879, 16104, 16105, 16134, 16135, 16195, 16196, 17693, 17694, 17804, 17805, 17806, 18164, 18165, 18428, 18429, 19248, 19249, 19258, 19259, 19265, 19266, 20506, 20507, 20554, 20555, 20672, 20673, 21689, 21690, 21702, 21703, 22641, 22642, 22983, 22984, 22996, 22997, 23062, 23063, 23064, 23468, 23469, 23584, 23585, 23933, 23934, 25221, 25222, 26235, 26236, 28725, 28726, 30196, 30197]
>>> p(ss)
Genesis 3:13 And the LORD God said unto the woman, What is this that thou hast done? And the woman said, The serpent beguiled me, and I did eat.
Genesis 3:14 And the LORD God said unto the serpent, Because thou hast done this, thou art cursed above all cattle, and above every beast of the field; upon thy belly shalt thou go, and dust shalt thou eat all the days of thy life:
Genesis 16
9 And the angel of the LORD said unto her, Return to thy mistress, and submit thyself under her hands.
10 And the angel of the LORD said unto her, I will multiply thy seed exceedingly, that it shall not be numbered for multitude.
11 And the angel of the LORD said unto her, Behold, thou art with child and shalt bear a son, and shalt call his name Ishmael; because the LORD hath heard thy affliction.
Genesis 29
33 And she conceived again, and bare a son; and said, Because the LORD hath heard I was hated, he hath therefore given me this son also: and she called his name Simeon.
34 And she conceived again, and bare a son; and said, Now this time will my husband be joined unto me, because I have born him three sons: therefore was his name called Levi.
35 And she conceived again, and bare a son: and she said, Now will I praise the LORD: therefore she called his name Judah; and left bearing.
Genesis 36:17 And these are the sons of Reuel Esau's son; duke Nahath, duke Zerah, duke Shammah, duke Mizzah: these are the dukes that came of Reuel in the land of Edom; these are the sons of Bashemath Esau's wife.
Genesis 36:18 And these are the sons of Aholibamah Esau's wife; duke Jeush, duke Jaalam, duke Korah: these were the dukes that came of Aholibamah the daughter of Anah, Esau's wife.
Genesis 45:3 And Joseph said unto his brethren, I am Joseph; doth my father yet live? And his brethren could not answer him; for they were troubled at his presence.
Genesis 45:4 And Joseph said unto his brethren, Come near to me, I pray you. And they came near. And he said, I am Joseph your brother, whom ye sold into Egypt.
Exodus 4:8 And it shall come to pass, if they will not believe thee, neither hearken to the voice of the first sign, that they will believe the voice of the latter sign.
Exodus 4:9 And it shall come to pass, if they will not believe also these two signs, neither hearken unto thy voice, that thou shalt take of the water of the river, and pour it upon the dry land: and the water which thou takest out of the river shall become blood upon the dry land.
Exodus 12:25 And it shall come to pass, when ye be come to the land which the LORD will give you, according as he hath promised, that ye shall keep this service.
Exodus 12:26 And it shall come to pass, when your children shall say unto you, What mean ye by this service?
Exodus 19:9 And the LORD said unto Moses, Lo, I come unto thee in a thick cloud, that the people may hear when I speak with thee, and believe thee for ever. And Moses told the words of the people unto the LORD.
Exodus 19:10 And the LORD said unto Moses, Go unto the people, and sanctify them to day and to morrow, and let them wash their clothes,
Exodus 28:22 And thou shalt make upon the breastplate chains at the ends of wreathen work of pure gold.
Exodus 28:23 And thou shalt make upon the breastplate two rings of gold, and shalt put the two rings on the two ends of the breastplate.
Exodus 40:5 And thou shalt set the altar of gold for the incense before the ark of the testimony, and put the hanging of the door to the tabernacle.
Exodus 40:6 And thou shalt set the altar of the burnt offering before the door of the tabernacle of the tent of the congregation.
Leviticus 13:5 And the priest shall look on him the seventh day: and, behold, if the plague in his sight be at a stay, and the plague spread not in the skin; then the priest shall shut him up seven days more:
Leviticus 13:6 And the priest shall look on him again the seventh day: and, behold, if the plague be somewhat dark, and the plague spread not in the skin, the priest shall pronounce him clean: it is but a scab: and he shall wash his clothes, and be clean.
Leviticus 14
14 And the priest shall take some of the blood of the trespass offering, and the priest shall put it upon the tip of the right ear of him that is to be cleansed, and upon the thumb of his right hand, and upon the great toe of his right foot:
15 And the priest shall take some of the log of oil, and pour it into the palm of his own left hand:
19 And the priest shall offer the sin offering, and make an atonement for him that is to be cleansed from his uncleanness; and afterward he shall kill the burnt offering:
20 And the priest shall offer the burnt offering and the meat offering upon the altar: and the priest shall make an atonement for him, and he shall be clean.
Leviticus 18
12 Thou shalt not uncover the nakedness of thy father's sister: she is thy father's near kinswoman.
13 Thou shalt not uncover the nakedness of thy mother's sister: for she is thy mother's near kinswoman.
14 Thou shalt not uncover the nakedness of thy father's brother, thou shalt not approach to his wife: she is thine aunt.
15 Thou shalt not uncover the nakedness of thy daughter in law: she is thy son's wife; thou shalt not uncover her nakedness.
16 Thou shalt not uncover the nakedness of thy brother's wife: it is thy brother's nakedness.
17 Thou shalt not uncover the nakedness of a woman and her daughter, neither shalt thou take her son's daughter, or her daughter's daughter, to uncover her nakedness; for they are her near kinswomen: it is wickedness.
Numbers 8:9 And thou shalt bring the Levites before the tabernacle of the congregation: and thou shalt gather the whole assembly of the children of Israel together:
Numbers 8:10 And thou shalt bring the Levites before the LORD: and the children of Israel shall put their hands upon the Levites:
Numbers 9:20 And so it was, when the cloud was a few days upon the tabernacle; according to the commandment of the LORD they abode in their tents, and according to the commandment of the LORD they journeyed.
Numbers 9:21 And so it was, when the cloud abode from even unto the morning, and that the cloud was taken up in the morning, then they journeyed: whether it was by day or by night that the cloud was taken up, they journeyed.
Numbers 10
15 And over the host of the tribe of the children of Issachar was Nethaneel the son of Zuar.
16 And over the host of the tribe of the children of Zebulun was Eliab the son of Helon.
19 And over the host of the tribe of the children of Simeon was Shelumiel the son of Zurishaddai.
20 And over the host of the tribe of the children of Gad was Eliasaph the son of Deuel.
23 And over the host of the tribe of the children of Manasseh was Gamaliel the son of Pedahzur.
24 And over the host of the tribe of the children of Benjamin was Abidan the son of Gideoni.
26 And over the host of the tribe of the children of Asher was Pagiel the son of Ocran.
27 And over the host of the tribe of the children of Naphtali was Ahira the son of Enan.
Numbers 21:12 From thence they removed, and pitched in the valley of Zared.
Numbers 21:13 From thence they removed, and pitched on the other side of Arnon, which is in the wilderness that cometh out of the coasts of the Amorites: for Arnon is the border of Moab, between Moab and the Amorites.
Numbers 34
24 And the prince of the tribe of the children of Ephraim, Kemuel the son of Shiphtan.
25 And the prince of the tribe of the children of Zebulun, Elizaphan the son of Parnach.
26 And the prince of the tribe of the children of Issachar, Paltiel the son of Azzan.
27 And the prince of the tribe of the children of Asher, Ahihud the son of Shelomi.
28 And the prince of the tribe of the children of Naphtali, Pedahel the son of Ammihud.
Numbers 35:16 And if he smite him with an instrument of iron, so that he die, he is a murderer: the murderer shall surely be put to death.
Numbers 35:17 And if he smite him with throwing a stone, wherewith he may die, and he die, he is a murderer: the murderer shall surely be put to death.
Deuteronomy 27
20 Cursed be he that lieth with his father's wife; because he uncovereth his father's skirt. And all the people shall say, Amen.
21 Cursed be he that lieth with any manner of beast. And all the people shall say, Amen.
22 Cursed be he that lieth with his sister, the daughter of his father, or the daughter of his mother. And all the people shall say, Amen.
23 Cursed be he that lieth with his mother in law. And all the people shall say, Amen.
Deuteronomy 30
5 And the LORD thy God will bring thee into the land which thy fathers possessed, and thou shalt possess it; and he will do thee good, and multiply thee above thy fathers.
6 And the LORD thy God will circumcise thine heart, and the heart of thy seed, to love the LORD thy God with all thine heart, and with all thy soul, that thou mayest live.
7 And the LORD thy God will put all these curses upon thine enemies, and on them that hate thee, which persecuted thee.
Joshua 17:1 There was also a lot for the tribe of Manasseh; for he was the firstborn of Joseph; to wit, for Machir the firstborn of Manasseh, the father of Gilead: because he was a man of war, therefore he had Gilead and Bashan.
Joshua 17:2 There was also a lot for the rest of the children of Manasseh by their families; for the children of Abiezer, and for the children of Helek, and for the children of Asriel, and for the children of Shechem, and for the children of Hepher, and for the children of Shemida: these were the male children of Manasseh the son of Joseph by their families.
Joshua 22:31 And Phinehas the son of Eleazar the priest said unto the children of Reuben, and to the children of Gad, and to the children of Manasseh, This day we perceive that the LORD is among us, because ye have not committed this trespass against the LORD: now ye have delivered the children of Israel out of the hand of the LORD.
Joshua 22:32 And Phinehas the son of Eleazar the priest, and the princes, returned from the children of Reuben, and from the children of Gad, out of the land of Gilead, unto the land of Canaan, to the children of Israel, and brought them word again.
Judges 8:23 And Gideon said unto them, I will not rule over you, neither shall my son rule over you: the LORD shall rule over you.
Judges 8:24 And Gideon said unto them, I would desire a request of you, that ye would give me every man the earrings of his prey. (For they had golden earrings, because they were Ishmaelites.)
1 Samuel 30
28 And to them which were in Aroer, and to them which were in Siphmoth, and to them which were in Eshtemoa,
29 And to them which were in Rachal, and to them which were in the cities of the Jerahmeelites, and to them which were in the cities of the Kenites,
30 And to them which were in Hormah, and to them which were in Chorashan, and to them which were in Athach,
31 And to them which were in Hebron, and to all the places where David himself and his men were wont to haunt.
2 Samuel 15:16 And the king went forth, and all his household after him. And the king left ten women, which were concubines, to keep the house.
2 Samuel 15:17 And the king went forth, and all the people after him, and tarried in a place that was far off.
1 Kings 21:15 And it came to pass, when Jezebel heard that Naboth was stoned, and was dead, that Jezebel said to Ahab, Arise, take possession of the vineyard of Naboth the Jezreelite, which he refused to give thee for money: for Naboth is not alive, but dead.
1 Kings 21:16 And it came to pass, when Ahab heard that Naboth was dead, that Ahab rose up to go down to the vineyard of Naboth the Jezreelite, to take possession of it.
1 Kings 22:32 And it came to pass, when the captains of the chariots saw Jehoshaphat, that they said, Surely it is the king of Israel. And they turned aside to fight against him: and Jehoshaphat cried out.
1 Kings 22:33 And it came to pass, when the captains of the chariots perceived that it was not the king of Israel, that they turned back from pursuing him.
1 Chronicles 16:41 And with them Heman and Jeduthun, and the rest that were chosen, who were expressed by name, to give thanks to the LORD, because his mercy endureth for ever;
1 Chronicles 16:42 And with them Heman and Jeduthun with trumpets and cymbals for those that should make a sound, and with musical instruments of God. And the sons of Jeduthun were porters.
Nehemiah 7:70 And some of the chief of the fathers gave unto the work. The Tirshatha gave to the treasure a thousand drams of gold, fifty basons, five hundred and thirty priests' garments.
Nehemiah 7:71 And some of the chief of the fathers gave to the treasure of the work twenty thousand drams of gold, and two thousand and two hundred pound of silver.
Job 1
7 And the LORD said unto Satan, Whence comest thou? Then Satan answered the LORD, and said, From going to and fro in the earth, and from walking up and down in it.
8 And the LORD said unto Satan, Hast thou considered my servant Job, that there is none like him in the earth, a perfect and an upright man, one that feareth God, and escheweth evil?
16 While he was yet speaking, there came also another, and said, The fire of God is fallen from heaven, and hath burned up the sheep, and the servants, and consumed them; and I only am escaped alone to tell thee.
17 While he was yet speaking, there came also another, and said, The Chaldeans made out three bands, and fell upon the camels, and have carried them away, yea, and slain the servants with the edge of the sword; and I only am escaped alone to tell thee.
18 While he was yet speaking, there came also another, and said, Thy sons and thy daughters were eating and drinking wine in their eldest brother's house:
Job 2:2 And the LORD said unto Satan, From whence comest thou? And Satan answered the LORD, and said, From going to and fro in the earth, and from walking up and down in it.
Job 2:3 And the LORD said unto Satan, Hast thou considered my servant Job, that there is none like him in the earth, a perfect and an upright man, one that feareth God, and escheweth evil? and still he holdeth fast his integrity, although thou movedst me against him, to destroy him without cause.
Psalms 29:3 The voice of the LORD is upon the waters: the God of glory thundereth: the LORD is upon many waters.
Psalms 29:4 The voice of the LORD is powerful; the voice of the LORD is full of majesty.
Psalms 103:1 Bless the LORD, O my soul: and all that is within me, bless his holy name.
Psalms 103:2 Bless the LORD, O my soul, and forget not all his benefits:
Psalms 118:8 It is better to trust in the LORD than to put confidence in man.
Psalms 118:9 It is better to trust in the LORD than to put confidence in princes.
Psalms 124:1 If it had not been the LORD who was on our side, now may Israel say;
Psalms 124:2 If it had not been the LORD who was on our side, when men rose up against us:
Psalms 129:1 Many a time have they afflicted me from my youth, may Israel now say:
Psalms 129:2 Many a time have they afflicted me from my youth: yet they have not prevailed against me.
Psalms 135:19 Bless the LORD, O house of Israel: bless the LORD, O house of Aaron:
Psalms 135:20 Bless the LORD, O house of Levi: ye that fear the LORD, bless the LORD.
Isaiah 2:7 Their land also is full of silver and gold, neither is there any end of their treasures; their land is also full of horses, neither is there any end of their chariots:
Isaiah 2:8 Their land also is full of idols; they worship the work of their own hands, that which their own fingers have made:
Isaiah 7
21 And it shall come to pass in that day, that a man shall nourish a young cow, and two sheep;
22 And it shall come to pass, for the abundance of milk that they shall give he shall eat butter: for butter and honey shall every one eat that is left in the land.
23 And it shall come to pass in that day, that every place shall be, where there were a thousand vines at a thousand silverlings, it shall even be for briers and thorns.
Isaiah 27:12 And it shall come to pass in that day, that the LORD shall beat off from the channel of the river unto the stream of Egypt, and ye shall be gathered one by one, O ye children of Israel.
Isaiah 27:13 And it shall come to pass in that day, that the great trumpet shall be blown, and they shall come which were ready to perish in the land of Assyria, and the outcasts in the land of Egypt, and shall worship the LORD in the holy mount at Jerusalem.
Isaiah 40:7 The grass withereth, the flower fadeth: because the spirit of the LORD bloweth upon it: surely the people is grass.
Isaiah 40:8 The grass withereth, the flower fadeth: but the word of our God shall stand for ever.
Jeremiah 11:21 Therefore thus saith the LORD of the men of Anathoth, that seek thy life, saying, Prophesy not in the name of the LORD, that thou die not by our hand:
Jeremiah 11:22 Therefore thus saith the LORD of hosts, Behold, I will punish them: the young men shall die by the sword; their sons and their daughters shall die by famine:
Jeremiah 12
8 Mine heritage is unto me as a lion in the forest; it crieth out against me: therefore have I hated it.
9 Mine heritage is unto me as a speckled bird, the birds round about are against her; come ye, assemble all the beasts of the field, come to devour.
15 And it shall come to pass, after that I have plucked them out I will return, and have compassion on them, and will bring them again, every man to his heritage, and every man to his land.
16 And it shall come to pass, if they will diligently learn the ways of my people, to swear by my name, The LORD liveth; as they taught my people to swear by Baal; then shall they be built in the midst of my people.
Ezekiel 3:3 And he said unto me, Son of man, cause thy belly to eat, and fill thy bowels with this roll that I give thee. Then did I eat it; and it was in my mouth as honey for sweetness.
Ezekiel 3:4 And he said unto me, Son of man, go, get thee unto the house of Israel, and speak with my words unto them.
Ezekiel 5:7 Therefore thus saith the Lord GOD; Because ye multiplied more than the nations that are round about you, and have not walked in my statutes, neither have kept my judgments, neither have done according to the judgments of the nations that are round about you;
Ezekiel 5:8 Therefore thus saith the Lord GOD; Behold, I, even I, am against thee, and will execute judgments in the midst of thee in the sight of the nations.
Ezekiel 11:16 Therefore say, Thus saith the Lord GOD; Although I have cast them far off among the heathen, and although I have scattered them among the countries, yet will I be to them as a little sanctuary in the countries where they shall come.
Ezekiel 11:17 Therefore say, Thus saith the Lord GOD; I will even gather you from the people, and assemble you out of the countries where ye have been scattered, and I will give you the land of Israel.
Ezekiel 47
9 And it shall come to pass, that every thing that liveth, which moveth, whithersoever the rivers shall come, shall live: and there shall be a very great multitude of fish, because these waters shall come thither: for they shall be healed; and every thing shall live whither the river cometh.
10 And it shall come to pass, that the fishers shall stand upon it from Engedi even unto Eneglaim; they shall be a place to spread forth nets; their fish shall be according to their kinds, as the fish of the great sea, exceeding many.
22 And it shall come to pass, that ye shall divide it by lot for an inheritance unto you, and to the strangers that sojourn among you, which shall beget children among you: and they shall be unto you as born in the country among the children of Israel; they shall have inheritance with you among the tribes of Israel.
23 And it shall come to pass, that in what tribe the stranger sojourneth, there shall ye give him his inheritance, saith the Lord GOD.
Micah 5:7 And the remnant of Jacob shall be in the midst of many people as a dew from the LORD, as the showers upon the grass, that tarrieth not for man, nor waiteth for the sons of men.
Micah 5:8 And the remnant of Jacob shall be among the Gentiles in the midst of many people as a lion among the beasts of the forest, as a young lion among the flocks of sheep: who, if he go through, both treadeth down, and teareth in pieces, and none can deliver.
Zechariah 8
6 Thus saith the LORD of hosts; If it be marvellous in the eyes of the remnant of this people in these days, should it also be marvellous in mine eyes? saith the LORD of hosts.
7 Thus saith the LORD of hosts; Behold, I will save my people from the east country, and from the west country;
19 Thus saith the LORD of hosts; The fast of the fourth month, and the fast of the fifth, and the fast of the seventh, and the fast of the tenth, shall be to the house of Judah joy and gladness, and cheerful feasts; therefore love the truth and peace.
20 Thus saith the LORD of hosts; It shall yet come to pass, that there shall come people, and the inhabitants of many cities:
Zechariah 13
2 And it shall come to pass in that day, saith the LORD of hosts, that I will cut off the names of the idols out of the land, and they shall no more be remembered: and also I will cause the prophets and the unclean spirit to pass out of the land.
3 And it shall come to pass, that when any shall yet prophesy, then his father and his mother that begat him shall say unto him, Thou shalt not live; for thou speakest lies in the name of the LORD: and his father and his mother that begat him shall thrust him through when he prophesieth.
4 And it shall come to pass in that day, that the prophets shall be ashamed every one of his vision, when he hath prophesied; neither shall they wear a rough garment to deceive:
Matthew 11:8 But what went ye out for to see? A man clothed in soft raiment? behold, they that wear soft clothing are in kings' houses.
Matthew 11:9 But what went ye out for to see? A prophet? yea, I say unto you, and more than a prophet.
Matthew 13:44 Again, the kingdom of heaven is like unto treasure hid in a field; the which when a man hath found, he hideth, and for joy thereof goeth and selleth all that he hath, and buyeth that field.
Matthew 13:45 Again, the kingdom of heaven is like unto a merchant man, seeking goodly pearls:
Matthew 23:14 Woe unto you, scribes and Pharisees, hypocrites! for ye devour widows' houses, and for a pretence make long prayer: therefore ye shall receive the greater damnation.
Matthew 23:15 Woe unto you, scribes and Pharisees, hypocrites! for ye compass sea and land to make one proselyte, and when he is made, ye make him twofold more the child of hell than yourselves.
Luke 7:25 But what went ye out for to see? A man clothed in soft raiment? Behold, they which are gorgeously apparelled, and live delicately, are in kings' courts.
Luke 7:26 But what went ye out for to see? A prophet? Yea, I say unto you, and much more than a prophet.
John 5:24 Verily, verily, I say unto you, He that heareth my word, and believeth on him that sent me, hath everlasting life, and shall not come into condemnation; but is passed from death unto life.
John 5:25 Verily, verily, I say unto you, The hour is coming, and now is, when the dead shall hear the voice of the Son of God: and they that hear shall live.
1 Corinthians 15:6 After that, he was seen of above five hundred brethren at once; of whom the greater part remain unto this present, but some are fallen asleep.
1 Corinthians 15:7 After that, he was seen of James; then of all the apostles.
Hebrews 11:23 By faith Moses, when he was born, was hid three months of his parents, because they saw he was a proper child; and they were not afraid of the king's commandment.
Hebrews 11:24 By faith Moses, when he was come to years, refused to be called the son of Pharaoh's daughter;
>>> 
>>> 
>>> 
>>> 
>>> 
