>>> from bible import *
>>> IKings[6:37:38]
1 Kings 6:37 In the fourth year was the foundation of the house of the LORD laid, in the month Zif:
1 Kings 6:38 And in the eleventh year, in the month Bul, which is the eighth month, was the house finished throughout all the parts thereof, and according to all the fashion of it. So was he seven years in building it.
>>> Ecclesiastes[8:4]
Ecclesiastes 8:4 Where the word of a king is, there is power: and who may say unto him, What doest thou?
>>> b.count(r'sinners\b')
48
>>> b/'sinners'
Genesis 13:13;Numbers 16:38;1 Samuel 15:18;Psalms 1:1,5;25:8;26:9;51:13;104:35;Proverbs 1:10;13:21;23:17;Isaiah 1:28;13:9;33:14;Amos 9:10;Matthew 9:10-11,13;11:19;26:45;Mark 2:15-17;14:41;Luke 5:30,32;6:32-34;7:34;13:2,4;15:1-2;John 9:31;Romans 5:8,19;Galatians 2:15,17;1 Timothy 1:9,15;Hebrews 7:26;12:3;James 4:8;Jude 1:15 (46 verses)
>>> b/'sinners.*sinners'
Mark 2:16 And when the scribes and Pharisees saw him eat with publicans and sinners, they said unto his disciples, How is it that he eateth and drinketh with publicans and sinners?
Luke 6:34 And if ye lend to them of whom ye hope to receive, what thank have ye? for sinners also lend to sinners, to receive as much again.
>>> James[4:8]
James 4:8 Draw nigh to God, and he will draw nigh to you. Cleanse your hands, ye sinners; and purify your hearts, ye double minded.
>>> James.words()[1611-1]
'sinners;'
>>> Psalms[68:11]
Psalms 68:11 The Lord gave the word: great was the company of those that published it.
>>> Deuteronomy[16:11]
Deuteronomy 16:11 And thou shalt rejoice before the LORD thy God, thou, and thy son, and thy daughter, and thy manservant, and thy maidservant, and the Levite that is within thy gates, and the stranger, and the fatherless, and the widow, that are among you, in the place which the LORD thy God hath chosen to place his name there.
>>> (Genesis-Deuteronomy[16:10]).count("lord\\b[^']")
1610
>>> Psalm[33:11]
Psalms 33:11 The counsel of the LORD standeth for ever, the thoughts of his heart to all generations.
>>> Deuteronomy[29:29]
Deuteronomy 29:29 The secret things belong unto the LORD our God: but those things which are revealed belong unto us and to our children for ever, that we may do all the words of this law.
>>> 
