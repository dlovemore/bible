>>> from bible import *
>>> b/'treasure'
Genesis 43:23;Exodus 1:11;19:5;Deuteronomy 28:12;32:34;33:19;1 Kings 7:51;14:26;15:18;2 Kings 12:18;14:14;16:8;18:15;20:13,15;24:13;1 Chronicles 26:20,22,24,26;27:25;29:8;2 Chronicles 5:1;8:15;12:9;16:2;25:24;36:18;Ezra 1:8;2:69;5:17-6:1;7:20-21;Nehemiah 7:70-71;10:38;12:44;13:13;Job 3:21;38:22;Psalms 17:14;135:4;Proverbs 2:4;8:21;10:2;15:6,16;21:6,20;Ecclesiastes 2:8;Isaiah 2:7;10:13;22:15;23:18;30:6;33:6;39:2,4;45:3;Jeremiah 10:13;15:13;17:3;20:5;41:8;48:7;49:4;50:37;51:13,16;Ezekiel 22:25;28:4;Daniel 1:2;3:2-3;11:43;Hosea 13:15;Micah 6:10;Matthew 2:11;6:19-21;12:35;13:44,52;19:21;Mark 10:21;Luke 6:45;12:21,33-34;18:22;Acts 8:27;Romans 2:5;2 Corinthians 4:7;Colossians 2:3;Hebrews 11:26;James 5:3 (98 verses)
>>> p(_)
Genesis 43:23 And he said, Peace be to you, fear not: your God, and the God of your father, hath given you treasure in your sacks: I had your money. And he brought Simeon out unto them.
Exodus 1:11 Therefore they did set over them taskmasters to afflict them with their burdens. And they built for Pharaoh treasure cities, Pithom and Raamses.
Exodus 19:5 Now therefore, if ye will obey my voice indeed, and keep my covenant, then ye shall be a peculiar treasure unto me above all people: for all the earth is mine:
Deuteronomy 28:12 The LORD shall open unto thee his good treasure, the heaven to give the rain unto thy land in his season, and to bless all the work of thine hand: and thou shalt lend unto many nations, and thou shalt not borrow.
Deuteronomy 32:34 Is not this laid up in store with me, and sealed up among my treasures?
Deuteronomy 33:19 They shall call the people unto the mountain; there they shall offer sacrifices of righteousness: for they shall suck of the abundance of the seas, and of treasures hid in the sand.
1 Kings 7:51 So was ended all the work that king Solomon made for the house of the LORD. And Solomon brought in the things which David his father had dedicated; even the silver, and the gold, and the vessels, did he put among the treasures of the house of the LORD.
1 Kings 14:26 And he took away the treasures of the house of the LORD, and the treasures of the king's house; he even took away all: and he took away all the shields of gold which Solomon had made.
1 Kings 15:18 Then Asa took all the silver and the gold that were left in the treasures of the house of the LORD, and the treasures of the king's house, and delivered them into the hand of his servants: and king Asa sent them to Benhadad, the son of Tabrimon, the son of Hezion, king of Syria, that dwelt at Damascus, saying,
2 Kings 12:18 And Jehoash king of Judah took all the hallowed things that Jehoshaphat, and Jehoram, and Ahaziah, his fathers, kings of Judah, had dedicated, and his own hallowed things, and all the gold that was found in the treasures of the house of the LORD, and in the king's house, and sent it to Hazael king of Syria: and he went away from Jerusalem.
2 Kings 14:14 And he took all the gold and silver, and all the vessels that were found in the house of the LORD, and in the treasures of the king's house, and hostages, and returned to Samaria.
2 Kings 16:8 And Ahaz took the silver and gold that was found in the house of the LORD, and in the treasures of the king's house, and sent it for a present to the king of Assyria.
2 Kings 18:15 And Hezekiah gave him all the silver that was found in the house of the LORD, and in the treasures of the king's house.
2 Kings 20:13 And Hezekiah hearkened unto them, and shewed them all the house of his precious things, the silver, and the gold, and the spices, and the precious ointment, and all the house of his armour, and all that was found in his treasures: there was nothing in his house, nor in all his dominion, that Hezekiah shewed them not.
2 Kings 20:15 And he said, What have they seen in thine house? And Hezekiah answered, All the things that are in mine house have they seen: there is nothing among my treasures that I have not shewed them.
2 Kings 24:13 And he carried out thence all the treasures of the house of the LORD, and the treasures of the king's house, and cut in pieces all the vessels of gold which Solomon king of Israel had made in the temple of the LORD, as the LORD had said.
1 Chronicles 26
20 And of the Levites, Ahijah was over the treasures of the house of God, and over the treasures of the dedicated things.
22 The sons of Jehieli; Zetham, and Joel his brother, which were over the treasures of the house of the LORD.
24 And Shebuel the son of Gershom, the son of Moses, was ruler of the treasures.
26 Which Shelomith and his brethren were over all the treasures of the dedicated things, which David the king, and the chief fathers, the captains over thousands and hundreds, and the captains of the host, had dedicated.
1 Chronicles 27:25 And over the king's treasures was Azmaveth the son of Adiel: and over the storehouses in the fields, in the cities, and in the villages, and in the castles, was Jehonathan the son of Uzziah:
1 Chronicles 29:8 And they with whom precious stones were found gave them to the treasure of the house of the LORD, by the hand of Jehiel the Gershonite.
2 Chronicles 5:1 Thus all the work that Solomon made for the house of the LORD was finished: and Solomon brought in all the things that David his father had dedicated; and the silver, and the gold, and all the instruments, put he among the treasures of the house of God.
2 Chronicles 8:15 And they departed not from the commandment of the king unto the priests and Levites concerning any matter, or concerning the treasures.
2 Chronicles 12:9 So Shishak king of Egypt came up against Jerusalem, and took away the treasures of the house of the LORD, and the treasures of the king's house; he took all: he carried away also the shields of gold which Solomon had made.
2 Chronicles 16:2 Then Asa brought out silver and gold out of the treasures of the house of the LORD and of the king's house, and sent to Benhadad king of Syria, that dwelt at Damascus, saying,
2 Chronicles 25:24 And he took all the gold and the silver, and all the vessels that were found in the house of God with Obededom, and the treasures of the king's house, the hostages also, and returned to Samaria.
2 Chronicles 36:18 And all the vessels of the house of God, great and small, and the treasures of the house of the LORD, and the treasures of the king, and of his princes; all these he brought to Babylon.
Ezra 1:8 Even those did Cyrus king of Persia bring forth by the hand of Mithredath the treasurer, and numbered them unto Sheshbazzar, the prince of Judah.
Ezra 2:69 They gave after their ability unto the treasure of the work threescore and one thousand drams of gold, and five thousand pound of silver, and one hundred priests' garments.
Ezra 5:17 Now therefore, if it seem good to the king, let there be search made in the king's treasure house, which is there at Babylon, whether it be so, that a decree was made of Cyrus the king to build this house of God at Jerusalem, and let the king send his pleasure to us concerning this matter.
Ezra 6:1 Then Darius the king made a decree, and search was made in the house of the rolls, where the treasures were laid up in Babylon.
Ezra 7:20 And whatsoever more shall be needful for the house of thy God, which thou shalt have occasion to bestow, bestow it out of the king's treasure house.
Ezra 7:21 And I, even I Artaxerxes the king, do make a decree to all the treasurers which are beyond the river, that whatsoever Ezra the priest, the scribe of the law of the God of heaven, shall require of you, it be done speedily,
Nehemiah 7:70 And some of the chief of the fathers gave unto the work. The Tirshatha gave to the treasure a thousand drams of gold, fifty basons, five hundred and thirty priests' garments.
Nehemiah 7:71 And some of the chief of the fathers gave to the treasure of the work twenty thousand drams of gold, and two thousand and two hundred pound of silver.
Nehemiah 10:38 And the priest the son of Aaron shall be with the Levites, when the Levites take tithes: and the Levites shall bring up the tithe of the tithes unto the house of our God, to the chambers, into the treasure house.
Nehemiah 12:44 And at that time were some appointed over the chambers for the treasures, for the offerings, for the firstfruits, and for the tithes, to gather into them out of the fields of the cities the portions of the law for the priests and Levites: for Judah rejoiced for the priests and for the Levites that waited.
Nehemiah 13:13 And I made treasurers over the treasuries, Shelemiah the priest, and Zadok the scribe, and of the Levites, Pedaiah: and next to them was Hanan the son of Zaccur, the son of Mattaniah: for they were counted faithful, and their office was to distribute unto their brethren.
Job 3:21 Which long for death, but it cometh not; and dig for it more than for hid treasures;
Job 38:22 Hast thou entered into the treasures of the snow? or hast thou seen the treasures of the hail,
Psalms 17:14 From men which are thy hand, O LORD, from men of the world, which have their portion in this life, and whose belly thou fillest with thy hid treasure: they are full of children, and leave the rest of their substance to their babes.
Psalms 135:4 For the LORD hath chosen Jacob unto himself, and Israel for his peculiar treasure.
Proverbs 2:4 If thou seekest her as silver, and searchest for her as for hid treasures;
Proverbs 8:21 That I may cause those that love me to inherit substance; and I will fill their treasures.
Proverbs 10:2 Treasures of wickedness profit nothing: but righteousness delivereth from death.
Proverbs 15:6 In the house of the righteous is much treasure: but in the revenues of the wicked is trouble.
Proverbs 15:16 Better is little with the fear of the LORD than great treasure and trouble therewith.
Proverbs 21:6 The getting of treasures by a lying tongue is a vanity tossed to and fro of them that seek death.
Proverbs 21:20 There is treasure to be desired and oil in the dwelling of the wise; but a foolish man spendeth it up.
Ecclesiastes 2:8 I gathered me also silver and gold, and the peculiar treasure of kings and of the provinces: I gat me men singers and women singers, and the delights of the sons of men, as musical instruments, and that of all sorts.
Isaiah 2:7 Their land also is full of silver and gold, neither is there any end of their treasures; their land is also full of horses, neither is there any end of their chariots:
Isaiah 10:13 For he saith, By the strength of my hand I have done it, and by my wisdom; for I am prudent: and I have removed the bounds of the people, and have robbed their treasures, and I have put down the inhabitants like a valiant man:
Isaiah 22:15 Thus saith the Lord GOD of hosts, Go, get thee unto this treasurer, even unto Shebna, which is over the house, and say,
Isaiah 23:18 And her merchandise and her hire shall be holiness to the LORD: it shall not be treasured nor laid up; for her merchandise shall be for them that dwell before the LORD, to eat sufficiently, and for durable clothing.
Isaiah 30:6 The burden of the beasts of the south: into the land of trouble and anguish, from whence come the young and old lion, the viper and fiery flying serpent, they will carry their riches upon the shoulders of young asses, and their treasures upon the bunches of camels, to a people that shall not profit them.
Isaiah 33:6 And wisdom and knowledge shall be the stability of thy times, and strength of salvation: the fear of the LORD is his treasure.
Isaiah 39:2 And Hezekiah was glad of them, and shewed them the house of his precious things, the silver, and the gold, and the spices, and the precious ointment, and all the house of his armour, and all that was found in his treasures: there was nothing in his house, nor in all his dominion, that Hezekiah shewed them not.
Isaiah 39:4 Then said he, What have they seen in thine house? And Hezekiah answered, All that is in mine house have they seen: there is nothing among my treasures that I have not shewed them.
Isaiah 45:3 And I will give thee the treasures of darkness, and hidden riches of secret places, that thou mayest know that I, the LORD, which call thee by thy name, am the God of Israel.
Jeremiah 10:13 When he uttereth his voice, there is a multitude of waters in the heavens, and he causeth the vapours to ascend from the ends of the earth; he maketh lightnings with rain, and bringeth forth the wind out of his treasures.
Jeremiah 15:13 Thy substance and thy treasures will I give to the spoil without price, and that for all thy sins, even in all thy borders.
Jeremiah 17:3 O my mountain in the field, I will give thy substance and all thy treasures to the spoil, and thy high places for sin, throughout all thy borders.
Jeremiah 20:5 Moreover I will deliver all the strength of this city, and all the labours thereof, and all the precious things thereof, and all the treasures of the kings of Judah will I give into the hand of their enemies, which shall spoil them, and take them, and carry them to Babylon.
Jeremiah 41:8 But ten men were found among them that said unto Ishmael, Slay us not: for we have treasures in the field, of wheat, and of barley, and of oil, and of honey. So he forbare, and slew them not among their brethren.
Jeremiah 48:7 For because thou hast trusted in thy works and in thy treasures, thou shalt also be taken: and Chemosh shall go forth into captivity with his priests and his princes together.
Jeremiah 49:4 Wherefore gloriest thou in the valleys, thy flowing valley, O backsliding daughter? that trusted in her treasures, saying, Who shall come unto me?
Jeremiah 50:37 A sword is upon their horses, and upon their chariots, and upon all the mingled people that are in the midst of her; and they shall become as women: a sword is upon her treasures; and they shall be robbed.
Jeremiah 51:13 O thou that dwellest upon many waters, abundant in treasures, thine end is come, and the measure of thy covetousness.
Jeremiah 51:16 When he uttereth his voice, there is a multitude of waters in the heavens; and he causeth the vapours to ascend from the ends of the earth: he maketh lightnings with rain, and bringeth forth the wind out of his treasures.
Ezekiel 22:25 There is a conspiracy of her prophets in the midst thereof, like a roaring lion ravening the prey; they have devoured souls; they have taken the treasure and precious things; they have made her many widows in the midst thereof.
Ezekiel 28:4 With thy wisdom and with thine understanding thou hast gotten thee riches, and hast gotten gold and silver into thy treasures:
Daniel 1:2 And the Lord gave Jehoiakim king of Judah into his hand, with part of the vessels of the house of God: which he carried into the land of Shinar to the house of his god; and he brought the vessels into the treasure house of his god.
Daniel 3:2 Then Nebuchadnezzar the king sent to gather together the princes, the governors, and the captains, the judges, the treasurers, the counsellors, the sheriffs, and all the rulers of the provinces, to come to the dedication of the image which Nebuchadnezzar the king had set up.
Daniel 3:3 Then the princes, the governors, and captains, the judges, the treasurers, the counsellors, the sheriffs, and all the rulers of the provinces, were gathered together unto the dedication of the image that Nebuchadnezzar the king had set up; and they stood before the image that Nebuchadnezzar had set up.
Daniel 11:43 But he shall have power over the treasures of gold and of silver, and over all the precious things of Egypt: and the Libyans and the Ethiopians shall be at his steps.
Hosea 13:15 Though he be fruitful among his brethren, an east wind shall come, the wind of the LORD shall come up from the wilderness, and his spring shall become dry, and his fountain shall be dried up: he shall spoil the treasure of all pleasant vessels.
Micah 6:10 Are there yet the treasures of wickedness in the house of the wicked, and the scant measure that is abominable?
Matthew 2:11 And when they were come into the house, they saw the young child with Mary his mother, and fell down, and worshipped him: and when they had opened their treasures, they presented unto him gifts; gold, and frankincense and myrrh.
Matthew 6
19 Lay not up for yourselves treasures upon earth, where moth and rust doth corrupt, and where thieves break through and steal:
20 But lay up for yourselves treasures in heaven, where neither moth nor rust doth corrupt, and where thieves do not break through nor steal:
21 For where your treasure is, there will your heart be also.
Matthew 12:35 A good man out of the good treasure of the heart bringeth forth good things: and an evil man out of the evil treasure bringeth forth evil things.
Matthew 13:44 Again, the kingdom of heaven is like unto treasure hid in a field; the which when a man hath found, he hideth, and for joy thereof goeth and selleth all that he hath, and buyeth that field.
Matthew 13:52 Then said he unto them, Therefore every scribe which is instructed unto the kingdom of heaven is like unto a man that is an householder, which bringeth forth out of his treasure things new and old.
Matthew 19:21 Jesus said unto him, If thou wilt be perfect, go and sell that thou hast, and give to the poor, and thou shalt have treasure in heaven: and come and follow me.
Mark 10:21 Then Jesus beholding him loved him, and said unto him, One thing thou lackest: go thy way, sell whatsoever thou hast, and give to the poor, and thou shalt have treasure in heaven: and come, take up the cross, and follow me.
Luke 6:45 A good man out of the good treasure of his heart bringeth forth that which is good; and an evil man out of the evil treasure of his heart bringeth forth that which is evil: for of the abundance of the heart his mouth speaketh.
Luke 12
21 So is he that layeth up treasure for himself, and is not rich toward God.
33 Sell that ye have, and give alms; provide yourselves bags which wax not old, a treasure in the heavens that faileth not, where no thief approacheth, neither moth corrupteth.
34 For where your treasure is, there will your heart be also.
Luke 18:22 Now when Jesus heard these things, he said unto him, Yet lackest thou one thing: sell all that thou hast, and distribute unto the poor, and thou shalt have treasure in heaven: and come, follow me.
Acts 8:27 And he arose and went: and, behold, a man of Ethiopia, an eunuch of great authority under Candace queen of the Ethiopians, who had the charge of all her treasure, and had come to Jerusalem for to worship,
Romans 2:5 But after thy hardness and impenitent heart treasurest up unto thyself wrath against the day of wrath and revelation of the righteous judgment of God;
2 Corinthians 4:7 But we have this treasure in earthen vessels, that the excellency of the power may be of God, and not of us.
Colossians 2:3 In whom are hid all the treasures of wisdom and knowledge.
Hebrews 11:26 Esteeming the reproach of Christ greater riches than the treasures in Egypt: for he had respect unto the recompence of the reward.
James 5:3 Your gold and silver is cankered; and the rust of them shall be a witness against you, and shall eat your flesh as it were fire. Ye have heaped treasure together for the last days.
>>> b.count('treasure[srd]')
69
>>> 
>>> 
