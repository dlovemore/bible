>>> from parle import *
>>> globals()+math
auto.Importer: Not importing e from math
auto.Importer: Not importing pow from math
<auto.auto.Importer.GWrap object at 0xb64ed430>
>>> log(23)-pi
-0.006098437660643441
>>> log(23)
3.1354942159291497
>>> log(23)*365/pi
364.2914645558547
>>> log(23)-pi
-0.006098437660643441
>>> _*24
-0.14636250385544258
>>> _*60
-8.781750231326555
>>> 75/4
18.75
>>> 71+16/4
75.0
>>> 71+16
87
>>> 87/4
21.75
>>> 20.6060606
20.6060606
>>> 20.6060606*71/4
365.75757565
>>> 20.6060606/87
0.2368512712643678
>>> _*2.54
0.6016022290114943
>>> 20.6060606/87*2.542
0.602075931554023
>>> 20.6/87*2.542
0.6018988505747126
>>> 20.601/87*2.542
0.6019280689655171
>>> 20.6060606/87*2.542
0.602075931554023
>>> 20.6060606/87*2.543
0.6023127828252873
>>> 20.6060606/87*2.55
0.6039707417241379
>>> 20.6060606/87*2.56
0.6063392544367816
>>> 20.6060606/74*2.56
0.7128583126486487
>>> 20.6060606/74
0.2784602783783784
>>> 1/_
3.5911764716444634
>>> 20.6/71*4
1.1605633802816901
>>> _-1
0.16056338028169015
>>> 1/_
6.228070175438596
>>> log
<built-in function log>
>>> log(23+(1/7))
3.1416861861770706
>>> log(23)
3.1354942159291497
>>> fr=fractions.Fraction
>>> fr(1,7)+23
Fraction(162, 7)
>>> log(_)
3.1416861861770706
>>> log(163/7)
3.147840051751449
>>> log(162/7)
3.1416861861770706
>>> log(20+pi)
3.1416315462592053
>>> log(20+22/7))
  File "<stdin>", line 1
    log(20+22/7))
                ^
SyntaxError: invalid syntax
>>> log(20+22/7)
3.1416861861770706
>>> log(23)
3.1354942159291497
>>> log(pi)
1.1447298858494002
>>> exp(pi)
23.140692632779267
>>> 23+14
37
>>> log(23.14)
3.1415627217655304
>>> b[23]
Isaiah 1:1-66:24 (1292 verses)
>>> b[23][14]
Isaiah 14:1-32 (32 verses)
>>> p(_)
Isaiah 14
1 For the LORD will have mercy on Jacob, and will yet choose Israel, and set them in their own land: and the strangers shall be joined with them, and they shall cleave to the house of Jacob.
2 And the people shall take them, and bring them to their place: and the house of Israel shall possess them in the land of the LORD for servants and handmaids: and they shall take them captives, whose captives they were; and they shall rule over their oppressors.
3 And it shall come to pass in the day that the LORD shall give thee rest from thy sorrow, and from thy fear, and from the hard bondage wherein thou wast made to serve,
4 That thou shalt take up this proverb against the king of Babylon, and say, How hath the oppressor ceased! the golden city ceased!
5 The LORD hath broken the staff of the wicked, and the sceptre of the rulers.
6 He who smote the people in wrath with a continual stroke, he that ruled the nations in anger, is persecuted, and none hindereth.
7 The whole earth is at rest, and is quiet: they break forth into singing.
8 Yea, the fir trees rejoice at thee, and the cedars of Lebanon, saying, Since thou art laid down, no feller is come up against us.
9 Hell from beneath is moved for thee to meet thee at thy coming: it stirreth up the dead for thee, even all the chief ones of the earth; it hath raised up from their thrones all the kings of the nations.
10 All they shall speak and say unto thee, Art thou also become weak as we? art thou become like unto us?
11 Thy pomp is brought down to the grave, and the noise of thy viols: the worm is spread under thee, and the worms cover thee.
12 How art thou fallen from heaven, O Lucifer, son of the morning! how art thou cut down to the ground, which didst weaken the nations!
13 For thou hast said in thine heart, I will ascend into heaven, I will exalt my throne above the stars of God: I will sit also upon the mount of the congregation, in the sides of the north:
14 I will ascend above the heights of the clouds; I will be like the most High.
15 Yet thou shalt be brought down to hell, to the sides of the pit.
16 They that see thee shall narrowly look upon thee, and consider thee, saying, Is this the man that made the earth to tremble, that did shake kingdoms;
17 That made the world as a wilderness, and destroyed the cities thereof; that opened not the house of his prisoners?
18 All the kings of the nations, even all of them, lie in glory, every one in his own house.
19 But thou art cast out of thy grave like an abominable branch, and as the raiment of those that are slain, thrust through with a sword, that go down to the stones of the pit; as a carcase trodden under feet.
20 Thou shalt not be joined with them in burial, because thou hast destroyed thy land, and slain thy people: the seed of evildoers shall never be renowned.
21 Prepare slaughter for his children for the iniquity of their fathers; that they do not rise, nor possess the land, nor fill the face of the world with cities.
22 For I will rise up against them, saith the LORD of hosts, and cut off from Babylon the name, and remnant, and son, and nephew, saith the LORD.
23 I will also make it a possession for the bittern, and pools of water: and I will sweep it with the besom of destruction, saith the LORD of hosts.
24 The LORD of hosts hath sworn, saying, Surely as I have thought, so shall it come to pass; and as I have purposed, so shall it stand:
25 That I will break the Assyrian in my land, and upon my mountains tread him under foot: then shall his yoke depart from off them, and his burden depart from off their shoulders.
26 This is the purpose that is purposed upon the whole earth: and this is the hand that is stretched out upon all the nations.
27 For the LORD of hosts hath purposed, and who shall disannul it? and his hand is stretched out, and who shall turn it back?
28 In the year that king Ahaz died was this burden.
29 Rejoice not thou, whole Palestina, because the rod of him that smote thee is broken: for out of the serpent's root shall come forth a cockatrice, and his fruit shall be a fiery flying serpent.
30 And the firstborn of the poor shall feed, and the needy shall lie down in safety: and I will kill thy root with famine, and he shall slay thy remnant.
31 Howl, O gate; cry, O city; thou, whole Palestina, art dissolved: for there shall come from the north a smoke, and none shall be alone in his appointed times.
32 What shall one then answer the messengers of the nation? That the LORD hath founded Zion, and the poor of his people shall trust in it.
>>> b[14]
2 Chronicles 1:1-36:23 (822 verses)
>>> exp
<built-in function exp>
>>> exp(exp(1)+69/163)
23.140740263427816
>>> exp(1)+69/163
3.141594711894628
>>> (exp(1)+69/163)-pi
2.058304835017566e-06
>>> fr(69/163)
Fraction(1906431744101621, 4503599627370496)
>>> fr(69,163)
Fraction(69, 163)
>>> divmod(163,69)
(2, 25)
>>> fbase(23,pi)
<generator object fbase at 0xb58fb7b0>
>>> firstn(5,_)
[3, 3, 5, 20, 17]
>>> 3/23
0.13043478260869565
>>> 5/23
0.21739130434782608
>>> 5/23**2
0.00945179584120983
>>> Genesis[1:1].text()/op.eq('e',...)
'eeeeeeeee'
>>> Genesis[1:1].text()/op.eq('e',...)*len
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't multiply sequence by non-int of type 'builtin_function_or_method'
>>> Genesis[1:1].text()/op.eq('e',...)*F(len)
9
>>> 9*5
45
>>> np(23)
9
>>> np(163)
38
>>> pi-exp(1)
0.423310825130748
>>> 69/163
0.4233128834355828
>>> np(163)
38
>>> 5**3/4**3
1.953125
>>> 5**3
125
>>> 4**3
64
>>> gcd
<built-in function gcd>
>>> gcd(2143,22)
1
>>> divmod(2143,22)
(97, 9)
>>> pi**4
97.40909103400242
>>> 9/22
0.4090909090909091
>>> (9-exp(1))/2
3.1408590857704777
>>> divmod(227,23)
(9, 20)
>>> pi**3
31.006276680299816
>>> pi**3-31
0.006276680299816206
>>> 1/_
159.31988762105377
>>> _-159
0.3198876210537662
>>> 1/_
3.1260978361895457
>>> 159*3
477
>>> 478/3
159.33333333333334
>>> 478*31
14818
>>> 478*31+3
14821
>>> 14821/478
31.006276150627617
>>> 14821/478-pi
27.864683497037824
>>> 14821/478-pi**3
-5.296721994341169e-07
>>> ns(478)
[2, 239] [1, 52]
>>> divmod(478,23)
(20, 18)
>>> divmod(478,22)
(21, 16)
>>> divmod(478,23)
(20, 18)
>>> 14820/478-pi**3
-0.002092579881406209
>>> 14821/478-pi**3
-5.296721994341169e-07
>>> int(3.9)
3
>>> [int(pi**i) for i in span(0,5)]
[1, 3, 9, 31, 97, 306]
>>> [int(pi**i) for i in span(0,7)]
[1, 3, 9, 31, 97, 306, 961, 3020]
>>> [int(pi**i) for i in span(0,10)]
[1, 3, 9, 31, 97, 306, 961, 3020, 9488, 29809, 93648]
>>> [int(pi**i) for i in span(0,30)]
[1, 3, 9, 31, 97, 306, 961, 3020, 9488, 29809, 93648, 294204, 924269, 2903677, 9122171, 28658145, 90032220, 282844563, 888582403, 2791563949, 8769956796, 27551631842, 86556004191, 271923706893, 854273519913, 2683779414317, 8431341691876, 26487841119103, 83214007069229, 261424513284460, 821289330402748]
>>> sqrt(961)
31.0
>>> 31**(7/2)
165869.26813306918
>>> 31**(2/7)
2.667493323489792
>>> pi**7
3020.2932277767914
>>> pi**6
961.3891935753043
>>> pi**3
31.006276680299816
>>> _*_
961.3891935753041
>>> prod(span(6))
720
>>> prod(span(7))
5040
>>> .98368*71
69.84128
>>> .98368*71/70
0.9977325714285714
>>> .98368*72/71
0.997534647887324
>>> 72/4
18.0
>>> 2.54/2.542
0.999213217938631
>>> 20.61/17.75
1.1611267605633802
>>> _*2.54
2.949261971830986
>>> _*12
35.39114366197183
>>> 35.4/36
0.9833333333333333
>>> 71/72
0.9861111111111112
>>> 1/71
0.014084507042253521
>>> 70/71
0.9859154929577465
>>> 515/300
1.7166666666666666
>>> 300/515
0.5825242718446602
>>> 515/300
1.7166666666666666
>>> c
299792458
>>> 365.25*24*3600
31557600.0
>>> 365.25*24*3600/c
0.10526482290625203
>>> 365.24*24*3600/c
0.10526194091246951
>>> 365.25/355
1.0288732394366198
>>> 355*1.03
365.65000000000003
>>> 355*1.028
364.94
>>> 355*1.031
366.005
>>> 355*1.029
365.29499999999996
>>> 355*1.0287
365.1885
>>> 355*1.0289
365.25949999999995
>>> 355*1.0289
365.25949999999995
>>> 27/28
0.9642857142857143
>>> 355*1.0289642
365.28229100000004
>>> 1/.035999
27.778549404150112
>>> 137*.035999
4.931863000000001
>>> span(10)@mul*137
137 274 411 548 685 822 959 1096 1233 1370
>>> b[:137].tells()
Righteous art thou, O  LORD, and upright are thy judgments.  =
    9      3    4   1    4    3     7     3   3      9       46
   122     39   64  15   49   19    99    24  53    113     597
   779    291  568  60  184   55   684    96 908    716     4341
>>> bases(137)
2 [1, 0, 0, 0, 1, 0, 0, 1]
3 [1, 2, 0, 0, 2]
4 [2, 0, 2, 1]
5 [1, 0, 2, 2]
6 [3, 4, 5]
7 [2, 5, 4]
8 [2, 1, 1]
9 [1, 6, 2]
10 [1, 3, 7]
11 [1, 1, 5]
12 [11, 5]
13 [10, 7]
14 [9, 11]
15 [9, 2]
16 [8, 9]
17 [8, 1]
18 [7, 11]
19 [7, 4]
20 [6, 17]
21 [6, 11]
22 [6, 5]
23 [5, 22]
24 [5, 17]
25 [5, 12]
26 [5, 7]
27 [5, 2]
28 [4, 25]
29 [4, 21]
30 [4, 17]
31 [4, 13]
32 [4, 9]
33 [4, 5]
34 [4, 1]
35 [3, 32]
36 [3, 29]
37 [3, 26]
38 [3, 23]
39 [3, 20]
40 [3, 17]
>>> c/254
1180285.2677165354
>>> c/254/1000
1180.2852677165354
>>> _*pi*2
7415.951052397059
>>> 6371*2*pi
40030.173592041145
>>> _/24
1667.923899668381
>>> c/254*44
51932551.77952756
>>> c/254*44/1000
51932.55177952756
>>> c/254/1000
1180.2852677165354
>>> 6371/1180
5.399152542372882
>>> 6371*2*pi
40030.173592041145
>>> 6371*2*pi/1180
33.923875925458596
>>> acos(34/340
... )
1.4706289056333368
>>> acos(34/340)/pi*360
168.52165904546644
>>> acos(34/34)/pi*360
0.0
>>> acos(33/34)/pi*360
27.861109124715313
>>> acos(24/34)/pi*360
90.19825568805459
>>> acos(24/34)/pi*180
45.099127844027294
>>> acos(17/34)/pi*180
60.00000000000001
>>> acos(24/34)/pi*180
45.099127844027294
>>> 6371/1180
5.399152542372882
>>> 6371/1180/2**.5
3.8177773753724527
>>> 6371*2*pi/1180
33.923875925458596
>>> 6371*2*pi/1180/2**.5
23.987802711022837
>>> int('b3',12)
135
>>> 137**3
2571353
>>> 
