>>> globals()+math
auto.Importer: Not importing e from math
auto.Importer: Not importing pow from math
<auto.auto.Importer.GWrap object at 0xb6236090>
>>> log(23.14)
3.1415627217655304
>>> pi
3.141592653589793
>>> log(20+pi)
3.1416315462592053
>>> 11+23
34
>>> 23-11
12
>>> 2300/355
6.47887323943662
>>> 6*355
2130
>>> 2300-2130
170
>>> 59*3
177
>>> 59*3+4
181
>>> 2314-2130-177
7
>>> b/'seventh'/'day'/'month'
Genesis 8:4;Leviticus 16:29;23:24,27,34,39,41;25:9;Numbers 29:1,7,12;2 Kings 25:8;2 Chronicles 7:10;Ezra 3:6;Nehemiah 8:2;Ezekiel 20:1;30:20;45:20,25;Haggai 2:1 (20 verses)
>>> p(_)
Genesis 8:4 And the ark rested in the seventh month, on the seventeenth day of the month, upon the mountains of Ararat.
Leviticus 16:29 And this shall be a statute for ever unto you: that in the seventh month, on the tenth day of the month, ye shall afflict your souls, and do no work at all, whether it be one of your own country, or a stranger that sojourneth among you:
Leviticus 23
24 Speak unto the children of Israel, saying, In the seventh month, in the first day of the month, shall ye have a sabbath, a memorial of blowing of trumpets, an holy convocation.
27 Also on the tenth day of this seventh month there shall be a day of atonement: it shall be an holy convocation unto you; and ye shall afflict your souls, and offer an offering made by fire unto the LORD.
34 Speak unto the children of Israel, saying, The fifteenth day of this seventh month shall be the feast of tabernacles for seven days unto the LORD.
39 Also in the fifteenth day of the seventh month, when ye have gathered in the fruit of the land, ye shall keep a feast unto the LORD seven days: on the first day shall be a sabbath, and on the eighth day shall be a sabbath.
41 And ye shall keep it a feast unto the LORD seven days in the year. It shall be a statute for ever in your generations: ye shall celebrate it in the seventh month.
Leviticus 25:9 Then shalt thou cause the trumpet of the jubile to sound on the tenth day of the seventh month, in the day of atonement shall ye make the trumpet sound throughout all your land.
Numbers 29
1 And in the seventh month, on the first day of the month, ye shall have an holy convocation; ye shall do no servile work: it is a day of blowing the trumpets unto you.
7 And ye shall have on the tenth day of this seventh month an holy convocation; and ye shall afflict your souls: ye shall not do any work therein:
12 And on the fifteenth day of the seventh month ye shall have an holy convocation; ye shall do no servile work, and ye shall keep a feast unto the LORD seven days:
2 Kings 25:8 And in the fifth month, on the seventh day of the month, which is the nineteenth year of king Nebuchadnezzar king of Babylon, came Nebuzaradan, captain of the guard, a servant of the king of Babylon, unto Jerusalem:
2 Chronicles 7:10 And on the three and twentieth day of the seventh month he sent the people away into their tents, glad and merry in heart for the goodness that the LORD had shewed unto David, and to Solomon, and to Israel his people.
Ezra 3:6 From the first day of the seventh month began they to offer burnt offerings unto the LORD. But the foundation of the temple of the LORD was not yet laid.
Nehemiah 8:2 And Ezra the priest brought the law before the congregation both of men and women, and all that could hear with understanding, upon the first day of the seventh month.
Ezekiel 20:1 And it came to pass in the seventh year, in the fifth month, the tenth day of the month, that certain of the elders of Israel came to enquire of the LORD, and sat before me.
Ezekiel 30:20 And it came to pass in the eleventh year, in the first month, in the seventh day of the month, that the word of the LORD came unto me, saying,
Ezekiel 45:20 And so thou shalt do the seventh day of the month for every one that erreth, and for him that is simple: so shall ye reconcile the house.
Ezekiel 45:25 In the seventh month, in the fifteenth day of the month, shall he do the like in the feast of the seven days, according to the sin offering, according to the burnt offering, and according to the meat offering, and according to the oil.
Haggai 2:1 In the seventh month, in the one and twentieth day of the month, came the word of the LORD by the prophet Haggai, saying,
>>> log(16.14+7)
3.1415627217655304
>>> b.count('log')
5
>>> exp(1)+69/163
3.141594711894628
>>> exp(1)+69/163-pi
2.058304835017566e-06
>>> int('137',12)
187
>>> 187-33
154
>>> 187-33-56
98
>>> 187-33-56-26
72
>>> 1189-187
1002
>>> pn(30)
113
>>> 

