>>> from bible import *
>>> [v for v in bible if v.count('true')>2]
[1 John 5:20 And we know that the Son of God is come, and hath given us an understanding, that we may know him that is true, and we are in him that is true, even in his Son Jesus Christ. This is the true God, and eternal life.]
>>> [v for v in bible if v.count('true')>3]
[]
>>> [v for v in bible if v.count('true')>1]
[John 19:35 And he that saw it bare record, and his record is true: and he knoweth that he saith true, that ye might believe., 1 John 2:8 Again, a new commandment I write unto you, which thing is true in him and in you: because the darkness is past, and the true light now shineth., 1 John 5:20 And we know that the Son of God is come, and hath given us an understanding, that we may know him that is true, and we are in him that is true, even in his Son Jesus Christ. This is the true God, and eternal life.]
>>> IJohn[5:20]&method.chn+method.vn
1164 30645
>>> b[62]
1 John 1:1-5:21 (105 verses)
>>> IJohn[2:8]&method.chn+method.vn
1161 30559
>>> John[19:35]&method.chn+method.vn
1016 26861
>>> (30645,30559,268610)@ns
[3, 3, 3, 5, 227] [2, 2, 2, 3, 49]
[30559] [3300]
[2, 5, 26861] [1, 3, 2946]
(None, None, None)
>>> allfactors(30645)
[1, 3, 5, 9, 15, 27, 45, 135, 227, 681, 1135, 2043, 3405, 6129, 10215, 30645]
>>> 1016*ns
[2, 2, 2, 127] [1, 1, 1, 31]
>>> (30645,30559,26861)@ns
[3, 3, 3, 5, 227] [2, 2, 2, 3, 49]
[30559] [3300]
[26861] [2946]
(None, None, None)
>>> ns(2946)
[2, 3, 491] [1, 2, 94]
>>> b.v(2946)
'And Moses took them from off their hands, and burnt them on the altar upon the burnt offering: they were consecrations for a sweet savour: it is an offering made by fire unto the LORD.'
>>> b.vi(2946)
Leviticus 8:28 And Moses took them from off their hands, and burnt them on the altar upon the burnt offering: they were consecrations for a sweet savour: it is an offering made by fire unto the LORD.
>>> b.vi(3300)
Leviticus 19:18 Thou shalt not avenge, nor bear any grudge against the children of thy people, but thou shalt love thy neighbour as thyself: I am the LORD.
>>> John[19]
John 19:1-42 (42 verses)
>>> b.vi(137)
Genesis 5:31 And all the days of Lamech were seven hundred seventy and seven years: and he died.
>>> b.ch(137)
Numbers 20:1-29 (29 verses)
>>> p(_)
Numbers 20
1 Then came the children of Israel, even the whole congregation, into the desert of Zin in the first month: and the people abode in Kadesh; and Miriam died there, and was buried there.
2 And there was no water for the congregation: and they gathered themselves together against Moses and against Aaron.
3 And the people chode with Moses, and spake, saying, Would God that we had died when our brethren died before the LORD!
4 And why have ye brought up the congregation of the LORD into this wilderness, that we and our cattle should die there?
5 And wherefore have ye made us to come up out of Egypt, to bring us in unto this evil place? it is no place of seed, or of figs, or of vines, or of pomegranates; neither is there any water to drink.
6 And Moses and Aaron went from the presence of the assembly unto the door of the tabernacle of the congregation, and they fell upon their faces: and the glory of the LORD appeared unto them.
7 And the LORD spake unto Moses, saying,
8 Take the rod, and gather thou the assembly together, thou, and Aaron thy brother, and speak ye unto the rock before their eyes; and it shall give forth his water, and thou shalt bring forth to them water out of the rock: so thou shalt give the congregation and their beasts drink.
9 And Moses took the rod from before the LORD, as he commanded him.
10 And Moses and Aaron gathered the congregation together before the rock, and he said unto them, Hear now, ye rebels; must we fetch you water out of this rock?
11 And Moses lifted up his hand, and with his rod he smote the rock twice: and the water came out abundantly, and the congregation drank, and their beasts also.
12 And the LORD spake unto Moses and Aaron, Because ye believed me not, to sanctify me in the eyes of the children of Israel, therefore ye shall not bring this congregation into the land which I have given them.
13 This is the water of Meribah; because the children of Israel strove with the LORD, and he was sanctified in them.
14 And Moses sent messengers from Kadesh unto the king of Edom, Thus saith thy brother Israel, Thou knowest all the travail that hath befallen us:
15 How our fathers went down into Egypt, and we have dwelt in Egypt a long time; and the Egyptians vexed us, and our fathers:
16 And when we cried unto the LORD, he heard our voice, and sent an angel, and hath brought us forth out of Egypt: and, behold, we are in Kadesh, a city in the uttermost of thy border:
17 Let us pass, I pray thee, through thy country: we will not pass through the fields, or through the vineyards, neither will we drink of the water of the wells: we will go by the king's high way, we will not turn to the right hand nor to the left, until we have passed thy borders.
18 And Edom said unto him, Thou shalt not pass by me, lest I come out against thee with the sword.
19 And the children of Israel said unto him, We will go by the high way: and if I and my cattle drink of thy water, then I will pay for it: I will only, without doing anything else, go through on my feet.
20 And he said, Thou shalt not go through. And Edom came out against him with much people, and with a strong hand.
21 Thus Edom refused to give Israel passage through his border: wherefore Israel turned away from him.
22 And the children of Israel, even the whole congregation, journeyed from Kadesh, and came unto mount Hor.
23 And the LORD spake unto Moses and Aaron in mount Hor, by the coast of the land of Edom, saying,
24 Aaron shall be gathered unto his people: for he shall not enter into the land which I have given unto the children of Israel, because ye rebelled against my word at the water of Meribah.
25 Take Aaron and Eleazar his son, and bring them up unto mount Hor:
26 And strip Aaron of his garments, and put them upon Eleazar his son: and Aaron shall be gathered unto his people, and shall die there.
27 And Moses did as the LORD commanded: and they went up into mount Hor in the sight of all the congregation.
28 And Moses stripped Aaron of his garments, and put them upon Eleazar his son; and Aaron died there in the top of the mount: and Moses and Eleazar came down from the mount.
29 And when all the congregation saw that Aaron was dead, they mourned for Aaron thirty days, even all the house of Israel.
>>> 859*ns
[859] [149]
>>> 149*ns
[149] [35]
>>> 2539/49
51.816326530612244
>>> 2539%49
40
>>> p(IJohn[5])
1 John 5
1 Whosoever believeth that Jesus is the Christ is born of God: and every one that loveth him that begat loveth him also that is begotten of him.
2 By this we know that we love the children of God, when we love God, and keep his commandments.
3 For this is the love of God, that we keep his commandments: and his commandments are not grievous.
4 For whatsoever is born of God overcometh the world: and this is the victory that overcometh the world, even our faith.
5 Who is he that overcometh the world, but he that believeth that Jesus is the Son of God?
6 This is he that came by water and blood, even Jesus Christ; not by water only, but by water and blood. And it is the Spirit that beareth witness, because the Spirit is truth.
7 For there are three that bear record in heaven, the Father, the Word, and the Holy Ghost: and these three are one.
8 And there are three that bear witness in earth, the Spirit, and the water, and the blood: and these three agree in one.
9 If we receive the witness of men, the witness of God is greater: for this is the witness of God which he hath testified of his Son.
10 He that believeth on the Son of God hath the witness in himself: he that believeth not God hath made him a liar; because he believeth not the record that God gave of his Son.
11 And this is the record, that God hath given to us eternal life, and this life is in his Son.
12 He that hath the Son hath life; and he that hath not the Son of God hath not life.
13 These things have I written unto you that believe on the name of the Son of God; that ye may know that ye have eternal life, and that ye may believe on the name of the Son of God.
14 And this is the confidence that we have in him, that, if we ask any thing according to his will, he heareth us:
15 And if we know that he hear us, whatsoever we ask, we know that we have the petitions that we desired of him.
16 If any man see his brother sin a sin which is not unto death, he shall ask, and he shall give him life for them that sin not unto death. There is a sin unto death: I do not say that he shall pray for it.
17 All unrighteousness is sin: and there is a sin not unto death.
18 We know that whosoever is born of God sinneth not; but he that is begotten of God keepeth himself, and that wicked one toucheth him not.
19 And we know that we are of God, and the whole world lieth in wickedness.
20 And we know that the Son of God is come, and hath given us an understanding, that we may know him that is true, and we are in him that is true, even in his Son Jesus Christ. This is the true God, and eternal life.
21 Little children, keep yourselves from idols. Amen.
>>> IJohn[5:20:21].tells()
And  we know that the Son of God  is come, and hath given  us an understanding, that  we may know him that  is true, and  we are in him that  is true, even in his Son Jesus Christ. This  is the true God, and eternal life.   =
 3   2   4    4    3   3  2   3   2    4    3   4     5    2  2        13        4    2   3   4    3   4    2    4    3   2   3  2   3   4    2    4    4   2   3   3    5      6     4    2   3   4    3    3     7      4    161
 19  28  63   49   33  48 21  26  28   36   19  37    57   40 15      150        49   28  39  63   30  49   28   64   19  28  24 23  30  49   28   64   46  23  36  48   74     77    56   28  33  64   26   19    75     32   1921
 55 505 630  409  213 210 66  71 109  108   55 217   471  400 51      870       409  505 741 630   57 409  109  595   55 505  96 59  57 409  109  595  460  59 117 210  515    410   317  109 213 595   71   55   381     50  13342
Little children, keep yourselves from idols. Amen.  =
  6        8      4       10      4     5      4    41
  78       73     37     161      52    59     33  493
 474      199    100     1790    196   203     96  3058
>>> 925*ns
[5, 5, 37] [3, 3, 12]
>>> 515*ns
[5, 103] [3, 27]
>>> 410*ns
[2, 5, 41] [1, 3, 13]
>>> [1,2,3,7,11,19,43,67,163]@np
[(0, 38873, 38872, 1, 1, 2, 1), 1, 2, 4, 5, 8, 14, 19, 38]
>>> pn(18)
61
>>> Genesis[1:1].words()@osum@mod*137
23 33 81 26 56 33 55 19 33 52
>>> Genesis[1:1].words()@osum*F(tale)@mod*137
23 56 0 26 82 115 33 52 85 0
>>> Genesis[1:1].words()@osum@F(lambda x:137-x)
[114, 104, 56, 111, 81, 104, 82, 118, 104, 85]
>>> Genesis[1:1].words()@osum*F(tale)@mod*163
23 56 137 0 56 89 144 0 33 85
>>> Genesis[1:1].words()@osum*F(tale)@mod*133
23 56 4 30 86 119 41 60 93 12
>>> Genesis[1:1].words()@osum*F(tale)@mod*122
23 56 15 41 97 8 63 82 115 45
>>> Genesis[1:1].words()@osum*F(tale)@mod*127
23 56 10 36 92 125 53 72 105 30
>>> Genesis[1:1].words()@osum*F(tale)@mod*56
23 0 25 51 51 28 27 46 23 19
>>> Genesis[1:1].words()@osum*F(tale)@mod*193
23 56 137 163 26 59 114 133 166 25
>>> Genesis[1:1].words()@osum*F(tale)@mod*23
0 10 22 2 12 22 8 4 14 20
>>> sums('god created the')
(13, 115, 592)
>>> 115/23
5.0
>>> 934*ns
[2, 467] [1, 91]
>>> 935*ns
[5, 11, 17] [3, 5, 7]
>>> b.chn(935)
<46>:1: TypeError: chn() takes 1 positional argument but 2 were given
>>> b.ch(935)
Matthew 6:1-34 (34 verses)
>>> 5*17
85
>>> 11*17
187
>>> allfactors(935)
[1, 5, 11, 17, 55, 85, 187, 935]
>>> allfactors(355)
[1, 5, 71, 355]
>>> allfactors(936)
[1, 2, 3, 4, 6, 8, 9, 12, 13, 18, 24, 26, 36, 39, 52, 72, 78, 104, 117, 156, 234, 312, 468, 936]
>>> ns(936)
[2, 2, 2, 3, 3, 13] [1, 1, 1, 2, 2, 6]
>>> 13*9
117
>>> 8*117
936
>>> b.v(3300)
'Thou shalt not avenge, nor bear any grudge against the children of thy people, but thou shalt love thy neighbour as thyself: I am the LORD.'
>>> b.v(pn(3300))
'Again, a new commandment I write unto you, which thing is true in him and in you: because the darkness is past, and the true light now shineth.'
>>> ns(2946)
[2, 3, 491] [1, 2, 94]
>>> ns(491)
[491] [94]
>>> ns(94)
[2, 47] [1, 15]
>>> divmod(2946,49)
(60, 6)
