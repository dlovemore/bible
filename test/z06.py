>>> base(7,c)
[1, 0, 3, 0, 0, 1, 2, 3, 3, 3, 0]
>>> int('10300123330'7)
  File "<stdin>", line 1
    int('10300123330'7)
                     ^
SyntaxError: invalid syntax
>>> int('10300123330',7)
299792458
>>> _==c
True
>>> base(7,c)
[1, 0, 3, 0, 0, 1, 2, 3, 3, 3, 0]
>>> 7**11
1977326743
>>> _/c
6.59565205939904
>>> 7**12
13841287201
>>> _/c
46.169564415793275
>>> 7**10
282475249
>>> 7**10+7**8
288240050
>>> 7**10+3*7**8
299769652
>>> int('12333',7)
3258
>>> _*7
22806
>>> _*ns
[2, 3, 3, 7, 181] [1, 2, 2, 4, 42]
>>> Psalm[103].lc
<bound method Sel.lc of Psalms 103:1-22 (22 verses)>
>>> Psalm[103].lc()
1444
>>> Psalm[103].wc()
342
>>> Genesis[1:1].lc()
44
>>> Genesis[1].lc()
3167
>>> Genesis[1]-Genesis[2.3]*I.lc()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 229, in __getitem__
    raise IndexError
IndexError
>>> (Genesis[1]-Genesis[2.3]).lc()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 229, in __getitem__
    raise IndexError
IndexError
>>> (Genesis[1]-Genesis[2:3]).lc()
3427
>>> (Genesis[1]-Genesis[2:3]).wc()
864
>>> 864*ns
[2, 2, 2, 2, 2, 3, 3, 3] [1, 1, 1, 1, 1, 2, 2, 2]
>>> 4*216
864
>>> (Genesis[1]-Genesis[4]).wc()
2756
>>> _*ns
[2, 2, 13, 53] [1, 1, 6, 16]
>>> _*rect
7598292
>>> _*nrect
2756
>>> _*nrect
52
>>> rect(52)
2756
>>> rect(31)
992
>>> rect(30)
930

