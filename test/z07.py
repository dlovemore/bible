>>> b.ch(848)
Ezekiel 46:1-24 (24 verses)
>>> Ezekiel
Ezekiel 1:1-48:35 (1273 verses)
>>> b.ch(848)
Ezekiel 46:1-24 (24 verses)
>>> p(_)
Ezekiel 46
1 Thus saith the Lord GOD; The gate of the inner court that looketh toward the east shall be shut the six working days; but on the sabbath it shall be opened, and in the day of the new moon it shall be opened.
2 And the prince shall enter by the way of the porch of that gate without, and shall stand by the post of the gate, and the priests shall prepare his burnt offering and his peace offerings, and he shall worship at the threshold of the gate: then he shall go forth; but the gate shall not be shut until the evening.
3 Likewise the people of the land shall worship at the door of this gate before the LORD in the sabbaths and in the new moons.
4 And the burnt offering that the prince shall offer unto the LORD in the sabbath day shall be six lambs without blemish, and a ram without blemish.
5 And the meat offering shall be an ephah for a ram, and the meat offering for the lambs as he shall be able to give, and an hin of oil to an ephah.
6 And in the day of the new moon it shall be a young bullock without blemish, and six lambs, and a ram: they shall be without blemish.
7 And he shall prepare a meat offering, an ephah for a bullock, and an ephah for a ram, and for the lambs according as his hand shall attain unto, and an hin of oil to an ephah.
8 And when the prince shall enter, he shall go in by the way of the porch of that gate, and he shall go forth by the way thereof.
9 But when the people of the land shall come before the LORD in the solemn feasts, he that entereth in by the way of the north gate to worship shall go out by the way of the south gate; and he that entereth by the way of the south gate shall go forth by the way of the north gate: he shall not return by the way of the gate whereby he came in, but shall go forth over against it.
10 And the prince in the midst of them, when they go in, shall go in; and when they go forth, shall go forth.
11 And in the feasts and in the solemnities the meat offering shall be an ephah to a bullock, and an ephah to a ram, and to the lambs as he is able to give, and an hin of oil to an ephah.
12 Now when the prince shall prepare a voluntary burnt offering or peace offerings voluntarily unto the LORD, one shall then open him the gate that looketh toward the east, and he shall prepare his burnt offering and his peace offerings, as he did on the sabbath day: then he shall go forth; and after his going forth one shall shut the gate.
13 Thou shalt daily prepare a burnt offering unto the LORD of a lamb of the first year without blemish: thou shalt prepare it every morning.
14 And thou shalt prepare a meat offering for it every morning, the sixth part of an ephah, and the third part of an hin of oil, to temper with the fine flour; a meat offering continually by a perpetual ordinance unto the LORD.
15 Thus shall they prepare the lamb, and the meat offering, and the oil, every morning for a continual burnt offering.
16 Thus saith the Lord GOD; If the prince give a gift unto any of his sons, the inheritance thereof shall be his sons'; it shall be their possession by inheritance.
17 But if he give a gift of his inheritance to one of his servants, then it shall be his to the year of liberty; after it shall return to the prince: but his inheritance shall be his sons' for them.
18 Moreover the prince shall not take of the people's inheritance by oppression, to thrust them out of their possession; but he shall give his sons inheritance out of his own possession: that my people be not scattered every man from his possession.
19 After he brought me through the entry, which was at the side of the gate, into the holy chambers of the priests, which looked toward the north: and, behold, there was a place on the two sides westward.
20 Then said he unto me, This is the place where the priests shall boil the trespass offering and the sin offering, where they shall bake the meat offering; that they bear them not out into the utter court, to sanctify the people.
21 Then he brought me forth into the utter court, and caused me to pass by the four corners of the court; and, behold, in every corner of the court there was a court.
22 In the four corners of the court there were courts joined of forty cubits long and thirty broad: these four corners were of one measure.
23 And there was a row of building round about in them, round about them four, and it was made with boiling places under the rows round about.
24 Then said he unto me, These are the places of them that boil, where the ministers of the house shall boil the sacrifice of the people.
>>> tells('angry')
a  n g  r  y   =
1  1 1  1  1   5
1 14 7 18  25  65
1 50 7 90 700 848
>>> 65+848
913
>>> b/'angry'
Genesis 18:30,32;45:5;Leviticus 10:16;Deuteronomy 1:37;4:21;9:8,20;Judges 18:25;2 Samuel 19:42;1 Kings 8:46;11:9;2 Kings 17:18;2 Chronicles 6:36;Ezra 9:14;Nehemiah 5:6;Psalms 2:12;7:11;76:7;79:5;80:4;85:5;Proverbs 14:17;21:19;22:24;25:23;29:22;Ecclesiastes 5:6;7:9;Song of Solomon 1:6;Isaiah 12:1;Ezekiel 16:42;Daniel 2:12;Jonah 4:1,4,9;Matthew 5:22;Luke 14:21;15:28;John 7:23;Ephesians 4:26;Titus 1:7;Revelation 11:18 (43 verses)
>>> b/'ang'/'shake'
1 Kings 14:15 For the LORD shall smite Israel, as a reed is shaken in the water, and he shall root up Israel out of this good land, which he gave to their fathers, and shall scatter them beyond the river, because they have made their groves, provoking the LORD to anger.
Isaiah 13:13 Therefore I will shake the heavens, and the earth shall remove out of her place, in the wrath of the LORD of hosts, and in the day of his fierce anger.
>>> 

