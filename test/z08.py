>>> b.count('Jesus')
977
>>> b.count("Jesus'")
10
>>> pn9163)
  File "<stdin>", line 1
    pn9163)
          ^
SyntaxError: invalid syntax
>>> pn(163)
967
>>> 967*74
71558
>>> 71558
71558
>>> ntri(58)
(10, 55, -3, 58, 8, 66, 11)
>>> nstar(58)
(3, 37, -21, 58, 15, 73, 4)
>>> nhex(58)
(5, 45, -13, 58, 8, 66, 6)
>>> nF(58)
(10, 55, -3, 58, 31, 89, 11)
>>> pn(15)
47
>>> pn(21)
73
>>> pn(20)
71
>>> 22+5
27
>>> tells('Authorised')
A  u   t  h  o  r i  s  e d  =
1  1   1  1  1  1 1  1  1 1  10
1  21  20 8 15 18 9  19 5 4 120
1 300 200 8 60 90 9 100 5 4 777
>>> tells('Authorized')
A  u   t  h  o  r i  z  e d   =
1  1   1  1  1  1 1  1  1 1  10
1  21  20 8 15 18 9  26 5 4  127
1 300 200 8 60 90 9 800 5 4 1477
>>> 1733+700
2433
>>> _*ns
[3, 811] [2, 141]
>>> _*nF
(17, 1597, -836, 2433, 151, 2584, 18)
>>> 1733*nF
(17, 1597, -136, 1733, 851, 2584, 18)
>>> b/'author'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 14:33;15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;Hebrews 5:9;12:2;1 Peter 3:22;Revelation 13:2 (38 verses)
>>> b/'authori'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;1 Peter 3:22;Revelation 13:2 (35 verses)
>>> b/'authori[sz]'

>>> b/'authori[szt]'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;1 Peter 3:22;Revelation 13:2 (35 verses)
>>> b/'authori'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;1 Peter 3:22;Revelation 13:2 (35 verses)
>>> b/'authority'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;Revelation 13:2 (34 verses)
>>> b/'authors'

>>> b/'author'
Esther 9:29;Proverbs 29:2;Matthew 7:29;8:9;20:25;21:23-24,27;Mark 1:22,27;10:42;11:28-29,33;13:34;Luke 4:36;7:8;9:1;19:17;20:2,8,20;22:25;John 5:27;Acts 8:27;9:14;26:10,12;1 Corinthians 14:33;15:24;2 Corinthians 10:8;1 Timothy 2:2,12;Titus 2:15;Hebrews 5:9;12:2;1 Peter 3:22;Revelation 13:2 (38 verses)
>>> p(_)
Esther 9:29 Then Esther the queen, the daughter of Abihail, and Mordecai the Jew, wrote with all authority, to confirm this second letter of Purim.
Proverbs 29:2 When the righteous are in authority, the people rejoice: but when the wicked beareth rule, the people mourn.
Matthew 7:29 For he taught them as one having authority, and not as the scribes.
Matthew 8:9 For I am a man under authority, having soldiers under me: and I say to this man, Go, and he goeth; and to another, Come, and he cometh; and to my servant, Do this, and he doeth it.
Matthew 20:25 But Jesus called them unto him, and said, Ye know that the princes of the Gentiles exercise dominion over them, and they that are great exercise authority upon them.
Matthew 21
23 And when he was come into the temple, the chief priests and the elders of the people came unto him as he was teaching, and said, By what authority doest thou these things? and who gave thee this authority?
24 And Jesus answered and said unto them, I also will ask you one thing, which if ye tell me, I in like wise will tell you by what authority I do these things.
27 And they answered Jesus, and said, We cannot tell. And he said unto them, Neither tell I you by what authority I do these things.
Mark 1:22 And they were astonished at his doctrine: for he taught them as one that had authority, and not as the scribes.
Mark 1:27 And they were all amazed, insomuch that they questioned among themselves, saying, What thing is this? what new doctrine is this? for with authority commandeth he even the unclean spirits, and they do obey him.
Mark 10:42 But Jesus called them to him, and saith unto them, Ye know that they which are accounted to rule over the Gentiles exercise lordship over them; and their great ones exercise authority upon them.
Mark 11
28 And say unto him, By what authority doest thou these things? and who gave thee this authority to do these things?
29 And Jesus answered and said unto them, I will also ask of you one question, and answer me, and I will tell you by what authority I do these things.
33 And they answered and said unto Jesus, We cannot tell. And Jesus answering saith unto them, Neither do I tell you by what authority I do these things.
Mark 13:34 For the Son of Man is as a man taking a far journey, who left his house, and gave authority to his servants, and to every man his work, and commanded the porter to watch.
Luke 4:36 And they were all amazed, and spake among themselves, saying, What a word is this! for with authority and power he commandeth the unclean spirits, and they come out.
Luke 7:8 For I also am a man set under authority, having under me soldiers, and I say unto one, Go, and he goeth; and to another, Come, and he cometh; and to my servant, Do this, and he doeth it.
Luke 9:1 Then he called his twelve disciples together, and gave them power and authority over all devils, and to cure diseases.
Luke 19:17 And he said unto him, Well, thou good servant: because thou hast been faithful in a very little, have thou authority over ten cities.
Luke 20
2 And spake unto him, saying, Tell us, by what authority doest thou these things? or who is he that gave thee this authority?
8 And Jesus said unto them, Neither tell I you by what authority I do these things.
20 And they watched him, and sent forth spies, which should feign themselves just men, that they might take hold of his words, that so they might deliver him unto the power and authority of the governor.
Luke 22:25 And he said unto them, The kings of the Gentiles exercise lordship over them; and they that exercise authority upon them are called benefactors.
John 5:27 And hath given him authority to execute judgment also, because he is the Son of man.
Acts 8:27 And he arose and went: and, behold, a man of Ethiopia, an eunuch of great authority under Candace queen of the Ethiopians, who had the charge of all her treasure, and had come to Jerusalem for to worship,
Acts 9:14 And here he hath authority from the chief priests to bind all that call on thy name.
Acts 26:10 Which thing I also did in Jerusalem: and many of the saints did I shut up in prison, having received authority from the chief priests; and when they were put to death, I gave my voice against them.
Acts 26:12 Whereupon as I went to Damascus with authority and commission from the chief priests,
1 Corinthians 14:33 For God is not the author of confusion, but of peace, as in all churches of the saints.
1 Corinthians 15:24 Then cometh the end, when he shall have delivered up the kingdom to God, even the Father; when he shall have put down all rule and all authority and power.
2 Corinthians 10:8 For though I should boast somewhat more of our authority, which the Lord hath given us for edification, and not for your destruction, I should not be ashamed:
1 Timothy 2:2 For kings, and for all that are in authority; that we may lead a quiet and peaceable life in all godliness and honesty.
1 Timothy 2:12 But I suffer not a woman to teach, nor to usurp authority over the man, but to be in silence.
Titus 2:15 These things speak, and exhort, and rebuke with all authority. Let no man despise thee.
Hebrews 5:9 And being made perfect, he became the author of eternal salvation unto all them that obey him;
Hebrews 12:2 Looking unto Jesus the author and finisher of our faith; who for the joy that was set before him endured the cross, despising the shame, and is set down at the right hand of the throne of God.
1 Peter 3:22 Who is gone into heaven, and is on the right hand of God; angels and authorities and powers being made subject unto him.
Revelation 13:2 And the beast which I saw was like unto a leopard, and his feet were as the feet of a bear, and his mouth as the mouth of a lion: and the dragon gave him his power, and his seat, and great authority.
>>> ln(23)
3.1354942159291497
>>> pi-ln(23)
0.006098437660643441
>>> exp(pi)
23.140692632779267
>>> exp(pi)-23
0.1406926327792668
>>> exp(pi)-pi
19.999099979189474
>>> pi-exp(pi)
-19.999099979189474
>>> exp(pi)-pi+20
39.999099979189474
>>> exp(pi)-pi-20
-0.0009000208105263141
>>> exp(pi)-pi
19.999099979189474
>>> c
299792458
>>> 999791894-299792458
699999436
>>> 2458-1894
564
>>> _*ns
[2, 2, 3, 47] [1, 1, 2, 15]
>>> allfactors(564)
[1, 2, 3, 4, 6, 12, 47, 94, 141, 188, 282, 564]
>>> 188*3
564
>>> tell("Jesus of Nazarareth")
Jesus of Nazarareth  =
  74  21     112    207
>>> tells("Jesus of Nazarareth")
Jesus of Nazarareth   =
  5    2     10      17
  74  21     112     207
 515  66    1246    1827
>>> tells("Jesus of Nazareth")
Jesus of Nazareth   =
  5    2     8     15
  74  21    93     188
 515  66   1155   1736
>>> bin(3088286401)
'0b10111000000100111000001011000001'
>>> 5*22+5
115
>>> tells('Jesus')
 J e  s   u   s   =
 1 1  1   1   1   5
10 5  19  21  19  74
10 5 100 300 100 515
>>> tells('Christ')
C h  r i  s   t   =
1 1  1 1  1   1   6
3 8 18 9  19  20  77
3 8 90 9 100 200 410
>>> tells('Joshua')
 J  o  s  h  u  a  =
 1  1  1  1  1  1  6
10 15  19 8  21 1  74
10 60 100 8 300 1 479
>>> 977*ns
[977] [165]
>>> 165*ns
[3, 5, 11] [2, 3, 5]
>>> 15*11
165
>>> 550+55
605
>>> 550+15
565
>>> 110+55
165
>>> tell('all')
a  l  l  =
1 12 12 25
>>> tells('all')
a  l  l  =
1  1  1  3
1 12 12 25
1 30 30 61
>>> tells('end')
e  n d  =
1  1 1  3
5 14 4 23
5 50 4 59
>>> 213+59
272
>>> 272*ns
[2, 2, 2, 2, 17] [1, 1, 1, 1, 7]
>>> 119*2
238
>>> 16*17
272
>>> tri(16)
136
>>> rect(16)
272
>>> tells('the end')
the end  =
 3   3   6
 33  23  56
213  59 272
>>> tells('in the beginning')
in the beginning  =
 2  3      9      14
23  33     81    137
59 213    189    461
>>> tells('BTAUJV')
B  T  A  U   J  V   =
1  1  1  1   1  1   6
2  20 1  21 10  22  76
2 200 1 300 10 400 913
>>> nt.chapters()*sort(...,key=method.vc)
[Revelation 15:1-8 (8 verses), 1 Thessalonians 1:1-10 (10 verses), 1 John 1:1-10 (10 verses), Revelation 4:1-11 (11 verses), Revelation 10:1-11 (11 verses), 2 Thessalonians 1:1-12 (12 verses), 1 Corinthians 5:1-13 (13 verses), 1 Corinthians 8:1-13 (13 verses), 1 Corinthians 13:1-13 (13 verses), 1 Thessalonians 3:1-13 (13 verses), Hebrews 8:1-13 (13 verses), 2 John 1:1-13 (13 verses), Revelation 8:1-13 (13 verses), Romans 13:1-14 (14 verses), 2 Corinthians 13:1-14 (14 verses), Hebrews 1:1-14 (14 verses), Hebrews 5:1-14 (14 verses), 1 Peter 5:1-14 (14 verses), 3 John 1:1-14 (14 verses), Revelation 5:1-14 (14 verses), Acts 6:1-15 (15 verses), 2 Corinthians 9:1-15 (15 verses), 1 Timothy 2:1-15 (15 verses), Titus 2:1-15 (15 verses), Titus 3:1-15 (15 verses), Revelation 20:1-15 (15 verses), 1 Corinthians 2:1-16 (16 verses), 2 Corinthians 7:1-16 (16 verses), 1 Timothy 3:1-16 (16 verses), 1 Timothy 4:1-16 (16 verses), Titus 1:1-16 (16 verses), Hebrews 4:1-16 (16 verses), Matthew 3:1-17 (17 verses), 2 Corinthians 2:1-17 (17 verses), 2 Thessalonians 2:1-17 (17 verses), 2 Timothy 3:1-17 (17 verses), James 4:1-17 (17 verses), Revelation 6:1-17 (17 verses), Revelation 7:1-17 (17 verses), Revelation 12:1-17 (17 verses), 2 Corinthians 3:1-18 (18 verses), 2 Corinthians 4:1-18 (18 verses), 2 Corinthians 6:1-18 (18 verses), 2 Corinthians 10:1-18 (18 verses), Galatians 6:1-18 (18 verses), Colossians 4:1-18 (18 verses), 1 Thessalonians 4:1-18 (18 verses), 2 Thessalonians 3:1-18 (18 verses), 2 Timothy 1:1-18 (18 verses), Hebrews 2:1-18 (18 verses), James 3:1-18 (18 verses), 2 Peter 3:1-18 (18 verses), Revelation 13:1-18 (18 verses), Revelation 17:1-18 (18 verses), Hebrews 3:1-19 (19 verses), 1 Peter 4:1-19 (19 verses), Revelation 11:1-19 (19 verses), Matthew 28:1-20 (20 verses), Mark 16:1-20 (20 verses), 1 Corinthians 6:1-20 (20 verses), 1 Thessalonians 2:1-20 (20 verses), 1 Timothy 1:1-20 (20 verses), Hebrews 6:1-20 (20 verses), James 5:1-20 (20 verses), Revelation 1:1-20 (20 verses), Revelation 14:1-20 (20 verses), Romans 5:1-21 (21 verses), Romans 10:1-21 (21 verses), Romans 12:1-21 (21 verses), 1 Corinthians 4:1-21 (21 verses), 2 Corinthians 5:1-21 (21 verses), 2 Corinthians 12:1-21 (21 verses), Galatians 2:1-21 (21 verses), Ephesians 3:1-21 (21 verses), Philippians 3:1-21 (21 verses), 1 Timothy 6:1-21 (21 verses), 2 Peter 1:1-21 (21 verses), 1 John 4:1-21 (21 verses), 1 John 5:1-21 (21 verses), Revelation 9:1-21 (21 verses), Revelation 16:1-21 (21 verses), Revelation 19:1-21 (21 verses), Revelation 22:1-21 (21 verses), Ephesians 2:1-22 (22 verses), 2 Timothy 4:1-22 (22 verses), 1 Peter 3:1-22 (22 verses), 2 Peter 2:1-22 (22 verses), Revelation 3:1-22 (22 verses), Matthew 2:1-23 (23 verses), Romans 6:1-23 (23 verses), Romans 14:1-23 (23 verses), 1 Corinthians 3:1-23 (23 verses), Ephesians 1:1-23 (23 verses), Philippians 4:1-23 (23 verses), Colossians 2:1-23 (23 verses), 1 Corinthians 16:1-24 (24 verses), 2 Corinthians 1:1-24 (24 verses), 2 Corinthians 8:1-24 (24 verses), Galatians 1:1-24 (24 verses), Ephesians 6:1-24 (24 verses), 1 John 3:1-24 (24 verses), Revelation 18:1-24 (24 verses), Matthew 1:1-25 (25 verses), Matthew 4:1-25 (25 verses), John 2:1-25 (25 verses), John 21:1-25 (25 verses), Acts 12:1-25 (25 verses), Romans 4:1-25 (25 verses), Romans 7:1-25 (25 verses), Colossians 3:1-25 (25 verses), 1 Timothy 5:1-25 (25 verses), Philemon 1:1-25 (25 verses), Hebrews 13:1-25 (25 verses), 1 Peter 1:1-25 (25 verses), 1 Peter 2:1-25 (25 verses), Jude 1:1-25 (25 verses), John 17:1-26 (26 verses), Acts 1:1-26 (26 verses), Acts 3:1-26 (26 verses), Galatians 5:1-26 (26 verses), 2 Timothy 2:1-26 (26 verses), James 2:1-26 (26 verses), Matthew 17:1-27 (27 verses), John 15:1-27 (27 verses), Acts 24:1-27 (27 verses), Acts 25:1-27 (27 verses), Romans 16:1-27 (27 verses), 1 Corinthians 9:1-27 (27 verses), James 1:1-27 (27 verses), Revelation 21:1-27 (27 verses), Matthew 16:1-28 (28 verses), Mark 2:1-28 (28 verses), Acts 14:1-28 (28 verses), Acts 18:1-28 (28 verses), 1 Thessalonians 5:1-28 (28 verses), Hebrews 7:1-28 (28 verses), Hebrews 9:1-28 (28 verses), Matthew 7:1-29 (29 verses), Romans 2:1-29 (29 verses), Galatians 3:1-29 (29 verses), Colossians 1:1-29 (29 verses), Hebrews 12:1-29 (29 verses), 1 John 2:1-29 (29 verses), Revelation 2:1-29 (29 verses), Matthew 11:1-30 (30 verses), Matthew 19:1-30 (30 verses), Acts 11:1-30 (30 verses), Acts 22:1-30 (30 verses), Philippians 1:1-30 (30 verses), Philippians 2:1-30 (30 verses), Luke 16:1-31 (31 verses), John 14:1-31 (31 verses), John 20:1-31 (31 verses), Acts 28:1-31 (31 verses), Romans 3:1-31 (31 verses), 1 Corinthians 1:1-31 (31 verses), 1 Corinthians 12:1-31 (31 verses), Galatians 4:1-31 (31 verses), Luke 15:1-32 (32 verses), Acts 26:1-32 (32 verses), Romans 1:1-32 (32 verses), Ephesians 4:1-32 (32 verses), Mark 11:1-33 (33 verses), John 16:1-33 (33 verses), Romans 9:1-33 (33 verses), Romans 15:1-33 (33 verses), 1 Corinthians 10:1-33 (33 verses), 2 Corinthians 11:1-33 (33 verses), Ephesians 5:1-33 (33 verses), Matthew 6:1-34 (34 verses), Matthew 8:1-34 (34 verses), Matthew 20:1-34 (34 verses), Acts 17:1-34 (34 verses), 1 Corinthians 11:1-34 (34 verses), Matthew 18:1-35 (35 verses), Mark 3:1-35 (35 verses), Luke 13:1-35 (35 verses), Luke 14:1-35 (35 verses), Acts 23:1-35 (35 verses), Matthew 14:1-36 (36 verses), John 3:1-36 (36 verses), Romans 11:1-36 (36 verses), Mark 7:1-37 (37 verses), Mark 13:1-37 (37 verses), Luke 17:1-37 (37 verses), Acts 4:1-37 (37 verses), Matthew 9:1-38 (38 verses), Mark 8:1-38 (38 verses), Luke 3:1-38 (38 verses), Luke 21:1-38 (38 verses), John 13:1-38 (38 verses), Acts 20:1-38 (38 verses), Matthew 15:1-39 (39 verses), Matthew 23:1-39 (39 verses), Luke 5:1-39 (39 verses), Romans 8:1-39 (39 verses), Hebrews 10:1-39 (39 verses), John 18:1-40 (40 verses), Acts 8:1-40 (40 verses), Acts 16:1-40 (40 verses), Acts 21:1-40 (40 verses), 1 Corinthians 7:1-40 (40 verses), 1 Corinthians 14:1-40 (40 verses), Hebrews 11:1-40 (40 verses), Mark 4:1-41 (41 verses), John 9:1-41 (41 verses), Acts 15:1-41 (41 verses), Acts 19:1-41 (41 verses), Matthew 10:1-42 (42 verses), Luke 10:1-42 (42 verses), John 10:1-42 (42 verses), John 19:1-42 (42 verses), Acts 5:1-42 (42 verses), Mark 5:1-43 (43 verses), Luke 18:1-43 (43 verses), Acts 9:1-43 (43 verses), Mark 12:1-44 (44 verses), Luke 4:1-44 (44 verses), Acts 27:1-44 (44 verses), Mark 1:1-45 (45 verses), Matthew 21:1-46 (46 verses), Matthew 22:1-46 (46 verses), Matthew 25:1-46 (46 verses), Mark 15:1-47 (47 verses), Luke 20:1-47 (47 verses), John 5:1-47 (47 verses), Acts 2:1-47 (47 verses), Matthew 5:1-48 (48 verses), Luke 19:1-48 (48 verses), Acts 10:1-48 (48 verses), Luke 6:1-49 (49 verses), Matthew 12:1-50 (50 verses), Mark 9:1-50 (50 verses), Luke 7:1-50 (50 verses), John 12:1-50 (50 verses), Matthew 24:1-51 (51 verses), John 1:1-51 (51 verses), Mark 10:1-52 (52 verses), Luke 2:1-52 (52 verses), Acts 13:1-52 (52 verses), Luke 24:1-53 (53 verses), John 7:1-53 (53 verses), Luke 11:1-54 (54 verses), John 4:1-54 (54 verses), Mark 6:1-56 (56 verses), Luke 8:1-56 (56 verses), Luke 23:1-56 (56 verses), John 11:1-57 (57 verses), Matthew 13:1-58 (58 verses), 1 Corinthians 15:1-58 (58 verses), Luke 12:1-59 (59 verses), John 8:1-59 (59 verses), Acts 7:1-60 (60 verses), Luke 9:1-62 (62 verses), Matthew 27:1-66 (66 verses), Luke 22:1-71 (71 verses), John 6:1-71 (71 verses), Mark 14:1-72 (72 verses), Matthew 26:1-75 (75 verses), Luke 1:1-80 (80 verses)]
>>> Luke.chn()
974
>>> Luke.chn()-929
45
>>> 28+16
44
>>> 974*ns
[2, 487] [1, 93]
>>> Luke.vn()
24895
>>> 24895-23145
1750
>>> _*ns
[2, 5, 5, 5, 7] [1, 3, 3, 3, 4]
>>> 7*125
875
>>> 14*125
1750
>>> 

