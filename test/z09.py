>>> ln(6)
1.791759469228055
>>> 49/20
2.45
>>> _-ln(6)
0.6582405307719452
>>> 363/140-ln(7)
0.6469469938018297
>>> 137/60
2.283333333333333
>>> 137/60-ln(5)
0.6738954208992329
>>> b.vi(5772)
Deuteronomy 32:13 He made him ride on the high places of the earth, that he might eat the increase of the fields; and he made him to suck honey out of the rock, and oil out of the flinty rock;
>>> ln(99)
4.59511985013459
>>> ln(388)
5.961005339623274
>>> divmod(5961,49)
(121, 32)
>>> ln(3088286401)
21.85088221124608
>>> 577*ns
[577] [106]
>>> tells('PERES')
 P E  R E  S   =
 1 1  1 1  1   5
16 5 18 5  19  63
70 5 90 5 100 270
>>> 

