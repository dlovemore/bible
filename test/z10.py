>>> b.ch(577)
Psalms 99:1-9 (9 verses)
>>> b.ch(577)[2]
Psalms 99:2 The LORD is great in Zion; and he is high above all the people.
>>> b.vi(5772)
Deuteronomy 32:13 He made him ride on the high places of the earth, that he might eat the increase of the fields; and he made him to suck honey out of the rock, and oil out of the flinty rock;
>>> _.tells()
He made him ride  on the high places of the earth, that he might eat the increase of the fields; and he made him  to suck honey out of the rock, and oil out of the flinty rock;   =
 2   4   3    4   2   3    4     6    2  3     5     4   2   5    3   3      8     2  3     6     3   2   4   3   2    4    5    3   2  3    4    3   3   3   2  3     6     4    133
13  23   30  36   29  33  32    56   21  33   52    49  13   57   26  33    74    21  33    55    19 13  23   30  35  54    67   56 21  33   47   19  36  56 21  33   86     47  1415
13  50   57  108 110 213  32    209  66 213   304   409 13  264  206 213    263   66 213   154    55 13  50   57 260  423  823  560 66 213  173   55  99 560 66 213   995   173  8030
>>> 1/0.57721566490153286060
1.7324547146006335
>>> exp(0.57721566490153286060)
1.781072417990198
>>> ln(388)
5.961005339623274
>>> ln(3088286401)
21.85088221124608
>>> pp(21)
10301
>>> pp(85)
76667
>>> pn(85)
439
>>> tells('let there be light')
let there be light  =
 3    5    2   5    15
 37   56   7   56  156
235  308   7  254  804
>>> Genesis[1:3].tells()
And God said, Let there be light: and there was light.   =
 3   3    4    3    5    2    5    3    5    3     5    41
 19  26   33   37   56   7   56    19   56   43   56    408
 55  71  114  235  308   7   254   55  308  601   254  2262
>>> tells("ACT')
  File "<stdin>", line 1
    tells("ACT')
               ^
SyntaxError: EOL while scanning string literal
>>> tells("ACT")
A C  T   =
1 1  1   3
1 3  20  24
1 3 200 204
>>> tells("ACTG")
A C  T  G  =
1 1  1  1  4
1 3  20 7  31
1 3 200 7 211
>>> tells("ACT of God")
ACT of God  =
 3   2  3   8
 24 21  26  71
204 66  71 341
>>> tells("Guanine")
G  u  a  n i  n e  =
1  1  1  1 1  1 1  7
7  21 1 14 9 14 5  71
7 300 1 50 9 50 5 422
>>> b.vi(22112)
Hosea 2:6 Therefore, behold, I will hedge up thy way with thorns, and make a wall, that she shall not find her paths.
>>> _.tells()
Therefore, behold, I will hedge  up thy  way with thorns, and make a wall, that she shall not find her paths.   =
     9        6    1   4    5    2   3    3    4     6     3    4  1   4     4   3    5    3    4   3     5    82
    100       46   9  56    29   37  53  49   60     94    19  30  1   48   49   32   52   49  33   31   64    941
    469      109   9  569   29  370 908 1201  717   508    55  66  1  561   409 113  169  310  69  103   379  7124
>>> b/'way'/'light'
Exodus 13:21;Nehemiah 9:12,19;Job 3:23;22:28;24:13;28:26;38:19,24-25;Proverbs 6:23;Isaiah 9:1;42:16;Nahum 2:4;Matthew 22:5;Luke 1:79;Acts 26:13 (17 verses)
>>> p(_)
Exodus 13:21 And the LORD went before them by day in a pillar of a cloud, to lead them the way; and by night in a pillar of fire, to give them light; to go by day and night:
Nehemiah 9:12 Moreover thou leddest them in the day by a cloudy pillar; and in the night by a pillar of fire, to give them light in the way wherein they should go.
Nehemiah 9:19 Yet thou in thy manifold mercies forsookest them not in the wilderness: the pillar of the cloud departed not from them by day, to lead them in the way; neither the pillar of fire by night, to shew them light, and the way wherein they should go.
Job 3:23 Why is light given to a man whose way is hid, and whom God hath hedged in?
Job 22:28 Thou shalt also decree a thing, and it shall be established unto thee: and the light shall shine upon thy ways.
Job 24:13 They are of those that rebel against the light; they know not the ways thereof, nor abide in the paths thereof.
Job 28:26 When he made a decree for the rain, and a way for the lightning of the thunder:
Job 38
19 Where is the way where light dwelleth? and as for darkness, where is the place thereof,
24 By what way is the light parted, which scattereth the east wind upon the earth?
25 Who hath divided a watercourse for the overflowing of waters, or a way for the lightning of thunder;
Proverbs 6:23 For the commandment is a lamp; and the law is light; and reproofs of instruction are the way of life:
Isaiah 9:1 Nevertheless the dimness shall not be such as was in her vexation, when at the first he lightly afflicted the land of Zebulun and the land of Naphtali, and afterward did more grievously afflict her by the way of the sea, beyond Jordan, in Galilee of the nations.
Isaiah 42:16 And I will bring the blind by a way that they knew not; I will lead them in paths that they have not known: I will make darkness light before them, and crooked things straight. These things will I do unto them, and not forsake them.
Nahum 2:4 The chariots shall rage in the streets, they shall justle one against another in the broad ways: they shall seem like torches, they shall run like the lightnings.
Matthew 22:5 But they made light of it, and went their ways, one to his farm, another to his merchandise:
Luke 1:79 To give light to them that sit in darkness and in the shadow of death, to guide our feet into the way of peace.
Acts 26:13 At midday, O king, I saw in the way a light from heaven, above the brightness of the sun, shining round about me and them which journeyed with me.
>>> 

