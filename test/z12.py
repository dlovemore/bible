>>> 1726*ns
[2, 863] [1, 150]
>>> nf(1726)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'nf' is not defined
>>> nF(1726)
(17, 1597, -129, 1726, 858, 2584, 18)
>>> b/'second part'

>>> b/'third part'
Numbers 15:6-7;28:14;2 Samuel 18:2;2 Kings 11:5-6;2 Chronicles 23:4-5;Nehemiah 10:32;Ezekiel 5:2,12;46:14;Zechariah 13:9;Revelation 8:7-12;9:15,18;12:4 (22 verses)
>>> fr=fractions.Fraction
>>> fr(1/2)-fr(1,60)
Fraction(29, 60)
>>> _*1.
0.48333333333333334
>>> .577215664901532860606
0.5772156649015329
>>> gamma=.577215664901532860606
>>> fr(1/2)-fr(1,60)
Fraction(29, 60)
>>> _-gamma
-0.09388233156819953
>>> ln(gamma)
-0.5495393129816448
>>> b.ch(549)
Psalms 71:1-24 (24 verses)
>>> b.ch(549)[5]
Psalms 71:5 For thou art my hope, O Lord GOD: thou art my trust from my youth.
>>> b.ch(549)[5][3]
'art'
>>> b.ch(549)[5].letters()[39-1]
'f'
>>> b.ch(549)[5].letters()[39-1:]
'frommyyouth'
>>> b.ch(549).ws()[53-1:60]
['thou', 'art', 'my', 'rock', 'and', 'my', 'fortress', 'Deliver']
>>> tells('thou')
 t  h  o  u   =
 1  1  1  1   4
 20 8 15  21  64
200 8 60 300 568
>>> b[54]
1 Timothy 1:1-6:21 (113 verses)
>>> b[54][9]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 228, in __getitem__
    return xs[k-1]
IndexError: list index out of range
>>> b[5][4][9]
Deuteronomy 4:9 Only take heed to thyself, and keep thy soul diligently, lest thou forget the things which thine eyes have seen, and lest they depart from thy heart all the days of thy life: but teach them thy sons, and thy sons' sons;
>>> b[5][4][8]
Deuteronomy 4:8 And what nation is there so great, that hath statutes and judgments so righteous as all this law, which I set before you this day?
>>> b[5][4][9]
Deuteronomy 4:9 Only take heed to thyself, and keep thy soul diligently, lest thou forget the things which thine eyes have seen, and lest they depart from thy heart all the days of thy life: but teach them thy sons, and thy sons' sons;
>>> b[5][4][9][3-1]
'take'
>>> b[5][4][9][5][3-1]
'y'
>>> b[5][4][9][5]
'thyself'
>>> tells(_)
 t  h  y   s  e  l f   =
 1  1  1   1  1  1 1   7
 20 8  25  19 5 12 6  95
200 8 700 100 5 30 6 1049
>>> tells('yself')
 y   s  e  l f  =
 1   1  1  1 1  5
 25  19 5 12 6  67
700 100 5 30 6 841
>>> tells('y')
 y   =
 1   1
 25  25
700 700
>>> 1+gamma
1.5772156649015328
>>> ln(_)
0.45566105506783555
>>> ot.wc()
609248
>>> ot.ws()[455660:455690]
['with', 'their', 'bread', 'him', 'that', 'fled', 'For', 'they', 'fled', 'from', 'the', 'swords', 'from', 'the', 'drawn', 'sword', 'and', 'from', 'the', 'bent', 'bow', 'and', 'from', 'the', 'grievousness', 'of', 'war', 'For', 'thus', 'hath']
>>> ot.ws()[455662:455690]
['bread', 'him', 'that', 'fled', 'For', 'they', 'fled', 'from', 'the', 'swords', 'from', 'the', 'drawn', 'sword', 'and', 'from', 'the', 'bent', 'bow', 'and', 'from', 'the', 'grievousness', 'of', 'war', 'For', 'thus', 'hath']
>>> ot.ws()[455662:455699]
['bread', 'him', 'that', 'fled', 'For', 'they', 'fled', 'from', 'the', 'swords', 'from', 'the', 'drawn', 'sword', 'and', 'from', 'the', 'bent', 'bow', 'and', 'from', 'the', 'grievousness', 'of', 'war', 'For', 'thus', 'hath', 'the', 'LORD', 'said', 'unto', 'me', 'Within', 'a', 'year', 'according']
>>> ot/'bent bow'/'bread'

>>> ot/'bread'/'fled'
Isaiah 21:14 The inhabitants of the land of Tema brought water to him that was thirsty, they prevented with their bread him that fled.
>>> _.vn()
18050
>>> ot/'bread'/'fled'
Isaiah 21:14 The inhabitants of the land of Tema brought water to him that was thirsty, they prevented with their bread him that fled.
>>> _.chn()
700
>>> Isaiah[21:14].letters()[60:]
'theypreventedwiththeirbreadhimthatfled'
>>> Isaiah[21:14].letters()[90:]
'thatfled'
>>> Isaiah[21:14].letters()[85:]
'adhimthatfled'
>>> Isaiah[21:14].letters()[80:]
'irbreadhimthatfled'
>>> Isaiah[21:14].letters()[78:]
'heirbreadhimthatfled'
>>> Isaiah[21:14].letters()[82:]
'breadhimthatfled'
>>> Isaiah[21:14].letters()[83-1:]
'breadhimthatfled'
>>> 455661*ns
[3, 3, 197, 257] [2, 2, 45, 55]
>>> 9*197
1773
>>> pn(270)
1733
>>> pn(55)
257
>>> Fn(55)
139583862445
>>> nF(455661)
(28, 317811, -137850, 455661, 58568, 514229, 29)
>>> (455661)**.5
675.0266661399385
>>> 675*ns
[3, 3, 3, 5, 5] [2, 2, 2, 3, 3]
>>> 27*25
675
>>> (455661)/675
675.0533333333333
>>> (455661)-675**2
36
>>> sos(455661)
[[6 6, 675 675], [90 90, 669 669]]
>>> b.chn(675)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: chn() takes 1 positional argument but 2 were given
>>> b.ch(675)
Song of Solomon 4:1-16 (16 verses)
>>> b.ch(669)
Ecclesiastes 10:1-20 (20 verses)
>>> p(_)
Ecclesiastes 10
1 Dead flies cause the ointment of the apothecary to send forth a stinking savour: so doth a little folly him that is in reputation for wisdom and honour.
2 A wise man's heart is at his right hand; but a fool's heart at his left.
3 Yea also, when he that is a fool walketh by the way, his wisdom faileth him, and he saith to every one that he is a fool.
4 If the spirit of the ruler rise up against thee, leave not thy place; for yielding pacifieth great offences.
5 There is an evil which I have seen under the sun, as an error which proceedeth from the ruler:
6 Folly is set in great dignity, and the rich sit in low place.
7 I have seen servants upon horses, and princes walking as servants upon the earth.
8 He that diggeth a pit shall fall into it; and whoso breaketh an hedge, a serpent shall bite him.
9 Whoso removeth stones shall be hurt therewith; and he that cleaveth wood shall be endangered thereby.
10 If the iron be blunt, and he do not whet the edge, then must he put to more strength: but wisdom is profitable to direct.
11 Surely the serpent will bite without enchantment; and a babbler is no better.
12 The words of a wise man's mouth are gracious; but the lips of a fool will swallow up himself.
13 The beginning of the words of his mouth is foolishness: and the end of his talk is mischievous madness.
14 A fool also is full of words: a man cannot tell what shall be; and what shall be after him, who can tell him?
15 The labour of the foolish wearieth every one of them, because he knoweth not how to go to the city.
16 Woe to thee, O land, when thy king is a child, and thy princes eat in the morning!
17 Blessed art thou, O land, when thy king is the son of nobles, and thy princes eat in due season, for strength, and not for drunkenness!
18 By much slothfulness the building decayeth; and through idleness of the hands the house droppeth through.
19 A feast is made for laughter, and wine maketh merry: but money answereth all things.
20 Curse not the king, no not in thy thought; and curse not the rich in thy bedchamber: for a bird of the air shall carry the voice, and that which hath wings shall tell the matter.
>>> ln(10)
2.302585092994046
>>> 

