>>> b[62]
1 John 1:1-5:21 (105 verses)
>>> _.vi(63)
1 John 3:24 And he that keepeth his commandments dwelleth in him, and he in him. And hereby we know that he abideth in us, by the Spirit which he hath given us.
>>> _.tells()
And he that keepeth his commandments dwelleth in him, and he in him. And hereby  we know that he abideth in us,  by the Spirit which he hath given us.   =
 3   2   4     7     3       12          8     2   3   3   2  2   3   3     6    2    4    4   2    7     2  2   2   3     6     5    2   4    5    2   115
 19 13  49     70    36      134        89    23  30   19 13 23  30   19   63    28  63   49  13    49   23  40  27  33   91     51  13  37    57   40 1244
 55 13  409   313   117      593        782   59  57   55 13 59  57   55   810  505  630  409 13   229   59 400 702 213   478   528  13  217  471  400 8714
>>> _.vn()
30604
>>> _-23145
7459
>>> nt.vi(7777)
Revelation 13:13 And he doeth great wonders, so that he maketh fire come down from heaven on the earth in the sight of men,
>>> exp(pi)-pi*exp(1)
14.6009584101057
>>> exp(pi)-pi**exp(1)
0.6815349144182257
>>> psi
-0.6180339887498949
>>> exp(pi)-pi**exp(1)
0.6815349144182257
>>> 1/_
1.4672762595789002
>>> 280*_
410.83735268209205
>>> exp(pi)-pi**exp(1)
0.6815349144182257
>>> _*280
190.8297760371032
>>> 19/28
0.6785714285714286
>>> tells('sixhundred threescore six')
sixhundred threescore six   =
    10         10      3   23
    126        116     52  294
   1170        566    709 2445
>>> tells('sixhundred threescore and six')
sixhundred threescore and six   =
    10         10      3   3   26
    126        116     19  52  313
   1170        566     55 709 2500
>>> b.count('Jesus')
977
>>> b.count("Jesus'")
10
>>> (Matthew-Acts).count("Jesus")
687
>>> (Matthew-Acts).count("Jesus'")
8
>>> 687-8
679
>>> b[44]
Acts 1:1-28:31 (1007 verses)
>>> b[45]
Romans 1:1-16:27 (433 verses)
>>> b[45]-Hebrews
Romans 1:1-Hebrews 13:25 (2336 verses)
>>> paul=_
>>> paul.count("Jesus")
235
>>> paul.count("Jesus'")
2
>>> Hebrews.bookn()
58
>>> b[59]-b[65]
James 1:1-Jude 1:25 (431 verses)
>>> general=_
>>> general.count("Jesus")
41
>>> general.count("Jesus'")
0
>>> Revelation.count("Jesus")
14
>>> Revelation.count("Jesus'")
0
>>> 687+233+41+14
975
>>> 687+233-10
910
>>> 687+233
920
>>> 679+233+55
967
>>> b.count("JESUS")
6
>>> b.count("JESUS'")
0
>>> 

