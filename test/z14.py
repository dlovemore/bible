>>> int('g0d',22)
7757
>>> b.vi(7757)
1 Samuel 20:26 Nevertheless Saul spake not any thing that day: for he thought, Something hath befallen him, he is not clean; surely he is not clean.
>>> _.bn()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 106, in __getattr__
    if len(bs)!=1: raise AttributeError('No attribute '+name)
AttributeError: No attribute bn
>>> _.bookn()
_.bookn(     _.bookname(  _.booknames  
>>> _.bookn()
9
>>> b.vi(7757)
1 Samuel 20:26 Nevertheless Saul spake not any thing that day: for he thought, Something hath befallen him, he is not clean; surely he is not clean.
>>> _.tells()
Nevertheless Saul spake not any thing that day: for he thought, Something hath befallen him, he  is not clean; surely he  is not clean.   =
     12        4    5    3   3    5     4    3   3   2     7        9       4      8      3   2  2   3     5      6    2  2   3     5    105
     152      53    52   49  40   58   49   30   39 13    99       110     37     57     30  13  28  49   35     100  13  28  49   35   1218
     998      431  196  310 751  274   409  705 156 13    783      479     217    129    57  13 109 310   89    1225  13 109 310   89   8175
>>> 595-40
555
>>> b.ch(558)
Psalms 80:1-19 (19 verses)
>>> b.ch(589)
Psalms 111:1-10 (10 verses)
>>> b/'man shall not live by bread'
Matthew 4:4 But he answered and said, It is written, Man shall not live by bread alone, but by every word that proceedeth out of the mouth of God.
Luke 4:4 And Jesus answered him, saying, It is written, That man shall not live by bread alone, but by every word of God.
>>> b/'man shall not live by bread alone'
Matthew 4:4 But he answered and said, It is written, Man shall not live by bread alone, but by every word that proceedeth out of the mouth of God.
Luke 4:4 And Jesus answered him, saying, It is written, That man shall not live by bread alone, but by every word of God.
>>> tell('the',26)
 t h e  =
20 8 5 33
>>> int('the',26)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 26: 'the'
>>> base(10,[20,8,5])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 12, in __call__
    return self.f(*args,**kwargs)
  File "/home/pi/python/parle/primes.py", line 156, in base
    return list(digits(x))[::-1]
  File "/home/pi/python/parle/primes.py", line 154, in digits
    yield x%b
TypeError: unsupported operand type(s) for %: 'list' and 'int'
>>> rebase(10,[20,8,5])
2085
>>> rebase(12,[20,8,5])
2981
>>> rebase(22,[20,8,5])
9861
>>> rebase(26,[20,8,5])
13733
>>> nF(_)
(21, 10946, -2787, 13733, 3978, 17711, 22)
>>> rebase(27,[20,8,5])
14801
>>> rebase(14,[20,8,5])
4037
>>> 2*14+8+5
41
>>> In
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'In' is not defined
>>> 9+14*5
79
>>> 76*ns
[2, 2, 19] [1, 1, 8]
>>> 2+20+1+21+10+22
76
>>> bases(1733)
2 [1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1]
3 [2, 1, 0, 1, 0, 1, 2]
4 [1, 2, 3, 0, 1, 1]
5 [2, 3, 4, 1, 3]
6 [1, 2, 0, 0, 5]
7 [5, 0, 2, 4]
8 [3, 3, 0, 5]
9 [2, 3, 3, 5]
10 [1, 7, 3, 3]
11 [1, 3, 3, 6]
12 [1, 0, 0, 5]
13 [10, 3, 4]
14 [8, 11, 11]
15 [7, 10, 8]
16 [6, 12, 5]
17 [5, 16, 16]
18 [5, 6, 5]
19 [4, 15, 4]
20 [4, 6, 13]
21 [3, 19, 11]
22 [3, 12, 17]
23 [3, 6, 8]
24 [3, 0, 5]
25 [2, 19, 8]
26 [2, 14, 17]
27 [2, 10, 5]
28 [2, 5, 25]
29 [2, 1, 22]
30 [1, 27, 23]
31 [1, 24, 28]
32 [1, 22, 5]
33 [1, 19, 17]
34 [1, 16, 33]
35 [1, 14, 18]
36 [1, 12, 5]
37 [1, 9, 31]
38 [1, 7, 23]
39 [1, 5, 17]
40 [1, 3, 13]
>>> [int('1611',n) for n in span(7,40)]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 1, in <listcomp>
ValueError: int() base must be >= 2 and <= 36, or 0
>>> [int('1611',n) for n in span(7,436)]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 1, in <listcomp>
ValueError: int() base must be >= 2 and <= 36, or 0
>>> [int('1611',n) for n in span(7,36)]
[645, 905, 1225, 1611, 2069, 2605, 3225, 3935, 4741, 5649, 6665, 7795, 9045, 10421, 11929, 13575, 15365, 17305, 19401, 21659, 24085, 26685, 29465, 32431, 35589, 38945, 42505, 46275, 50261, 54469]
>>> 1611*ns
[3, 3, 179] [2, 2, 41]
>>> tells('james'
... )
 j a  m e  s   =
 1 1  1 1  1   5
10 1 13 5  19  48
10 1 40 5 100 156
>>> 41+48
89
>>> tells('vi i')
 vi i  =
 2  1  3
 31 9  40
409 9 418
>>> 418+156+86
660
>>> 478+151
629
>>> tells('King James of England and Scotland')
King James of England and Scotland  =
  4    5    2    7     3      8     29
 41    48  21    57    19    88    274
 86   156  66   147    55    448   958
>>> tells('King James VI and I of England and Scotland')
King James  VI and I of England and Scotland   =
  4    5    2   3  1  2    7     3      8     35
 41    48   31  19 9 21    57    19    88     333
 86   156  409  55 9 66   147    55    448   1431
>>> tells('King James VI and I of Scotalnd and England')
King James  VI and I of Scotalnd and England   =
  4    5    2   3  1  2     8     3     7     35
 41    48   31  19 9 21    88     19    57    333
 86   156  409  55 9 66    448    55   147   1431
>>> 1431*ns
[3, 3, 3, 53] [2, 2, 2, 16]
>>> 27*53
1431
>>> 

