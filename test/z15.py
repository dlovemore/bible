>>> b/'Reuel'
Genesis 36:4,10,13,17;Exodus 2:18;Numbers 2:14;1 Chronicles 1:35,37;9:8 (9 verses)
>>> p(_)
Genesis 36
4 And Adah bare to Esau Eliphaz; and Bashemath bare Reuel;
10 These are the names of Esau's sons; Eliphaz the son of Adah the wife of Esau, Reuel the son of Bashemath the wife of Esau.
13 And these are the sons of Reuel; Nahath, and Zerah, Shammah, and Mizzah: these were the sons of Bashemath Esau's wife.
17 And these are the sons of Reuel Esau's son; duke Nahath, duke Zerah, duke Shammah, duke Mizzah: these are the dukes that came of Reuel in the land of Edom; these are the sons of Bashemath Esau's wife.
Exodus 2:18 And when they came to Reuel their father, he said, How is it that ye are come so soon to day?
Numbers 2:14 Then the tribe of Gad: and the captain of the sons of Gad shall be Eliasaph the son of Reuel.
1 Chronicles 1:35 The sons of Esau; Eliphaz, Reuel, and Jeush, and Jaalam, and Korah.
1 Chronicles 1:37 The sons of Reuel; Nahath, Zerah, Shammah, and Mizzah.
1 Chronicles 9:8 And Ibneiah the son of Jeroham, and Elah the son of Uzzi, the son of Michri, and Meshullam the son of Shephathiah, the son of Reuel, the son of Ibnijah;
>>> p(Numbers[2])
Numbers 2
1 And the LORD spake unto Moses and unto Aaron, saying,
2 Every man of the children of Israel shall pitch by his own standard, with the ensign of their father's house: far off about the tabernacle of the congregation shall they pitch.
3 And on the east side toward the rising of the sun shall they of the standard of the camp of Judah pitch throughout their armies: and Nahshon the son of Amminadab shall be captain of the children of Judah.
4 And his host, and those that were numbered of them, were threescore and fourteen thousand and six hundred.
5 And those that do pitch next unto him shall be the tribe of Issachar: and Nethaneel the son of Zuar shall be captain of the children of Issachar.
6 And his host, and those that were numbered thereof, were fifty and four thousand and four hundred.
7 Then the tribe of Zebulun: and Eliab the son of Helon shall be captain of the children of Zebulun.
8 And his host, and those that were numbered thereof, were fifty and seven thousand and four hundred.
9 All that were numbered in the camp of Judah were an hundred thousand and fourscore thousand and six thousand and four hundred, throughout their armies. These shall first set forth.
10 On the south side shall be the standard of the camp of Reuben according to their armies: and the captain of the children of Reuben shall be Elizur the son of Shedeur.
11 And his host, and those that were numbered thereof, were forty and six thousand and five hundred.
12 And those which pitch by him shall be the tribe of Simeon: and the captain of the children of Simeon shall be Shelumiel the son of Zurishaddai.
13 And his host, and those that were numbered of them, were fifty and nine thousand and three hundred.
14 Then the tribe of Gad: and the captain of the sons of Gad shall be Eliasaph the son of Reuel.
15 And his host, and those that were numbered of them, were forty and five thousand and six hundred and fifty.
16 All that were numbered in the camp of Reuben were an hundred thousand and fifty and one thousand and four hundred and fifty, throughout their armies. And they shall set forth in the second rank.
17 Then the tabernacle of the congregation shall set forward with the camp of the Levites in the midst of the camp: as they encamp, so shall they set forward, every man in his place by their standards.
18 On the west side shall be the standard of the camp of Ephraim according to their armies: and the captain of the sons of Ephraim shall be Elishama the son of Ammihud.
19 And his host, and those that were numbered of them, were forty thousand and five hundred.
20 And by him shall be the tribe of Manasseh: and the captain of the children of Manasseh shall be Gamaliel the son of Pedahzur.
21 And his host, and those that were numbered of them, were thirty and two thousand and two hundred.
22 Then the tribe of Benjamin: and the captain of the sons of Benjamin shall be Abidan the son of Gideoni.
23 And his host, and those that were numbered of them, were thirty and five thousand and four hundred.
24 All that were numbered of the camp of Ephraim were an hundred thousand and eight thousand and an hundred, throughout their armies. And they shall go forward in the third rank.
25 The standard of the camp of Dan shall be on the north side by their armies: and the captain of the children of Dan shall be Ahiezer the son of Ammishaddai.
26 And his host, and those that were numbered of them, were threescore and two thousand and seven hundred.
27 And those that encamp by him shall be the tribe of Asher: and the captain of the children of Asher shall be Pagiel the son of Ocran.
28 And his host, and those that were numbered of them, were forty and one thousand and five hundred.
29 Then the tribe of Naphtali: and the captain of the children of Naphtali shall be Ahira the son of Enan.
30 And his host, and those that were numbered of them, were fifty and three thousand and four hundred.
31 All they that were numbered in the camp of Dan were an hundred thousand and fifty and seven thousand and six hundred. They shall go hindmost with their standards.
32 These are those which were numbered of the children of Israel by the house of their fathers: all those that were numbered of the camps throughout their hosts were six hundred thousand and three thousand and five hundred and fifty.
33 But the Levites were not numbered among the children of Israel; as the LORD commanded Moses.
34 And the children of Israel did according to all that the LORD commanded Moses: so they pitched by their standards, and so they set forward, every one after their families, according to the house of their fathers.
>>> 45650*ns
[2, 5, 5, 11, 83] [1, 3, 3, 5, 23]
>>> 55*85
4675
>>> 55*83
4565
>>> tells('Euler')
E  u   l e  r  =
1  1   1 1  1  5
5  21 12 5 18  61
5 300 30 5 90 430
>>> tells('Euled')
E  u   l e d  =
1  1   1 1 1  5
5  21 12 5 4  47
5 300 30 5 4 344
>>> b/'log of oil'
Leviticus 14:10,12,15,21,24 (5 verses)
>>> p(_)
Leviticus 14
10 And on the eighth day he shall take two he lambs without blemish, and one ewe lamb of the first year without blemish, and three tenth deals of fine flour for a meat offering, mingled with oil, and one log of oil.
12 And the priest shall take one he lamb, and offer him for a trespass offering, and the log of oil, and wave them for a wave offering before the LORD:
15 And the priest shall take some of the log of oil, and pour it into the palm of his own left hand:
21 And if he be poor, and cannot get so much; then he shall take one lamb for a trespass offering to be waved, to make an atonement for him, and one tenth deal of fine flour mingled with oil for a meat offering, and a log of oil;
24 And the priest shall take the lamb of the trespass offering, and the log of oil, and the priest shall wave them for a wave offering before the LORD:
>>> 
