>>> from bible import *
>>> tells('alpha')
a  l  p h a  =
1  1  1 1 1  5
1 12 16 8 1  38
1 30 70 8 1 110
>>> tells('alpha omega')
alpha omega  =
  5     5    10
  38    41   79
 110   113  223
>>> 6*9
54
>>> base(13,'54')
<5>:1: TypeError: not all arguments converted during string formatting
/home/pi/python/parle/func.py:12: TypeError: not all arguments converted during string formatting
  __call__(self=Func(<function base at 0xb63d08e8>))
    args=(13, '54')
    kwargs={}
/home/pi/python/parle/primes.py:156: TypeError: not all arguments converted during string formatting
  base(b=13, x=54)
    digits=<function base.<locals>.digits at 0xb67c97c8>
/home/pi/python/parle/primes.py:154: TypeError: not all arguments converted during string formatting
  digits(x=54)
    b=13
>>> base(13,69)
[5, 4]
>>> tell("I am + I am")
I am + I am  =
9 14 0 9 14 46
>>> tells('Adam')
A d a  m  =
1 1 1  1  4
1 4 1 13 19
1 4 1 40 46
>>> IChronicles[7:27]
1 Chronicles 7:27 Non his son, Jehoshuah his son.
>>> b/'Peleg'
Genesis 10:25;11:16-19;1 Chronicles 1:19,25 (7 verses)
>>> p(_)
Genesis 10:25 And unto Eber were born two sons: the name of one was Peleg; for in his days was the earth divided; and his brother's name was Joktan.
Genesis 11
16 And Eber lived four and thirty years, and begat Peleg:
17 And Eber lived after he begat Peleg four hundred and thirty years, and begat sons and daughters.
18 And Peleg lived thirty years, and begat Reu:
19 And Peleg lived after he begat Reu two hundred and nine years, and begat sons and daughters.
1 Chronicles 1:19 And unto Eber were born two sons: the name of the one was Peleg; because in his days the earth was divided: and his brother's name was Joktan.
1 Chronicles 1:25 Eber, Peleg, Reu,
>>> IChronicles[1:25]
1 Chronicles 1:25 Eber, Peleg, Reu,
>>> _.tells()
Eber, Peleg, Reu,  =
  4      5     3   12
  30    45    44  119
 102    117   395 614
>>> _.vn(),_.chn()
(10278, 339)
>>> 339*ns
[3, 113] [2, 30]
>>> 
>>> 
>>> 
