>>> from bible import *
>>> 702/37
18.972972972972972
>>> 702/117
6.0
>>> 117/3
39.0
>>> _/3
13.0
>>> 13*9
117
>>> 117*ns
[3, 3, 13] [2, 2, 6]
>>> b.count('covered')
107
>>> b.count('cover')
268
>>> 268-107
161
>>> 107*ns
[107] [28]
>>> b/'fourth'/'month'
Numbers 28:14;1 Kings 6:1,37;2 Kings 25:3;1 Chronicles 27:7;2 Chronicles 3:2;Nehemiah 9:1;Jeremiah 28:1;39:2;52:6;Ezekiel 1:1;Zechariah 7:1;8:19 (13 verses)
>>> p(_)
Numbers 28:14 And their drink offerings shall be half an hin of wine unto a bullock, and the third part of an hin unto a ram, and a fourth part of an hin unto a lamb: this is the burnt offering of every month throughout the months of the year.
1 Kings 6:1 And it came to pass in the four hundred and eightieth year after the children of Israel were come out of the land of Egypt, in the fourth year of Solomon's reign over Israel, in the month Zif, which is the second month, that he began to build the house of the LORD.
1 Kings 6:37 In the fourth year was the foundation of the house of the LORD laid, in the month Zif:
2 Kings 25:3 And on the ninth day of the fourth month the famine prevailed in the city, and there was no bread for the people of the land.
1 Chronicles 27:7 The fourth captain for the fourth month was Asahel the brother of Joab, and Zebadiah his son after him: and in his course were twenty and four thousand.
2 Chronicles 3:2 And he began to build in the second day of the second month, in the fourth year of his reign.
Nehemiah 9:1 Now in the twenty and fourth day of this month the children of Israel were assembled with fasting, and with sackclothes, and earth upon them.
Jeremiah 28:1 And it came to pass the same year, in the beginning of the reign of Zedekiah king of Judah, in the fourth year, and in the fifth month, that Hananiah the son of Azur the prophet, which was of Gibeon, spake unto me in the house of the LORD, in the presence of the priests and of all the people, saying,
Jeremiah 39:2 And in the eleventh year of Zedekiah, in the fourth month, the ninth day of the month, the city was broken up.
Jeremiah 52:6 And in the fourth month, in the ninth day of the month, the famine was sore in the city, so that there was no bread for the people of the land.
Ezekiel 1:1 Now it came to pass in the thirtieth year, in the fourth month, in the fifth day of the month, as I was among the captives by the river of Chebar, that the heavens were opened, and I saw visions of God.
Zechariah 7:1 And it came to pass in the fourth year of king Darius, that the word of the LORD came unto Zechariah in the fourth day of the ninth month, even in Chisleu;
Zechariah 8:19 Thus saith the LORD of hosts; The fast of the fourth month, and the fast of the fifth, and the fast of the seventh, and the fast of the tenth, shall be to the house of Judah joy and gladness, and cheerful feasts; therefore love the truth and peace.
>>> 383*ns
[383] [76]

