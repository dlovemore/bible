>>> b/'god of gods'
Deuteronomy 10:17;Joshua 22:22;Psalms 136:2;Daniel 2:47;11:36 (5 verses)
>>> p(_)
Deuteronomy 10:17 For the LORD your God is God of gods, and Lord of lords, a great God, a mighty, and a terrible, which regardeth not persons, nor taketh reward:
Joshua 22:22 The LORD God of gods, the LORD God of gods, he knoweth, and Israel he shall know; if it be in rebellion, or if in transgression against the LORD, (save us not this day,)
Psalms 136:2 O give thanks unto the God of gods: for his mercy endureth for ever.
Daniel 2:47 The king answered unto Daniel, and said, Of a truth it is, that your God is a God of gods, and a Lord of kings, and a revealer of secrets, seeing thou couldest reveal this secret.
Daniel 11:36 And the king shall do according to his will; and he shall exalt himself, and magnify himself above every god, and shall speak marvellous things against the God of gods, and shall prosper till the indignation be accomplished: for that that is determined shall be done.
>>> tell("The LORD God of gods")
The LORD God of gods  =
 33  49   26 21  45  174
>>> tells("The LORD God of gods")
The LORD God of gods  =
 3    4   3   2   4   16
 33  49   26 21  45  174
213  184  71 66  171 705
>>> b/'King of kings'
Ezra 7:12;Ezekiel 26:7;Daniel 2:37;1 Timothy 6:15;Revelation 17:14;19:16 (6 verses)
>>> p(_)
Ezra 7:12 Artaxerxes, king of kings, unto Ezra the priest, a scribe of the law of the God of heaven, perfect peace, and at such a time.
Ezekiel 26:7 For thus saith the Lord GOD; Behold, I will bring upon Tyrus Nebuchadrezzar king of Babylon, a king of kings, from the north, with horses, and with chariots, and with horsemen, and companies, and much people.
Daniel 2:37 Thou, O king, art a king of kings: for the God of heaven hath given thee a kingdom, power, and strength, and glory.
1 Timothy 6:15 Which in his times he shall shew, who is the blessed and only Potentate, the King of kings, and Lord of lords;
Revelation 17:14 These shall make war with the Lamb, and the Lamb shall overcome them: for he is Lord of lords, and King of kings: and they that are with him are called, and chosen, and faithful.
Revelation 19:16 And he hath on his vesture and on his thigh a name written, KING OF KINGS, AND LORD OF LORDS.
>>> tell('KING OF KINGS, LORD OF LORDS')
KING OF KINGS, LORD OF LORDS  =
 41  21   60    49  21   68  260
>>> tells('KING OF KINGS, LORD OF LORDS')
KING OF KINGS, LORD OF LORDS  =
  4   2    5     4   2   5    22
 41  21   60    49  21   68  260
 86  66   186   184 66  284  872
>>> b/'stretch'/'heavens'
Psalms 104:2;Isaiah 40:22;42:5;44:24;45:12;51:13;Jeremiah 10:12;Zechariah 12:1 (8 verses)
>>> p(_)
Psalms 104:2 Who coverest thyself with light as with a garment: who stretchest out the heavens like a curtain:
Isaiah 40:22 It is he that sitteth upon the circle of the earth, and the inhabitants thereof are as grasshoppers; that stretcheth out the heavens as a curtain, and spreadeth them out as a tent to dwell in:
Isaiah 42:5 Thus saith God the LORD, he that created the heavens, and stretched them out; he that spread forth the earth, and that which cometh out of it; he that giveth breath unto the people upon it, and spirit to them that walk therein:
Isaiah 44:24 Thus saith the LORD, thy redeemer, and he that formed thee from the womb, I am the LORD that maketh all things; that stretcheth forth the heavens alone; that spreadeth abroad the earth by myself;
Isaiah 45:12 I have made the earth, and created man upon it: I, even my hands, have stretched out the heavens, and all their host have I commanded.
Isaiah 51:13 And forgettest the LORD thy maker, that hath stretched forth the heavens, and laid the foundations of the earth; and hast feared continually every day because of the fury of the oppressor, as if he were ready to destroy? and where is the fury of the oppressor?
Jeremiah 10:12 He hath made the earth by his power, he hath established the world by his wisdom, and hath stretched out the heavens by his discretion.
Zechariah 12:1 The burden of the word of the LORD for Israel, saith the LORD, which stretcheth forth the heavens, and layeth the foundation of the earth, and formeth the spirit of man within him.
>>> 
