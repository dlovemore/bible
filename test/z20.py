>>> from bible import *
>>> 33+81
114
>>> ln
Func(<built-in function log>)
>>> 114*ln
4.736198448394496
>>> 4*73
292
>>> tells
Func(functools.partial(<function tell at 0xb6319738>, Func(<functools._lru_cache_wrapper object at 0xb631d0d0>), Func(<function count at 0xb6319540>), Func(<functools._lru_cache_wrapper object at 0xb639ccb0>)))
>>> tells('the beginning')
the beginning  =
 3      9      12
 33     81    114
213    189    402
>>> ln(402)
5.996452088619021
>>> b/'shewbread'
Exodus 25:30;35:13;39:36;Numbers 4:7;1 Samuel 21:6;1 Kings 7:48;1 Chronicles 9:32;23:29;28:16;2 Chronicles 2:4;4:19;13:11;29:18;Nehemiah 10:33;Matthew 12:4;Mark 2:26;Luke 6:4;Hebrews 9:2 (18 verses)
>>> Exodus[25:23]
Exodus 25:23 Thou shalt also make a table of shittim wood: two cubits shall be the length thereof, and a cubit the breadth thereof, and a cubit and a half the height thereof.
>>> 5**.5*71/4
39.690206600621266
>>> 
>>> 6066-5996
70
>>> span
Func(<function span at 0xb642dc48>)
>>> list(span(10))
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>>> prod(span(10))
3628800
>>> f=span*prod
>>> f(6)
720
>>> 
>>> 
