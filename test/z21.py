>>> from bible import *
>>> Revelation[17:5]
Revelation 17:5 And upon her forehead was a name written, MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH.
>>> tells("MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH.")
MYSTERY, BABYLON THE GREAT, THE MOTHER OF HARLOTS AND ABOMINATIONS OF THE EARTH.   =
    7       7     3     5    3     6    2    7     3       12       2  3     5    65
   125      71    33   51    33   79   21    93    19      132     21  33   52    763
  1835     845   213   303  213   403  66   489    55      582     66 213   304  5587
>>> 5587*ns
[37, 151] [12, 36]
>>> 763*ns
[7, 109] [4, 29]
>>> 151*npp
8
>>> 535*npp
(14, 383, -152, 535, 192, 727, 15)
>>> b/'harp'
Genesis 4:21;31:27;1 Samuel 10:5;16:16,23;2 Samuel 6:5;1 Kings 10:12;1 Chronicles 13:8;15:16,21,28;16:5;25:1,3,6;2 Chronicles 5:12;9:11;20:28;29:25;Nehemiah 12:27;Job 21:12;30:31;Psalms 33:2;43:4;49:4;57:8;71:22;81:2;92:3;98:5;108:2;137:2;147:7;149:3;150:3;Isaiah 5:12;16:11;23:16;24:8;30:32;Ezekiel 26:13;Daniel 3:5,7,10,15;1 Corinthians 14:7;Revelation 5:8;14:2;15:2;18:22 (50 verses)
>>> b/'string'
Psalms 11:2;21:12;33:2;92:3;144:9;150:4;Isaiah 38:20;Habakkuk 3:19;Mark 7:35 (9 verses)
>>> Genesis[4:21]
Genesis 4:21 And his brother's name was Jubal: he was the father of all such as handle the harp and organ.
>>> _.vn()
101
>>> pi/7
0.4487989505128276
>>> 1/_
2.228169203286535
>>> pi*7
21.991148575128552
>>> 4*pi
12.566370614359172
>>> 7*pi
21.991148575128552
>>> 74*pi
232.4778563656447
>>> _*2
464.9557127312894
>>> 360/7
51.42857142857143
>>> 2*pi/7
0.8975979010256552
>>> 7/4*pi
5.497787143782138
>>> b/'circle'
Isaiah 40:22 It is he that sitteth upon the circle of the earth, and the inhabitants thereof are as grasshoppers; that stretcheth out the heavens as a curtain, and spreadeth them out as a tent to dwell in:
>>> _.vn(),_.chn()
(18443, 719)
>>> 18443/719
25.650904033379692
>>> 28/7
4.0
>>> 74/22
3.3636363636363638
>>> pi*7.4
23.24778563656447
>>> Isaiah[40:23]
Isaiah 40:23 That bringeth the princes to nothing; he maketh the judges of the earth as vanity.
>>> 28+15
43
>>> 360/40
9.0
>>> alphabeta
'αβγδεϝζηθικλμνξοπϙρστυφχψωϡ'
>>> 
