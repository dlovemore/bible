>>> from bible import *
>>> b/'round about'
Genesis 23:17;35:5;37:7;41:48;Exodus 7:24;16:13;19:12;25:11,24-25;27:17;28:32-34;29:16,20;30:3;37:2,11-12,26;38:16,20,31;39:23,25-26;40:8,33;Leviticus 1:5,11;3:2,8,13;7:2;8:15,19,24;9:12,18;14:41;16:18;25:31,44;Numbers 1:50,53;3:26,37;4:26,32;11:24,31-32;16:34;22:4;32:33;34:12;35:2,4;Deuteronomy 6:14;12:10;13:7;21:2;25:19;Joshua 6:3;15:12;18:20;19:8;21:11,42,44;23:1;Judges 2:12,14;7:21;19:22;20:5,29,43;1 Samuel 14:21;23:26;26:5,7;31:9;2 Samuel 5:9;7:1;22:12;1 Kings 3:1;4:24,31;6:5-6,29;7:12,18,20,23-24,36;18:35;2 Kings 6:17;11:8,11;17:15;23:5;25:1,4,10,17;1 Chronicles 4:33;6:55;9:27;10:9;11:8;22:9;28:12;2 Chronicles 4:2-3;14:14;15:15;17:10;20:30;23:7,10;34:6;Nehemiah 12:28-29;Job 10:8;16:13;19:12;22:10;37:12;41:14;Psalms 3:6;18:11;27:6;34:7;44:13;48:12;50:3;59:6,14;76:11;78:28;79:3-4;88:17;89:8;97:2-3;125:2;128:3;Isaiah 15:8;29:3;42:25;49:18;60:4;Jeremiah 1:15;4:17;6:3;12:9;21:14;25:9;46:5,14;50:14-15,29,32;51:2;52:4,7,14,22-23;Lamentations 1:17;2:3,22;Ezekiel 1:18,27-28;4:2;5:5-7,12,14-15;6:5,13;8:10;10:12;11:12;16:37,57;23:24;27:11;28:24,26;31:4;32:23-26;34:26;36:4,36;37:2;40:5,14,16-17,25,29-30,33,36,43;41:5-8,10-12,16-17,19;42:15-17,20;43:12-13,20;45:1-2;46:23;48:35;Joel 3:11-12;Amos 3:11;Jonah 2:5;Nahum 3:8;Zechariah 2:5;7:7;12:2,6;14:14;Matthew 3:5;14:35;21:33;Mark 1:28;3:5,34;5:32;6:6,36,55;9:8;10:23;11:11;Luke 1:65;2:9;4:14,37;6:10;7:17;8:37;9:12;John 10:24;Acts 5:16;9:3;14:6,20;22:6;25:7;26:13;Romans 15:19;Hebrews 9:4;Revelation 4:3,6;5:11;7:11 (285 verses)
>>> b.count("round about")
306
>>> b/'sea saw'
Psalms 114:3 The sea saw it, and fled: Jordan was driven back.
John 6:22 The day following, when the people which stood on the other side of the sea saw that there was none other boat there, save that one whereinto his disciples were entered, and that Jesus went not with his disciples into the boat, but that his disciples were gone away alone;
>>> b/'swings'

>>> b/'swing'

>>> b/'s wing'
Daniel 7:4 The first was like a lion, and had eagle's wings: I beheld till the wings thereof were plucked, and it was lifted up from the earth, and made stand upon the feet as a man, and a man's heart was given to it.
>>> b/'s' wings'
invalid syntax (<8>, line 1)
>>> b/"s' wings"

>>> 
