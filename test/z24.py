>>> from bible import *
>>> Genesis[13:16].tells()
And I will make thy seed  as the dust of the earth:  so that if a man can number the dust of the earth, then shall thy seed also be numbered.   =
 3  1   4    4   3    4   2   3    4   2  3     5    2    4   2 1  3   3     6    3    4   2  3     5     4    5    3    4    4   2     8      106
 19 9  56   30   53  33   20  33  64  21  33   52    34  49  15 1  28  18   73    33  64  21  33   52    47    52   53  33   47   7     82    1165
 55 9  569  66  908  114 101 213  604 66 213   304  160  409 15 1  91  54   487  213  604 66 213   304   263  169  908  114  191  7    496    7987
>>> tells("dust of the earth")
dust of the earth   =
  4   2  3    5    14
 64  21  33   52   170
 604 66 213  304  1187
>>> b.ch(1187)
Revelation 20:1-15 (15 verses)
>>> tells("sand of the sea")
sand of the sea  =
  4   2  3   3   12
 38  21  33  25 117
 155 66 213 106 540
>>> IIIJohn.vc()
14
>>> IIIJohn.wc()
294
>>> tells("clay")
c  l a  y   =
1  1 1  1   4
3 12 1  25  41
3 30 1 700 734
>>> b/'dust'/'earth'
Genesis 13:16;28:14;Exodus 8:17;Numbers 5:17;Joshua 7:6;1 Samuel 2:8;2 Samuel 22:43;2 Chronicles 1:9;Job 14:19;39:14;Psalms 7:5;22:29;44:25;Proverbs 8:26;Ecclesiastes 12:7;Isaiah 26:19;40:12;49:23;Daniel 12:2;Amos 2:7;Micah 7:17 (21 verses)
>>> _.tells()
And I will make thy seed  as the dust of the earth:  so that if a man can number the dust of the earth, then shall thy seed also be numbered.   =
 3  1   4    4   3    4   2   3    4   2  3     5    2    4   2 1  3   3     6    3    4   2  3     5     4    5    3    4    4   2     8      106
 19 9  56   30   53  33   20  33  64  21  33   52    34  49  15 1  28  18   73    33  64  21  33   52    47    52   53  33   47   7     82    1165
 55 9  569  66  908  114 101 213  604 66 213   304  160  409 15 1  91  54   487  213  604 66 213   304   263  169  908  114  191  7    496    7987
And thy seed shall be  as the dust of the earth, and thou shalt spread abroad  to the west, and  to the east, and  to the north, and  to the south: and in thee and in thy seed shall all the families of the earth be blessed.   =
 3   3    4    5    2  2   3    4   2  3     5    3    4    5      6      6    2   3    4    3   2   3    4    3   2   3     5    3   2   3     5    3   2   4   3   2  3    4    5    3   3      8     2  3    5    2     7     166
 19  53  33    52   7  20  33  64  21  33   52    19  64    60    63     41    35  33   67   19  35  33   45   19  35  33   75    19  35  33   83    19 23  38   19 23  53  33    52   25  33    74    21  33   52   7    66     1804
 55 908  114  169   7 101 213  604 66 213   304   55  568  339    270    158  260 213  805   55 260 213  306   55 260 213   408   55 260 213   668   55 59  218  55 59 908  114  169   61 213    200   66 213  304   7    246   11335
And they did so; for Aaron stretched out his hand with his rod, and smote the dust of the earth, and  it became lice in man, and in beast; all the dust of the land became lice throughout all the land of Egypt.   =
 3    4   3   2   3    5       9      3   3    4    4   3    3   3    5    3    4   2  3     5    3   2     6     4   2   3   3   2    5    3   3    4   2  3    4     6     4      10      3   3    4   2    5    160
 19  58   17  34  39   49     102     56  36  27   60   36  37   19   72   33  64  21  33   52    19  29   29    29  23  28   19 23   47    25  33  64  21  33  31    29    29      153     25  33  31  21   73    1711
 55  913  17 160 156  202     615    560 117  63   717 117  154  55  405  213  604 66 213   304   55 209   56    47  59  91   55 59   308   61 213  604 66 213  85    56    47     1233     61 213  85  66   982  10630
And the priest shall take holy water in an earthen vessel; and of the dust that  is in the floor of the tabernacle the priest shall take, and put  it into the water:   =
 3   3     6     5     4    4    5    2  2    7       6     3   2  3    4    4   2   2  3    5    2  3      10      3     6     5     4    3   3   2    4   3     5    128
 19  33   87     52   37   60    67  23 15    71      82    19 21  33  64   49   28 23  33   66  21  33     81      33   87     52    37   19  57  29  58   33   67   1489
 55 213   474   169   226  798  796  59 51   359     640    55 66 213  604  409 109 59 213  246  66 213     387    213   474   169   226   55 570 209  319 213   796  9724
And Joshua rent his clothes, and fell  to the earth upon his face before the ark of the LORD until the eventide, he and the elders of Israel, and put dust upon their heads.   =
 3     6     4   3      7     3    4   2   3    5     4   3    4     6    3   3   2  3    4    5    3      8      2  3   3     6    2    6     3   3    4    4    5      5    134
 19   74    57   36    82     19  35   35  33   52   66   36  15    51    33  30 21  33  49    76   33     84    13  19  33   63   21    64    19  57  64   66    60    37   1485
 55   479   345 117    406    55  71  260 213  304   480 117  15    168  213 111 66 213  184  589  213    678    13  55 213   234  66   235    55 570  604  480  312    118  8307
He raiseth  up the poor out of the dust, and lifteth  up the beggar from the dunghill,  to set them among princes, and  to make them inherit the throne of glory: for the pillars of the earth are the LORD's, and he hath set the world upon them.   =
 2    7     2   3    4   3   2  3    4    3     7     2   3     6     4   3      8      2   3    4    5       7     3   2    4    4     7     3     6    2    5    3   3     7     2  3    5    3   3     5     3   2   4   3   3    5     4    4    185
13    80    37  33  64   56 21  33   64   19    80    37  33   40    52   33     87     35  44  46    50     84     19  35  30   46     83    33   80   21   77    39  33    87   21  33   52   24  33    68    19 13  37   44  33   72   66    46   2185
13   413   370 213  280 560 66 213  604   55   458   370 213   112   196 213    438    260 305  253  158     327    55 260  66   253   371   213   413  66   887  156 213   330   66 213  304   96 213   284    55 13  217 305 213  684   480  253  12769
Then did I beat them  as small  as the dust of the earth, I did stamp them  as the mire of the street, and did spread them abroad.   =
  4   3  1   4    4   2    5    2   3    4   2  3     5   1  3    5     4   2   3    4   2  3     6     3   3     6     4     6     97
 47   17 9  28   46   20   57   20  33  64  21  33   52   9  17   69   46   20  33  45  21  33    87    19  17   63    46     41   1013
 263  17 9  208  253 101  201  101 213  604 66 213   304  9  17  411   253 101 213  144 66 213   600    55  17   270   253   158   5333
Now,  O LORD God, let thy promise unto David  my father be established: for thou hast made me king over a people like the dust of the earth in multitude.   =
  3   1   4    3   3   3     7      4    5    2     6    2      11       3    4    4    4   2   4    4  1    6     4   3    4   2  3    5    2      9      118
 52  15  49   26   37  53    95    70    40   38   58    7      104      39  64   48   23  18  41   60  1   69    37   33  64  21  33   52  23     125    1395
 610 60  184  71  235 908   374    610  418  740   310   7      464     156  568  309  50  45  86   555 1   240   64  213  604 66 213  304  59    1088    9612
The waters wear the stones: thou washest away the things which grow out of the dust of the earth; and thou destroyest the hope of man.   =
 3     6     4   3     6      4     7      4   3     6     5     4   3   2  3    4   2  3     5    3    4      10      3    4   2   3   106
 33   86    47   33    92    64     95    50   33   77     51   63   56 21  33  64  21  33   52    19  64      150     33  44  21  28   1363
213   896   596 213   515    568   914   1202 213   374   528   657 560 66 213  604 66 213   304   55  568    1464    213  143 66  91  11515
Which leaveth her eggs in the earth, and warmeth them in dust,   =
  5      7     3    4   2  3     5    3     7      4   2   4    49
  51     73    31  38  23  33   52    19    88    46  23   64   541
 528    649   103  119 59 213   304   55   844    253 59  604  3790
Let the enemy persecute  my soul, and take it; yea, let him tread down  my life upon the earth, and lay mine honour in the dust. Selah.   =
 3   3    5       9      2    4    3    4   2    3   3   3    5     4   2    4    4   3     5    3   3    4     6    2  3    4      5    101
 37  33   62     112     38   67   19  37   29  31   37  30   48   56   38  32   66   33   52    19  38  41    91   23  33   64    45   1211
235 213  800     778    740  490   55  226 209  706 235  57  300   614 740  50   480 213   304   55 731  104   568  59 213  604    144  9923
All they that be fat upon earth shall eat and worship: all they that go down  to the dust shall bow before him: and none can keep alive his own soul.   =
 3    4    4   2  3    4    5     5    3   3      7     3    4    4   2   4   2   3    4    5    3     6     3   3    4   3    4    5    3   3    4    115
 25  58   49   7  27  66    52    52   26  19    108    25  58   49  22  56   35  33  64    52   40   51    30   19  48   18  37    49   36  52   67  1330
 61  913  409  7 207  480  304   169  206  55    837    61  913  409 67  614 260 213  604  169  562   168   57   55  165  54  100  445  117 610  490  9781
For our soul  is bowed down  to the dust: our belly cleaveth unto the earth.   =
 3   3    4   2    5     4   2   3    4    3    5       8      4   3     5    58
 39  54  67   28   49   56   35  33   64   54   56     76     70   33   52    766
156 450  490 109  571   614 260 213  604  450  767     652    610 213   304  6463
While  as yet he had not made the earth, nor the fields, nor the highest part of the dust of the world.   =
  5    2   3   2  3   3    4   3     5    3   3     6     3   3     7      4   2  3    4   2  3     5    78
  57   20  50 13  13  49  23   33   52    47  33    55    47  33    76    55  21  33  64  21  33   72    900
 552  101 905 13  13 310  50  213   304  200 213   154   200 213   337    361 66 213  604 66 213   684  5985
Then shall the dust return  to the earth  as  it was: and the spirit shall return unto God who gave it.   =
  4    5    3    4     6    2   3    5    2   2    3   3   3     6     5      6     4   3   3    4   2   78
 47    52   33  64    96    35  33   52   20  29  43   19  33   91     52    96    70   26  46  35   29 1001
 263  169  213  604   735  260 213  304  101 209  601  55 213   478   169    735   610  71 568  413 209 7193
Thy dead men shall live, together with  my dead body shall they arise. Awake and sing,  ye that dwell in dust: for thy dew  is  as the dew of herbs, and the earth shall cast out the dead.   =
 3    4   3    5     4       8      4   2    4    4    5     4     5     5    3    4    2    4    5    2   4    3   3   3   2   2   3   3   2    5    3   3    5     5     4   3   3    4    140
 53  14   32   52    48     98     60   38  14   46    52   58    52     41   19   49   30  49    56  23   64   39  53  32  28  20  33  32 21   52    19  33   52    52   43   56  33   14   1560
908  14   95  169   444     575    717 740  14   766  169   913   205   527   55  166  705  409  569  59  604  156 908 509 109 101 213 509 66   205   55 213  304   169   304 560 213   14  13431
Who hath measured the waters in the hollow of his hand, and meted out heaven with the span, and comprehended the dust of the earth in a measure, and weighed the mountains in scales, and the hills in a balance?   =
 3    4      8     3     6    2  3     6    2  3    4    3    5    3     6     4   3    4    3       12       3    4   2  3    5    2 1     7     3     7     3      9      2    6     3   3    5    2 1     7     165
 46  37     86     33   86   23  33   85   21  36   27   19   47   56   55    60   33   50   19      110      33  64  21  33   52  23 1    82     19    61    33    126    23    59    19  33   60  23 1    38     1766
568  217    545   213   896  59 213   688  66 117   63   55  254  560   469   717 213  221   55      344     213  604 66 213  304  59 1    541    55   538   213    810    59   239    55 213  177  59 1    92    11045
And kings shall be thy nursing fathers, and their queens thy nursing mothers: they shall bow down  to thee with their face toward the earth, and lick  up the dust of thy feet; and thou shalt know that I am the LORD: for they shall not be ashamed that wait for me.   =
 3    5     5    2  3     7        7     3    5      6    3     7        7      4    5    3    4   2    4    4    5     4     6    3     5    3    4   2   3    4   2  3    4    3    4    5     4    4  1  2  3    4    3    4    5    3   2    7      4    4   3   2   204
 19   60    52   7  53   102      77     19   60    81    53   102      98     58    52   40  56   35  38   60    60   15    81    33   52    19  35   37  33  64  21  53   36   19  64    60   63   49  9 14  33   49   39  58    52   49  7    51    49   53   39  18  2436
 55  186   169   7 908   606      410    55  312    540  908   606      503    913  169  562  614 260  218  717  312   15    855  213   304   55  62  370 213  604 66 908  216   55  568  339   630  409 9 41 213  184  156  913  169  310  7   159    409  710 156  45 18393
And many of them that sleep in the dust of the earth shall awake, some  to everlasting life, and some  to shame and everlasting contempt.   =
 3    4   2   4    4    5    2  3    4   2  3    5     5      5     4   2       11       4    3    4   2    5    3       11         8      108
 19  53  21  46   49    57  23  33  64  21  33   52    52    41    52   35     132       32   19  52   35   46   19     132        106    1224
 55  791 66  253  409  210  59 213  604 66 213  304   169    527   205 260     897       50   55  205 260  154   55     897        628    7605
That pant after the dust of the earth  on the head of the poor, and turn aside the  way of the meek: and a man and his father will go in unto the same maid,  to profane  my holy name:   =
  4    4    5    3    4   2  3    5    2   3    4   2  3    4    3    4    5    3    3   2  3    4    3  1  3   3   3     6     4   2  2   4   3    4    4    2     7     2    4    4    136
 49   51    50   33  64  21  33   52   29  33  18  21  33   64   19  73    38   33  49  21  33   34   19 1  28  19  36   58    56  22 23  70   33  38    27   35    75    38  60    33   1522
 409  321  302  213  604 66 213  304  110 213  18  66 213  280   55  640  119  213 1201 66 213   70   55 1  91  55 117   310   569 67 59  610 213  146   54  260   282   740  798   96  10432
They shall lick the dust like a serpent, they shall move out of their holes like worms of the earth: they shall be afraid of the LORD our God, and shall fear because of thee.   =
  4    5     4   3    4    4  1     7      4    5     4   3   2   5     5     4    5    2  3     5     4    5    2    6    2  3    4   3    3   3    5     4     7     2   4    136
 58    52   35   33  64   37  1    97     58    52   55   56 21   60    59   37    88  21  33   52    58    52   7   39   21  33  49   54  26   19   52   30     56   21   38  1524
 913  169   62  213  604  64  1    520    913  169   505 560 66  312   203   64   790  66 213   304   913  169   7   111  66 213  184 450  71   55  169   102   416   66  218  9921
>>> 
>>> 
>>> Genesis/'tell'
Genesis 12:18;15:5;21:26;22:2;24:23,49;26:2;29:15;31:27;32:5,29;37:16;40:8;43:6,22;45:13;49:1 (17 verses)
>>> _.tells()
And Pharaoh called Abram and said, What  is this that thou hast done unto me?  why didst thou not tell me that she was thy wife?   =
 3     7       6     5    3    4     4   2    4    4    4    4    4    4   2    3    5     4   3    4   2   4   3   3   3    4    98
 19    67     37     35   19   33   52   28  56   49   64   48   38   70   18  56    56   64   49  49  18  49   32  43  53   43  1145
 55   238     73    134   55  114   709 109  317  409  568  309  119  610  45 1208  317   568 310  265 45  409 113 601 908  520  9128
And he brought him forth abroad, and said, Look now toward heaven, and tell the stars, if thou be able  to number them: and he said unto him,  So shall thy seed be.   =
 3   2    7     3    5      6     3    4     4   3     6      6     3    4   3     5    2   4   2   4   2     6     4    3   2   4    4    3   2    5    3    4   2   123
 19 13    91    30   67     41    19   33   53   52   81      55    19  49   33   77   15  64   7  20   35   73     46   19 13  33   70   30   34   52   53  33   7  1336
 55 13   667    57  364    158    55  114   170 610   855    469    55  265 213   491  15  568  7  38  260   487   253   55 13  114  610  57  160  169  908  114  7  8446
And Abimelech said, I wot not who hath done this thing; neither didst thou tell me, neither yet heard I of it, but  to day.   =
 3      9       4   1  3   3   3    4    4    4     5      7      5     4    4   2     7     3    5   1  2  2   3   2    3   93
 19     58      33  9  58  49  46  37   38   56    58      79     56   64   49   18    79    50   36  9 21  29  43  35  30  1059
 55    103     114  9 760 310 568  217  119  317   274    367    317   568  265  45   367   905  108  9 66 209 502 260  705 7539
And he said, Take now thy son, thine only son Isaac, whom thou lovest, and get thee into the land of Moriah; and offer him there for a burnt offering upon one of the mountains which I will tell thee of.   =
 3   2   4     4   3   3    3    5     4   3     5     4    4     6     3   3    4    4   3    4   2    6     3    5    3    5    3  1   5       8      4   3   2  3      9       5   1   4    4    4   2   156
 19 13   33   37   52  53  48    56   66   48   33    59   64     93    19  32  38   58   33  31  21    64    19   50   30   56   39 1   75     80     66   34 21  33    126      51  9  56   49   38   21  1824
 55 13  114   226 610 908  210  272   840 210   114   608  568   795    55 212  218  319 213  85  66   208    55  167   57  308  156 1  642     233    480 115 66 213    810     528  9  569  265  218  66 11877
And said, Whose daughter art thou? tell me, I pray thee:  is there room in thy father's house for  us  to lodge in?   =
 3    4     5       8     3    4     4   2  1   4    4    2    5     4   2  3      7      5    3   2   2    5    2   84
 19   33    70     84     39   64   49   18 9  60    38   28   56   61  23  53    77      68   39  40  35   43   23 1029
 55  114   673     615   291  568   265  45 9  861  218  109  308   250 59 908    410    473  156 400 260  106   59 7212
And now if  ye will deal kindly and truly with  my master, tell me: and if not, tell me; that I may turn  to the right hand,  or  to the left.   =
 3   3   2  2    4    4     6    3    5     4   2     6      4   2   3   2   3    4   2    4  1  3    4   2   3    5     4    2   2   3    4    101
 19  52 15  30  56   22    75    19   96   60   38    76    49   18  19 15  49   49   18  49  9  39  73   35  33   62    27   33  35  33   43   1246
 55 610 15 705  569  40    813   55  1320  717 740   436    265  45  55 15  310  265  45  409 9 741  640 260 213  314    63  150 260 213  241  10588
And the LORD appeared unto him, and said, Go not down into Egypt; dwell in the land which I shall tell thee of:   =
 3   3    4      8      4    3   3    4    2  3    4    4     5     5    2  3    4    5   1   5     4    4   2   85
 19  33  49     66     70   30   19   33  22  49  56   58    73     56  23  33  31    51  9   52   49   38   21  940
 55 213  184    246    610  57   55  114  67 310  614  319   982   569  59 213  85   528  9  169   265  218  66 6007
And Laban said unto Jacob, Because thou art  my brother, shouldest thou therefore serve me for nought? tell me, what shall thy wages be?   =
 3    5     4    4     5      7      4   3   2      7        9       4      9       5    2  3     6      4   2    4    5    3    5    2   107
 19   30   33   70    31      56    64   39  38    86       123     64     100      69  18  39    85    49   18  52    52   53   55   7  1250
 55   84   114  610   76     416    568 291 740    455      807     568    469     600  45 156   625    265  45  709  169  908  613   7  9395
Wherefore didst thou flee away secretly, and steal away from me; and didst not tell me, that I might have sent thee away with mirth, and with songs, with tabret, and with harp?   =
    9       5     4    4    4      8      3    5     4    4   2   3    5    3    4   2    4  1   5     4    4    4    4    4     5    3    4     5     4     6     3    4    4    137
   103      56   64   28   50     107     19   57   50   52   18  19   56   49  49   18  49  9   57   36   58   38   50   60    68    19  60    74    60     66    19  60    43   1621
   769     317   568  46  1202    1133    55  336  1202  196  45  55  317  310  265  45  409 9  264   414  355  218 1202  717   347   55  717   317   717   498    55  717  169  14041
And I have oxen, and asses, flocks, and menservants, and womenservants: and I have sent  to tell  my lord, that I may find grace in thy sight.   =
 3  1   4    4    3     5      6     3       11       3        13        3  1   4    4   2    4   2    4     4  1  3    4    5    2  3     5    107
 19 9  36    58   19   63      66    19      150      19       188       19 9  36   58   35  49   38   49   49  9  39  33    34  23  53   63   1242
 55 9  414  715   55   306    219    55     1041      55      1601       55 9  414  355 260  265 740  184   409 9 741  69   106  59 908   324  9432
And Jacob asked him, and said, Tell me, I pray thee, thy name. And he said, Wherefore  is  it that thou dost ask after  my name? And he blessed him there.   =
 3    5     5     3   3    4     4   2  1   4    4    3    4    3   2   4       9      2   2    4    4    4   3    5    2    4    3   2    7     3     5    113
 19   31    40   30   19   33   49   18 9  60    38   53   33   19 13   33     103     28  29  49   64   58   31   50   38   33   19 13    66    30   56   1164
 55   76   130   57   55  114   265  45 9  861  218  908   96   55 13  114     769    109 209  409  568  364 121  302  740   96   55 13   246    57   308  7437
And he said, I seek  my brethren: tell me, I pray thee, where they feed their flocks.   =
 3   2   4   1   4   2      8       4   2  1   4    4     5     4    4    5      6     63
 19 13   33  9  40   38     90     49   18 9  60    38    59   58   20    60     66    679
 55 13  114  9  130 740    450     265  45 9  861  218   608   913  20   312    219   4981
And they said unto him,  We have dreamed a dream, and there  is  no interpreter of it. And Joseph said unto them, Do not interpretations belong  to God? tell me them, I pray you.   =
 3    4    4    4    3   2    4     7    1    5    3    5    2   2       11      2  2   3     6     4    4    4    2  3         15          6    2    3    4   2   4   1   4    3   134
 19  58   33   70   30   28  36     50   1   41    19   56   28  29     148     21  29  19   73    33   70    46  19  49       203         55    35  26   49  18   46  9  60   61   1567
 55  913  114  610  57  505  414   149   1   140   55  308  109 110     814     66 209  55   253   114  610  253  64 310       1139        154  260  71   265 45  253  9  861 1060 10405
And Israel said, Wherefore dealt  ye  so ill with me,  as  to tell the man whether  ye had yet a brother?   =
 3     6     4       9       5    2   2   3    4   2   2   2    4   3   3     7     2   3   3  1     7     77
 19   64     33     103      42   30  34  33  60   18  20  35  49   33  28    87    30  13  50 1    86     868
 55   235   114     769     240  705 160  69  717  45 101 260  265 213  91   816   705  13 905 1    455   6934
And other money have  we brought down in our hands  to  buy food:  we cannot tell who put our money in our sacks.   =
 3    5     5     4   2     7      4   2  3    5    2    3    4    2     6     4   3   3   3    5    2  3     5    85
 19   66    72   36   28    91    56  23  54   46   35  48    40   28   67    49   46  57  54   72  23  54   53   1117
 55  363   855   414 505   667    614 59 450  163  260 1002  130  505   364   265 568 570 450  855  59 450   224  9847
And  ye shall tell  my father of all  my glory in Egypt, and of all that  ye have seen; and  ye shall haste and bring down  my father hither.   =
 3   2    5     4   2     6    2  3   2    5    2    5    3   2  3    4   2    4    4    3   2    5     5    3    5     4   2     6      6     104
 19  30   52   49   38   58   21  25  38   77  23   73    19 21  25  49   30  36    43   19  30   52    53   19   50   56   38   58      68    1169
 55 705  169   265 740   310  66  61 740  887  59   982   55 66  61  409 705  414  160   55 705  169   314   55  158   614 740   310    320   10349
And Jacob called unto his sons, and said, Gather yourselves together, that I may tell  you that which shall befall  you in the last days.   =
 3    5      6     4   3    4    3    4      6       10         8       4  1  3    4    3    4    5     5      6     3   2  3    4    4    107
 19   31    37    70   36   67   19   33    59       161        98     49  9  39  49   61   49    51    52    38    61  23  33  52    49   1245
 55   76    73    610 117  310   55  114    311     1790       575     409 9 741  265 1060  409  528   169    74   1060 59 213  331  805  10218
>>> 
