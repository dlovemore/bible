>>> from bible import *
>>> Psalm[117]
Psalms 117:1 O praise the LORD, all ye nations: praise him, all ye people.
Psalms 117:2 For his merciful kindness is great toward us: and the truth of the LORD endureth for ever. Praise ye the LORD.
>>> Psalm[117].tells()
 O praise the LORD, all  ye nations: praise him, all  ye people.   =
 1    6    3    4    3   2      7       6     3   3   2     6     46
15   68    33   49   25  30    92      68    30   25  30    69    534
60   275  213  184   61 705    470     275   57   61 705   240   3306
For his merciful kindness  is great toward us: and the truth of the LORD endureth for ever. Praise  ye the LORD.   =
 3   3      8        8     2    5      6    2   3   3    5    2  3    4      8     3    4      6    2   3    4    87
 39  36    87       95     28   51    81    40  19  33   87  21  33  49     95     39   50    68    30  33   49  1063
156 117    483      338   109  303    855  400  55 213  798  66 213  184    662   156  500    275  705 213  184  6985
>>> 1063+6985
8048
>>> tells("endureth ever")
endureth ever   =
    8      4   12
   95     50   145
   662    500 1162
>>> tells("true")
 t   r  u  e  =
 1   1  1  1  4
 20 18  21 5  64
200 90 300 5 595
>>> tells("Jeshoshuah")
 J e  s  h  o  s  h  u  a h  =
 1 1  1  1  1  1  1  1  1 1  10
10 5  19 8 15  19 8  21 1 8 114
10 5 100 8 60 100 8 300 1 8 600
10 5 100 8 60 100 8 300 1 8 600
>>> tells("Jehoshuah")
 J e h  o  s  h  u  a h  =
 1 1 1  1  1  1  1  1 1  9
10 5 8 15  19 8  21 1 8  95
10 5 8 60 100 8 300 1 8 500
>>> 17+69
86
>>> 121*49
5929
>>> b/"God Almighty"
Genesis 28:3;35:11;43:14;48:3;Exodus 6:3;Revelation 4:8;11:17;15:3;16:7,14;21:22 (11 verses)
>>> p(_)
Genesis 28:3 And God Almighty bless thee, and make thee fruitful, and multiply thee, that thou mayest be a multitude of people;
Genesis 35:11 And God said unto him, I am God Almighty: be fruitful and multiply; a nation and a company of nations shall be of thee, and kings shall come out of thy loins;
Genesis 43:14 And God Almighty give you mercy before the man, that he may send away your other brother, and Benjamin. If I be bereaved of my children, I am bereaved.
Genesis 48:3 And Jacob said unto Joseph, God Almighty appeared unto me at Luz in the land of Canaan, and blessed me,
Exodus 6:3 And I appeared unto Abraham, unto Isaac, and unto Jacob, by the name of God Almighty, but by my name JEHOVAH was I not known to them.
Revelation 4:8 And the four beasts had each of them six wings about him; and they were full of eyes within: and they rest not day and night, saying, Holy, holy, holy, LORD God Almighty, which was, and is, and is to come.
Revelation 11:17 Saying, We give thee thanks, O LORD God Almighty, which art, and wast, and art to come; because thou hast taken to thee thy great power, and hast reigned.
Revelation 15:3 And they sing the song of Moses the servant of God, and the song of the Lamb, saying, Great and marvellous are thy works, Lord God Almighty; just and true are thy ways, thou King of saints.
Revelation 16:7 And I heard another out of the altar say, Even so, Lord God Almighty, true and righteous are thy judgments.
Revelation 16:14 For they are the spirits of devils, working miracles, which go forth unto the kings of the earth and of the whole world, to gather them to the battle of that great day of God Almighty.
Revelation 21:22 And I saw no temple therein: for the Lord God Almighty and the Lamb are the temple of it.
>>> 35*ppn
<13>:1: NameError: name 'ppn' is not defined
>>> 35*pp
15551
>>> 3083803*np
222405
>>> tells(" וַיֹּאמֶר לוֹ אֱלֹהִים אֲנִי אֵל שַׁדַּי, פְּרֵה וּרְבֵה--גּוֹי וּקְהַל גּוֹיִם, יִהְיֶה מִמֶּךָּ; וּמְלָכִים, מֵחֲלָצֶיךָ יֵצֵאוּ.")
וַיֹּאמֶר לוֹ אֱלֹהִים אֲנִי אֵל שַׁדַּי, פְּרֵה וּרְבֵה--גּוֹי וּקְהַל גּוֹיִם, יִהְיֶה מִמֶּךָּ; וּמְלָכִים, מֵחֲלָצֶיךָ יֵצֵאוּ.   =
  5    2   5    3   2   3   3      7       4    4     4    3     6       6     4    61
  50  18   41   25 13  35   42     52     42    32   30   37     65     72     35   589
 257  36   86   61 31  314 285    232     141   59   30   100   146     198   107  2083
>>> tells("God Almighty")
God Almighty   =
 3      8     11
 26    95     121
 71    995   1066
>>> pi
3.141592653589793
>>> tells("אֵל שַׁדַּי",dsum)
אֵל שַׁדַּי  =
 2  3   5
13  35  48
31 314 345
13 134 147
>>> 
>>> 314*ns
[2, 157] [1, 37]
>>> 37*ns
[37] [12]
>>> 378*ns
[2, 3, 3, 3, 7] [1, 2, 2, 2, 4]
>>> 378+42
420
>>> 116+42
158
>>> _*ns
[2, 79] [1, 22]
>>> 

