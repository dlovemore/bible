>>> from bible import *
>>> tells("What do you get when you multiply seven by nine")
What do  you get when  you multiply seven  by nine   =
  4   2   3   3    4    3      8      5    2    4   38
 52  19  61   32  50   61     128     65   27  42   537
 709 64 1060 212  563 1060   1379    560  702  114 6423
>>> tells("What do you get when you multiply seven by nine?")
What do  you get when  you multiply seven  by nine?   =
  4   2   3   3    4    3      8      5    2    4    38
 52  19  61   32  50   61     128     65   27   42   537
 709 64 1060 212  563 1060   1379    560  702  114  6423
>>> ord("?")
63
>>> tells("What do you get when you multiply six by nine?")
What do  you get when  you multiply six  by nine?   =
  4   2   3   3    4    3      8     3   2    4    36
 52  19  61   32  50   61     128    52  27   42   524
 709 64 1060 212  563 1060   1379   709 702  114  6572
>>> 52*42*27
58968
>>> 52*42*27/360
163.8
>>> 52*42*27/7
8424.0
>>> 52*42*27/28
2106.0
>>> 52*42*27/27
2184.0
>>> 52*42*27/28
2106.0
>>> 52*42*27/30
1965.6
>>> 52*42*27/29.5
1998.915254237288
>>> 52*42*27/29.53
1996.884524212665
>>> 52*42*27/29.530589
1996.8446955121688
>>> 52*42*27/27.3
2160.0
>>> 3**3
27
>>> 3**4
81
>>> 3**5
243
>>> 52*42*27/27.321661
2158.2875213919096
>>> pn(35)
149
>>> 52*42
2184
>>> 52*42
2184
>>> 709*114
80826
>>> 709*114/30
2694.2
>>> 709*114/42
1924.4285714285713
>>> 709*114*702
56739852
>>> 709*114*702/360
157610.7
>>> 709*114*702/360/24
6567.1125
>>> 709*114
80826
>>> tells("seven by nine")
seven  by nine   =
  5    2    4   11
  65   27  42   134
 560  702  114 1376
>>> 65*42
2730
>>> _*ns
[2, 3, 5, 7, 13] [1, 2, 3, 4, 6]
>>> 21*13
273
>>> tells("earth")
e a  r  t  h  =
1 1  1  1  1  5
5 1 18  20 8  52
5 1 90 200 8 304
>>> 21+4+10
35
>>> pn(35)
149
>>> ppn(21)
<38>:1: NameError: name 'ppn' is not defined
>>> pp(21)
10301
>>> tells("one two three four five six")
one two three four five six   =
 3   3    5     4    4   3   22
 34  58   56   60   42   52  302
115 760  308   456  420 709 2768
>>> tells("one two three four five six seven")
one two three four five six seven   =
 3   3    5     4    4   3    5    27
 34  58   56   60   42   52   65   367
115 760  308   456  420 709  560  3328
>>> tells("one two three four five six seven eight nine")
one two three four five six seven eight nine   =
 3   3    5     4    4   3    5     5     4   36
 34  58   56   60   42   52   65    49   42   458
115 760  308   456  420 709  560   229   114 3671
>>> tells("one two three")
one two three   =
 3   3    5    11
 34  58   56   148
115 760  308  1183
>>> 2702*ns
[2, 7, 193] [1, 4, 44]
>>> tells("six by nine")
six  by nine   =
 3   2    4    9
 52  27  42   121
709 702  114 1525
>>> 114*ns
[2, 3, 19] [1, 2, 8]
>>> 3*19
57
>>> tells("מְנֵא מְנֵא, תְּקֵל וּפַרְסִין")
מְנֵא מְנֵא, תְּקֵל וּפַרְסִין   =
 3    3   3     6    15
 28  28   53   82    191
 91  91  530   406  1118
>>> tells("מְנֵא מְנֵא, תְּקֵל פַרְסִ")
מְנֵא מְנֵא, תְּקֵל פַרְסִ   =
 3    3   3   3   12
 28  28   53  52  161
 91  91  530 340 1052
>>> tells("lig ht")
lig  ht  =
 3   2   5
 28  28  56
 46 208 254
>>> tells("th ere")
 th ere  =
 2   3   5
 28  28  56
208 100 308
>>> b/'think on these thing'
Philippians 4:8 Finally, brethren, whatsoever things are true, whatsoever things are honest, whatsoever things are just, whatsoever things are pure, whatsoever things are lovely, whatsoever things are of good report; if there be any virtue, and if there be any praise, think on these things.
>>> tells("these things")
these things  =
  5      6    11
  57    77   134
 318    374  692
>>> tells("rod of iron")
rod of iron  =
 3   2   4   9
 37 21  56  114
154 66  209 429
>>> b/'rod of iron'
Psalms 2:9;Revelation 2:27;12:5;19:15 (4 verses)
>>> p(_)
Psalms 2:9 Thou shalt break them with a rod of iron; thou shalt dash them in pieces like a potter's vessel.
Revelation 2:27 And he shall rule them with a rod of iron; as the vessels of a potter shall they be broken to shivers: even as I received of my Father.
Revelation 12:5 And she brought forth a man child, who was to rule all nations with a rod of iron: and her child was caught up unto God, and to his throne.
Revelation 19:15 And out of his mouth goeth a sharp sword, that with it he should smite the nations: and he shall rule them with a rod of iron: and he treadeth the winepress of the fierceness and wrath of Almighty God.
>>> _.tells()
Thou shalt break them with a rod of iron; thou shalt dash them in pieces like a potter's vessel.   =
  4    5     5     4    4  1  3   2   4     4    5     4    4   2    6     4  1     7       6     75
 64    60    37   46   60  1  37 21   56   64    60   32   46  23   57    37  1    113      82    897
 568  339   118   253  717 1 154 66  209   568  339   113  253 59   192   64  1    725     640   5379
And he shall rule them with a rod of iron;  as the vessels of a potter shall they be broken  to shivers: even  as I received of  my Father.   =
 3   2   5     4    4    4  1  3   2   4    2   3     7     2 1    6     5     4   2    6    2      7      4   2  1     8     2  2     6     104
 19 13   52   56   46   60  1  37 21   56   20  33   101   21 1   94     52   58   7   65    35    100    46   20 9    71    21  38    58   1211
 55 13  169   425  253  717 1 154 66  209  101 213   740   66 1   625   169   913  7   227  260    712    460 101 9    521   66 740   310   8303
And she brought forth a man child, who was  to rule all nations with a rod of iron: and her child was caught  up unto God, and  to his throne.   =
 3   3     7      5   1  3     5    3   3   2    4   3     7      4  1  3   2   4    3   3    5    3     6    2    4    3   3   2   3     6     106
 19  32    91     67  1  28   36    46  43  35  56   25    92    60  1  37 21   56   19  31   36   43   60    37  70   26   19  35  36    80   1238
 55 113   667    364  1  91   54   568 601 260  425  61   470    717 1 154 66  209   55 103   54  601   519  370  610  71   55 260 117   413   8105
And out of his mouth goeth a sharp sword, that with  it he should smite the nations: and he shall rule them with a rod of iron: and he treadeth the winepress of the fierceness and wrath of Almighty God.   =
 3   3   2  3    5     5   1   5      5     4    4   2   2    6     5    3      7     3   2   5     4    4    4  1  3   2   4    3   2     8     3      9      2  3      10      3    5    2     8      3   158
 19  56 21  36   77    55  1   62    79    49   60   29 13   79     66   33    92     19 13   52   56   46   60  1  37 21   56   19 13    81     33    128    21  33     103     19   70  21    95     26   1850
 55 560 66 117  608   280  1  269    754   409  717 209 13   502   354  213    470    55 13  169   425  253  717 1 154 66  209   55 13    513   213    929    66 213     373     55  799  66    995    71  12020
>>> tells("rod of iron")
rod of iron  =
 3   2   4   9
 37 21  56  114
154 66  209 429
>>> tells("with a rod of iron",dsum)
with a rod of iron   =
  4  1  3   2   4   14
 60  1  37 21  56   175
 717 1 154 66  209 1147
 269 1 136 54  177  637
>>> dsum("r")
84
>>> Genesis[1:1].tell()
In the beginning God created the heaven and the earth.  =
23  33     81     26    56    33   55    19  33   52   411
>>> 
