>>> from bible import *
>>> tells("meaning of life",dsum)
meaning of life  =
   7     2   4   13
   63   21  32  116
  162   66  50  278
  118   54  32  204
>>> tells("What is the meaning of life?",dsum)
What  is the meaning of life?   =
  4   2   3     7     2   4    22
 52   28  33    63   21   32   229
 709 109 213   162   66   50  1309
 261 105 121   118   54   32   691
>>> tells("Douglas Noel Adams",dsum)
Douglas Noel Adams  =
   7      4    5    16
   79    46    38  163
  502    145  146  793
  288    101  126  515
>>> tells("Douglas Adams",dsum)
Douglas Adams  =
   7      5    12
   79     38  117
  502    146  648
  288    126  414
>>> tells("six by nine",dsum)
six  by nine   =
 3   2    4    9
 52  27  42   121
709 702  114 1525
393 434  86   913
>>> (393,434,86,913)@base12
('289', '302', '72', '641')
>>> 1525*ns
[5, 5, 61] [3, 3, 18]
>>> :x
invalid syntax (<9>, line 1)
>>> 
>>> 
>>> 
>>> 204*base12
'150'
>>> 691*base12
'497'
>>> b/'rule'/'rod of iron'
Revelation 2:27;12:5;19:15 (3 verses)
>>> p(_)
Revelation 2:27 And he shall rule them with a rod of iron; as the vessels of a potter shall they be broken to shivers: even as I received of my Father.
Revelation 12:5 And she brought forth a man child, who was to rule all nations with a rod of iron: and her child was caught up unto God, and to his throne.
Revelation 19:15 And out of his mouth goeth a sharp sword, that with it he should smite the nations: and he shall rule them with a rod of iron: and he treadeth the winepress of the fierceness and wrath of Almighty God.
>>> _.tells()
And he shall rule them with a rod of iron;  as the vessels of a potter shall they be broken  to shivers: even  as I received of  my Father.   =
 3   2   5     4    4    4  1  3   2   4    2   3     7     2 1    6     5     4   2    6    2      7      4   2  1     8     2  2     6     104
 19 13   52   56   46   60  1  37 21   56   20  33   101   21 1   94     52   58   7   65    35    100    46   20 9    71    21  38    58   1211
 55 13  169   425  253  717 1 154 66  209  101 213   740   66 1   625   169   913  7   227  260    712    460 101 9    521   66 740   310   8303
And she brought forth a man child, who was  to rule all nations with a rod of iron: and her child was caught  up unto God, and  to his throne.   =
 3   3     7      5   1  3     5    3   3   2    4   3     7      4  1  3   2   4    3   3    5    3     6    2    4    3   3   2   3     6     106
 19  32    91     67  1  28   36    46  43  35  56   25    92    60  1  37 21   56   19  31   36   43   60    37  70   26   19  35  36    80   1238
 55 113   667    364  1  91   54   568 601 260  425  61   470    717 1 154 66  209   55 103   54  601   519  370  610  71   55 260 117   413   8105
And out of his mouth goeth a sharp sword, that with  it he should smite the nations: and he shall rule them with a rod of iron: and he treadeth the winepress of the fierceness and wrath of Almighty God.   =
 3   3   2  3    5     5   1   5      5     4    4   2   2    6     5    3      7     3   2   5     4    4    4  1  3   2   4    3   2     8     3      9      2  3      10      3    5    2     8      3   158
 19  56 21  36   77    55  1   62    79    49   60   29 13   79     66   33    92     19 13   52   56   46   60  1  37 21   56   19 13    81     33    128    21  33     103     19   70  21    95     26   1850
 55 560 66 117  608   280  1  269    754   409  717 209 13   502   354  213    470    55 13  169   425  253  717 1 154 66  209   55 13    513   213    929    66 213     373     55  799  66    995    71  12020
>>> 
>>> 
>>> 
