>>> from bible import *
>>> Joshua/'tell'
Joshua 7:19 And Joshua said unto Achan, My son, give, I pray thee, glory to the LORD God of Israel, and make confession unto him; and tell me now what thou hast done; hide it not from me.
>>> Joshua[7:19].tells()
And Joshua said unto Achan,  My son, give, I pray thee, glory  to the LORD God of Israel, and make confession unto him; and tell me now what thou hast done; hide  it not from me.   =
 3     6     4    4     5    2    3    4   1   4    4     5    2   3    4   3   2    6     3    4      10       4    3   3    4   2  3    4    4    4    4     4   2   3    4   2   132
 19   74    33   70    27    38  48    43  9  60    38    77   35  33  49   26 21    64    19  30      119     70   30   19  49  18  52  52   64   48    38   26   29  49  52   18  1546
 55   479   114  610   63   740  210  421  9  861  218   887  260 213  184  71 66   235    55  66      443     610  57   55  265 45 610  709  568  309  119   26  209 310  196  45 10393
>>> Judges/'tell'
Judges 7:15;14:16;16:6,10,13;20:3 (6 verses)
>>> tells("me now what thou hast done")
me now what thou hast done   =
 2  3    4    4    4    4   21
18  52  52   64   48   38   272
45 610  709  568  309  119 2360
>>> tells("Joshua now what thou hast done")
Joshua now what thou hast done   =
   6    3    4    4    4    4   25
  74    52  52   64   48   38   328
  479  610  709  568  309  119 2794
>>> tells("now what thou hast done")
now what thou hast done   =
 3    4    4    4    4   19
 52  52   64   48   38   254
610  709  568  309  119 2315
>>> Judges/'tell'
Judges 7:15;14:16;16:6,10,13;20:3 (6 verses)
>>> p(_)
Judges 7:15 And it was so, when Gideon heard the telling of the dream, and the interpretation thereof, that he worshipped, and returned into the host of Israel, and said, Arise; for the LORD hath delivered into your hand the host of Midian.
Judges 14:16 And Samson's wife wept before him, and said, Thou dost but hate me, and lovest me not: thou hast put forth a riddle unto the children of my people, and hast not told it me. And he said unto her, Behold, I have not told it my father nor my mother, and shall I tell it thee?
Judges 16
6 And Delilah said to Samson, Tell me, I pray thee, wherein thy great strength lieth, and wherewith thou mightest be bound to afflict thee.
10 And Delilah said unto Samson, Behold, thou hast mocked me, and told me lies: now tell me, I pray thee, wherewith thou mightest be bound.
13 And Delilah said unto Samson, Hitherto thou hast mocked me, and told me lies: tell me wherewith thou mightest be bound. And he said unto her, If thou weavest the seven locks of my head with the web.
Judges 20:3 (Now the children of Benjamin heard that the children of Israel were gone up to Mizpeh.) Then said the children of Israel, Tell us, how was this wickedness?
>>> _.tells()
And  it was so, when Gideon heard the telling of the dream, and the interpretation thereof, that he worshipped, and returned into the host of Israel, and said, Arise; for the LORD hath delivered into your hand the host of Midian.   =
 3   2   3   2    4     6     5    3     7     2  3     5    3   3        14           7      4   2      10      3      8      4   3    4   2    6     3    4      5    3   3    4    4      9       4    4    4   3    4   2    6     180
 19  29  43  34  50    54     36   33    79   21  33   41    19  33       184         77     49  13     133      19    105    58   33  62  21    64    19   33    52    39  33  49   37      84     58   79   27   33  62  21    50    2018
 55 209 601 160  563   135   108  213   331   66 213   140   55 213      1039         374    409 13     916      55    744    319 213  368 66   235    55  114    205  156 213  184  217    552     319 1150  63  213  368 66   113   11801
And Samson's wife wept before him, and said, Thou dost but hate me, and lovest me not: thou hast put forth a riddle unto the children of  my people, and hast not told  it me. And he said unto her, Behold, I have not told  it  my father nor  my mother, and shall I tell  it thee?   =
 3      7      4    4     6     3   3    4     4    4   3    4   2   3     6    2   3    4    4   3    5   1    6     4   3      8     2  2     6     3    4   3    4   2   2   3   2   4    4    3     6    1   4   3    4   2   2     6    3   2     6     3    5   1   4   2    4    205
 19    100    43   64    51    30   19   33   64   58   43  34   18  19   93   18  49   64   48   57   67  1   52    70   33    73    21  38    69    19  48   49  51   29  18  19 13  33   70   31     46   9  36   49  51   29  38   58    47  38    79    19   52  9  49   29   38   2404
 55    451    520  775   168   57   55  114   568  364 502  214  45  55   795  45  310  568  309 570  364  1   142   610 213    199   66 740   240    55  309 310  294 209  45  55 13  114  610  103   109   9  414 310  294 209 740   310  200 740   403    55  169  9  265 209  218  15895
And Delilah said  to Samson, Tell me, I pray thee, wherein thy great strength lieth, and wherewith thou mightest be bound  to afflict thee.   =
 3     7      4   2     6      4   2  1   4    4      7     3    5       8       5    3      9       4      8     2   5    2     7      4    109
 19    51    33   35    81    49   18 9  60    38     82    53   51     111     54    19    119     64     101    7   56   35    57     38  1240
 55    87    114 260   351    265  45 9  861  218    667   908  303     660     252   55    1325    568    569    7  416  260   255    218  8728
And Delilah said unto Samson, Behold, thou hast mocked me, and told me lies: now tell me, I pray thee, wherewith thou mightest be bound.   =
 3     7      4    4     6       6      4    4     6    2   3    4   2   4    3    4   2  1   4    4       9       4      8     2    5    105
 19    51    33   70     81      46    64   48    51    18  19  51  18   45   52  49   18 9  60    38     119     64     101    7   56   1187
 55    87    114  610   351     109    568  309   132   45  55  294 45  144  610  265  45 9  861  218     1325    568    569    7   416  7811
And Delilah said unto Samson, Hitherto thou hast mocked me, and told me lies: tell me wherewith thou mightest be bound. And he said unto her, If thou weavest the seven locks of  my head with the web.   =
 3     7      4    4     6        8      4    4     6    2   3    4   2   4     4   2     9       4      8     2    5    3   2   4    4    3   2   4     7     3    5     5    2  2    4    4   3    3   155
 19    51    33   70     81      103    64   48    51    18  19  51  18   45   49  18    119     64     101    7   56    19 13  33   70   31  15  64     95    33   65    60  21  38  18   60   33  30   1783
 55    87    114  610   351      580    568  309   132   45  55  294 45  144   265 45    1325    568    569    7   416   55 13  114  610  103 15  568   1211  213  560   213  66 740  18   717 213  507 12520
(Now the children of Benjamin heard that the children of Israel were gone  up  to Mizpeh.) Then said the children of Israel, Tell us, how was this wickedness?   =
  3   3      8     2     8      5     4   3      8     2    6     4    4   2   2      6      4    4   3      8     2    6      4   2   3   3    4       10      123
 52   33    73    21    68      36   49   33    73    21   64    51   41   37  35    77     47   33   33    73    21    64    49   40  46  43  56      112     1381
 610 213    199   66    167    108   409 213    199   66   235   600  122 370 260    932    263  114 213    199   66   235    265 400 568 601  317     796     8806
>>> 
