>>> tells("nothing")
 n  o  t  h i  n g  =
 1  1  1  1 1  1 1  7
14 15  20 8 9 14 7  87
50 60 200 8 9 50 7 384
>>> tells("no thing")
 no thing  =
 2    5    7
 29   58   87
110  274  384
>>> tells("one")
 o  n e  =
 1  1 1  3
15 14 5  34
60 50 5 115
>>> tells("ghost")
g h  o  s   t   =
1 1  1  1   1   5
7 8 15  19  20  69
7 8 60 100 200 375
>>> tells("tells")
 t  e  l  l  s   =
 1  1  1  1  1   5
 20 5 12 12  19  68
200 5 30 30 100 365
>>> tells("holy")
h  o  l  y   =
1  1  1  1   4
8 15 12  25  60
8 60 30 700 798
>>> tells("holy",rsum)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'rsum' is not defined
>>> 0x89
137
>>> 296*ns
[2, 2, 2, 37] [1, 1, 1, 12]
>>> 4*74
296
>>> b.midw()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 106, in __getattr__
    if len(bs)!=1: raise AttributeError('No attribute '+name)
AttributeError: No attribute midw
>>> b.midword()
['soul:']
>>> tells("holy holy holy LORD God Alimighty
  File "<stdin>", line 1
    tells("holy holy holy LORD God Alimighty
                                           ^
SyntaxError: EOL while scanning string literal
>>> tells("holy holy holy LORD God Alimighty")
holy holy holy LORD God Alimighty   =
  4    4    4    4   3      9      28
 60   60   60   49   26    104     359
 798  798  798  184  71    1004   3653
>>> tells("holy holy holy LORD God Almighty")
holy holy holy LORD God Almighty   =
  4    4    4    4   3      8     27
 60   60   60   49   26    95     350
 798  798  798  184  71    995   3644
>>> tells("know thing")
know thing  =
  4    5    9
 63    58  121
 630  274  904
>>> b/3/5
Exodus 27:1;38:1;Leviticus 27:6;1 Kings 6:6;1 Chronicles 26:4;2 Chronicles 6:13;Isaiah 17:6;Ezekiel 40:48;Luke 12:52 (9 verses)
>>> b/3/4
Genesis 2:14;Exodus 34:7;Numbers 28:14;1 Kings 7:27;1 Chronicles 3:2,15;11:21;23:19;24:8,23;26:2,4,11;Proverbs 30:15,18,21,29;Isaiah 17:6;Jeremiah 36:23;Ezekiel 10:14;Daniel 11:2;Amos 1:3,6,9,11,13;2:1,4,6;Zechariah 6:3;Luke 12:52;Revelation 4:7;6:6;8:12;9:15;21:19 (36 verses)
>>> math.asin(3/4)
0.848062078981481
>>> math.asi(3/4)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/auto/auto.py", line 37, in __getattr__
    return getattr(mod,a)
AttributeError: module 'math' has no attribute 'asi'
>>> math.acos(3/4)
0.7227342478134157
>>> math.asin(3/4)*2
1.696124157962962
>>> 696*ns
[2, 2, 2, 3, 29] [1, 1, 1, 2, 10]
>>> 1696*ns
[2, 2, 2, 2, 2, 53] [1, 1, 1, 1, 1, 16]
>>> 5*2**.5
7.0710678118654755
>>> 5*2**.5**2
5.946035575013605
>>> (5*2**.5)**2
50.00000000000001
>>> 50+9
59
>>> 59**.5
7.681145747868608
>>> pi/2
1.5707963267948966
>>> 5**.25
1.4953487812212205
>>> 2**.25
1.189207115002721
>>> 2**.25*5
5.946035575013605
>>> 5.95-_
0.003964424986395265
>>> Deuteronomy.vi(946)
Deuteronomy 33:28 Israel then shall dwell in safety alone: the fountain of Jacob shall be upon a land of corn and wine; also his heavens shall drop down dew.
>>> b.vi*(7115)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for *: 'method' and 'int'
>>> b.vi(7115)
Judges 21:12 And they found among the inhabitants of Jabeshgilead four hundred young virgins, that had known no man by lying with any male: and they brought them unto the camp to Shiloh, which is in the land of Canaan.
>>> b[7][1:15]
Judges 1:15 And she said unto him, Give me a blessing: for thou hast given me a south land; give me also springs of water. And Caleb gave her the upper springs and the nether springs.
>>> 414**.5
20.346989949375804
>>> 2**.5
1.4142135623730951
>>> 1.414**.5
1.18911731969558
>>> tells("tree")
 t   r e e  =
 1   1 1 1  4
 20 18 5 5  48
200 90 5 5 300
>>> tells("the tree")
the tree  =
 3    4   7
 33  48   81
213  300 513
>>> b/'root'/'tree'
Jeremiah 17:8;Daniel 4:23,26;Matthew 3:10;Mark 11:20;Luke 3:9;17:6;Romans 11:17;Jude 1:12 (9 verses)
>>> p(_)
Jeremiah 17:8 For he shall be as a tree planted by the waters, and that spreadeth out her roots by the river, and shall not see when heat cometh, but her leaf shall be green; and shall not be careful in the year of drought, neither shall cease from yielding fruit.
Daniel 4:23 And whereas the king saw a watcher and an holy one coming down from heaven, and saying, Hew the tree down, and destroy it; yet leave the stump of the roots thereof in the earth, even with a band of iron and brass, in the tender grass of the field; and let it be wet with the dew of heaven, and let his portion be with the beasts of the field, till seven times pass over him;
Daniel 4:26 And whereas they commanded to leave the stump of the tree roots; thy kingdom shall be sure unto thee, after that thou shalt have known that the heavens do rule.
Matthew 3:10 And now also the axe is laid unto the root of the trees: therefore every tree which bringeth not forth good fruit is hewn down, and cast into the fire.
Mark 11:20 And in the morning, as they passed by, they saw the fig tree dried up from the roots.
Luke 3:9 And now also the axe is laid unto the root of the trees: every tree therefore which bringeth not forth good fruit is hewn down, and cast into the fire.
Luke 17:6 And the Lord said, If ye had faith as a grain of mustard seed, ye might say unto this sycamine tree, Be thou plucked up by the root, and be thou planted in the sea; and it should obey you.
Romans 11:17 And if some of the branches be broken off, and thou, being a wild olive tree, wert graffed in among them, and with them partakest of the root and fatness of the olive tree;
Jude 1:12 These are spots in your feasts of charity, when they feast with you, feeding themselves without fear: clouds they are without water, carried about of winds; trees whose fruit withereth, without fruit, twice dead, plucked up by the roots;
>>> b/'musi'
1 Samuel 18:6;1 Chronicles 15:16;16:42;2 Chronicles 5:13;7:6;23:13;34:12;Nehemiah 12:36;Psalms 39:3;Ecclesiastes 2:8;12:4;Lamentations 3:63;5:14;Daniel 3:5,7,10,15;6:18;Amos 6:5;Luke 15:25;Revelation 18:22 (21 verses)
>>> b/'music'
1 Samuel 18:6;1 Chronicles 15:16;16:42;2 Chronicles 5:13;7:6;23:13;34:12;Nehemiah 12:36;Ecclesiastes 2:8;12:4;Lamentations 3:63;5:14;Daniel 3:5,7,10,15;6:18;Amos 6:5;Luke 15:25;Revelation 18:22 (20 verses)
>>> b/'musick'
1 Samuel 18:6;1 Chronicles 15:16;2 Chronicles 5:13;7:6;23:13;34:12;Ecclesiastes 12:4;Lamentations 3:63;5:14;Daniel 3:5,7,10,15;6:18;Amos 6:5;Luke 15:25 (16 verses)
>>> b/'music'
1 Samuel 18:6;1 Chronicles 15:16;16:42;2 Chronicles 5:13;7:6;23:13;34:12;Nehemiah 12:36;Ecclesiastes 2:8;12:4;Lamentations 3:63;5:14;Daniel 3:5,7,10,15;6:18;Amos 6:5;Luke 15:25;Revelation 18:22 (20 verses)
>>> Psalm[39:3]
Psalms 39:3 My heart was hot within me, while I was musing the fire burned: then spake I with my tongue,
>>> b/'music'@(I+method.chapter())
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 24, in __call__
    return self.f(*args,**kwargs)
TypeError: callmethod() missing 1 required positional argument: 'object'
>>> b/'music'@(I+meth.chapter())
1 Samuel 18:6 And it came to pass as they came, when David was returned from the slaughter of the Philistine, that the women came out of all cities of Israel, singing and dancing, to meet king Saul, with tabrets, with joy, and with instruments of musick. 18 1 Chronicles 15:16 And David spake to the chief of the Levites to appoint their brethren to be the singers with instruments of musick, psalteries and harps and cymbals, sounding, by lifting up the voice with joy. 15 1 Chronicles 16:42 And with them Heman and Jeduthun with trumpets and cymbals for those that should make a sound, and with musical instruments of God. And the sons of Jeduthun were porters. 16 2 Chronicles 5:13 It came even to pass, as the trumpeters and singers were as one, to make one sound to be heard in praising and thanking the LORD; and when they lifted up their voice with the trumpets and cymbals and instruments of musick, and praised the LORD, saying, For he is good; for his mercy endureth for ever: that then the house was filled with a cloud, even the house of the LORD; 5 2 Chronicles 7:6 And the priests waited on their offices: the Levites also with instruments of musick of the LORD, which David the king had made to praise the LORD, because his mercy endureth for ever, when David praised by their ministry; and the priests sounded trumpets before them, and all Israel stood. 7 2 Chronicles 23:13 And she looked, and, behold, the king stood at his pillar at the entering in, and the princes and the trumpets by the king: and all the people of the land rejoiced, and sounded with trumpets, also the singers with instruments of musick, and such as taught to sing praise. Then Athaliah rent her clothes, and said, Treason, Treason. 23 2 Chronicles 34:12 And the men did the work faithfully: and the overseers of them were Jahath and Obadiah, the Levites, of the sons of Merari; and Zechariah and Meshullam, of the sons of the Kohathites, to set it forward; and other of the Levites, all that could skill of instruments of musick. 34 Nehemiah 12:36 And his brethren, Shemaiah, and Azarael, Milalai, Gilalai, Maai, Nethaneel, and Judah, Hanani, with the musical instruments of David the man of God, and Ezra the scribe before them. 12 Ecclesiastes 2:8 I gathered me also silver and gold, and the peculiar treasure of kings and of the provinces: I gat me men singers and women singers, and the delights of the sons of men, as musical instruments, and that of all sorts. 2 Ecclesiastes 12:4 And the doors shall be shut in the streets, when the sound of the grinding is low, and he shall rise up at the voice of the bird, and all the daughters of musick shall be brought low; 12 Lamentations 3:63 Behold their sitting down, and their rising up; I am their musick. 3 Lamentations 5:14 The elders have ceased from the gate, the young men from their musick. 5 Daniel 3:5 That at what time ye hear the sound of the cornet, flute, harp, sackbut, psaltery, dulcimer, and all kinds of musick, ye fall down and worship the golden image that Nebuchadnezzar the king hath set up: 3 Daniel 3:7 Therefore at that time, when all the people heard the sound of the cornet, flute, harp, sackbut, psaltery, and all kinds of musick, all the people, the nations, and the languages, fell down and worshipped the golden image that Nebuchadnezzar the king had set up. 3 Daniel 3:10 Thou, O king, hast made a decree, that every man that shall hear the sound of the cornet, flute, harp, sackbut, psaltery, and dulcimer, and all kinds of musick, shall fall down and worship the golden image: 3 Daniel 3:15 Now if ye be ready that at what time ye hear the sound of the cornet, flute, harp, sackbut, psaltery, and dulcimer, and all kinds of musick, ye fall down and worship the image which I have made; well: but if ye worship not, ye shall be cast the same hour into the midst of a burning fiery furnace; and who is that God that shall deliver you out of my hands? 3 Daniel 6:18 Then the king went to his palace, and passed the night fasting: neither were instruments of musick brought before him: and his sleep went from him. 6 Amos 6:5 That chant to the sound of the viol, and invent to themselves instruments of musick, like David; 6 Luke 15:25 Now his elder son was in the field: and as he came and drew nigh to the house, he heard musick and dancing. 15 Revelation 18:22 And the voice of harpers, and musicians, and of pipers, and trumpeters, shall be heard no more at all in thee; and no craftsman, of whatsoever craft he be, shall be found any more in thee; and the sound of a millstone shall be heard no more at all in thee; 18
>>> _*p
1 Samuel 18:6 And it came to pass as they came, when David was returned from the slaughter of the Philistine, that the women came out of all cities of Israel, singing and dancing, to meet king Saul, with tabrets, with joy, and with instruments of musick. 18 1 Chronicles 15:16 And David spake to the chief of the Levites to appoint their brethren to be the singers with instruments of musick, psalteries and harps and cymbals, sounding, by lifting up the voice with joy. 15 1 Chronicles 16:42 And with them Heman and Jeduthun with trumpets and cymbals for those that should make a sound, and with musical instruments of God. And the sons of Jeduthun were porters. 16 2 Chronicles 5:13 It came even to pass, as the trumpeters and singers were as one, to make one sound to be heard in praising and thanking the LORD; and when they lifted up their voice with the trumpets and cymbals and instruments of musick, and praised the LORD, saying, For he is good; for his mercy endureth for ever: that then the house was filled with a cloud, even the house of the LORD; 5 2 Chronicles 7:6 And the priests waited on their offices: the Levites also with instruments of musick of the LORD, which David the king had made to praise the LORD, because his mercy endureth for ever, when David praised by their ministry; and the priests sounded trumpets before them, and all Israel stood. 7 2 Chronicles 23:13 And she looked, and, behold, the king stood at his pillar at the entering in, and the princes and the trumpets by the king: and all the people of the land rejoiced, and sounded with trumpets, also the singers with instruments of musick, and such as taught to sing praise. Then Athaliah rent her clothes, and said, Treason, Treason. 23 2 Chronicles 34:12 And the men did the work faithfully: and the overseers of them were Jahath and Obadiah, the Levites, of the sons of Merari; and Zechariah and Meshullam, of the sons of the Kohathites, to set it forward; and other of the Levites, all that could skill of instruments of musick. 34 Nehemiah 12:36 And his brethren, Shemaiah, and Azarael, Milalai, Gilalai, Maai, Nethaneel, and Judah, Hanani, with the musical instruments of David the man of God, and Ezra the scribe before them. 12 Ecclesiastes 2:8 I gathered me also silver and gold, and the peculiar treasure of kings and of the provinces: I gat me men singers and women singers, and the delights of the sons of men, as musical instruments, and that of all sorts. 2 Ecclesiastes 12:4 And the doors shall be shut in the streets, when the sound of the grinding is low, and he shall rise up at the voice of the bird, and all the daughters of musick shall be brought low; 12 Lamentations 3:63 Behold their sitting down, and their rising up; I am their musick. 3 Lamentations 5:14 The elders have ceased from the gate, the young men from their musick. 5 Daniel 3:5 That at what time ye hear the sound of the cornet, flute, harp, sackbut, psaltery, dulcimer, and all kinds of musick, ye fall down and worship the golden image that Nebuchadnezzar the king hath set up: 3 Daniel 3:7 Therefore at that time, when all the people heard the sound of the cornet, flute, harp, sackbut, psaltery, and all kinds of musick, all the people, the nations, and the languages, fell down and worshipped the golden image that Nebuchadnezzar the king had set up. 3 Daniel 3:10 Thou, O king, hast made a decree, that every man that shall hear the sound of the cornet, flute, harp, sackbut, psaltery, and dulcimer, and all kinds of musick, shall fall down and worship the golden image: 3 Daniel 3:15 Now if ye be ready that at what time ye hear the sound of the cornet, flute, harp, sackbut, psaltery, and dulcimer, and all kinds of musick, ye fall down and worship the image which I have made; well: but if ye worship not, ye shall be cast the same hour into the midst of a burning fiery furnace; and who is that God that shall deliver you out of my hands? 3 Daniel 6:18 Then the king went to his palace, and passed the night fasting: neither were instruments of musick brought before him: and his sleep went from him. 6 Amos 6:5 That chant to the sound of the viol, and invent to themselves instruments of musick, like David; 6 Luke 15:25 Now his elder son was in the field: and as he came and drew nigh to the house, he heard musick and dancing. 15 Revelation 18:22 And the voice of harpers, and musicians, and of pipers, and trumpeters, shall be heard no more at all in thee; and no craftsman, of whatsoever craft he be, shall be found any more in thee; and the sound of a millstone shall be heard no more at all in thee; 18
>>> b/'music'@(meth.ref()+meth.chapter())
'1 Samuel 18:6' 18 '1 Chronicles 15:16' 15 '1 Chronicles 16:42' 16 '2 Chronicles 5:13' 5 '2 Chronicles 7:6' 7 '2 Chronicles 23:13' 23 '2 Chronicles 34:12' 34 'Nehemiah 12:36' 12 'Ecclesiastes 2:8' 2 'Ecclesiastes 12:4' 12 'Lamentations 3:63' 3 'Lamentations 5:14' 5 'Daniel 3:5' 3 'Daniel 3:7' 3 'Daniel 3:10' 3 'Daniel 3:15' 3 'Daniel 6:18' 6 'Amos 6:5' 6 'Luke 15:25' 15 'Revelation 18:22' 18
>>> b/'music'@(meth.ref()+meth.chn())
'1 Samuel 18:6' 254 '1 Chronicles 15:16' 353 '1 Chronicles 16:42' 354 '2 Chronicles 5:13' 372 '2 Chronicles 7:6' 374 '2 Chronicles 23:13' 390 '2 Chronicles 34:12' 401 'Nehemiah 12:36' 425 'Ecclesiastes 2:8' 661 'Ecclesiastes 12:4' 671 'Lamentations 3:63' 800 'Lamentations 5:14' 802 'Daniel 3:5' 853 'Daniel 3:7' 853 'Daniel 3:10' 853 'Daniel 3:15' 853 'Daniel 6:18' 856 'Amos 6:5' 885 'Luke 15:25' 988 'Revelation 18:22' 1185
>>> b/'music'@(meth.ref()+meth.chn()+meth.vn())
'1 Samuel 18:6' 254 7683 '1 Chronicles 15:16' 353 10808 '1 Chronicles 16:42' 354 10863 '2 Chronicles 5:13' 372 11282 '2 Chronicles 7:6' 374 11331 '2 Chronicles 23:13' 390 11670 '2 Chronicles 34:12' 401 11946 'Nehemiah 12:36' 425 12661 'Ecclesiastes 2:8' 661 17342 'Ecclesiastes 12:4' 671 17528 'Lamentations 3:63' 800 20418 'Lamentations 5:14' 802 20457 'Daniel 3:5' 853 21813 'Daniel 3:7' 853 21815 'Daniel 3:10' 853 21818 'Daniel 3:15' 853 21823 'Daniel 6:18' 856 21924 'Amos 6:5' 885 22456 'Luke 15:25' 988 25614 'Revelation 18:22' 1185 31016
>>> b/'music'*L@(meth.ref()+meth.chn()+meth.vn())
['1 Samuel 18:6' 254 7683, '1 Chronicles 15:16' 353 10808, '1 Chronicles 16:42' 354 10863, '2 Chronicles 5:13' 372 11282, '2 Chronicles 7:6' 374 11331, '2 Chronicles 23:13' 390 11670, '2 Chronicles 34:12' 401 11946, 'Nehemiah 12:36' 425 12661, 'Ecclesiastes 2:8' 661 17342, 'Ecclesiastes 12:4' 671 17528, 'Lamentations 3:63' 800 20418, 'Lamentations 5:14' 802 20457, 'Daniel 3:5' 853 21813, 'Daniel 3:7' 853 21815, 'Daniel 3:10' 853 21818, 'Daniel 3:15' 853 21823, 'Daniel 6:18' 856 21924, 'Amos 6:5' 885 22456, 'Luke 15:25' 988 25614, 'Revelation 18:22' 1185 31016]
>>> b/'music'*L@(meth.ref()+meth.chn()+meth.vn())*prettyprint.prettyprint
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'prettyprint' is not defined
>>> b/'music'*L@(meth.ref()+meth.chn()+meth.vn())*pretty.prettyprint
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'pretty' is not defined
>>> b/'music'*L@(meth.ref()+meth.chn()+meth.vn())*pretty.prettyprint
[1]+  Stopped                 python3 -i -c "from bible import *;"
pi@raspberrypi:~ $ fg
python3 -i -c "from bible import *;"
^[[A

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'pretty' is not defined
>>> pr
prime(     primes(    print(     prod(      prop(      property(  
>>> auto
<module 'auto.auto' from '/home/pi/python/auto/auto.py'>
>>> from auto import *
>>> pr
prime(     primes(    print(     prod(      prop(      property(  
>>> b/'God Almighty'
Genesis 28:3;35:11;43:14;48:3;Exodus 6:3;Revelation 4:8;11:17;15:3;16:7,14;21:22 (11 verses)
>>> p(_)
Genesis 28:3 And God Almighty bless thee, and make thee fruitful, and multiply thee, that thou mayest be a multitude of people;
Genesis 35:11 And God said unto him, I am God Almighty: be fruitful and multiply; a nation and a company of nations shall be of thee, and kings shall come out of thy loins;
Genesis 43:14 And God Almighty give you mercy before the man, that he may send away your other brother, and Benjamin. If I be bereaved of my children, I am bereaved.
Genesis 48:3 And Jacob said unto Joseph, God Almighty appeared unto me at Luz in the land of Canaan, and blessed me,
Exodus 6:3 And I appeared unto Abraham, unto Isaac, and unto Jacob, by the name of God Almighty, but by my name JEHOVAH was I not known to them.
Revelation 4:8 And the four beasts had each of them six wings about him; and they were full of eyes within: and they rest not day and night, saying, Holy, holy, holy, LORD God Almighty, which was, and is, and is to come.
Revelation 11:17 Saying, We give thee thanks, O LORD God Almighty, which art, and wast, and art to come; because thou hast taken to thee thy great power, and hast reigned.
Revelation 15:3 And they sing the song of Moses the servant of God, and the song of the Lamb, saying, Great and marvellous are thy works, Lord God Almighty; just and true are thy ways, thou King of saints.
Revelation 16:7 And I heard another out of the altar say, Even so, Lord God Almighty, true and righteous are thy judgments.
Revelation 16:14 For they are the spirits of devils, working miracles, which go forth unto the kings of the earth and of the whole world, to gather them to the battle of that great day of God Almighty.
Revelation 21:22 And I saw no temple therein: for the Lord God Almighty and the Lamb are the temple of it.
>>> b/'God'/'Almighty'
Genesis 17:1;28:3;35:11;43:14;48:3;49:25;Exodus 6:3;Numbers 24:4,16;Job 5:17;6:4;8:3,5;11:7;13:3;15:25;22:17,26;23:16;27:2,10-11,13;31:2;33:4;34:10,12;35:13;40:2;Ezekiel 10:5;Revelation 4:8;11:17;15:3;16:7,14;19:15;21:22 (37 verses)
>>> p(_)
Genesis 17:1 And when Abram was ninety years old and nine, the LORD appeared to Abram, and said unto him, I am the Almighty God; walk before me, and be thou perfect.
Genesis 28:3 And God Almighty bless thee, and make thee fruitful, and multiply thee, that thou mayest be a multitude of people;
Genesis 35:11 And God said unto him, I am God Almighty: be fruitful and multiply; a nation and a company of nations shall be of thee, and kings shall come out of thy loins;
Genesis 43:14 And God Almighty give you mercy before the man, that he may send away your other brother, and Benjamin. If I be bereaved of my children, I am bereaved.
Genesis 48:3 And Jacob said unto Joseph, God Almighty appeared unto me at Luz in the land of Canaan, and blessed me,
Genesis 49:25 Even by the God of thy father, who shall help thee; and by the Almighty, who shall bless thee with blessings of heaven above, blessings of the deep that lieth under, blessings of the breasts, and of the womb:
Exodus 6:3 And I appeared unto Abraham, unto Isaac, and unto Jacob, by the name of God Almighty, but by my name JEHOVAH was I not known to them.
Numbers 24:4 He hath said, which heard the words of God, which saw the vision of the Almighty, falling into a trance, but having his eyes open:
Numbers 24:16 He hath said, which heard the words of God, and knew the knowledge of the most High, which saw the vision of the Almighty, falling into a trance, but having his eyes open:
Job 5:17 Behold, happy is the man whom God correcteth: therefore despise not thou the chastening of the Almighty:
Job 6:4 For the arrows of the Almighty are within me, the poison whereof drinketh up my spirit: the terrors of God do set themselves in array against me.
Job 8:3 Doth God pervert judgment? or doth the Almighty pervert justice?
Job 8:5 If thou wouldest seek unto God betimes, and make thy supplication to the Almighty;
Job 11:7 Canst thou by searching find out God? canst thou find out the Almighty unto perfection?
Job 13:3 Surely I would speak to the Almighty, and I desire to reason with God.
Job 15:25 For he stretcheth out his hand against God, and strengtheneth himself against the Almighty.
Job 22:17 Which said unto God, Depart from us: and what can the Almighty do for them?
Job 22:26 For then shalt thou have thy delight in the Almighty, and shalt lift up thy face unto God.
Job 23:16 For God maketh my heart soft, and the Almighty troubleth me:
Job 27
2 As God liveth, who hath taken away my judgment; and the Almighty, who hath vexed my soul;
10 Will he delight himself in the Almighty? will he always call upon God?
11 I will teach you by the hand of God: that which is with the Almighty will I not conceal.
13 This is the portion of a wicked man with God, and the heritage of oppressors, which they shall receive of the Almighty.
Job 31:2 For what portion of God is there from above? and what inheritance of the Almighty from on high?
Job 33:4 The spirit of God hath made me, and the breath of the Almighty hath given me life.
Job 34:10 Therefore hearken unto me ye men of understanding: far be it from God, that he should do wickedness; and from the Almighty, that he should commit iniquity.
Job 34:12 Yea, surely God will not do wickedly, neither will the Almighty pervert judgment.
Job 35:13 Surely God will not hear vanity, neither will the Almighty regard it.
Job 40:2 Shall he that contendeth with the Almighty instruct him? he that reproveth God, let him answer it.
Ezekiel 10:5 And the sound of the cherubims' wings was heard even to the outer court, as the voice of the Almighty God when he speaketh.
Revelation 4:8 And the four beasts had each of them six wings about him; and they were full of eyes within: and they rest not day and night, saying, Holy, holy, holy, LORD God Almighty, which was, and is, and is to come.
Revelation 11:17 Saying, We give thee thanks, O LORD God Almighty, which art, and wast, and art to come; because thou hast taken to thee thy great power, and hast reigned.
Revelation 15:3 And they sing the song of Moses the servant of God, and the song of the Lamb, saying, Great and marvellous are thy works, Lord God Almighty; just and true are thy ways, thou King of saints.
Revelation 16:7 And I heard another out of the altar say, Even so, Lord God Almighty, true and righteous are thy judgments.
Revelation 16:14 For they are the spirits of devils, working miracles, which go forth unto the kings of the earth and of the whole world, to gather them to the battle of that great day of God Almighty.
Revelation 19:15 And out of his mouth goeth a sharp sword, that with it he should smite the nations: and he shall rule them with a rod of iron: and he treadeth the winepress of the fierceness and wrath of Almighty God.
Revelation 21:22 And I saw no temple therein: for the Lord God Almighty and the Lamb are the temple of it.
>>> b/'gates of hell'
Matthew 16:18 And I say also unto thee, That thou art Peter, and upon this rock I will build my church; and the gates of hell shall not prevail against it.
>>> tells("el shaddai')
  File "<stdin>", line 1
    tells("el shaddai')
                      ^
SyntaxError: EOL while scanning string literal
>>> tells("el shadai")
el shadai  =
 2    6    8
17   42    59
35   123  158
>>> tells("God el shadai")
God el shadai  =
 3   2    6    11
 26 17   42    85
 71 35   123  229
>>> tells("Jehovah el shadai")
Jehovah el shadai  =
   7     2    6    15
   69   17   42   128
  492   35   123  650
>>> tells("el shadai")
el shadai  =
 2    6    8
17   42    59
35   123  158
>>> 2**7
128
>>> tells("lord god almighty")
lord god almighty   =
  4   3      8     15
 49   26    95     170
 184  71    995   1250
>>> 1704
1704
>>> tells("TEKEL UPHARSIN")
TEKEL UPHARSIN  =
  5       8     13
  53     106   159
 260     628   888
>>> tells("TEKEL PERES")
TEKEL PERES  =
  5     5    10
  53    63  116
 260   270  530
>>> pn(270)
1733
>>> tells("Authorised King James Version")
Authorised King James Version   =
    10       4    5      7     26
    120     41    48    102    311
    777     86   156    714   1733
>>> chr(42)
'*'
>>> chr(48)
'0'
>>> 48+49
97
>>> ord("L")
76
>>> 2021-1918
103
>>> b/'mustard'
Matthew 13:31;17:20;Mark 4:31;Luke 13:19;17:6 (5 verses)
>>> p(_)
Matthew 13:31 Another parable put he forth unto them, saying, The kingdom of heaven is like to a grain of mustard seed, which a man took, and sowed in his field:
Matthew 17:20 And Jesus said unto them, Because of your unbelief: for verily I say unto you, If ye have faith as a grain of mustard seed, ye shall say unto this mountain, Remove hence to yonder place; and it shall remove; and nothing shall be impossible unto you.
Mark 4:31 It is like a grain of mustard seed, which, when it is sown in the earth, is less than all the seeds that be in the earth:
Luke 13:19 It is like a grain of mustard seed, which a man took, and cast into his garden; and it grew, and waxed a great tree; and the fowls of the air lodged in the branches of it.
Luke 17:6 And the Lord said, If ye had faith as a grain of mustard seed, ye might say unto this sycamine tree, Be thou plucked up by the root, and be thou planted in the sea; and it should obey you.
>>> 53*np
16
>>> 11*np
5
>>> 23*np
9
>>> 929*npp
20
>>> b/'mighty'+b/'almighty'
Genesis 6:4;10:8-9;17:1;18:18;23:6;28:3;35:11;43:14;48:3;49:24-25;Exodus 1:7,20;3:19;6:3;9:28;10:19;15:10,15;32:11;Leviticus 19:15;Numbers 22:6;24:4,16;Deuteronomy 3:24;4:34,37;5:15;6:21;7:8,19,21,23;9:26,29;10:17;11:2;26:5,8;34:12;Joshua 1:14;4:24;6:2;8:3;10:2,7;Judges 5:13,22-23;6:12;11:1;Ruth 1:20-21;2:1;1 Samuel 2:4;4:8;9:1;16:18;2 Samuel 1:19,21-22,25,27;10:7;16:6;17:8,10;20:7;23:8-9,16-17,22;1 Kings 1:8,10;11:28;2 Kings 5:1;15:20;24:14-15;1 Chronicles 1:10;5:24;7:7,9,11,40;8:40;11:10-11;12:1,4,21,25,28,30;19:8;26:6,31;27:6;28:1;29:24;2 Chronicles 6:32;13:3,21;14:8;17:13-14,16-17;25:6;26:12-13;27:6;28:7;32:3,21;Ezra 4:20;7:28;Nehemiah 3:16;9:11,32;11:14;Job 5:15,17;6:4,14,23;8:3,5;9:4;11:7;12:19,21;13:3;15:25;21:7,15,20;22:3,8,17,23,25-26;23:16;24:1,22;27:2,10-11,13;29:5;31:2,35;32:8;33:4;34:10,12,20,24;35:9,13;36:5;37:23;40:2;41:25;Psalms 24:8;29:1;33:16;45:3;50:1;52:1;59:3;68:14,33;69:4;74:15;78:65;82:1;89:6,13,19,50;91:1;93:4;106:2,8;112:2;120:4;127:4;132:2,5;135:10;145:4,12;150:2;Proverbs 16:32;18:18;21:22;23:11;Ecclesiastes 7:19;Song of Solomon 4:4;Isaiah 1:24;3:2,25;5:15,22;9:6;10:21,34;11:15;13:3,6;17:12;21:17;22:17;28:2;30:29;31:8;42:13;43:16;49:24-26;60:16;63:1;Jeremiah 5:15-16;9:23;14:9;20:11;26:21;32:18-19;33:3;41:16;46:5-6,9,12;48:14,41;49:22;50:9,36;51:30,56-57;Lamentations 1:15;Ezekiel 1:24;10:5;17:13,17;20:33-34;31:11;32:12,21,27;38:15;39:18,20;Daniel 3:20;4:3;8:24;9:15;11:3,25;Hosea 10:13;Joel 1:15;2:7;3:9,11;Amos 2:14,16;5:12,24;Obadiah 1:9;Jonah 1:4;Nahum 2:3;Habakkuk 1:12;Zephaniah 1:14;3:17;Zechariah 9:13;10:5,7;11:2;Matthew 11:20-21,23;13:54,58;14:2;Mark 6:2,5,14;Luke 1:49,52;9:43;10:13;15:14;19:37;24:19;Acts 2:2;7:22;18:24;Romans 15:19;1 Corinthians 1:26-27;2 Corinthians 6:18;10:4;12:12;13:3;Galatians 2:8;Ephesians 1:19;2 Thessalonians 1:7;1 Peter 5:6;Revelation 1:8;4:8;6:13,15;10:1;11:17;15:3;16:7,14,18;18:10,21;19:6,15,18;21:22 (334 verses)
>>> tells("lemuel")
 l e  m  u  e  l  =
 1 1  1  1  1  1  6
12 5 13  21 5 12  68
30 5 40 300 5 30 410
>>> tells("chapter")
c h a  p  t  e  r  =
1 1 1  1  1  1  1  7
3 8 1 16  20 5 18  71
3 8 1 70 200 5 90 377
>>> 377*ns
[13, 29] [6, 10]
>>> (16,11)@on
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for @: 'tuple' and 'On'
>>> (16,11)@pn
(53, 31)
>>> 15*pn
47
>>> b/'good'/'pure'/'honest'
Philippians 4:8 Finally, brethren, whatsoever things are true, whatsoever things are honest, whatsoever things are just, whatsoever things are pure, whatsoever things are lovely, whatsoever things are of good report; if there be any virtue, and if there be any praise, think on these things.
>>> tells("LORD GOD ALMIGHTY")
LORD GOD ALMIGHTY   =
  4   3      8     15
 49   26    95     170
 184  71    995   1250
>>> tells("GOD ALMIGHTY")
GOD ALMIGHTY   =
 3      8     11
 26    95     121
 71    995   1066
>>> pp(20)
929
>>> pp(36)
16061
>>> pp(46)
19991
>>> 1.02**360
1247.561127748876
>>> 1.002**360
2.052956518999314
>>> 42*pn
181
>>> rect(6)
42
>>> rect(31))
  File "<stdin>", line 1
    rect(31))
            ^
SyntaxError: invalid syntax
>>> rect(31)
992
>>> rect(30)
930
>>> b/42
Numbers 35:6;2 Kings 2:24;10:14;2 Chronicles 22:2;Ezra 2:24;Nehemiah 7:28;Revelation 11:2;13:5 (8 verses)
>>> p(_)
Numbers 35:6 And among the cities which ye shall give unto the Levites there shall be six cities for refuge, which ye shall appoint for the manslayer, that he may flee thither: and to them ye shall add forty and two cities.
2 Kings 2:24 And he turned back, and looked on them, and cursed them in the name of the LORD. And there came forth two she bears out of the wood, and tare forty and two children of them.
2 Kings 10:14 And he said, Take them alive. And they took them alive, and slew them at the pit of the shearing house, even two and forty men; neither left he any of them.
2 Chronicles 22:2 Forty and two years old was Ahaziah when he began to reign, and he reigned one year in Jerusalem. His mother's name also was Athaliah the daughter of Omri.
Ezra 2:24 The children of Azmaveth, forty and two.
Nehemiah 7:28 The men of Bethazmaveth, forty and two.
Revelation 11:2 But the court which is without the temple leave out, and measure it not; for it is given unto the Gentiles: and the holy city shall they tread under foot forty and two months.
Revelation 13:5 And there was given unto him a mouth speaking great things and blasphemies; and power was given unto him to continue forty and two months.
>>> Ezra[2:24].tells()
The children of Azmaveth, forty and two.   =
 3      8     2     8       5    3    3   32
 33    73    21     96      84   19  58   384
213    199   66    1455    1056  55  760 3804
>>> 42*30
1260
>>> tells("Jehovah el shadai")
Jehovah el shadai  =
   7     2    6    15
   69   17   42   128
  492   35   123  650
>>> tells("opened")
 o  p e  n e d  =
 1  1 1  1 1 1  6
15 16 5 14 5 4  59
60 70 5 50 5 4 194
>>> tells("Michael Wilcox")
Michael Wilcox   =
   7       6    13
   51     86    137
   96    1202  1298
>>> tells("I saw heaven opened")
I saw heaven opened   =
1  3     6      6    16
9  43   55     59    166
9 601   469    194  1273
>>> 888*7
6216
>>> tells("sarai")
 s  a  r a i  =
 1  1  1 1 1  5
 19 1 18 1 9  48
100 1 90 1 9 201
>>> tells("Abram")
A b  r a  m  =
1 1  1 1  1  5
1 2 18 1 13  35
1 2 90 1 40 134
>>> tells("Abraham")
A b  r a h a  m  =
1 1  1 1 1 1  1  7
1 2 18 1 8 1 13  44
1 2 90 1 8 1 40 143
>>> 75**(1/2)
8.660254037844387
>>> 75**(1/3)
4.217163326508746
>>> tells("wife")
 w  i f e  =
 1  1 1 1  4
 23 9 6 5  43
500 9 6 5 520
>>> tells("thy wife")
thy wife   =
 3    4    7
 53  43   96
908  520 1428
>>> tells("she was thy wife")
she was thy wife   =
 3   3   3    4   13
 32  43  53  43   171
113 601 908  520 2142
>>> five.books()
[Genesis 1:1-50:26 (1533 verses), Exodus 1:1-40:38 (1213 verses), Leviticus 1:1-27:34 (859 verses), Numbers 1:1-36:13 (1288 verses), Deuteronomy 1:1-34:12 (959 verses)]
>>> five.vc()
5852
>>> five.chaptercount()
187
>>> 34**.5
5.830951894845301
>>> 59**.5
7.681145747868608
>>> chr(36)
'$'
>>> chr(35)
'#'
>>> 676*ns
[2, 2, 13, 13] [1, 1, 6, 6]

