>>> 3*pi/4
2.356194490192345
>>> b.ch[353]
b.ch(               b.chaptercount(     b.chc(
b.chapter(          b.chapterix         b.chn(
b.chapterandverse(  b.chapters(         
>>> b.ch(353)
1 Chronicles 15:1-29 (29 verses)
>>> b.ch(676)
Song of Solomon 5:1-16 (16 verses)
>>> p(_)
Song of Solomon 5
1 I am come into my garden, my sister, my spouse: I have gathered my myrrh with my spice; I have eaten my honeycomb with my honey; I have drunk my wine with my milk: eat, O friends; drink, yea, drink abundantly, O beloved.
2 I sleep, but my heart waketh: it is the voice of my beloved that knocketh, saying, Open to me, my sister, my love, my dove, my undefiled: for my head is filled with dew, and my locks with the drops of the night.
3 I have put off my coat; how shall I put it on? I have washed my feet; how shall I defile them?
4 My beloved put in his hand by the hole of the door, and my bowels were moved for him.
5 I rose up to open to my beloved; and my hands dropped with myrrh, and my fingers with sweet smelling myrrh, upon the handles of the lock.
6 I opened to my beloved; but my beloved had withdrawn himself, and was gone: my soul failed when he spake: I sought him, but I could not find him; I called him, but he gave me no answer.
7 The watchmen that went about the city found me, they smote me, they wounded me; the keepers of the walls took away my veil from me.
8 I charge you, O daughters of Jerusalem, if ye find my beloved, that ye tell him, that I am sick of love.
9 What is thy beloved more than another beloved, O thou fairest among women? what is thy beloved more than another beloved, that thou dost so charge us?
10 My beloved is white and ruddy, the chiefest among ten thousand.
11 His head is as the most fine gold, his locks are bushy, and black as a raven.
12 His eyes are as the eyes of doves by the rivers of waters, washed with milk, and fitly set.
13 His cheeks are as a bed of spices, as sweet flowers: his lips like lilies, dropping sweet smelling myrrh.
14 His hands are as gold rings set with the beryl: his belly is as bright ivory overlaid with sapphires.
15 His legs are as pillars of marble, set upon sockets of fine gold: his countenance is as Lebanon, excellent as the cedars.
16 His mouth is most sweet: yea, he is altogether lovely. This is my beloved, and this is my friend, O daughters of Jerusalem.
>>> tells("O beloved")
 O beloved  =
 1    7     8
15    65    80
60   506   566
>>> b.ch(353)
1 Chronicles 15:1-29 (29 verses)
>>> p(_)
1 Chronicles 15
1 And David made him houses in the city of David, and prepared a place for the ark of God, and pitched for it a tent.
2 Then David said, None ought to carry the ark of God but the Levites: for them hath the LORD chosen to carry the ark of God, and to minister unto him for ever.
3 And David gathered all Israel together to Jerusalem, to bring up the ark of the LORD unto his place, which he had prepared for it.
4 And David assembled the children of Aaron, and the Levites:
5 Of the sons of Kohath; Uriel the chief, and his brethren an hundred and twenty:
6 Of the sons of Merari; Asaiah the chief, and his brethren two hundred and twenty:
7 Of the sons of Gershom; Joel the chief and his brethren an hundred and thirty:
8 Of the sons of Elizaphan; Shemaiah the chief, and his brethren two hundred:
9 Of the sons of Hebron; Eliel the chief, and his brethren fourscore:
10 Of the sons of Uzziel; Amminadab the chief, and his brethren an hundred and twelve.
11 And David called for Zadok and Abiathar the priests, and for the Levites, for Uriel, Asaiah, and Joel, Shemaiah, and Eliel, and Amminadab,
12 And said unto them, Ye are the chief of the fathers of the Levites: sanctify yourselves, both ye and your brethren, that ye may bring up the ark of the LORD God of Israel unto the place that I have prepared for it.
13 For because ye did it not at the first, the LORD our God made a breach upon us, for that we sought him not after the due order.
14 So the priests and the Levites sanctified themselves to bring up the ark of the LORD God of Israel.
15 And the children of the Levites bare the ark of God upon their shoulders with the staves thereon, as Moses commanded according to the word of the LORD.
16 And David spake to the chief of the Levites to appoint their brethren to be the singers with instruments of musick, psalteries and harps and cymbals, sounding, by lifting up the voice with joy.
17 So the Levites appointed Heman the son of Joel; and of his brethren, Asaph the son of Berechiah; and of the sons of Merari their brethren, Ethan the son of Kushaiah;
18 And with them their brethren of the second degree, Zechariah, Ben, and Jaaziel, and Shemiramoth, and Jehiel, and Unni, Eliab, and Benaiah, and Maaseiah, and Mattithiah, and Elipheleh, and Mikneiah, and Obededom, and Jeiel, the porters.
19 So the singers, Heman, Asaph, and Ethan, were appointed to sound with cymbals of brass;
20 And Zechariah, and Aziel, and Shemiramoth, and Jehiel, and Unni, and Eliab, and Maaseiah, and Benaiah, with psalteries on Alamoth;
21 And Mattithiah, and Elipheleh, and Mikneiah, and Obededom, and Jeiel, and Azaziah, with harps on the Sheminith to excel.
22 And Chenaniah, chief of the Levites, was for song: he instructed about the song, because he was skilful.
23 And Berechiah and Elkanah were doorkeepers for the ark.
24 And Shebaniah, and Jehoshaphat, and Nethaneel, and Amasai, and Zechariah, and Benaiah, and Eliezer, the priests, did blow with the trumpets before the ark of God: and Obededom and Jehiah were doorkeepers for the ark.
25 So David, and the elders of Israel, and the captains over thousands, went to bring up the ark of the covenant of the LORD out of the house of Obededom with joy.
26 And it came to pass, when God helped the Levites that bare the ark of the covenant of the LORD, that they offered seven bullocks and seven rams.
27 And David was clothed with a robe of fine linen, and all the Levites that bare the ark, and the singers, and Chenaniah the master of the song with the singers: David also had upon him an ephod of linen.
28 Thus all Israel brought up the ark of the covenant of the LORD with shouting, and with sound of the cornet, and with trumpets, and with cymbals, making a noise with psalteries and harps.
29 And it came to pass, as the ark of the covenant of the LORD came to the city of David, that Michal, the daughter of Saul looking out at a window saw king David dancing and playing: and she despised him in her heart.
>>> tells("TEKEL UPHARSIN")
TEKEL UPHARSIN  =
  5       8     13
  53     106   159
 260     628   888
>>> tells("TEKEL UPHARSIN")
TEKEL UPHARSIN  =
  5       8     13
  53     106   159
 260     628   888
>>> 353*ppn
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'ppn' is not defined
>>> 353*npp
12
>>> Psalm[35:3].tells()
Draw out also the spear, and stop the  way against them that persecute me: say unto  my soul, I am thy salvation.   =
  4   3    4   3     5    3    4   3    3     7      4    4      9      2   3    4   2    4   1  2  3       9       86
 46   56  47   33   59    19  70   33  49     71    46   49     112     18  45  70   38   67  9 14  53     113     1117
 595 560  191 213   266   55  430 213 1201   368    253  409    778     45 801  610 740  490  9 41 908     851    10027
>>> Psalm[67:6].tells()
Then shall the earth yield her increase; and God, even our own God, shall bless us.   =
  4    5    3    5     5    3      8      3    3    4   3   3    3    5     5    2   64
 47    52   33   52    55   31     74     19  26   46   54  52  26    52    57   40  716
 263  169  213  304   748  103    263     55  71   460 450 610  71   169   237  400 4586
>>> 

