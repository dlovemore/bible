>>> tells("King David")
King David  =
  4    5    9
 41    40   81
 86   418  504
>>> tells("King Solomon")
King Solomon  =
  4     7     11
 41    103   144
 86    400   486
>>> tells("Honey")
H  o  n e  y   =
1  1  1 1  1   5
8 15 14 5  25  67
8 60 50 5 700 823
>>> tells("milk Honey")
milk Honey  =
  4    5    9
 45    67  112
 99   823  922
>>> tells("milk and honey")
milk and honey  =
  4   3    5    12
 45   19   67  131
 99   55  823  977
>>> tells("milk")
 m i  l  k  =
 1 1  1  1  4
13 9 12 11 45
40 9 30 20 99
>>> tells("spilt milk")
spilt milk  =
  5     4   9
  76   45  121
 409   99  508
>>> tells("no use crying")
 no use crying   =
 2   3     6    11
 29  45   76    150
110 405   859  1374
>>> 150/121
1.2396694214876034
>>> tells("know use crying")
know use crying   =
  4   3     6    13
 63   45   76    184
 630 405   859  1894
>>> 184/121
1.5206611570247934
>>> b/'cross'
Obadiah 1:14;Matthew 10:38;16:24;27:32,40,42;Mark 8:34;10:21;15:21,30,32;Luke 9:23;14:27;23:26;John 19:17,19,25,31;1 Corinthians 1:17-18;Galatians 5:11;6:12,14;Ephesians 2:16;Philippians 2:8;3:18;Colossians 1:20;2:14;Hebrews 12:2 (29 verses)
>>> ot/'cross'
Obadiah 1:14 Neither shouldest thou have stood in the crossway, to cut off those of his that did escape; neither shouldest thou have delivered up those of his that did remain in the day of distress.
>>> _.cv()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 106, in __getattr__
    if len(bs)!=1: raise AttributeError('No attribute '+name)
AttributeError: No attribute cv
>>> _.chapter()
_.chapter(          _.chaptercount(     _.chapters(
_.chapterandverse(  _.chapterix         
>>> _.chapter()
1
>>> ot/'cross'
Obadiah 1:14 Neither shouldest thou have stood in the crossway, to cut off those of his that did escape; neither shouldest thou have delivered up those of his that did remain in the day of distress.
>>> (_.vn())
22525
>>> ot/'cross'
Obadiah 1:14 Neither shouldest thou have stood in the crossway, to cut off those of his that did escape; neither shouldest thou have delivered up those of his that did remain in the day of distress.
>>> _.chn()
889
>>> b/'lord jehovah'
Isaiah 12:2 Behold, God is my salvation; I will trust, and not be afraid: for the LORD JEHOVAH is my strength and my song; he also is become my salvation.
Isaiah 26:4 Trust ye in the LORD for ever: for in the LORD JEHOVAH is everlasting strength:
>>> b/'lord jehovah'
Isaiah 12:2 Behold, God is my salvation; I will trust, and not be afraid: for the LORD JEHOVAH is my strength and my song; he also is become my salvation.
Isaiah 26:4 Trust ye in the LORD for ever: for in the LORD JEHOVAH is everlasting strength:
>>> _@method.chn
691 705
>>> b/'lord jehovah'
Isaiah 12:2 Behold, God is my salvation; I will trust, and not be afraid: for the LORD JEHOVAH is my strength and my song; he also is become my salvation.
Isaiah 26:4 Trust ye in the LORD for ever: for in the LORD JEHOVAH is everlasting strength:
>>> _@method.chn
691 705
>>> 
>>> b/'lord jehovah'
Isaiah 12:2 Behold, God is my salvation; I will trust, and not be afraid: for the LORD JEHOVAH is my strength and my song; he also is become my salvation.
Isaiah 26:4 Trust ye in the LORD for ever: for in the LORD JEHOVAH is everlasting strength:
>>> _@method.vn
17903 18135
>>> (18135+17903+1)/2
18019.5
>>> b.vn(18019,18020)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: vn() takes 1 positional argument but 3 were given
>>> b.vn(18019)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: vn() takes 1 positional argument but 2 were given
>>> b.vi(18019)
Isaiah 19:14 The LORD hath mingled a perverse spirit in the midst thereof: and they have caused Egypt to err in every work thereof, as a drunken man staggereth in his vomit.
>>> b.vi(18020)
Isaiah 19:15 Neither shall there be any work for Egypt, which the head or tail, branch or rush, may do.
>>> 22525*ns
[5, 5, 17, 53] [3, 3, 7, 16]
>>> 25*17
425
>>> 25*17*53
22525
>>> 5*17
85
>>> 225*ns
[3, 3, 5, 5] [2, 2, 3, 3]
>>> 48*pn
223
>>> 49*pn
227
>>> 353*np
71
>>> 353*npp
12
>>> 71*ns
[71] [20]
>>> Obadiah[14]
Obadiah 1:14 Neither shouldest thou have stood in the crossway, to cut off those of his that did escape; neither shouldest thou have delivered up those of his that did remain in the day of distress.
>>> Obadiah[14].tells()
Neither shouldest thou have stood in the crossway,  to cut off those of his that did escape; neither shouldest thou have delivered  up those of his that did remain in the day of distress.   =
   7        9       4    4    5    2  3      8      2   3   3    5    2  3    4   3     6       7        9       4    4      9      2    5    2  3    4   3     6    2  3   3   2     8      149
   79      123     64   36    73  23  33    123     35  44  27   67  21  36  49   17    49      79      123     64   36      84     37   67  21  36  49   17   60   23  33  30 21    113     1792
  367      807     568  414  424  59 213    1554   260 503  72  373  66 117  409  17   184     367      807     568  414    552    370  373  66 117  409  17   195  59 213 705 66    608    12313
>>> Habakkuk[3:6]
Habakkuk 3:6 He stood, and measured the earth: he beheld, and drove asunder the nations; and the everlasting mountains were scattered, the perpetual hills did bow: his ways are everlasting.
>>> Obadiah[14].tells()
Neither shouldest thou have stood in the crossway,  to cut off those of his that did escape; neither shouldest thou have delivered  up those of his that did remain in the day of distress.   =
   7        9       4    4    5    2  3      8      2   3   3    5    2  3    4   3     6       7        9       4    4      9      2    5    2  3    4   3     6    2  3   3   2     8      149
   79      123     64   36    73  23  33    123     35  44  27   67  21  36  49   17    49      79      123     64   36      84     37   67  21  36  49   17   60   23  33  30 21    113     1792
  367      807     568  414  424  59 213    1554   260 503  72  373  66 117  409  17   184     367      807     568  414    552    370  373  66 117  409  17   195  59 213 705 66    608    12313
>>> Habakkuk[3:6].tells()
He stood, and measured the earth: he beheld, and drove asunder the nations; and the everlasting mountains were scattered, the perpetual hills did bow: his ways are everlasting.   =
 2    5    3      8     3     5    2    6     3    5      7     3      7     3   3       11         9       4       9      3      9       5    3    3   3    4   3       11       142
13   73    19    86     33   52   13    36    19   64     82    33    92     19  33     132        126     51      95      33    114      60   17  40   36  68   24      132      1595
13   73    19    86     33   52   13    36    19   64     82    33    92     19  33     132        126     51      95      33    114      60   17  40   36  68   24      132      1595
13   424   55    545   213   304  13    54    55  559    550   213    470    55 213     897        810     600     608    213    771     177   17  562 117 1301  96      897     10802
>>> Revelation[3:7].tells()
And  to the angel of the church in Philadelphia write; These things saith he that  is holy, he that  is true, he that hath the key of David, he that openeth, and  no man shutteth; and shutteth, and  no man openeth;   =
 3   2   3    5    2  3     6    2      12         5     5      6     5    2   4   2    4    2   4   2    4    2   4    4   3   3   2    5    2   4      7     3   2   3      8      3      8      3   2   3      7     161
 19  35  33   39  21  33   61   23      101       75     57    77     57  13  49   28   60  13  49   28   64  13  49   37   33  41 21   40   13  49     83     19  29  28    121     19    121     19  29  28    83     1810
 55 260 213   93  66 213   412  59      245       804   318    374   318  13  409 109  798  13  409 109  595  13  409  217 213 725 66   418  13  409    398    55 110  91    1021    55    1021    55 110  91    398   11773
>>> 353676*ns
[2, 2, 3, 29473] [1, 1, 2, 3202]
>>> b.vi(29473)
Colossians 1:7 As ye also learned of Epaphras our dear fellowservant, who is for you a faithful minister of Christ;
>>> _.
_.book(             _.divide(           _.text(
_.bookix            _.doc               _.v(
_.bookn(            _.ix                _.vc(
_.bookname(         _.lc(               _.verse(
_.booknames         _.lcs(              _.versecount(
_.books(            _.letters(          _.verses(
_.cav(              _.midch(            _.vi(
_.ch(               _.midchapter(       _.vn(
_.chapter(          _.midv(             _.vns(
_.chapterandverse(  _.midverse(         _.vs(
_.chaptercount(     _.midword(          _.wc(
_.chapterix         _.name(             _.wcs(
_.chapters(         _.numbers(          _.wordcount(
_.chc(              _.pattern(          _.words(
_.chn(              _.ref(              _.ws(
_.ci(               _.tell(             
_.count(            _.tells(            
>>> _.tells()
 As  ye also learned of Epaphras our dear fellowservant, who  is for  you a faithful minister of Christ;   =
 2   2    4     7     2     8     3    4        13        3   2   3    3  1     8        8     2    6     81
 20  30  47     59   21    84     54  28        172       46  28  39  61  1    83       107   21    77    978
101 705  191   185   66    345   450  100      1477      568 109 156 1060 1    560      503   66   410   7053
>>> e**pi
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for ** or pow(): 'Unique' and 'float'
>>> exp(pi)
23.140692632779267
>>> b/'LORD JEHOVAH'*meth.tells
Func(<function meth.<locals>.callmeth.<locals>.callmeth at 0xb64e2f18>)
>>> b/'LORD JEHOVAH'*meth.tells()
Behold, God  is  my salvation; I will trust, and not be afraid: for the LORD JEHOVAH  is  my strength and  my song; he also  is become  my salvation.   =
   6     3   2   2       9     1   4     5    3   3   2    6     3   3    4     7     2   2      8     3   2    4    2   4   2     6    2       9      109
   46    26  28  38     113    9  56    98    19  49  7    39    39  33  49     69    28  38    111    19  38   55  13  47   28   43    38     113    1289
  109    71 109 740     851    9  569   890   55 310  7   111   156 213  184   492   109 740    660    55 740  217  13  191 109   115  740     851    9416
Trust  ye in the LORD for ever: for in the LORD JEHOVAH  is everlasting strength:   =
  5    2   2  3    4   3    4    3   2  3    4     7     2       11         8      63
  98   30 23  33  49   39   50   39 23  33  49     69    28     132        111     806
 890  705 59 213  184 156  500  156 59 213  184   492   109     897        660    5477
>>> 

