>>> from bible import *
>>> Leviticus[14].tells()
And the LORD spake unto Moses, saying,   =
 3   3    4    5     4     5      6     30
 19  33  49    52   70    71      75    369
 55 213  184  196   610   305    867   2430
This shall be the law of the leper in the day of his cleansing: He shall be brought unto the priest:   =
  4    5    2  3   3   2  3    5    2  3   3   2  3       9      2   5    2    7      4   3     6     78
 56    52   7  33  36 21  33   56  23  33  30 21  36     84     13   52   7    91    70   33    87    874
 317  169   7 213 531 66 213  200  59 213 705 66 117     255    13  169   7   667    610 213   474   5284
And the priest shall go forth out of the camp; and the priest shall look, and, behold, if the plague of leprosy be healed in the leper;   =
 3   3     6     5    2   5    3   2  3    4    3   3     6     5     4     3     6     2  3     6    2    7     2    6    2  3     5    104
 19  33   87     52  22   67   56 21  33   33   19  33   87     52    53   19     46   15  33   62   21   110    7   35   23  33   56   1127
 55 213   474   169  67  364  560 66 213  114   55 213   474   169   170   55    109   15 213   413  66   1055   7   53   59 213   200  5834
Then shall the priest command  to take for him that  is  to be cleansed two birds alive and clean, and cedar wood, and scarlet, and hyssop:   =
  4    5    3     6      7     2    4   3   3    4   2   2   2     8     3    5     5    3     5    3    5     4    3      7     3     6     107
 47    52   33   87      63    35  37   39  30  49   28  35  7    63     58   52    49   19   35    19   31    57   19    78     19   102   1143
 263  169  213   474    198   260  226 156  57  409 109 260  7    198   760  205   445   55   89    55  103   624   55    429    55   1038  6912
And the priest shall command that one of the birds be killed in an earthen vessel over running water:   =
 3   3     6     5      7      4   3   2  3    5    2    6    2  2    7       6     4     7       5    82
 19  33   87     52     63    49   34 21  33   52   7   53   23 15    71     82    60     97     67    918
 55 213   474   169    198    409 115 66 213  205   7   98   59 51   359     640   555   556     796  5238
 As for the living bird, he shall take it, and the cedar wood, and the scarlet, and the hyssop, and shall dip them and the living bird in the blood of the bird that was killed over the running water:   =
 2   3   3     6     4    2   5     4   2   3   3    5     4    3   3      7     3   3     6     3    5    3    4   3   3     6     4   2  3    5    2  3    4    4   3     6     4   3     7       5    153
 20  39  33   73     33  13   52   37   29  19  33   31    57   19  33    78     19  33   102    19   52   29  46   19  33   73    33  23  33   48  21  33  33   49   43   53    60   33    97     67    1650
101 156 213   505   105  13  169   226 209  55 213  103   624   55 213    429    55 213   1038   55  169   83  253  55 213   505   105 59 213  156  66 213  105  409 601   98    555 213   556     796  10173
And he shall sprinkle upon him that  is  to be cleansed from the leprosy seven times, and shall pronounce him clean, and shall let the living bird loose into the open field.   =
 3   2   5       8      4   3    4   2   2   2     8      4   3     7      5      5    3    5       9      3     5    3    5    3   3     6     4    5     4   3    4     5    137
 19 13   52     104    66   30  49   28  35  7    63     52   33   110     65    66    19   52     121     30   35    19   52   37  33   73    33    66   58   33  50    36   1539
 55 13  169     374    480  57  409 109 260  7    198    196 213   1055   560    354   55  169     688     57   89    55  169  235 213   505   105  255   319 213  185   54   7875
And he that  is  to be cleansed shall wash his clothes, and shave off all his hair, and wash himself in water, that he may be clean: and after that he shall come into the camp, and shall tarry abroad out of his tent seven days.   =
 3   2   4   2   2   2     8      5     4   3      7     3    5    3   3   3    4    3    4     7     2    5     4   2  3   2    5    3    5     4   2   5     4    4   3    4    3    5     5      6    3   2  3    4    5     4    174
 19 13  49   28  35  7    63      52   51   36    82     19   55   27  25  36   36   19  51     72   23   67    49  13  39  7   35    19   50   49  13   52   36   58   33   33   19   52    82    41    56 21  36  59    65    49   1831
 55 13  409 109 260  7    198    169   609 117    406    55  514   72  61 117  108   55  609   198   59   796   409 13 741  7   89    55  302   409 13  169   108  319 213  114   55  169   1081   158  560 66 117  455  560   805  11983
But  it shall be  on the seventh day, that he shall shave all his hair off his head and his beard and his eyebrows, even all his hair he shall shave off: and he shall wash his clothes, also he shall wash his flesh in water, and he shall be clean.   =
 3   2    5    2  2   3     7      3    4   2   5     5    3   3    4   3   3    4   3   3    5    3   3      8       4   3   3    4   2   5     5     3   3   2   5     4   3      7      4   2   5     4   3    5    2    5    3   2   5    2    5    188
 43  29   52   7  29  33    93    30   49  13   52    55   25  36  36   27  36  18   19  36   30   19  36    112     46   25  36  36  13   52    55   27   19 13   52   51   36    82     47  13   52   51   36   50  23   67    19 13   52   7   35    1923
502 209  169   7 110 213   768    705  409 13  169   514   61 117  108  72 117  18   55 117  102   55 117    1462    460  61 117  108 13  169   514   72   55 13  169   609 117    406    191 13  169   609 117  149  59   796   55 13  169   7   89   11508
And  on the eighth day he shall take two he lambs without blemish, and one ewe lamb of the first year without blemish, and three tenth deals of fine flour for a meat offering, mingled with oil, and one log of oil.   =
 3   2   3     6    3   2   5     4   3   2   5      7        7     3   3   3    4   2  3    5     4     7        7     3    5     5     5    2   4    5    3  1   4      8        7      4    3   3   3   3   2   3   166
 19  29  33   57    30 13   52   37   58 13   47    116      68     19  34  33  28  21  33   72   49    116      68     19   56    67    41  21  34    72   39 1  39      80       64    60   36   19  34  34 21  36   1818
 55 110 213   237  705 13  169   226 760 13  173    1277     194    55 115 510  73  66 213  405   796   1277     194    55  308   463   140  66  70   486  156 1  246    233      145    717  99   55 115  97 66  99  11466
And the priest that maketh him clean shall present the man that  is  to be made clean, and those things, before the LORD,  at the door of the tabernacle of the congregation:   =
 3   3     6     4     6    3    5     5      7     3   3    4   2   2   2   4     5    3    5      6       6    3    4    2   3    4   2  3      10      2  3        12       135
 19  33   87    49    58    30   35    52     97    33  28  49   28  35  7  23    35    19   67     77     51    33   49   21  33  52  21  33     81     21  33      128      1417
 55 213   474   409   274   57   89   169    520   213  91  409 109 260  7  50    89    55  373    374     168  213  184  201 213  214 66 213     387    66 213      542      6970
And the priest shall take one he lamb, and offer him for a trespass offering, and the log of oil, and wave them for a wave offering before the LORD:   =
 3   3     6     5     4   3   2   4    3    5    3   3  1     8        8      3   3   3   2   3   3    4    4   3  1   4      8       6    3    4    115
 19  33   87     52   37   34 13   28   19   50   30  39 1    117       80     19  33  34 21  36   19  51   46   39 1  51     80      51    33   49  1202
 55 213   474   169   226 115 13   73   55  167   57 156 1    666      233     55 213  97 66  99   55  906  253 156 1  906    233     168  213  184  6278
And he shall slay the lamb in the place where he shall kill the sin offering and the burnt offering, in the holy place: for  as the sin offering  is the priest's,  so  is the trespass offering:  it  is most holy:   =
 3   2   5     4   3    4   2  3    5     5    2   5     4   3   3      8     3   3    5       8      2  3    4     5    3   2   3   3      8     2   3      7      2   2   3      8        8      2   2    4    4    160
 19 13   52   57   33  28  23  33   37    59  13   52   44   33  42    80     19  33   75      80    23  33  60    37    39  20  33  42    80     28  33    106     34  28  33    117       80     29  28  67    60   1835
 55 13  169   831 213  73  59 213  109   608  13  169   89  213 159    233    55 213  642     233    59 213  798   109  156 101 213 159    233   109 213    574    160 109 213    666      233    209 109  400  798  10196
And the priest shall take some of the blood of the trespass offering, and the priest shall put  it upon the tip of the right ear of him that  is  to be cleansed, and upon the thumb of his right hand, and upon the great toe of his right foot:   =
 3   3     6     5     4    4   2  3    5    2  3      8        8      3   3     6     5    3   2    4   3   3   2  3    5    3   2  3    4   2   2   2     8      3    4   3    5    2  3    5     4    3    4   3    5    3   2  3    5     4    185
 19  33   87     52   37   52  21  33   48  21  33    117       80     19  33   87     52   57  29  66   33  45 21  33   62   24 21  30  49   28  35  7     63     19  66   33   64  21  36   62    27   19  66   33   51   40 21  36   62    56   2139
 55 213   474   169   226  205 66 213  156  66 213    666      233     55 213   474   169  570 209  480 213 279 66 213  314   96 66  57  409 109 260  7    198     55  480 213  550  66 117  314    63   55  480 213  303  265 66 117  314   326  11409
And the priest shall take some of the log of oil, and pour  it into the palm of his own left hand:   =
 3   3     6     5     4    4   2  3   3   2   3   3    4   2    4   3    4   2  3   3    4    4    74
 19  33   87     52   37   52  21  33  34 21  36   19  70   29  58   33  42  21  36  52  43    27   855
 55 213   474   169   226  205 66 213  97 66  99   55  520 209  319 213  141 66 117 610  241   63  4437
And the priest shall dip his right finger in the oil that  is in his left hand, and shall sprinkle of the oil with his finger seven times before the LORD:   =
 3   3     6     5    3   3    5      6    2  3   3    4   2   2  3    4    4    3    5       8     2  3   3    4   3     6     5     5      6    3    4    121
 19  33   87     52   29  36   62    59   23  33  36  49   28 23  36  43    27   19   52     104   21  33  36  60   36   59     65    66    51    33   49  1359
 55 213   474   169   83 117  314    167  59 213  99  409 109 59 117  241   63   55  169     374   66 213  99  717 117   167   560   354    168  213  184  6417
And of the rest of the oil that  is in his hand shall the priest put upon the tip of the right ear of him that  is  to be cleansed, and upon the thumb of his right hand, and upon the great toe of his right foot, upon the blood of the trespass offering:   =
 3   2  3    4   2  3   3    4   2   2  3    4    5    3     6    3    4   3   3   2  3    5    3   2  3    4   2   2   2     8      3    4   3    5    2  3    5     4    3    4   3    5    3   2  3    5     4     4   3    5    2  3      8        8      192
 19 21  33  62  21  33  36  49   28 23  36  27    52   33   87    57  66   33  45 21  33   62   24 21  30  49   28  35  7     63     19  66   33   64  21  36   62    27   19  66   33   51   40 21  36   62    56   66   33   48  21  33    117       80     2244
 55 66 213  395 66 213  99  409 109 59 117  63   169  213   474  570  480 213 279 66 213  314   96 66  57  409 109 260  7    198     55  480 213  550  66 117  314    63   55  480 213  303  265 66 117  314   326   480 213  156  66 213    666      233    12081
And the remnant of the oil that  is in the priest's hand he shall pour upon the head of him that  is  to be cleansed: and the priest shall make an atonement for him before the LORD.   =
 3   3     7     2  3   3    4   2   2  3      7      4   2   5     4    4   3    4   2  3    4   2   2   2     8      3   3     6     5     4   2     9      3   3     6    3    4    139
 19  33    85   21  33  36  49   28 23  33    106    27  13   52   70   66   33  18  21  30  49   28  35  7     63     19  33   87     52   30  15    107     39  30   51    33   49  1523
 55 213   436   66 213  99  409 109 59 213    574    63  13  169   520  480 213  18  66  57  409 109 260  7    198     55 213   474   169   66  51    611    156  57   168  213  184  7445
And the priest shall offer the sin offering, and make an atonement for him that  is  to be cleansed from his uncleanness; and afterward he shall kill the burnt offering:   =
 3   3     6     5     5    3   3      8      3    4   2     9      3   3    4   2   2   2     8      4   3       11       3      9      2   5     4   3    5       8      135
 19  33   87     52    50   33  42     80     19  30  15    107     39  30  49   28  35  7    63     52   36      127      19     96    13   52   44   33   75      80    1445
 55 213   474   169   167  213 159    233     55  66  51    611    156  57  409 109 260  7    198    196 117      694      55    897    13  169   89  213  642     233    6980
And the priest shall offer the burnt offering and the meat offering upon the altar: and the priest shall make an atonement for him, and he shall be clean.   =
 3   3     6     5     5    3    5       8     3   3    4      8      4   3     5    3   3     6     5     4   2     9      3    3   3   2   5    2    5    123
 19  33   87     52    50   33   75     80     19  33  39     80     66   33   52    19  33   87     52   30  15    107     39  30   19 13   52   7   35   1289
 55 213   474   169   167  213  642     233    55 213  246    233    480 213   322   55 213   474   169   66  51    611    156  57   55 13  169   7   89   6113
And if he be poor, and cannot get  so much; then he shall take one lamb for a trespass offering  to be waved,  to make an atonement for him, and one tenth deal of fine flour mingled with oil for a meat offering, and a log of oil;   =
 3   2  2  2   4    3     6    3   2    4     4   2   5     4   3    4   3  1     8        8     2   2    5    2    4   2     9      3    3   3   3    5     4   2   4    5      7      4   3   3  1   4      8      3  1  3   2   3   173
 19 15 13  7   64   19   67    32  34   45   47  13   52   37   34  28   39 1    117      80     35  7   55    35  30  15    107     39  30   19  34   67   22  21  34    72     64    60   36  39 1  39      80     19 1  34 21  36  1815
 55 15 13  7  280   55   364  212 160  351   263 13  169   226 115  73  156 1    666      233   260  7   910  260  66  51    611    156  57   55 115  463   40  66  70   486    145    717  99 156 1  246    233     55 1  97 66  99  9015
And two turtledoves,  or two young pigeons, such  as he  is able  to get; and the one shall be a sin offering, and the other a burnt offering.   =
 3   3       11       2   3    5       7      4   2   2  2    4   2    3   3   3   3    5    2 1  3      8      3   3    5   1   5       8      106
 19  58      161      33  58   82     85     51   20 13  28  20   35  32   19  33  34   52   7 1  42     80     19  33   66  1   75      80    1237
 55 760     1394     150 760  1117    301    411 101 13 109  38  260  212  55 213 115  169   7 1 159    233     55 213  363  1  642     233    8140
And he shall bring them  on the eighth day for his cleansing unto the priest, unto the door of the tabernacle of the congregation, before the LORD.   =
 3   2   5     5     4   2   3     6    3   3   3      9       4   3     6      4   3    4   2  3      10      2  3        12         6    3    4    117
 19 13   52    50   46   29  33   57    30  39  36     84     70   33    87    70   33  52  21  33     81     21  33      128        51    33   49  1283
 55 13  169   158   253 110 213   237  705 156 117    255     610 213   474    610 213  214 66 213     387    66 213      542        168  213  184  6827
And the priest shall take the lamb of the trespass offering, and the log of oil, and the priest shall wave them for a wave offering before the LORD:   =
 3   3     6     5     4   3    4   2  3      8        8      3   3   3   2   3   3   3     6     5     4    4   3  1   4      8       6    3    4    117
 19  33   87     52   37   33  28  21  33    117       80     19  33  34 21  36   19  33   87     52   51   46   39 1  51     80      51    33   49  1275
 55 213   474   169   226 213  73  66 213    666      233     55 213  97 66  99   55 213   474   169   906  253 156 1  906    233     168  213  184  7062
And he shall kill the lamb of the trespass offering, and the priest shall take some of the blood of the trespass offering, and put  it upon the tip of the right ear of him that  is  to be cleansed, and upon the thumb of his right hand, and upon the great toe of his right foot:   =
 3   2   5     4   3    4   2  3      8        8      3   3     6     5     4    4   2  3    5    2  3      8        8      3   3   2    4   3   3   2  3    5    3   2  3    4   2   2   2     8      3    4   3    5    2  3    5     4    3    4   3    5    3   2  3    5     4    213
 19 13   52   44   33  28  21  33    117       80     19  33   87     52   37   52  21  33   48  21  33    117       80     19  57  29  66   33  45 21  33   62   24 21  30  49   28  35  7     63     19  66   33   64  21  36   62    27   19  66   33   51   40 21  36   62    56   2407
 55 13  169   89  213  73  66 213    666      233     55 213   474   169   226  205 66 213  156  66 213    666      233     55 570 209  480 213 279 66 213  314   96 66  57  409 109 260  7    198     55  480 213  550  66 117  314    63   55  480 213  303  265 66 117  314   326  12343
And the priest shall pour of the oil into the palm of his own left hand:   =
 3   3     6     5     4   2  3   3    4   3    4   2  3   3    4    4    56
 19  33   87     52   70  21  33  36  58   33  42  21  36  52  43    27   663
 55 213   474   169   520 66 213  99  319 213  141 66 117 610  241   63  3579
And the priest shall sprinkle with his right finger some of the oil that  is in his left hand seven times before the LORD:   =
 3   3     6     5       8      4   3    5      6     4   2  3   3    4   2   2  3    4    4    5     5      6    3    4    97
 19  33   87     52     104    60   36   62    59    52  21  33  36  49   28 23  36  43   27    65    66    51    33   49  1124
 55 213   474   169     374    717 117  314    167   205 66 213  99  409 109 59 117  241  63   560   354    168  213  184  5660
And the priest shall put of the oil that  is in his hand upon the tip of the right ear of him that  is  to be cleansed, and upon the thumb of his right hand, and upon the great toe of his right foot, upon the place of the blood of the trespass offering:   =
 3   3     6     5    3   2  3   3    4   2   2  3    4    4   3   3   2  3    5    3   2  3    4   2   2   2     8      3    4   3    5    2  3    5     4    3    4   3    5    3   2  3    5     4     4   3    5    2  3    5    2  3      8        8      193
 19  33   87     52   57 21  33  36  49   28 23  36  27   66   33  45 21  33   62   24 21  30  49   28  35  7     63     19  66   33   64  21  36   62    27   19  66   33   51   40 21  36   62    56   66   33   37  21  33   48  21  33    117       80     2219
 55 213   474   169  570 66 213  99  409 109 59 117  63   480 213 279 66 213  314   96 66  57  409 109 260  7    198     55  480 213  550  66 117  314    63   55  480 213  303  265 66 117  314   326   480 213  109  66 213  156  66 213    666      233    11795
And the rest of the oil that  is in the priest's hand he shall put upon the head of him that  is  to be cleansed,  to make an atonement for him before the LORD.   =
 3   3    4   2  3   3    4   2   2  3      7      4   2   5    3    4   3    4   2  3    4   2   2   2     8      2    4   2     9      3   3     6    3    4    120
 19  33  62  21  33  36  49   28 23  33    106    27  13   52   57  66   33  18  21  30  49   28  35  7     63     35  30  15    107     39  30   51    33   49  1331
 55 213  395 66 213  99  409 109 59 213    574    63  13  169  570  480 213  18  66  57  409 109 260  7    198    260  66  51    611    156  57   168  213  184  6803
And he shall offer the one of the turtledoves,  or of the young pigeons, such  as he can get;   =
 3   2   5     5    3   3   2  3       11       2   2  3    5       7      4   2   2  3    3   70
 19 13   52    50   33  34 21  33      161      33 21  33   82     85     51   20 13  18  32   804
 55 13  169   167  213 115 66 213     1394     150 66 213  1117    301    411 101 13  54  212 5043
Even such  as he  is able  to get, the one for a sin offering, and the other for a burnt offering, with the meat offering: and the priest shall make an atonement for him that  is  to be cleansed before the LORD.   =
  4    4   2   2  2    4   2    3   3   3   3  1  3      8      3   3    5    3  1   5       8       4   3    4      8      3   3     6     5     4   2     9      3   3    4   2   2   2     8       6    3    4    160
 46   51   20 13  28  20   35  32   33  34  39 1  42     80     19  33   66   39 1   75      80     60   33  39      80     19  33   87     52   30  15    107     39  30  49   28  35  7    63      51    33   49  1726
 460  411 101 13 109  38  260  212 213 115 156 1 159    233     55 213  363  156 1  642     233     717 213  246    233     55 213   474   169   66  51    611    156  57  409 109 260  7    198     168  213  184  8953
This  is the law of him in whom  is the plague of leprosy, whose hand  is not able  to get that which pertaineth  to his cleansing.   =
  4   2   3   3   2  3   2   4   2   3     6    2     7      5     4   2   3    4   2   3    4    5       10      2   3       9      99
 56   28  33  36 21  30 23  59   28  33   62   21    110     70   27   28  49  20   35  32  49    51      116     35  36     84     1172
 317 109 213 531 66  57 59  608 109 213   413  66   1055    673   63  109 310  38  260 212  409  528      638    260 117     255    7688
And the LORD spake unto Moses and unto Aaron, saying,   =
 3   3    4    5     4    5    3    4     5      6     42
 19  33  49    52   70    71   19  70    49      75    507
 55 213  184  196   610  305   55  610   202    867   3297
When  ye be come into the land of Canaan, which I give  to  you for a possession, and I put the plague of leprosy in a house of the land of your possession;   =
  4   2   2   4    4   3    4   2    6      5   1   4   2    3   3  1      10      3  1  3   3     6    2    7     2 1   5    2  3    4   2   4       10      118
 50   30  7  36   58   33  31  21    34     51  9  43   35  61   39 1     150      19 9  57  33   62   21   110   23 1   68  21  33  31  21  79      150      1427
 563 705  7  108  319 213  85  66   106    528  9  421 260 1060 156 1     654      55 9 570 213   413  66   1055  59 1  473  66 213  85  66 1150     654     10409
And he that owneth the house shall come and tell the priest, saying,  It seemeth  to me there  is  as  it were a plague in the house:   =
 3   2   4     6    3    5     5     4   3    4   3     6       6     2     7     2   2   5    2   2   2    4  1    6    2  3     5    99
 19 13  49    85    33   68    52   36   19  49   33    87      75    29    75    35 18   56   28  20  29  51  1   62   23  33   68   1146
 55 13  409   823  213  473   169   108  55  265 213   474     867   209   363   260 45  308  109 101 209  600 1   413  59 213   473  7500
Then the priest shall command that they empty the house, before the priest go into  it  to see the plague, that all that  is in the house be not made unclean: and afterward the priest shall go in  to see the house:   =
  4   3     6     5      7      4    4    5    3     5      6    3     6    2   4   2   2   3   3     6      4   3    4   2   2  3    5    2  3    4      7     3      9      3     6     5    2  2  2   3   3     5    165
 47   33   87     52     63    49   58    79   33   68     51    33   87   22  58   29  35  29  33    62    49   25  49   28 23  33   68   7  49  23     70     19     96     33   87     52  22 23  35  29  33   68    1929
 263 213   474   169    198    409  913  1015 213   473    168  213   474  67  319 209 260 110 213   413    409  61  409 109 59 213  473   7 310  50     439    55    897    213   474   169  67 59 260 110 213   473  12315
And he shall look  on the plague, and, behold, if the plague be in the walls of the house with hollow strakes, greenish  or reddish, which in sight are lower than the wall;   =
 3   2   5     4   2   3     6      3     6     2  3     6    2  2  3    5    2  3    5     4     6       7        8     2      7      5    2   5    3    5     4   3    4    132
 19 13   52   53   29  33    62    19     46   15  33   62    7 23  33   67  21  33   68   60    85      93       85     33    67      51  23   63   24   73   43   33   48  1469
 55 13  169   170 110 213   413    55    109   15 213   413   7 59 213  661  66 213  473   717   688     516      274   150    220    528  59  324   96  685   259 213  561  8930
Then the priest shall go out of the house  to the door of the house, and shut  up the house seven days:   =
  4   3     6     5    2  3   2  3    5    2   3    4   2  3     5    3    4   2   3    5     5     4    78
 47   33   87     52  22  56 21  33   68   35  33  52  21  33   68    19  68   37  33   68    65    49  1000
 263 213   474   169  67 560 66 213  473  260 213  214 66 213   473   55  608 370 213  473   560   805  7021
And the priest shall come again the seventh day, and shall look: and, behold, if the plague be spread in the walls of the house;   =
 3   3     6     5     4    5    3     7      3   3    5     4     3     6     2  3     6    2    6    2  3    5    2  3     5    99
 19  33   87     52   36    32   33    93    30   19   52    53   19     46   15  33   62    7   63   23  33   67  21  33   68   1029
 55 213   474   169   108   68  213   768    705  55  169   170   55    109   15 213   413   7   270  59 213  661  66 213   473  5934
Then the priest shall command that they take away the stones in which the plague is, and they shall cast them into an unclean place without the city:   =
  4   3     6     5      7      4    4    4    4   3     6    2   5    3     6    2   3    4    5     4    4    4   2    7      5      7     3    4    120
 47   33   87     52     63    49   58   37   50   33   92   23   51   33   62    28  19  58    52   43   46   58  15    70     37    116    33   57   1402
 263 213   474   169    198    409  913  226 1202 213   515  59  528  213   413  109  55  913  169   304  253  319 51   439    109    1277  213  912  11131
And he shall cause the house  to be scraped within round about, and they shall pour out the dust that they scrape off without the city into an unclean place:   =
 3   2   5     5    3    5    2   2    7       6     5      5    3    4    5     4   3   3    4    4    4     6    3     7     3    4    4   2    7       5    125
 19 13   52    49   33   68   35  7    66     83     72    59    19  58    52   70   56  33  64   49   58    62    27   116    33  57   58  15    70     37    1490
 55 13  169   409  213  473  260  7   273     776   504    563   55  913  169   520 560 213  604  409  913   269   72   1277  213  912  319 51   439     109  11732
And they shall take other stones, and put them in the place of those stones; and he shall take other morter, and shall plaister the house.   =
 3    4    5     4    5      6     3   3    4   2  3    5    2   5      6     3   2   5     4    5      6     3    5       8     3     5    109
 19  58    52   37    66     92    19  57  46  23  33   37  21   67     92    19 13   52   37    66     89    19   52     100    33   68   1267
 55  913  169   226  363    515    55 570  253 59 213  109  66  373    515    55 13  169   226  363    485    55  169     505   213   473  7180
And if the plague come again, and break out in the house, after that he hath taken away the stones, and after he hath scraped the house, and after  it  is plaistered;   =
 3   2  3     6     4     5    3    5    3   2  3     5     5     4   2   4    5     4   3     6     3    5    2   4     7     3     5    3    5    2   2       10      128
 19 15  33   62    36    32    19   37   56 23  33   68     50   49  13  37    51   50   33    92    19   50  13  37     66    33   68    19   50   29  28     109     1329
 55 15 213   413   108   68    55  118  560 59 213   473   302   409 13  217  276  1202 213   515    55  302  13  217   273   213   473   55  302  209 109     514     8232
Then the priest shall come and look, and, behold, if the plague be spread in the house,  it  is a fretting leprosy in the house;  it  is unclean.   =
  4   3     6     5     4   3    4     3     6     2  3     6    2    6    2  3     5    2   2  1     8       7     2  3     5    2   2      7     108
 47   33   87     52   36   19   53   19     46   15  33   62    7   63   23  33   68    29  28 1    99      110   23  33   68    29  28    70    1214
 263 213   474   169   108  55  170   55    109   15 213   413   7   270  59 213   473  209 109 1    567     1055  59 213   473  209 109    439   6722
And he shall break down the house, the stones of it, and the timber thereof, and all the morter of the house; and he shall carry them forth out of the city into an unclean place.   =
 3   2   5     5     4   3     5    3     6    2  2   3   3     6       7     3   3   3     6    2  3     5    3   2   5     5     4    5    3   2  3    4    4   2    7       5    138
 19 13   52    37   56   33   68    33   92   21  29  19  33   67      77     19  25  33   89   21  33   68    19 13   52    65   46    67   56 21  33  57   58  15    70     37   1546
 55 13  169   118   614 213   473  213   515  66 209  55 213   346     374    55  61 213   485  66 213   473   55 13  169   884   253  364  560 66 213  912  319 51   439     109  9619
Moreover he that goeth into the house all the while that  it  is shut  up shall be unclean until the even.   =
    8     2   4    5     4   3    5    3   3    5     4   2   2    4   2    5    2    7      5    3    4    82
   111   13  49    55   58   33   68   25  33   57   49   29  28  68   37   52   7    70     76   33   46   997
   750   13  409  280   319 213  473   61 213  552   409 209 109  608 370  169   7   439    589  213  460  6865
And he that lieth in the house shall wash his clothes; and he that eateth in the house shall wash his clothes.   =
 3   2   4    5    2  3    5     5     4   3      7     3   2   4     6    2  3    5     5     4   3      7     87
 19 13  49    54  23  33   68    52   51   36    82     19 13  49    59   23  33   68    52   51   36    82     965
 55 13  409  252  59 213  473   169   609 117    406    55 13  409   419  59 213  473   169   609 117    406   5717
And if the priest shall come in, and look upon it, and, behold, the plague hath not spread in the house, after the house was plaistered: then the priest shall pronounce the house clean, because the plague  is healed.   =
 3   2  3     6     5     4   2   3    4    4   2    3     6     3     6     4   3     6    2  3     5     5    3    5    3       10       4   3     6     5       9      3    5      5      7     3     6    2     6     169
 19 15  33   87     52   36   23  19  53   66   29  19     46    33   62    37   49   63   23  33   68     50   33   68   43     109      47   33   87     52     121     33   68    35      56    33   62    28    35    1858
 55 15 213   474   169   108  59  55  170  480 209  55    109   213   413   217 310   270  59 213   473   302  213  473  601     514      263 213   474   169     688    213  473    89     416   213   413  109    53   10228
And he shall take  to cleanse the house two birds, and cedar wood, and scarlet, and hyssop:   =
 3   2   5     4   2     7     3    5    3     5    3    5     4    3      7     3     6     70
 19 13   52   37   35    59    33   68   58   52    19   31    57   19    78     19   102    751
 55 13  169   226 260   194   213  473  760   205   55  103   624   55    429    55   1038  4927
And he shall kill the one of the birds in an earthen vessel over running water:   =
 3   2   5     4   3   3   2  3    5    2  2    7       6     4     7       5    63
 19 13   52   44   33  34 21  33   52  23 15    71     82    60     97     67    716
 55 13  169   89  213 115 66 213  205  59 51   359     640   555   556     796  4154
And he shall take the cedar wood, and the hyssop, and the scarlet, and the living bird, and dip them in the blood of the slain bird, and in the running water, and sprinkle the house seven times:   =
 3   2   5     4   3    5     4    3   3     6     3   3      7     3   3     6     4    3   3    4   2  3    5    2  3    5     4    3   2  3     7       5    3      8     3    5     5      5    150
 19 13   52   37   33   31    57   19  33   102    19  33    78     19  33   73     33   19  29  46  23  33   48  21  33   55    33   19 23  33    97     67    19    104    33   68    65    66   1618
 55 13  169   226 213  103   624   55 213   1038   55 213    429    55 213   505   105   55  83  253 59 213  156  66 213  190   105   55 59 213   556     796   55    374   213  473   560    354  9385
And he shall cleanse the house with the blood of the bird, and with the running water, and with the living bird, and with the cedar wood, and with the hyssop, and with the scarlet:   =
 3   2   5      7     3    5     4   3    5    2  3    4    3    4   3     7       5    3    4   3     6     4    3    4   3    5     4    3    4   3     6     3    4   3      7     140
 19 13   52     59    33   68   60   33   48  21  33   33   19  60   33    97     67    19  60   33   73     33   19  60   33   31    57   19  60   33   102    19  60   33    78     1570
 55 13  169    194   213  473   717 213  156  66 213  105   55  717 213   556     796   55  717 213   505   105   55  717 213  103   624   55  717 213   1038   55  717 213    429   11668
But he shall let go the living bird out of the city into the open fields, and make an atonement for the house: and  it shall be clean.   =
 3   2   5    3   2  3     6     4   3   2  3    4    4   3    4     6     3    4   2     9      3   3     5    3   2    5    2    5    103
 43 13   52   37 22  33   73    33   56 21  33  57   58   33  50     55    19  30  15    107     39  33   68    19  29   52   7   35   1122
502 13  169  235 67 213   505   105 560 66 213  912  319 213  185   154    55  66  51    611    156 213   473   55 209  169   7   89   6585
This  is the law for all manner of plague of leprosy, and scall,   =
  4   2   3   3   3   3     6    2    6    2     7     3     5    49
 56   28  33  36  39  25   65   21   62   21    110    19   47    562
 317 109 213 531 156  61   236  66   413  66   1055    55   164  3442
And for the leprosy of a garment, and of a house,   =
 3   3   3     7     2 1     7     3   2 1    5    37
 19  39  33   110   21 1    78     19 21 1   68    410
 55 156 213   1055  66 1    393    55 66 1   473  2534
And for a rising, and for a scab, and for a bright spot:   =
 3   3  1    6     3   3  1   4    3   3  1    6     4    41
 19  39 1    76    19  39 1   25   19  39 1   64     70   412
 55 156 1   265    55 156 1  106   55 156 1   316   430  1753
 To teach when  it  is unclean, and when  it  is clean: this  is the law of leprosy.   =
 2    5     4   2   2      7     3    4   2   2     5     4   2   3   3   2     7     59
 35   37   50   29  28    70     19  50   29  28   35    56   28  33  36 21    110    694
260  217   563 209 109    439    55  563 209 109   89    317 109 213 531 66   1055   5113
>>> 
