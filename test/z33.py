And the LORD spake unto Moses, saying,
 Send thou men, that they may search the land of Canaan, which I give unto the children of Israel: of every tribe of their fathers shall ye send a man, every one a ruler among them.
 And Moses by the commandment of the LORD sent them from the wilderness of Paran: all those men were heads of the children of Israel.
 And these were their names: of the tribe of Reuben, Shammua the son of Zaccur.
 Of the tribe of Simeon, Shaphat the son of Hori.
 Of the tribe of Judah, Caleb the son of Jephunneh.
 Of the tribe of Issachar, Igal the son of Joseph.
 Of the tribe of Ephraim, Oshea the son of Nun.
 Of the tribe of Benjamin, Palti the son of Raphu.
 Of the tribe of Zebulun, Gaddiel the son of Sodi.
 Of the tribe of Joseph, namely, of the tribe of Manasseh, Gaddi the son of Susi.
 Of the tribe of Dan, Ammiel the son of Gemalli.
 Of the tribe of Asher, Sethur the son of Michael.
 Of the tribe of Naphtali, Nahbi the son of Vophsi.
 Of the tribe of Gad, Geuel the son of Machi.
 These are the names of the men which Moses sent to spy out the land. And Moses called Oshea the son of Nun Jehoshua.

 And Moses sent them to spy out the land of Canaan, and said unto them, Get you up this way southward, and go up into the mountain:
 And see the land, what it is; and the people that dwelleth therein, whether they be strong or weak, few or many;
 And what the land is that they dwell in, whether it be good or bad; and what cities they be that they dwell in, whether in tents, or in strong holds;
 And what the land is, whether it be fat or lean, whether there be wood therein, or not. And be ye of good courage, and bring of the fruit of the land. Now the time was the time of the firstripe grapes.

 So they went up, and searched the land from the wilderness of Zin unto Rehob, as men come to Hamath.
 And they ascended by the south, and came unto Hebron; where Ahiman, Sheshai, and Talmai, the children of Anak, were. (Now Hebron was built seven years before Zoan in Egypt.)
 And they came unto the brook of Eshcol, and cut down from thence a branch with one cluster of grapes, and they bare it between two upon a staff; and they brought of the pomegranates, and of the figs.
 The place was called the brook Eshcol, because of the cluster of grapes which the children of Israel cut down from thence.
 And they returned from searching of the land after forty days.

 And they went and came to Moses, and to Aaron, and to all the congregation of the children of Israel, unto the wilderness of Paran, to Kadesh; and brought back word unto them, and unto all the congregation, and shewed them the fruit of the land.
 And they told him, and said, We came unto the land whither thou sentest us, and surely it floweth with milk and honey; and this is the fruit of it.
 Nevertheless the people be strong that dwell in the land, and the cities are walled, and very great: and moreover we saw the children of Anak there.
 The Amalekites dwell in the land of the south: and the Hittites, and the Jebusites, and the Amorites, dwell in the mountains: and the Canaanites dwell by the sea, and by the coast of Jordan.
 And Caleb stilled the people before Moses, and said, Let us go up at once, and possess it; for we are well able to overcome it.
 But the men that went up with him said, We be not able to go up against the people; for they are stronger than we.
 And they brought up an evil report of the land which they had searched unto the children of Israel, saying, The land, through which we have gone to search it, is a land that eateth up the inhabitants thereof; and all the people that we saw in it are men of a great stature.
 And there we saw the giants, the sons of Anak, which come of the giants: and we were in our own sight as grasshoppers, and so we were in their sight.

   >>> from bible import *
   >>> Numbers[13]*p
   Numbers 13
   1 And the LORD spake unto Moses, saying,
   2 Send thou men, that they may search the land of Canaan, which I give unto the children of Israel: of every tribe of their fathers shall ye send a man, every one a ruler among them.
   3 And Moses by the commandment of the LORD sent them from the wilderness of Paran: all those men were heads of the children of Israel.
   4 And these were their names: of the tribe of Reuben, Shammua the son of Zaccur.
   5 Of the tribe of Simeon, Shaphat the son of Hori.
   6 Of the tribe of Judah, Caleb the son of Jephunneh.
   7 Of the tribe of Issachar, Igal the son of Joseph.
   8 Of the tribe of Ephraim, Oshea the son of Nun.
   9 Of the tribe of Benjamin, Palti the son of Raphu.
   10 Of the tribe of Zebulun, Gaddiel the son of Sodi.
   11 Of the tribe of Joseph, namely, of the tribe of Manasseh, Gaddi the son of Susi.
   12 Of the tribe of Dan, Ammiel the son of Gemalli.
   13 Of the tribe of Asher, Sethur the son of Michael.
   14 Of the tribe of Naphtali, Nahbi the son of Vophsi.
   15 Of the tribe of Gad, Geuel the son of Machi.
   16 These are the names of the men which Moses sent to spy out the land. And Moses called Oshea the son of Nun Jehoshua.
   17 And Moses sent them to spy out the land of Canaan, and said unto them, Get you up this way southward, and go up into the mountain:
   18 And see the land, what it is, and the people that dwelleth therein, whether they be strong or weak, few or many;
   19 And what the land is that they dwell in, whether it be good or bad; and what cities they be that they dwell in, whether in tents, or in strong holds;
   20 And what the land is, whether it be fat or lean, whether there be wood therein, or not. And be ye of good courage, and bring of the fruit of the land. Now the time was the time of the firstripe grapes.
   21 So they went up, and searched the land from the wilderness of Zin unto Rehob, as men come to Hamath.
   22 And they ascended by the south, and came unto Hebron; where Ahiman, Sheshai, and Talmai, the children of Anak, were. (Now Hebron was built seven years before Zoan in Egypt.)
   23 And they came unto the brook of Eshcol, and cut down from thence a branch with one cluster of grapes, and they bare it between two upon a staff; and they brought of the pomegranates, and of the figs.
   24 The place was called the brook Eshcol, because of the cluster of grapes which the children of Israel cut down from thence.
   25 And they returned from searching of the land after forty days.
   26 And they went and came to Moses, and to Aaron, and to all the congregation of the children of Israel, unto the wilderness of Paran, to Kadesh; and brought back word unto them, and unto all the congregation, and shewed them the fruit of the land.
   27 And they told him, and said, We came unto the land whither thou sentest us, and surely it floweth with milk and honey; and this is the fruit of it.
   28 Nevertheless the people be strong that dwell in the land, and the cities are walled, and very great: and moreover we saw the children of Anak there.
   29 The Amalekites dwell in the land of the south: and the Hittites, and the Jebusites, and the Amorites, dwell in the mountains: and the Canaanites dwell by the sea, and by the coast of Jordan.
   30 And Caleb stilled the people before Moses, and said, Let us go up at once, and possess it; for we are well able to overcome it.
   31 But the men that went up with him said, We be not able to go up against the people; for they are stronger than we.
   32 And they brought up an evil report of the land which they had searched unto the children of Israel, saying, The land, through which we have gone to search it, is a land that eateth up the inhabitants thereof; and all the people that we saw in it are men of a great stature.
   33 And there we saw the giants, the sons of Anak, which come of the giants: and we were in our own sight as grasshoppers, and so we were in their sight.
   >>> Numbers[13].wc()
   741
   >>> Numbers.chapters()@method.wc
   [1333, 828, 1291, 1416, 898, 742, 1939, 687, 722, 891, 1056, 384, 741, 1180, 1068, 1341, 335, 1111, 683, 791, 931, 1201, 743, 659, 445, 1446, 610, 779, 954, 503, 1208, 1009, 928, 630, 965, 445]
   >>> Numbers[6]*p
   Numbers 6
   1 And the LORD spake unto Moses, saying,
   2 Speak unto the children of Israel, and say unto them, When either man or woman shall separate themselves to vow a vow of a Nazarite, to separate themselves unto the LORD:
   3 He shall separate himself from wine and strong drink, and shall drink no vinegar of wine, or vinegar of strong drink, neither shall he drink any liquor of grapes, nor eat moist grapes, or dried.
   4 All the days of his separation shall he eat nothing that is made of the vine tree, from the kernels even to the husk.
   5 All the days of the vow of his separation there shall no razor come upon his head: until the days be fulfilled, in the which he separateth himself unto the LORD, he shall be holy, and shall let the locks of the hair of his head grow.
   6 All the days that he separateth himself unto the LORD he shall come at no dead body.
   7 He shall not make himself unclean for his father, or for his mother, for his brother, or for his sister, when they die: because the consecration of his God is upon his head.
   8 All the days of his separation he is holy unto the LORD.
   9 And if any man die very suddenly by him, and he hath defiled the head of his consecration; then he shall shave his head in the day of his cleansing, on the seventh day shall he shave it.
   10 And on the eighth day he shall bring two turtles, or two young pigeons, to the priest, to the door of the tabernacle of the congregation:
   11 And the priest shall offer the one for a sin offering, and the other for a burnt offering, and make an atonement for him, for that he sinned by the dead, and shall hallow his head that same day.
   12 And he shall consecrate unto the LORD the days of his separation, and shall bring a lamb of the first year for a trespass offering: but the days that were before shall be lost, because his separation was defiled.
   13 And this is the law of the Nazarite, when the days of his separation are fulfilled: he shall be brought unto the door of the tabernacle of the congregation:
   14 And he shall offer his offering unto the LORD, one he lamb of the first year without blemish for a burnt offering, and one ewe lamb of the first year without blemish for a sin offering, and one ram without blemish for peace offerings,
   15 And a basket of unleavened bread, cakes of fine flour mingled with oil, and wafers of unleavened bread anointed with oil, and their meat offering, and their drink offerings.
   16 And the priest shall bring them before the LORD, and shall offer his sin offering, and his burnt offering:
   17 And he shall offer the ram for a sacrifice of peace offerings unto the LORD, with the basket of unleavened bread: the priest shall offer also his meat offering, and his drink offering.
   18 And the Nazarite shall shave the head of his separation at the door of the tabernacle of the congregation, and shall take the hair of the head of his separation, and put it in the fire which is under the sacrifice of the peace offerings.
   19 And the priest shall take the sodden shoulder of the ram, and one unleavened cake out of the basket, and one unleavened wafer, and shall put them upon the hands of the Nazarite, after the hair of his separation is shaven:
   20 And the priest shall wave them for a wave offering before the LORD: this is holy for the priest, with the wave breast and heave shoulder: and after that the Nazarite may drink wine.
   21 This is the law of the Nazarite who hath vowed, and of his offering unto the LORD for his separation, beside that that his hand shall get: according to the vow which he vowed, so he must do after the law of his separation.
   22 And the LORD spake unto Moses, saying,
   23 Speak unto Aaron and unto his sons, saying, On this wise ye shall bless the children of Israel, saying unto them,
   24 The LORD bless thee, and keep thee:
   25 The LORD make his face shine upon thee, and be gracious unto thee:
   26 The LORD lift up his countenance upon thee, and give thee peace.
   27 And they shall put my name upon the children of Israel, and I will bless them.
   >>> 
