>>> from bible import *
>>> import htmldraw
>>> dir(htmldraw)
['AndFunc', 'AndState', 'Attr', 'Binop', 'Choice', 'Dict', 'F', 'FR', 'Fun', 'Func', 'FuncRow', 'GetItem', 'I', 'Ith', 'K', 'L', 'LeftOp', 'Lookup', 'R', 'Record', 'Rewindable', 'RightOp', 'Row', 'Selection', 'TAG', 'Unique', 'X', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'add', 'amp', 'andfn', 'apply', 'aslist', 'builtins', 'callmethod', 'compose', 'const', 'delay', 'dmap', 'draw', 'e', 'escape', 'failas', 'fggf', 'filter', 'first', 'firstn', 'floordiv', 'fun', 'functools', 'getitem', 'getprop', 'html', 'htmltable', 'inks', 'l', 'li', 'lmap', 'meth', 'method', 'mod', 'mul', 'op', 'operator', 'orf', 'orr', 'over', 'p', 'pairs', 'partial', 'perm', 'permargs', 'plain', 'pmap', 'positions', 'pow', 'prop', 'push', 'q', 'qstr', 'redparts', 'reduce', 'rowtype', 'same', 'scale', 'show', 'showfile', 'showt', 'sort', 'span', 'splitat', 'star', 'sub', 'subprocess', 'svg', 'swap', 'swapargs', 'th', 'threes', 'translate', 'trrow', 'truediv', 'trystr', 'ttable', 'twos', 'unstar', 'values', 'webbrowser', 'windows', 'withoutrepeats']
>>> 
>>> # htmldraw.show(htmldraw.ttable(Table([["אל","7X"]])))
>>> 
