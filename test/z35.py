>>> from bible import *
>>> tell("stars")
 s  t a  r  s  =
19 20 1 18 19 77
>>> tell("Christ")
C h  r i  s  t  =
3 8 18 9 19 20 77
>>> Genesis[1:1].tell()
In the beginning God created the heaven and the earth.  =
23  33     81     26    56    33   55    19  33   52   411
>>> 
>>> Genesis[1:1].text()*words@I[0]*tell
I  t b G c  t h a  t e  =
9 20 2 7 3 20 8 1 20 5 95
>>> Genesis[1:1].text()*words@I[-1]*tell
 n e g d d e  n d e h  =
14 5 7 4 4 5 14 4 5 8 70
>>> 
>>> tell("I AM THAT I AM")
I AM THAT I AM  =
9 14  49  9 14 95
>>> Revelation[22:21].text()*words@I[0]*tell
 T g  o  o  L  J C b  w  y a A  =
20 7 15 15 12 10 3 2 23 25 1 1 134
>>> Revelation[22:21].text()*words@I[-1]*tell
e e f  r d  s  t e h  u  l  n  =
5 5 6 18 4 19 20 5 8 21 12 14 137
>>>
