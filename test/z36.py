>>> from bible import *
>>> Genesis[1:1].text()*words*I[:6]*tells
In the beginning God created the   =
 2  3      9      3     7     3   27
23  33     81     26    56    33  252
59 213    189     71   308   213 1053
>>> 252*ns
[2, 2, 3, 3, 7] [1, 1, 2, 2, 4]
>>> 6*7*6
252
>>> 1053*ns
[3, 3, 3, 3, 13] [2, 2, 2, 2, 6]
>>> 27*39
1053
>>> Genesis[1:1].text()*words*I[:7]*tells
In the beginning God created the heaven   =
 2  3      9      3     7     3     6    33
23  33     81     26    56    33   55    307
59 213    189     71   308   213   469  1522
>>> 307*ns
[307] [63]
>>> 1522*ns
[2, 761] [1, 135]
>>> 189*ns
[3, 3, 3, 7] [2, 2, 2, 4]
>>> 7*27
189
>>> 469*ns
[7, 67] [4, 19]
>>> 1522/33
46.121212121212125
>>> Genesis[1:1].tells()
In the beginning God created the heaven and the earth.   =
 2  3      9      3     7     3     6    3   3     5    44
23  33     81     26    56    33   55    19  33   52    411
59 213    189     71   308   213   469   55 213   304  2094
>>> 2094/44
47.59090909090909
>>> Genesis[1:2].text()*words@osum*tale
[19, 52, 104, 147, 263, 315, 334, 384, 403, 494, 537, 603, 636, 651, 672, 705, 735, 754, 787, 878, 899, 925, 984, 1050, 1083, 1098, 1119, 1152, 1238]
>>> Genesis[1:2].tells()
And the earth was without form, and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters.   =
 3   3    5    3     7      4    3    4    3      8     3    4   3    4   2  3    4    3   3     6    2  3    5     4   3    4   2  3     6     110
 19  33   52   43   116     52   19   50   19    91     43  66   33  15  21  33   30   19  33   91   21  26   59   66   33  15  21  33    86   1238
 55 213  304  601   1277   196   55  473   55    370   601  480 213  15  66 213   84   55 213   478  66  71  509   480 213  15  66 213   896   8546
>>> 
