>>> from bible import *
>>> 29979245800000000-1123581321345589
28855664478654411
>>> 29979-1123
28856
>>> b.vi(28856)
2 Corinthians 3:14 But their minds were blinded: for until this day remaineth the same vail untaken away in the reading of the old testament; which vail is done away in Christ.
>>> b.vi(29979)
Hebrews 2:1 Therefore we ought to give the more earnest heed to the things which we have heard, lest at any time we should let them slip.
>>> b[29][1:9].tells()
The meat offering and the drink offering  is cut off from the house of the LORD; the priests, the LORD's ministers, mourn.   =
 3    4      8     3   3    5       8     2   3   3    4   3    5    2  3    4    3      7     3     5        9        5    95
 33  39     80     19  33   56     80     28  44  27  52   33   68  21  33   49   33    106    33   68       126      81   1142
213  246    233    55 213  173     233   109 503  72  196 213  473  66 213  184  213    574   213   284      603      540  5822
>>> b[29][1:9].text()*letters*I[78]
'r'
>>> 
