>>> from bible import *
>>> tells("six by nine")
six  by nine   =
 3   2    4    9
 52  27  42   121
709 702  114 1525
>>> tells("six by",dsum)
six  by   =
 3   2    5
 52  27  79
709 702 1411
393 434  827
>>> tells("six nine",dsum)
six nine  =
 3    4   7
 52  42   94
709  114 823
393  86  479
>>> 
>>> tells("by nine")
 by nine  =
 2    4   6
 27  42   69
702  114 816
>>> tells("what is")
what  is  =
  4   2   6
 52   28  80
 709 109 818
>>> tells("what is the")
what  is the   =
  4   2   3    9
 52   28  33  113
 709 109 213 1031
>>> tells("of life")
of life  =
 2   4   6
21  32   53
66  50  116
>>> tells("six by nine")
six  by nine   =
 3   2    4    9
 52  27  42   121
709 702  114 1525
>>> tells("six by nine",dsum)
six  by nine   =
 3   2    4    9
 52  27  42   121
709 702  114 1525
393 434  86   913
>>> 393*ns
[3, 131] [2, 32]

