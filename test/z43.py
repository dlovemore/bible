>>> from bible import *
>>> tells('aquilla')
a  q  u  i  l  l a  =
1  1  1  1  1  1 1  7
1 17  21 9 12 12 1  73
1 80 300 9 30 30 1 451
>>> tells('eagle')
e a g  l e  =
1 1 1  1 1  5
5 1 7 12 5 30
5 1 7 30 5 48
>>> tells('eagles')
e a g  l e  s   =
1 1 1  1 1  1   6
5 1 7 12 5  19  49
5 1 7 30 5 100 148
>>> tells('carcase')
c a  r c a  s  e  =
1 1  1 1 1  1  1  7
3 1 18 3 1  19 5  50
3 1 90 3 1 100 5 203
>>> tells('swift')
 s   w  i f  t   =
 1   1  1 1  1   5
 19  23 9 6  20  77
100 500 9 6 200 815
>>> 
>>> tells("swifter")
 s   w  i f  t  e  r  =
 1   1  1 1  1  1  1  7
 19  23 9 6  20 5 18 100
100 500 9 6 200 5 90 910
>>> b.vi(13900)
Job 41:11 Who hath prevented me, that I should repay him? whatsoever is under the whole heaven is mine.
>>> _.tells()
Who hath prevented me, that I should repay him? whatsoever  is under the whole heaven  is mine.   =
 3    4      9      2    4  1    6     5     3      10      2    5    3    5      6    2    4    74
 46  37     109     18  49  9   79     65   30      136     28   62   33   63    55    28   41   888
568  217    829     45  409 9   502   866   57     1369    109  449  213  603    469  109  104  6927
>>> 
>>> 
>>> [v.vn() for v in b if v.text()*osum==888]
[404, 495, 1248, 1336, 2421, 3286, 5296, 7829, 8362, 10022, 11318, 13900, 14972, 15660, 16238, 16540, 16827, 17021, 17418, 20274, 24933, 24980, 25158, 25331, 25745, 27038, 28162, 28491, 28827, 30266, 31018]
>>> Sel(b,_@sub*1)
Genesis 17:6;19:37;41:52;44:11;Exodus 30:38;Leviticus 19:4;Deuteronomy 14:5;1 Samuel 23:18;2 Samuel 14:5;2 Kings 17:38;2 Chronicles 6:35;Job 41:11;Psalms 69:36;106:8;138:6;Proverbs 5:22;15:19;22:5;Ecclesiastes 5:20;Jeremiah 51:61;Luke 1:39;2:6;6:11;9:29;19:13;Acts 4:15;Romans 9:6;1 Corinthians 7:3;2 Corinthians 2:2;Hebrews 13:24;Revelation 18:24 (31 verses)
>>> 
>>> 
>>> 
>>> 
>>> b[1:1].text()*sums
(6677, 76580, 511559)
>>> b[1].text()*psum
11481554
