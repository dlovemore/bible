>>> b.ws()[29999:30003]
['And', 'when', 'they', 'had']
>>> b.ws()[29999:30005]
['And', 'when', 'they', 'had', 'eaten', 'them']
>>> b/'And when they had eaten them'
Genesis 41:21 And when they had eaten them up, it could not be known that they had eaten them; but they were still ill favoured, as at the beginning. So I awoke.
>>> _.vn()
1217
>>> b/'And when they had eaten them'
Genesis 41:21 And when they had eaten them up, it could not be known that they had eaten them; but they were still ill favoured, as at the beginning. So I awoke.
>>> Genesis[41:21].tells()
And when they had eaten them up,  it could not be known that they had eaten them; but they were still ill favoured,  as  at the beginning.  So I awoke.   =
 3    4    4   3    5     4   2   2    5    3   2   5     4    4   3    5     4    3    4    4    5    3      8      2   2   3       9      2  1    5    113
 19  50   58   13   45   46   37  29   55   49  7   77   49   58   13   45    46   43  58   51    72   33     92     20  21  33     81      34 9   55    1298
 55  563  913  13  261   253 370 209  397  310  7  680   409  913  13  261   253  502  913  600  369   69    866    101 201 213     189    160 9   586  10658
>>> 1433*ns
[1433] [227]
>>> b.vi(22722)
Nahum 3:9 Ethiopia and Egypt were her strength, and it was infinite; Put and Lubim were thy helpers.
>>> _.tells()
Ethiopia and Egypt were her strength, and  it was infinite; Put and Lubim were thy helpers.   =
    8     3    5     4   3      8      3   2   3      8      3   3    5     4   3      7     72
   83     19   73   51   31    111     19  29  43     86     57  19   57   51   53    83     865
   362    55  982   600 103    660     55 209 601    338    570  55  381   600 908    308   6787
>>> "Ethiopia and Egypt were her strength, and  it was infinite"*tells
Ethiopia and Egypt were her strength, and  it was infinite   =
    8     3    5     4   3      8      3   2   3      8     47
   83     19   73   51   31    111     19  29  43    86     545
   362    55  982   600 103    660     55 209 601    338   3965
>>> 113-47
66
>>> "Put and Lubim were thy helpers."*tells
Put and Lubim were thy helpers.   =
 3   3    5     4   3      7     25
 57  19   57   51   53    83     320
570  55  381   600 908    308   2822
>>> Nahum[3:9].midw()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/bible/search.py", line 106, in __getattr__
    if len(bs)!=1: raise AttributeError('No attribute '+name)
AttributeError: No attribute midw
>>> Nahum[3:9].midword()
['it', 'was']
>>> Nahum[3:9].midword()@tells
i  t   =
1  1   2
9  20  29
9 200 209
 w  a  s   =
 1  1  1   3
 23 1  19  43
500 1 100 601
[None, None]
>>> 9*1433
12897
>>> 19*1433
27227
>>> 144*19
2736
>>> b/144000
Revelation 7:4;14:1,3 (3 verses)
>>> b[7:77]
Numbers 7:77 And for a sacrifice of peace offerings, two oxen, five rams, five he goats, five lambs of the first year: this was the offering of Pagiel the son of Ocran.
>>> b/'Pagiel'
Numbers 1:13;2:27;7:72,77;10:26 (5 verses)
>>> 27227*ns
[19, 1433] [8, 227]
>>> b/'Pagiel'
Numbers 1:13;2:27;7:72,77;10:26 (5 verses)
>>> p(_.vn)
<bound method Sel.vn of Numbers 1:13;2:27;7:72,77;10:26 (5 verses)>
>>> p(_.vn())
3618
>>> p(_@method.vn)
3618 3686 3923 3928 4015
>>> p(_@m.vn)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'm' is not defined
>>> m=method
>>> p(_@m.vn)
3618 3686 3923 3928 4015
>>> p(_@m.wc)
7 27 16 30 18
>>> tells('oxen')
 o  x  e  n  =
 1  1  1  1  4
15  24 5 14  58
60 600 5 50 715
>>> tells('rams he-goats lambs')
rams he-goats lambs  =
  4      7      5    16
 51     75      47  173
 231    381    173  785
>>> tells('rams he-goats lambs of the first year')
rams he-goats lambs of the first year   =
  4      7      5    2  3    5     4   30
 51     75      47  21  33   72   49   348
 231    381    173  66 213  405   796 2265
>>> tells('rams goats lambs')
rams goats lambs  =
  4    5     5    14
 51    62    47  160
 231  368   173  772
>>> tells('rams egoats lambs')
rams egoats lambs  =
  4     6     5    15
 51    67     47  165
 231   373   173  777
>>> b/'pag'
Numbers 1:13;2:27;7:72,77;10:26 (5 verses)
>>> tells("page")
 p a g e  =
 1 1 1 1  4
16 1 7 5 29
70 1 7 5 83
>>> tells("pag")
 p a g  =
 1 1 1  3
16 1 7 24
70 1 7 78
>>> tells("pag iel")
pag iel  =
 3   3   6
 24  26  50
 78  44 122
>>> tells("pagiel")
 p a g i e  l  =
 1 1 1 1 1  1  6
16 1 7 9 5 12  50
70 1 7 9 5 30 122
>>> tells("pagiels")
 p a g i e  l  s   =
 1 1 1 1 1  1  1   7
16 1 7 9 5 12  19  69
70 1 7 9 5 30 100 222
>>> Numbers[7].ws()[776]
'the'
>>> Numbers[7].ws()[777]
'children'
>>> Numbers[7].ws()[776:790]
['the', 'children', 'of', 'Simeon', 'did', 'offer', 'His', 'offering', 'was', 'one', 'silver', 'charger', 'the', 'weight']
>>> Numbers[7].ws()[1600:1623]
['of', 'Pagiel', 'the', 'son', 'of', 'Ocran', 'On', 'the', 'twelfth', 'day', 'Ahira', 'the', 'son', 'of', 'Enan', 'prince', 'of', 'the', 'children', 'of', 'Naphtali', 'offered', 'His']
>>> b/'enan
  File "<stdin>", line 1
    b/'enan
          ^
SyntaxError: EOL while scanning string literal
>>> b/'enan'
Numbers 1:15;2:29;7:78,83;10:27 (5 verses)
>>> p(_)
Numbers 1:15 Of Naphtali; Ahira the son of Enan.
Numbers 2:29 Then the tribe of Naphtali: and the captain of the children of Naphtali shall be Ahira the son of Enan.
Numbers 7:78 On the twelfth day Ahira the son of Enan, prince of the children of Naphtali, offered:
Numbers 7:83 And for a sacrifice of peace offerings, two oxen, five rams, five he goats, five lambs of the first year: this was the offering of Ahira the son of Enan.
Numbers 10:27 And over the host of the tribe of the children of Naphtali was Ahira the son of Enan.
>>> tells('oxen')
 o  x  e  n  =
 1  1  1  1  4
15  24 5 14  58
60 600 5 50 715
>>> tells('oxen')*2
 o  x  e  n  =
 1  1  1  1  4
15  24 5 14  58
60 600 5 50 715
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'
>>> 58*2
116
>>> 715*2
1430
>>> tells('rams goats lambs')
rams goats lambs  =
  4    5     5    14
 51    62    47  160
 231  368   173  772
>>> 772*5
3860
>>> 777*5
3885
>>> 779*5
3895
>>> 779*5+2*715
5325
>>> 777*5+2*714
5313
>>> 772*5+2*715
5290
>>> five.vc()
5852
>>> tells('enan')
e  n a  n  =
1  1 1  1  4
5 14 1 14  34
5 50 1 50 106
>>> tells('ocran')
 o c  r a  n  =
 1 1  1 1  1  5
15 3 18 1 14  51
60 3 90 1 50 204
>>> tells('ahira')
a h i  r a  =
1 1 1  1 1  5
1 8 9 18 1  37
1 8 9 90 1 109
>>> b/'pagiel'
Numbers 1:13;2:27;7:72,77;10:26 (5 verses)
>>> p(_)
Numbers 1:13 Of Asher; Pagiel the son of Ocran.
Numbers 2:27 And those that encamp by him shall be the tribe of Asher: and the captain of the children of Asher shall be Pagiel the son of Ocran.
Numbers 7:72 On the eleventh day Pagiel the son of Ocran, prince of the children of Asher, offered:
Numbers 7:77 And for a sacrifice of peace offerings, two oxen, five rams, five he goats, five lambs of the first year: this was the offering of Pagiel the son of Ocran.
Numbers 10:26 And over the host of the tribe of the children of Asher was Pagiel the son of Ocran.
>>> p(_.ws()@X[0])
['O', 'A', 'P', 't', 's', 'o', 'O', 'A', 't', 't', 'e', 'b', 'h', 's', 'b', 't', 't', 'o', 'A', 'a', 't', 'c', 'o', 't', 'c', 'o', 'A', 's', 'b', 'P', 't', 's', 'o', 'O', 'O', 't', 'e', 'd', 'P', 't', 's', 'o', 'O', 'p', 'o', 't', 'c', 'o', 'A', 'o', 'A', 'f', 'a', 's', 'o', 'p', 'o', 't', 'o', 'f', 'r', 'f', 'h', 'g', 'f', 'l', 'o', 't', 'f', 'y', 't', 'w', 't', 'o', 'o', 'P', 't', 's', 'o', 'O', 'A', 'o', 't', 'h', 'o', 't', 't', 'o', 't', 'c', 'o', 'A', 'w', 'P', 't', 's', 'o', 'O']
>>> p(_.ws()@X[0])@F(''.join)
['O', 'A', 'P', 't', 's', 'o', 'O', 'A', 't', 't', 'e', 'b', 'h', 's', 'b', 't', 't', 'o', 'A', 'a', 't', 'c', 'o', 't', 'c', 'o', 'A', 's', 'b', 'P', 't', 's', 'o', 'O', 'O', 't', 'e', 'd', 'P', 't', 's', 'o', 'O', 'p', 'o', 't', 'c', 'o', 'A', 'o', 'A', 'f', 'a', 's', 'o', 'p', 'o', 't', 'o', 'f', 'r', 'f', 'h', 'g', 'f', 'l', 'o', 't', 'f', 'y', 't', 'w', 't', 'o', 'o', 'P', 't', 's', 'o', 'O', 'A', 'o', 't', 'h', 'o', 't', 't', 'o', 't', 'c', 'o', 'A', 'w', 'P', 't', 's', 'o', 'O']
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 26, in __rmatmul__
    return dmap(self,left)
  File "/home/pi/python/parle/func.py", line 291, in dmap
    return rowtype(l)(map(f,l))
TypeError: 'NoneType' object is not iterable
>>> p(_.ws()@X[0])*F(''.join)
['O', 'A', 'P', 't', 's', 'o', 'O', 'A', 't', 't', 'e', 'b', 'h', 's', 'b', 't', 't', 'o', 'A', 'a', 't', 'c', 'o', 't', 'c', 'o', 'A', 's', 'b', 'P', 't', 's', 'o', 'O', 'O', 't', 'e', 'd', 'P', 't', 's', 'o', 'O', 'p', 'o', 't', 'c', 'o', 'A', 'o', 'A', 'f', 'a', 's', 'o', 'p', 'o', 't', 'o', 'f', 'r', 'f', 'h', 'g', 'f', 'l', 'o', 't', 'f', 'y', 't', 'w', 't', 'o', 'o', 'P', 't', 's', 'o', 'O', 'A', 'o', 't', 'h', 'o', 't', 't', 'o', 't', 'c', 'o', 'A', 'w', 'P', 't', 's', 'o', 'O']
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 31, in __rmul__
    return Fun(left) and compose(left,self) or self(left)
  File "/home/pi/python/parle/func.py", line 24, in __call__
    return self.f(*args,**kwargs)
TypeError: can only join an iterable
>>> p(_.ws()@X[0])
['O', 'A', 'P', 't', 's', 'o', 'O', 'A', 't', 't', 'e', 'b', 'h', 's', 'b', 't', 't', 'o', 'A', 'a', 't', 'c', 'o', 't', 'c', 'o', 'A', 's', 'b', 'P', 't', 's', 'o', 'O', 'O', 't', 'e', 'd', 'P', 't', 's', 'o', 'O', 'p', 'o', 't', 'c', 'o', 'A', 'o', 'A', 'f', 'a', 's', 'o', 'p', 'o', 't', 'o', 'f', 'r', 'f', 'h', 'g', 'f', 'l', 'o', 't', 'f', 'y', 't', 'w', 't', 'o', 'o', 'P', 't', 's', 'o', 'O', 'A', 'o', 't', 'h', 'o', 't', 't', 'o', 't', 'c', 'o', 'A', 'w', 'P', 't', 's', 'o', 'O']
>>> ''.join(_)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: sequence item 0: expected str instance, Sel found
>>> p(...join(_.ws()@X[0]))
  File "<stdin>", line 1
    p(...join(_.ws()@X[0]))
            ^
SyntaxError: invalid syntax
>>> p(''.join(_.ws()@X[0]))
OAPtsoOAttebhsbttoAatcotcoAsbPtsoOOtedPtsoOpotcoAoAfasopotofrfhgflotfytwtooPtsoOAothottotcoAwPtsoO
>>> p(''.join(_.ws()@(X[0]*tells)))
 O  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 P  =
 1  1
16 16
70 70
 t   =
 1   1
 20  20
200 200
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 t   =
 1   1
 20  20
200 200
 t   =
 1   1
 20  20
200 200
e =
1 1
5 5
5 5
b =
1 1
2 2
2 2
h =
1 1
8 8
8 8
 s   =
 1   1
 19  19
100 100
b =
1 1
2 2
2 2
 t   =
 1   1
 20  20
200 200
 t   =
 1   1
 20  20
200 200
 o  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
a =
1 1
1 1
1 1
 t   =
 1   1
 20  20
200 200
c =
1 1
3 3
3 3
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
c =
1 1
3 3
3 3
 o  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 s   =
 1   1
 19  19
100 100
b =
1 1
2 2
2 2
 P  =
 1  1
16 16
70 70
 t   =
 1   1
 20  20
200 200
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
e =
1 1
5 5
5 5
d =
1 1
4 4
4 4
 P  =
 1  1
16 16
70 70
 t   =
 1   1
 20  20
200 200
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
 p  =
 1  1
16 16
70 70
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
c =
1 1
3 3
3 3
 o  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 o  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
f =
1 1
6 6
6 6
a =
1 1
1 1
1 1
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 p  =
 1  1
16 16
70 70
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
 o  =
 1  1
15 15
60 60
f =
1 1
6 6
6 6
 r  =
 1  1
18 18
90 90
f =
1 1
6 6
6 6
h =
1 1
8 8
8 8
g =
1 1
7 7
7 7
f =
1 1
6 6
6 6
 l  =
 1  1
12 12
30 30
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
f =
1 1
6 6
6 6
 y   =
 1   1
 25  25
700 700
 t   =
 1   1
 20  20
200 200
 w   =
 1   1
 23  23
500 500
 t   =
 1   1
 20  20
200 200
 o  =
 1  1
15 15
60 60
 o  =
 1  1
15 15
60 60
 P  =
 1  1
16 16
70 70
 t   =
 1   1
 20  20
200 200
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
h =
1 1
8 8
8 8
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
 t   =
 1   1
 20  20
200 200
 o  =
 1  1
15 15
60 60
 t   =
 1   1
 20  20
200 200
c =
1 1
3 3
3 3
 o  =
 1  1
15 15
60 60
A =
1 1
1 1
1 1
 w   =
 1   1
 23  23
500 500
 P  =
 1  1
16 16
70 70
 t   =
 1   1
 20  20
200 200
 s   =
 1   1
 19  19
100 100
 o  =
 1  1
15 15
60 60
 O  =
 1  1
15 15
60 60
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: sequence item 0: expected str instance, NoneType found
>>> p(_@method.ws@(I@X[0])@F(''.join)@tells))
  File "<stdin>", line 1
    p(_@method.ws@(I@X[0])@F(''.join)@tells))
                                            ^
SyntaxError: invalid syntax
>>> p(_@method.ws@(I@X[0])@F(''.join)@tells)))
  File "<stdin>", line 1
    p(_@method.ws@(I@X[0])@F(''.join)@tells)))
                                            ^
SyntaxError: invalid syntax
>>> p(_@method.ws@(I@X[0])@F(''.join)@tells)
 O A  P  t   s   o  O  =
 1 1  1  1   1   1  1  7
15 1 16  20  19 15 15 101
60 1 70 200 100 60 60 551
A  t   t  e b h  s  b  t   t   o A a  t  c  o  t  c  o A  s  b  P  t   s   o  O   =
1  1   1  1 1 1  1  1  1   1   1 1 1  1  1  1  1  1  1 1  1  1  1  1   1   1  1  27
1  20  20 5 2 8  19 2  20  20 15 1 1  20 3 15  20 3 15 1  19 2 16  20  19 15 15  317
1 200 200 5 2 8 100 2 200 200 60 1 1 200 3 60 200 3 60 1 100 2 70 200 100 60 60 2099
 O  t  e d  P  t   s   o  O  p  o  t  c  o A  o   =
 1  1  1 1  1  1   1   1  1  1  1  1  1  1 1  1  16
15  20 5 4 16  20  19 15 15 16 15  20 3 15 1 15  214
60 200 5 4 70 200 100 60 60 70 60 200 3 60 1 60 1213
A f a  s   o  p  o  t   o f  r f h g f  l  o  t  f  y   t   w   t   o  o  P  t   s   o  O   =
1 1 1  1   1  1  1  1   1 1  1 1 1 1 1  1  1  1  1  1   1   1   1   1  1  1  1   1   1  1  30
1 6 1  19 15 16 15  20 15 6 18 6 8 7 6 12 15  20 6  25  20  23  20 15 15 16  20  19 15 15  415
1 6 1 100 60 70 60 200 60 6 90 6 8 7 6 30 60 200 6 700 200 500 200 60 60 70 200 100 60 60 3187
A  o  t  h  o  t   t   o  t  c  o A  w   P  t   s   o  O   =
1  1  1  1  1  1   1   1  1  1  1 1  1   1  1   1   1  1  18
1 15  20 8 15  20  20 15  20 3 15 1  23 16  20  19 15 15  261
1 60 200 8 60 200 200 60 200 3 60 1 500 70 200 100 60 60 2043
None None None None None
>>> Genesis.vc()
1533
>>> 551+1213+2043
3807
>>> five.vc()
5852
>>> b/'pagiel'
Numbers 1:13;2:27;7:72,77;10:26 (5 verses)
>>> Numbers[1:13].tells()
Of Asher; Pagiel the son of Ocran.   =
 2    5      6    3   3   2    5    26
21   51     50    33  48 21   51    275
66   204    122  213 210 66   204  1085
>>> Numbers[3:55].tells()
>>> Numbers[35:5].tells()
And  ye shall measure from without the city  on the east side two thousand cubits, and  on the south side two thousand cubits, and  on the west side two thousand cubits, and  on the north side two thousand cubits; and the city shall be in the midst: this shall be  to them the suburbs of the cities.   =
 3   2    5      7      4     7     3    4   2   3    4    4   3      8       6     3   2   3    5     4   3      8       6     3   2   3    4    4   3      8       6     3   2   3    5     4   3      8       6     3   3    4    5    2  2  3     5     4    5    2  2    4   3     7     2  3     6     231
 19  30   52     82    52    116    33  57   29  33  45   37   58    102      74    19  29  33   83   37   58    102      74    19  29  33  67   37   58    102      74    19  29  33   75   37   58    102      74    19  33  57    52   7 23  33   65    56    52   7  35  46   33   102   21  33    65    2839
 55 705  169    541    196   1277  213  912 110 213  306  118 760    723     614    55 110 213  668   118 760    723     614    55 110 213  805  118 760    723     614    55 110 213  408   118 760    723     614    55 213  912  169   7 59 213   353   317  169   7 260  253 213   894   66 213   326   21271
>>> tells('Authorised')
A  u   t  h  o  r i  s  e d  =
1  1   1  1  1  1 1  1  1 1  10
1  21  20 8 15 18 9  19 5 4 120
1 300 200 8 60 90 9 100 5 4 777
>>> Numbers[2:27].tells()
And those that encamp  by him shall be the tribe of Asher: and the captain of the children of Asher shall be Pagiel the son of Ocran.   =
 3    5     4     6    2   3    5    2  3    5    2    5    3   3     7     2  3      8     2   5     5    2    6    3   3   2    5    104
 19   67   49    52    27  30   52   7  33   54  21   51    19  33    64   21  33    73    21   51    52   7   50    33  48 21   51   1039
 55  373   409   169  702  57  169   7 213  306  66   204   55 213   334   66 213    199   66  204   169   7   122  213 210 66   204  5071
>>> [v for v in b if v.ws()[0]=='Of']
[Genesis 6:20 Of fowls after their kind, and of cattle after their kind, of every creeping thing of the earth after his kind, two of every sort shall come unto thee, to keep them alive., Genesis 7:2 Of every clean beast thou shalt take to thee by sevens, the male and his female: and of beasts that are not clean by two, the male and his female., Genesis 7:3 Of fowls also of the air by sevens, the male and the female; to keep seed alive upon the face of all the earth., Genesis 7:8 Of clean beasts, and of beasts that are not clean, and of fowls, and of every thing that creepeth upon the earth,, Exodus 25:39 Of a talent of pure gold shall he make it, with all these vessels., Exodus 37:24 Of a talent of pure gold made he it, and all the vessels thereof., Leviticus 11:8 Of their flesh shall ye not eat, and their carcase shall ye not touch; they are unclean to you., Leviticus 11:34 Of all meat which may be eaten, that on which such water cometh shall be unclean: and all drink that may be drunk in every such vessel shall be unclean., Numbers 1:6 Of Simeon; Shelumiel the son of Zurishaddai., Numbers 1:7 Of Judah; Nahshon the son of Amminadab., Numbers 1:8 Of Issachar; Nethaneel the son of Zuar., Numbers 1:9 Of Zebulun; Eliab the son of Helon., Numbers 1:10 Of the children of Joseph: of Ephraim; Elishama the son of Ammihud: of Manasseh; Gamaliel the son of Pedahzur., Numbers 1:11 Of Benjamin; Abidan the son of Gideoni., Numbers 1:12 Of Dan; Ahiezer the son of Ammishaddai., Numbers 1:13 Of Asher; Pagiel the son of Ocran., Numbers 1:14 Of Gad; Eliasaph the son of Deuel., Numbers 1:15 Of Naphtali; Ahira the son of Enan., Numbers 1:22 Of the children of Simeon, by their generations, after their families, by the house of their fathers, those that were numbered of them, according to the number of the names, by their polls, every male from twenty years old and upward, all that were able to go forth to war;, Numbers 1:24 Of the children of Gad, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:26 Of the children of Judah, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:28 Of the children of Issachar, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:30 Of the children of Zebulun, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:32 Of the children of Joseph, namely, of the children of Ephraim, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:34 Of the children of Manasseh, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:36 Of the children of Benjamin, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:38 Of the children of Dan, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:40 Of the children of Asher, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 1:42 Of the children of Naphtali, throughout their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;, Numbers 3:21 Of Gershon was the family of the Libnites, and the family of the Shimites: these are the families of the Gershonites., Numbers 3:33 Of Merari was the family of the Mahlites, and the family of the Mushites: these are the families of Merari., Numbers 3:50 Of the firstborn of the children of Israel took he the money; a thousand three hundred and threescore and five shekels, after the shekel of the sanctuary:, Numbers 13:5 Of the tribe of Simeon, Shaphat the son of Hori., Numbers 13:6 Of the tribe of Judah, Caleb the son of Jephunneh., Numbers 13:7 Of the tribe of Issachar, Igal the son of Joseph., Numbers 13:8 Of the tribe of Ephraim, Oshea the son of Nun., Numbers 13:9 Of the tribe of Benjamin, Palti the son of Raphu., Numbers 13:10 Of the tribe of Zebulun, Gaddiel the son of Sodi., Numbers 13:11 Of the tribe of Joseph, namely, of the tribe of Manasseh, Gaddi the son of Susi., Numbers 13:12 Of the tribe of Dan, Ammiel the son of Gemalli., Numbers 13:13 Of the tribe of Asher, Sethur the son of Michael., Numbers 13:14 Of the tribe of Naphtali, Nahbi the son of Vophsi., Numbers 13:15 Of the tribe of Gad, Geuel the son of Machi., Numbers 15:21 Of the first of your dough ye shall give unto the LORD an heave offering in your generations., Numbers 26:6 Of Hezron, the family of the Hezronites: of Carmi, the family of the Carmites., Numbers 26:13 Of Zerah, the family of the Zarhites: of Shaul, the family of the Shaulites., Numbers 26:16 Of Ozni, the family of the Oznites: of Eri, the family of the Erites:, Numbers 26:17 Of Arod, the family of the Arodites: of Areli, the family of the Arelites., Numbers 26:23 Of the sons of Issachar after their families: of Tola, the family of the Tolaites: of Pua, the family of the Punites:, Numbers 26:24 Of Jashub, the family of the Jashubites: of Shimron, the family of the Shimronites., Numbers 26:26 Of the sons of Zebulun after their families: of Sered, the family of the Sardites: of Elon, the family of the Elonites: of Jahleel, the family of the Jahleelites., Numbers 26:29 Of the sons of Manasseh: of Machir, the family of the Machirites: and Machir begat Gilead: of Gilead come the family of the Gileadites., Numbers 26:39 Of Shupham, the family of the Shuphamites: of Hupham, the family of the Huphamites., Numbers 26:44 Of the children of Asher after their families: of Jimna, the family of the Jimnites: of Jesui, the family of the Jesuites: of Beriah, the family of the Beriites., Numbers 26:45 Of the sons of Beriah: of Heber, the family of the Heberites: of Malchiel, the family of the Malchielites., Numbers 26:48 Of the sons of Naphtali after their families: of Jahzeel, the family of the Jahzeelites: of Guni, the family of the Gunites:, Numbers 26:49 Of Jezer, the family of the Jezerites: of Shillem, the family of the Shillemites., Numbers 31:4 Of every tribe a thousand, throughout all the tribes of Israel, shall ye send to the war., Numbers 34:21 Of the tribe of Benjamin, Elidad the son of Chislon., Deuteronomy 14:11 Of all clean birds ye shall eat., Deuteronomy 15:3 Of a foreigner thou mayest exact it again: but that which is thine with thy brother thine hand shall release;, Deuteronomy 32:18 Of the Rock that begat thee thou art unmindful, and hast forgotten God that formed thee., 2 Samuel 8:12 Of Syria, and of Moab, and of the children of Ammon, and of the Philistines, and of Amalek, and of the spoil of Hadadezer, son of Rehob, king of Zobah., 1 Kings 11:2 Of the nations concerning which the LORD said unto the children of Israel, Ye shall not go in to them, neither shall they come in unto you: for surely they will turn away your heart after their gods: Solomon clave unto these in love., 2 Kings 19:17 Of a truth, LORD, the kings of Assyria have destroyed the nations and their lands,, 1 Chronicles 6:20 Of Gershom; Libni his son, Jahath his son, Zimmah his son,, 1 Chronicles 11:21 Of the three, he was more honourable than the two; for he was their captain: howbeit he attained not to the first three., 1 Chronicles 12:25 Of the children of Simeon, mighty men of valour for the war, seven thousand and one hundred., 1 Chronicles 12:26 Of the children of Levi four thousand and six hundred., 1 Chronicles 12:33 Of Zebulun, such as went forth to battle, expert in war, with all instruments of war, fifty thousand, which could keep rank: they were not of double heart., 1 Chronicles 15:5 Of the sons of Kohath; Uriel the chief, and his brethren an hundred and twenty:, 1 Chronicles 15:6 Of the sons of Merari; Asaiah the chief, and his brethren two hundred and twenty:, 1 Chronicles 15:7 Of the sons of Gershom; Joel the chief and his brethren an hundred and thirty:, 1 Chronicles 15:8 Of the sons of Elizaphan; Shemaiah the chief, and his brethren two hundred:, 1 Chronicles 15:9 Of the sons of Hebron; Eliel the chief, and his brethren fourscore:, 1 Chronicles 15:10 Of the sons of Uzziel; Amminadab the chief, and his brethren an hundred and twelve., 1 Chronicles 22:16 Of the gold, the silver, and the brass, and the iron, there is no number. Arise therefore, and be doing, and the LORD be with thee., 1 Chronicles 23:4 Of which, twenty and four thousand were to set forward the work of the house of the LORD; and six thousand were officers and judges:, 1 Chronicles 23:7 Of the Gershonites were, Laadan, and Shimei., 1 Chronicles 23:16 Of the sons of Gershom, Shebuel was the chief., 1 Chronicles 23:18 Of the sons of Izhar; Shelomith the chief., 1 Chronicles 23:19 Of the sons of Hebron; Jeriah the first, Amariah the second, Jahaziel the third, and Jekameam the fourth., 1 Chronicles 23:20 Of the sons of Uzziel; Micah the first and Jesiah the second., 1 Chronicles 24:22 Of the Izharites; Shelomoth: of the sons of Shelomoth; Jahath., 1 Chronicles 24:24 Of the sons of Uzziel; Michah: of the sons of Michah; Shamir., 1 Chronicles 24:28 Of Mahli came Eleazar, who had no sons., 1 Chronicles 25:2 Of the sons of Asaph; Zaccur, and Joseph, and Nethaniah, and Asarelah, the sons of Asaph under the hands of Asaph, which prophesied according to the order of the king., 1 Chronicles 25:3 Of Jeduthun: the sons of Jeduthun; Gedaliah, and Zeri, and Jeshaiah, Hashabiah, and Mattithiah, six, under the hands of their father Jeduthun, who prophesied with a harp, to give thanks and to praise the LORD., 1 Chronicles 25:4 Of Heman: the sons of Heman: Bukkiah, Mattaniah, Uzziel, Shebuel, and Jerimoth, Hananiah, Hanani, Eliathah, Giddalti, and Romamtiezer, Joshbekashah, Mallothi, Hothir, and Mahazioth:, 1 Chronicles 26:23 Of the Amramites, and the Izharites, the Hebronites, and the Uzzielites:, 1 Chronicles 26:29 Of the Izharites, Chenaniah and his sons were for the outward business over Israel, for officers and judges., 1 Chronicles 27:3 Of the children of Perez was the chief of all the captains of the host for the first month., 1 Chronicles 27:17 Of the Levites, Hashabiah the son of Kemuel: of the Aaronites, Zadok:, 1 Chronicles 27:18 Of Judah, Elihu, one of the brethren of David: of Issachar, Omri the son of Michael:, 1 Chronicles 27:19 Of Zebulun, Ishmaiah the son of Obadiah: of Naphtali, Jerimoth the son of Azriel:, 1 Chronicles 27:20 Of the children of Ephraim, Hoshea the son of Azaziah: of the half tribe of Manasseh, Joel the son of Pedaiah:, 1 Chronicles 27:21 Of the half tribe of Manasseh in Gilead, Iddo the son of Zechariah: of Benjamin, Jaasiel the son of Abner:, 1 Chronicles 27:22 Of Dan, Azareel the son of Jeroham. These were the princes of the tribes of Israel., Ezra 8:2 Of the sons of Phinehas; Gershom: of the sons of Ithamar; Daniel: of the sons of David; Hattush., Ezra 8:3 Of the sons of Shechaniah, of the sons of Pharosh; Zechariah: and with him were reckoned by genealogy of the males an hundred and fifty., Ezra 8:4 Of the sons of Pahathmoab; Elihoenai the son of Zerahiah, and with him two hundred males., Ezra 8:5 Of the sons of Shechaniah; the son of Jahaziel, and with him three hundred males., Ezra 8:6 Of the sons also of Adin; Ebed the son of Jonathan, and with him fifty males., Ezra 8:9 Of the sons of Joab; Obadiah the son of Jehiel, and with him two hundred and eighteen males., Ezra 8:14 Of the sons also of Bigvai; Uthai, and Zabbud, and with them seventy males., Ezra 10:24 Of the singers also; Eliashib: and of the porters; Shallum, and Telem, and Uri., Ezra 10:28 Of the sons also of Bebai; Jehohanan, Hananiah, Zabbai, and Athlai., Ezra 10:33 Of the sons of Hashum; Mattenai, Mattathah, Zabad, Eliphelet, Jeremai, Manasseh, and Shimei., Ezra 10:34 Of the sons of Bani; Maadai, Amram, and Uel,, Ezra 10:43 Of the sons of Nebo; Jeiel, Mattithiah, Zabad, Zebina, Jadau, and Joel, Benaiah., Nehemiah 11:10 Of the priests: Jedaiah the son of Joiarib, Jachin., Nehemiah 12:13 Of Ezra, Meshullam; of Amariah, Jehohanan;, Nehemiah 12:14 Of Melicu, Jonathan; of Shebaniah, Joseph;, Nehemiah 12:15 Of Harim, Adna; of Meraioth, Helkai;, Nehemiah 12:16 Of Iddo, Zechariah; of Ginnethon, Meshullam;, Nehemiah 12:17 Of Abijah, Zichri; of Miniamin, of Moadiah, Piltai:, Nehemiah 12:18 Of Bilgah, Shammua; of Shemaiah, Jehonathan;, Nehemiah 12:20 Of Sallai, Kallai; of Amok, Eber;, Nehemiah 12:21 Of Hilkiah, Hashabiah; of Jedaiah, Nethaneel., Psalms 102:25 Of old hast thou laid the foundation of the earth: and the heavens are the work of thy hands., Isaiah 9:7 Of the increase of his government and peace there shall be no end, upon the throne of David, and upon his kingdom, to order it, and to establish it with judgment and with justice from henceforth even for ever. The zeal of the LORD of hosts will perform this., Isaiah 37:18 Of a truth, LORD, the kings of Assyria have laid waste all the nations, and their countries,, Ezekiel 27:6 Of the oaks of Bashan have they made thine oars; the company of the Ashurites have made thy benches of ivory, brought out of the isles of Chittim., Ezekiel 45:2 Of this there shall be for the sanctuary five hundred in length, with five hundred in breadth, square round about; and fifty cubits round about for the suburbs thereof., Luke 12:44 Of a truth I say unto you, that he will make him ruler over all that he hath., John 16:9 Of sin, because they believe not on me;, John 16:10 Of righteousness, because I go to my Father, and ye see me no more;, John 16:11 Of judgment, because the prince of this world is judged., Acts 13:23 Of this man's seed hath God according to his promise raised unto Israel a Saviour, Jesus:, Acts 25:26 Of whom I have no certain thing to write unto my lord. Wherefore I have brought him forth before you, and specially before thee, O king Agrippa, that, after examination had, I might have somewhat to write., 2 Corinthians 11:24 Of the Jews five times received I forty stripes save one., 2 Corinthians 12:5 Of such an one will I glory: yet of myself I will not glory, but in mine infirmities., Ephesians 3:15 Of whom the whole family in heaven and earth is named,, 1 Timothy 1:20 Of whom is Hymenaeus and Alexander; whom I have delivered unto Satan, that they may learn not to blaspheme., 2 Timothy 2:14 Of these things put them in remembrance, charging them before the Lord that they strive not about words to no profit, but to the subverting of the hearers., 2 Timothy 4:15 Of whom be thou ware also; for he hath greatly withstood our words., Hebrews 5:11 Of whom we have many things to say, and hard to be uttered, seeing ye are dull of hearing., Hebrews 6:2 Of the doctrine of baptisms, and of laying on of hands, and of resurrection of the dead, and of eternal judgment., Hebrews 10:29 Of how much sorer punishment, suppose ye, shall he be thought worthy, who hath trodden under foot the Son of God, and hath counted the blood of the covenant, wherewith he was sanctified, an unholy thing, and hath done despite unto the Spirit of grace?, Hebrews 11:18 Of whom it was said, That in Isaac shall thy seed be called:, Hebrews 11:38 (Of whom the world was not worthy:) they wandered in deserts, and in mountains, and in dens and caves of the earth., James 1:18 Of his own will begat he us with the word of truth, that we should be a kind of firstfruits of his creatures., 1 Peter 1:10 Of which salvation the prophets have enquired and searched diligently, who prophesied of the grace that should come unto you:, Revelation 7:5 Of the tribe of Juda were sealed twelve thousand. Of the tribe of Reuben were sealed twelve thousand. Of the tribe of Gad were sealed twelve thousand., Revelation 7:6 Of the tribe of Aser were sealed twelve thousand. Of the tribe of Nephthalim were sealed twelve thousand. Of the tribe of Manasses were sealed twelve thousand., Revelation 7:7 Of the tribe of Simeon were sealed twelve thousand. Of the tribe of Levi were sealed twelve thousand. Of the tribe of Issachar were sealed twelve thousand., Revelation 7:8 Of the tribe of Zabulon were sealed twelve thousand. Of the tribe of Joseph were sealed twelve thousand. Of the tribe of Benjamin were sealed twelve thousand.]
>>> [v.ix for v in b if v.ws()[0]=='Of']
[range(157, 158), range(161, 162), range(162, 163), range(167, 168), range(2234, 2235), range(2628, 2629), range(3005, 3006), range(3031, 3032), range(3610, 3611), range(3611, 3612), range(3612, 3613), range(3613, 3614), range(3614, 3615), range(3615, 3616), range(3616, 3617), range(3617, 3618), range(3618, 3619), range(3619, 3620), range(3626, 3627), range(3628, 3629), range(3630, 3631), range(3632, 3633), range(3634, 3635), range(3636, 3637), range(3638, 3639), range(3640, 3641), range(3642, 3643), range(3644, 3645), range(3646, 3647), range(3713, 3714), range(3725, 3726), range(3742, 3743), range(4080, 4081), range(4081, 4082), range(4082, 4083), range(4083, 4084), range(4084, 4085), range(4085, 4086), range(4086, 4087), range(4087, 4088), range(4088, 4089), range(4089, 4090), range(4090, 4091), range(4174, 4175), range(4495, 4496), range(4502, 4503), range(4505, 4506), range(4506, 4507), range(4512, 4513), range(4513, 4514), range(4515, 4516), range(4518, 4519), range(4528, 4529), range(4533, 4534), range(4534, 4535), range(4537, 4538), range(4538, 4539), range(4668, 4669), range(4837, 4838), range(5301, 5302), range(5322, 5323), range(5776, 5777), range(8221, 8222), range(9110, 9111), range(10078, 10079), range(10474, 10475), range(10694, 10695), range(10745, 10746), range(10746, 10747), range(10753, 10754), range(10796, 10797), range(10797, 10798), range(10798, 10799), range(10799, 10800), range(10800, 10801), range(10801, 10802), range(10980, 10981), range(10987, 10988), range(10990, 10991), range(10999, 11000), range(11001, 11002), range(11002, 11003), range(11003, 11004), range(11037, 11038), range(11039, 11040), range(11043, 11044), range(11048, 11049), range(11049, 11050), range(11050, 11051), range(11100, 11101), range(11106, 11107), range(11112, 11113), range(11126, 11127), range(11127, 11128), range(11128, 11129), range(11129, 11130), range(11130, 11131), range(11131, 11132), range(12203, 12204), range(12204, 12205), range(12205, 12206), range(12206, 12207), range(12207, 12208), range(12210, 12211), range(12215, 12216), range(12276, 12277), range(12280, 12281), range(12285, 12286), range(12286, 12287), range(12295, 12296), range(12598, 12599), range(12637, 12638), range(12638, 12639), range(12639, 12640), range(12640, 12641), range(12641, 12642), range(12642, 12643), range(12644, 12645), range(12645, 12646), range(15546, 15547), range(17836, 17837), range(18370, 18371), range(21127, 21128), range(21632, 21633), range(25503, 25504), range(26735, 26736), range(26736, 26737), range(26737, 26738), range(27385, 27386), range(27822, 27823), range(29013, 29014), range(29027, 29028), range(29266, 29267), range(29716, 29717), range(29841, 29842), range(29885, 29886), range(30041, 30042), range(30046, 30047), range(30162, 30163), range(30190, 30191), range(30210, 30211), range(30284, 30285), range(30384, 30385), range(30815, 30816), range(30816, 30817), range(30817, 30818), range(30818, 30819)]
>>> [v.ix.start for v in b if v.ws()[0]=='Of']
[157, 161, 162, 167, 2234, 2628, 3005, 3031, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3626, 3628, 3630, 3632, 3634, 3636, 3638, 3640, 3642, 3644, 3646, 3713, 3725, 3742, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089, 4090, 4174, 4495, 4502, 4505, 4506, 4512, 4513, 4515, 4518, 4528, 4533, 4534, 4537, 4538, 4668, 4837, 5301, 5322, 5776, 8221, 9110, 10078, 10474, 10694, 10745, 10746, 10753, 10796, 10797, 10798, 10799, 10800, 10801, 10980, 10987, 10990, 10999, 11001, 11002, 11003, 11037, 11039, 11043, 11048, 11049, 11050, 11100, 11106, 11112, 11126, 11127, 11128, 11129, 11130, 11131, 12203, 12204, 12205, 12206, 12207, 12210, 12215, 12276, 12280, 12285, 12286, 12295, 12598, 12637, 12638, 12639, 12640, 12641, 12642, 12644, 12645, 15546, 17836, 18370, 21127, 21632, 25503, 26735, 26736, 26737, 27385, 27822, 29013, 29027, 29266, 29716, 29841, 29885, 30041, 30046, 30162, 30190, 30210, 30284, 30384, 30815, 30816, 30817, 30818]
>>> Sel(b,[v.ix.start for v in b if v.ws()[0]=='Of'])
Genesis 6:20;7:2-3,8;Exodus 25:39;37:24;Leviticus 11:8,34;Numbers 1:6-15,22,24,26,28,30,32,34,36,38,40,42;3:21,33,50;13:5-15;15:21;26:6,13,16-17,23-24,26,29,39,44-45,48-49;31:4;34:21;Deuteronomy 14:11;15:3;32:18;2 Samuel 8:12;1 Kings 11:2;2 Kings 19:17;1 Chronicles 6:20;11:21;12:25-26,33;15:5-10;22:16;23:4,7,16,18-20;24:22,24,28;25:2-4;26:23,29;27:3,17-22;Ezra 8:2-6,9,14;10:24,28,33-34,43;Nehemiah 11:10;12:13-18,20-21;Psalms 102:25;Isaiah 9:7;37:18;Ezekiel 27:6;45:2;Luke 12:44;John 16:9-11;Acts 13:23;25:26;2 Corinthians 11:24;12:5;Ephesians 3:15;1 Timothy 1:20;2 Timothy 2:14;4:15;Hebrews 5:11;6:2;10:29;11:18,38;James 1:18;1 Peter 1:10;Revelation 7:5-8 (147 verses)
>>> p(_)
Genesis 6:20 Of fowls after their kind, and of cattle after their kind, of every creeping thing of the earth after his kind, two of every sort shall come unto thee, to keep them alive.
Genesis 7
2 Of every clean beast thou shalt take to thee by sevens, the male and his female: and of beasts that are not clean by two, the male and his female.
3 Of fowls also of the air by sevens, the male and the female; to keep seed alive upon the face of all the earth.
8 Of clean beasts, and of beasts that are not clean, and of fowls, and of every thing that creepeth upon the earth,
Exodus 25:39 Of a talent of pure gold shall he make it, with all these vessels.
Exodus 37:24 Of a talent of pure gold made he it, and all the vessels thereof.
Leviticus 11:8 Of their flesh shall ye not eat, and their carcase shall ye not touch; they are unclean to you.
Leviticus 11:34 Of all meat which may be eaten, that on which such water cometh shall be unclean: and all drink that may be drunk in every such vessel shall be unclean.
Numbers 1
6 Of Simeon; Shelumiel the son of Zurishaddai.
7 Of Judah; Nahshon the son of Amminadab.
8 Of Issachar; Nethaneel the son of Zuar.
9 Of Zebulun; Eliab the son of Helon.
10 Of the children of Joseph: of Ephraim; Elishama the son of Ammihud: of Manasseh; Gamaliel the son of Pedahzur.
11 Of Benjamin; Abidan the son of Gideoni.
12 Of Dan; Ahiezer the son of Ammishaddai.
13 Of Asher; Pagiel the son of Ocran.
14 Of Gad; Eliasaph the son of Deuel.
15 Of Naphtali; Ahira the son of Enan.
22 Of the children of Simeon, by their generations, after their families, by the house of their fathers, those that were numbered of them, according to the number of the names, by their polls, every male from twenty years old and upward, all that were able to go forth to war;
24 Of the children of Gad, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
26 Of the children of Judah, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
28 Of the children of Issachar, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
30 Of the children of Zebulun, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
32 Of the children of Joseph, namely, of the children of Ephraim, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
34 Of the children of Manasseh, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
36 Of the children of Benjamin, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
38 Of the children of Dan, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
40 Of the children of Asher, by their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
42 Of the children of Naphtali, throughout their generations, after their families, by the house of their fathers, according to the number of the names, from twenty years old and upward, all that were able to go forth to war;
Numbers 3
21 Of Gershon was the family of the Libnites, and the family of the Shimites: these are the families of the Gershonites.
33 Of Merari was the family of the Mahlites, and the family of the Mushites: these are the families of Merari.
50 Of the firstborn of the children of Israel took he the money; a thousand three hundred and threescore and five shekels, after the shekel of the sanctuary:
Numbers 13
5 Of the tribe of Simeon, Shaphat the son of Hori.
6 Of the tribe of Judah, Caleb the son of Jephunneh.
7 Of the tribe of Issachar, Igal the son of Joseph.
8 Of the tribe of Ephraim, Oshea the son of Nun.
9 Of the tribe of Benjamin, Palti the son of Raphu.
10 Of the tribe of Zebulun, Gaddiel the son of Sodi.
11 Of the tribe of Joseph, namely, of the tribe of Manasseh, Gaddi the son of Susi.
12 Of the tribe of Dan, Ammiel the son of Gemalli.
13 Of the tribe of Asher, Sethur the son of Michael.
14 Of the tribe of Naphtali, Nahbi the son of Vophsi.
15 Of the tribe of Gad, Geuel the son of Machi.
Numbers 15:21 Of the first of your dough ye shall give unto the LORD an heave offering in your generations.
Numbers 26
6 Of Hezron, the family of the Hezronites: of Carmi, the family of the Carmites.
13 Of Zerah, the family of the Zarhites: of Shaul, the family of the Shaulites.
16 Of Ozni, the family of the Oznites: of Eri, the family of the Erites:
17 Of Arod, the family of the Arodites: of Areli, the family of the Arelites.
23 Of the sons of Issachar after their families: of Tola, the family of the Tolaites: of Pua, the family of the Punites:
24 Of Jashub, the family of the Jashubites: of Shimron, the family of the Shimronites.
26 Of the sons of Zebulun after their families: of Sered, the family of the Sardites: of Elon, the family of the Elonites: of Jahleel, the family of the Jahleelites.
29 Of the sons of Manasseh: of Machir, the family of the Machirites: and Machir begat Gilead: of Gilead come the family of the Gileadites.
39 Of Shupham, the family of the Shuphamites: of Hupham, the family of the Huphamites.
44 Of the children of Asher after their families: of Jimna, the family of the Jimnites: of Jesui, the family of the Jesuites: of Beriah, the family of the Beriites.
45 Of the sons of Beriah: of Heber, the family of the Heberites: of Malchiel, the family of the Malchielites.
48 Of the sons of Naphtali after their families: of Jahzeel, the family of the Jahzeelites: of Guni, the family of the Gunites:
49 Of Jezer, the family of the Jezerites: of Shillem, the family of the Shillemites.
Numbers 31:4 Of every tribe a thousand, throughout all the tribes of Israel, shall ye send to the war.
Numbers 34:21 Of the tribe of Benjamin, Elidad the son of Chislon.
Deuteronomy 14:11 Of all clean birds ye shall eat.
Deuteronomy 15:3 Of a foreigner thou mayest exact it again: but that which is thine with thy brother thine hand shall release;
Deuteronomy 32:18 Of the Rock that begat thee thou art unmindful, and hast forgotten God that formed thee.
2 Samuel 8:12 Of Syria, and of Moab, and of the children of Ammon, and of the Philistines, and of Amalek, and of the spoil of Hadadezer, son of Rehob, king of Zobah.
1 Kings 11:2 Of the nations concerning which the LORD said unto the children of Israel, Ye shall not go in to them, neither shall they come in unto you: for surely they will turn away your heart after their gods: Solomon clave unto these in love.
2 Kings 19:17 Of a truth, LORD, the kings of Assyria have destroyed the nations and their lands,
1 Chronicles 6:20 Of Gershom; Libni his son, Jahath his son, Zimmah his son,
1 Chronicles 11:21 Of the three, he was more honourable than the two; for he was their captain: howbeit he attained not to the first three.
1 Chronicles 12
25 Of the children of Simeon, mighty men of valour for the war, seven thousand and one hundred.
26 Of the children of Levi four thousand and six hundred.
33 Of Zebulun, such as went forth to battle, expert in war, with all instruments of war, fifty thousand, which could keep rank: they were not of double heart.
1 Chronicles 15
5 Of the sons of Kohath; Uriel the chief, and his brethren an hundred and twenty:
6 Of the sons of Merari; Asaiah the chief, and his brethren two hundred and twenty:
7 Of the sons of Gershom; Joel the chief and his brethren an hundred and thirty:
8 Of the sons of Elizaphan; Shemaiah the chief, and his brethren two hundred:
9 Of the sons of Hebron; Eliel the chief, and his brethren fourscore:
10 Of the sons of Uzziel; Amminadab the chief, and his brethren an hundred and twelve.
1 Chronicles 22:16 Of the gold, the silver, and the brass, and the iron, there is no number. Arise therefore, and be doing, and the LORD be with thee.
1 Chronicles 23
4 Of which, twenty and four thousand were to set forward the work of the house of the LORD; and six thousand were officers and judges:
7 Of the Gershonites were, Laadan, and Shimei.
16 Of the sons of Gershom, Shebuel was the chief.
18 Of the sons of Izhar; Shelomith the chief.
19 Of the sons of Hebron; Jeriah the first, Amariah the second, Jahaziel the third, and Jekameam the fourth.
20 Of the sons of Uzziel; Micah the first and Jesiah the second.
1 Chronicles 24
22 Of the Izharites; Shelomoth: of the sons of Shelomoth; Jahath.
24 Of the sons of Uzziel; Michah: of the sons of Michah; Shamir.
28 Of Mahli came Eleazar, who had no sons.
1 Chronicles 25
2 Of the sons of Asaph; Zaccur, and Joseph, and Nethaniah, and Asarelah, the sons of Asaph under the hands of Asaph, which prophesied according to the order of the king.
3 Of Jeduthun: the sons of Jeduthun; Gedaliah, and Zeri, and Jeshaiah, Hashabiah, and Mattithiah, six, under the hands of their father Jeduthun, who prophesied with a harp, to give thanks and to praise the LORD.
4 Of Heman: the sons of Heman: Bukkiah, Mattaniah, Uzziel, Shebuel, and Jerimoth, Hananiah, Hanani, Eliathah, Giddalti, and Romamtiezer, Joshbekashah, Mallothi, Hothir, and Mahazioth:
1 Chronicles 26:23 Of the Amramites, and the Izharites, the Hebronites, and the Uzzielites:
1 Chronicles 26:29 Of the Izharites, Chenaniah and his sons were for the outward business over Israel, for officers and judges.
1 Chronicles 27
3 Of the children of Perez was the chief of all the captains of the host for the first month.
17 Of the Levites, Hashabiah the son of Kemuel: of the Aaronites, Zadok:
18 Of Judah, Elihu, one of the brethren of David: of Issachar, Omri the son of Michael:
19 Of Zebulun, Ishmaiah the son of Obadiah: of Naphtali, Jerimoth the son of Azriel:
20 Of the children of Ephraim, Hoshea the son of Azaziah: of the half tribe of Manasseh, Joel the son of Pedaiah:
21 Of the half tribe of Manasseh in Gilead, Iddo the son of Zechariah: of Benjamin, Jaasiel the son of Abner:
22 Of Dan, Azareel the son of Jeroham. These were the princes of the tribes of Israel.
Ezra 8
2 Of the sons of Phinehas; Gershom: of the sons of Ithamar; Daniel: of the sons of David; Hattush.
3 Of the sons of Shechaniah, of the sons of Pharosh; Zechariah: and with him were reckoned by genealogy of the males an hundred and fifty.
4 Of the sons of Pahathmoab; Elihoenai the son of Zerahiah, and with him two hundred males.
5 Of the sons of Shechaniah; the son of Jahaziel, and with him three hundred males.
6 Of the sons also of Adin; Ebed the son of Jonathan, and with him fifty males.
9 Of the sons of Joab; Obadiah the son of Jehiel, and with him two hundred and eighteen males.
14 Of the sons also of Bigvai; Uthai, and Zabbud, and with them seventy males.
Ezra 10
24 Of the singers also; Eliashib: and of the porters; Shallum, and Telem, and Uri.
28 Of the sons also of Bebai; Jehohanan, Hananiah, Zabbai, and Athlai.
33 Of the sons of Hashum; Mattenai, Mattathah, Zabad, Eliphelet, Jeremai, Manasseh, and Shimei.
34 Of the sons of Bani; Maadai, Amram, and Uel,
43 Of the sons of Nebo; Jeiel, Mattithiah, Zabad, Zebina, Jadau, and Joel, Benaiah.
Nehemiah 11:10 Of the priests: Jedaiah the son of Joiarib, Jachin.
Nehemiah 12
13 Of Ezra, Meshullam; of Amariah, Jehohanan;
14 Of Melicu, Jonathan; of Shebaniah, Joseph;
15 Of Harim, Adna; of Meraioth, Helkai;
16 Of Iddo, Zechariah; of Ginnethon, Meshullam;
17 Of Abijah, Zichri; of Miniamin, of Moadiah, Piltai:
18 Of Bilgah, Shammua; of Shemaiah, Jehonathan;
20 Of Sallai, Kallai; of Amok, Eber;
21 Of Hilkiah, Hashabiah; of Jedaiah, Nethaneel.
Psalms 102:25 Of old hast thou laid the foundation of the earth: and the heavens are the work of thy hands.
Isaiah 9:7 Of the increase of his government and peace there shall be no end, upon the throne of David, and upon his kingdom, to order it, and to establish it with judgment and with justice from henceforth even for ever. The zeal of the LORD of hosts will perform this.
Isaiah 37:18 Of a truth, LORD, the kings of Assyria have laid waste all the nations, and their countries,
Ezekiel 27:6 Of the oaks of Bashan have they made thine oars; the company of the Ashurites have made thy benches of ivory, brought out of the isles of Chittim.
Ezekiel 45:2 Of this there shall be for the sanctuary five hundred in length, with five hundred in breadth, square round about; and fifty cubits round about for the suburbs thereof.
Luke 12:44 Of a truth I say unto you, that he will make him ruler over all that he hath.
John 16
9 Of sin, because they believe not on me;
10 Of righteousness, because I go to my Father, and ye see me no more;
11 Of judgment, because the prince of this world is judged.
Acts 13:23 Of this man's seed hath God according to his promise raised unto Israel a Saviour, Jesus:
Acts 25:26 Of whom I have no certain thing to write unto my lord. Wherefore I have brought him forth before you, and specially before thee, O king Agrippa, that, after examination had, I might have somewhat to write.
2 Corinthians 11:24 Of the Jews five times received I forty stripes save one.
2 Corinthians 12:5 Of such an one will I glory: yet of myself I will not glory, but in mine infirmities.
Ephesians 3:15 Of whom the whole family in heaven and earth is named,
1 Timothy 1:20 Of whom is Hymenaeus and Alexander; whom I have delivered unto Satan, that they may learn not to blaspheme.
2 Timothy 2:14 Of these things put them in remembrance, charging them before the Lord that they strive not about words to no profit, but to the subverting of the hearers.
2 Timothy 4:15 Of whom be thou ware also; for he hath greatly withstood our words.
Hebrews 5:11 Of whom we have many things to say, and hard to be uttered, seeing ye are dull of hearing.
Hebrews 6:2 Of the doctrine of baptisms, and of laying on of hands, and of resurrection of the dead, and of eternal judgment.
Hebrews 10:29 Of how much sorer punishment, suppose ye, shall he be thought worthy, who hath trodden under foot the Son of God, and hath counted the blood of the covenant, wherewith he was sanctified, an unholy thing, and hath done despite unto the Spirit of grace?
Hebrews 11:18 Of whom it was said, That in Isaac shall thy seed be called:
Hebrews 11:38 (Of whom the world was not worthy:) they wandered in deserts, and in mountains, and in dens and caves of the earth.
James 1:18 Of his own will begat he us with the word of truth, that we should be a kind of firstfruits of his creatures.
1 Peter 1:10 Of which salvation the prophets have enquired and searched diligently, who prophesied of the grace that should come unto you:
Revelation 7
5 Of the tribe of Juda were sealed twelve thousand. Of the tribe of Reuben were sealed twelve thousand. Of the tribe of Gad were sealed twelve thousand.
6 Of the tribe of Aser were sealed twelve thousand. Of the tribe of Nephthalim were sealed twelve thousand. Of the tribe of Manasses were sealed twelve thousand.
7 Of the tribe of Simeon were sealed twelve thousand. Of the tribe of Levi were sealed twelve thousand. Of the tribe of Issachar were sealed twelve thousand.
8 Of the tribe of Zabulon were sealed twelve thousand. Of the tribe of Joseph were sealed twelve thousand. Of the tribe of Benjamin were sealed twelve thousand.
>>> 147*ns
[3, 7, 7] [2, 4, 4]
>>> 147/60
2.45
>>> 49/20
2.45
>>> Sel(b,[v.ix.start for v in b if v.ws()[0]=='Of'])
Genesis 6:20;7:2-3,8;Exodus 25:39;37:24;Leviticus 11:8,34;Numbers 1:6-15,22,24,26,28,30,32,34,36,38,40,42;3:21,33,50;13:5-15;15:21;26:6,13,16-17,23-24,26,29,39,44-45,48-49;31:4;34:21;Deuteronomy 14:11;15:3;32:18;2 Samuel 8:12;1 Kings 11:2;2 Kings 19:17;1 Chronicles 6:20;11:21;12:25-26,33;15:5-10;22:16;23:4,7,16,18-20;24:22,24,28;25:2-4;26:23,29;27:3,17-22;Ezra 8:2-6,9,14;10:24,28,33-34,43;Nehemiah 11:10;12:13-18,20-21;Psalms 102:25;Isaiah 9:7;37:18;Ezekiel 27:6;45:2;Luke 12:44;John 16:9-11;Acts 13:23;25:26;2 Corinthians 11:24;12:5;Ephesians 3:15;1 Timothy 1:20;2 Timothy 2:14;4:15;Hebrews 5:11;6:2;10:29;11:18,38;James 1:18;1 Peter 1:10;Revelation 7:5-8 (147 verses)
>>> 

