>>> from bible import *
>>> Numbers/'meek'
Numbers 12:3 (Now the man Moses was very meek, above all the men which were upon the face of the earth.)
>>> tells("laughing")
 l a  u  g h i  n g  =
 1 1  1  1 1 1  1 1  8
12 1  21 7 8 9 14 7  79
30 1 300 7 8 9 50 7 412
>>> tells("Isaac")
I  s  a a c  =
1  1  1 1 1  5
9  19 1 1 3  33
9 100 1 1 3 114
>>> tells("rib bing")
rib bing  =
 3    4   7
 29  32   61
101  68  169
>>> 101/3
33.666666666666664
>>> 29/3
9.666666666666666
>>> tells("LXX")
 L  X   X    =
 1  1   1    3
12  24  24  60
30 600 600 1230
>>> tells("septugint")
 s  e  p  t   u  g i  n  t   =
 1  1  1  1   1  1 1  1  1   9
 19 5 16  20  21 7 9 14  20 131
100 5 70 200 300 7 9 50 200 941
>>> "ah 2 hah 2 h"
invalid syntax (<10>, line 1)
>>> 
>>> b.lc()
3222405
