>>> from bible import *
>>> 253
253
>>> tri(22)+24+25
302
>>> tri(22)+1300
1553
>>> tri(22)-3+33
283
>>> 0x3a+0x3b
117
>>> base12(22)
'1↊'
>>> base(12,22)
[1, 10]
>>> int('3a3b')
<9>:1: ValueError: invalid literal for int() with base 10: '3a3b'
>>> int('3a3b',12)
6671
>>> int('3a3b',16)
14907
>>> int('3a',16)
58
>>> int('3b',16)
59
>>> int('3b',12)
47
>>> 46+47
93
>>> 0x3a3b
14907
>>> Psalm[149]
Psalms 149:1-9 (9 verses)
>>> Psalm[149:7]
Psalms 149:7 To execute vengeance upon the heathen, and punishments upon the people;
>>> b.vi(14907)
Psalms 68:6 God setteth the solitary in families: he bringeth out those which are bound with chains: but the rebellious dwell in a dry land.
>>> b[46:47]

>>> b[47:46]

>>> b/'man'/'animal'

>>> b/'beast'
Genesis 1:24-25,30;2:19-20;3:1,14;6:7;7:2,8,14,21;8:19-20;9:2,5,10;31:39;34:23;36:6;37:20,33;45:17;Exodus 8:17-18;9:9-10,19,22,25;11:5,7;12:12;13:2,12,15;19:13;21:34;22:5,10,19,31;23:11,29;Leviticus 5:2;7:21,24-26;11:2-3,26-27,39,46-47;17:13,15;18:23;20:15-16,25;22:8;24:18,21;25:7;26:6,22;27:9-11,26-28;Numbers 3:13;8:17;18:15;20:8,11;31:11,26,30,47;35:3;Deuteronomy 4:17;7:22;14:4,6;27:21;28:26;32:24;Judges 20:48;1 Samuel 17:44,46;2 Samuel 21:10;1 Kings 4:33;18:5;2 Kings 3:17;14:9;2 Chronicles 25:18;32:28;Ezra 1:4,6;Nehemiah 2:12,14;Job 5:22-23;12:7;18:3;35:11;37:8;39:15;40:20;Psalms 8:7;36:6;49:12,20;50:10-11;73:22;79:2;80:13;104:11,20,25;135:8;147:9;148:10;Proverbs 9:2;12:10;30:30;Ecclesiastes 3:18-19,21;Isaiah 1:11;13:21-22;18:6;30:6;34:14;35:9;40:16;43:20;46:1;56:9;63:14;66:20;Jeremiah 7:20,33;9:10;12:4,9;15:3;16:4;19:7;21:6;27:5;28:14;31:27;32:43;33:10,12;34:20;36:29;50:3,39;51:62;Ezekiel 5:17;8:10;14:13,15,17,19,21;25:13;29:5,8,11;31:6,13;32:4,13;33:27;34:5,8,25,28;36:11;38:20;39:4,17;44:31;Daniel 2:38;4:12,14-16,21,23,25,32;5:21;7:3,5-7,11-12,17,19,23;8:4;Hosea 2:12,18;4:3;13:8;Joel 1:18,20;2:22;Amos 5:22;Jonah 3:7-8;Micah 1:13;5:8;Habakkuk 2:17;Zephaniah 1:3;2:14-15;Zechariah 8:10;14:15;Mark 1:13;Luke 10:34;Acts 7:42;10:12;11:6;23:24;28:4-5;Romans 1:23;1 Corinthians 15:32,39;Titus 1:12;Hebrews 12:20;13:11;James 3:7;2 Peter 2:12;Jude 1:10;Revelation 4:6-9;5:6,8,11,14-6:1,3,5-8;7:11;11:7;13:1-4,11-12,14-15,17-18;14:3,9,11;15:2,7;16:2,10,13;17:3,7-8,11-13,16-17;18:13;19:4,19-20;20:4,10 (296 verses)
>>> b/'beast'/'man'
Genesis 6:7;7:21;9:5;Exodus 8:17-18;9:9-10,19,22,25;11:7;12:12;13:2,15;19:13;22:5,10;23:11;Leviticus 7:21,26;11:27;17:13;20:15,25;24:21;27:9,26,28;Numbers 3:13;8:17;18:15;31:26,30,47;Deuteronomy 27:21;28:26;2 Chronicles 32:28;Nehemiah 2:12;Psalms 36:6;49:12,20;135:8;Proverbs 12:10;Ecclesiastes 3:18-19,21;Jeremiah 7:20;21:6;27:5;31:27;32:43;33:10,12;36:29;50:3;51:62;Ezekiel 14:13,15,17,19,21;25:13;29:8,11;32:13;36:11;39:17;Daniel 4:16;Jonah 3:7-8;Micah 5:8;Zephaniah 1:3;Zechariah 8:10;Acts 10:12;28:4;Romans 1:23;1 Corinthians 15:32;James 3:7;Revelation 4:7;5:11;13:15,17-18;14:3,9 (85 verses)
>>> b/'beast'/'man'/'consider'

>>> b/'beast'/'man'/'is'
Exodus 11:7;13:2;Leviticus 17:13;27:26,28;Numbers 3:13;8:17;31:30,47;Psalms 36:6;49:12,20;Ecclesiastes 3:19;Jeremiah 31:27;32:43;33:12;Ezekiel 39:17;Jonah 3:8;Acts 28:4;James 3:7;Revelation 13:18 (21 verses)
>>> p(_)
Exodus 11:7 But against any of the children of Israel shall not a dog move his tongue, against man or beast: that ye may know how that the LORD doth put a difference between the Egyptians and Israel.
Exodus 13:2 Sanctify unto me all the firstborn, whatsoever openeth the womb among the children of Israel, both of man and of beast: it is mine.
Leviticus 17:13 And whatsoever man there be of the children of Israel, or of the strangers that sojourn among you, which hunteth and catcheth any beast or fowl that may be eaten; he shall even pour out the blood thereof, and cover it with dust.
Leviticus 27:26 Only the firstling of the beasts, which should be the LORD's firstling, no man shall sanctify it; whether it be ox, or sheep: it is the LORD's.
Leviticus 27:28 Notwithstanding no devoted thing, that a man shall devote unto the LORD of all that he hath, both of man and beast, and of the field of his possession, shall be sold or redeemed: every devoted thing is most holy unto the LORD.
Numbers 3:13 Because all the firstborn are mine; for on the day that I smote all the firstborn in the land of Egypt I hallowed unto me all the firstborn in Israel, both man and beast: mine shall they be: I am the LORD.
Numbers 8:17 For all the firstborn of the children of Israel are mine, both man and beast: on the day that I smote every firstborn in the land of Egypt I sanctified them for myself.
Numbers 31:30 And of the children of Israel's half, thou shalt take one portion of fifty, of the persons, of the beeves, of the asses, and of the flocks, of all manner of beasts, and give them unto the Levites, which keep the charge of the tabernacle of the LORD.
Numbers 31:47 Even of the children of Israel's half, Moses took one portion of fifty, both of man and of beast, and gave them unto the Levites, which kept the charge of the tabernacle of the LORD; as the LORD commanded Moses.
Psalms 36:6 Thy righteousness is like the great mountains; thy judgments are a great deep: O LORD, thou preservest man and beast.
Psalms 49:12 Nevertheless man being in honour abideth not: he is like the beasts that perish.
Psalms 49:20 Man that is in honour, and understandeth not, is like the beasts that perish.
Ecclesiastes 3:19 For that which befalleth the sons of men befalleth beasts; even one thing befalleth them: as the one dieth, so dieth the other; yea, they have all one breath; so that a man hath no preeminence above a beast: for all is vanity.
Jeremiah 31:27 Behold, the days come, saith the LORD, that I will sow the house of Israel and the house of Judah with the seed of man, and with the seed of beast.
Jeremiah 32:43 And fields shall be bought in this land, whereof ye say, It is desolate without man or beast; it is given into the hand of the Chaldeans.
Jeremiah 33:12 Thus saith the LORD of hosts; Again in this place, which is desolate without man and without beast, and in all the cities thereof, shall be an habitation of shepherds causing their flocks to lie down.
Ezekiel 39:17 And, thou son of man, thus saith the Lord GOD; Speak unto every feathered fowl, and to every beast of the field, Assemble yourselves, and come; gather yourselves on every side to my sacrifice that I do sacrifice for you, even a great sacrifice upon the mountains of Israel, that ye may eat flesh, and drink blood.
Jonah 3:8 But let man and beast be covered with sackcloth, and cry mightily unto God: yea, let them turn every one from his evil way, and from the violence that is in their hands.
Acts 28:4 And when the barbarians saw the venomous beast hang on his hand, they said among themselves, No doubt this man is a murderer, whom, though he hath escaped the sea, yet vengeance suffereth not to live.
James 3:7 For every kind of beasts, and of birds, and of serpents, and of things in the sea, is tamed, and hath been tamed of mankind:
Revelation 13:18 Here is wisdom. Let him that hath understanding count the number of the beast: for it is the number of a man; and his number is Six hundred threescore and six.
>>> 31*32
992
>>> 46*47
2162
>>> b[31:32]
Genesis 31:32;Numbers 31:32;Job 31:32;Jeremiah 31:32 (4 verses)
>>> p(_)
Genesis 31:32 With whomsoever thou findest thy gods, let him not live: before our brethren discern thou what is thine with me, and take it to thee. For Jacob knew not that Rachel had stolen them.
Numbers 31:32 And the booty, being the rest of the prey which the men of war had caught, was six hundred thousand and seventy thousand and five thousand sheep,
Job 31:32 The stranger did not lodge in the street: but I opened my doors to the traveller.
Jeremiah 31:32 Not according to the covenant that I made with their fathers in the day that I took them by the hand to bring them out of the land of Egypt; which my covenant they brake, although I was an husband unto them, saith the LORD:
>>> 0xab
171
>>> import dna
>>> dna.C
198295559
>>> 198295559
198295559
>>> Psalm[82]
Psalms 82:1-8 (8 verses)
>>> Psalm[118:8]
Psalms 118:8 It is better to trust in the LORD than to put confidence in man.
>>> Psalm[118:9]
Psalms 118:9 It is better to trust in the LORD than to put confidence in princes>>> Psalm[118:8:9].tells()

