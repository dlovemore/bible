>>> 37311373*ns
[11, 47, 72169] [5, 15, 7144]
>>> 37311373*allfactors
[1, 11, 47, 517, 72169, 793859, 3391943, 37311373]
>>> 793859-789629
4230
>>> 793859-790849
3010
>>> 72169+31102
103271
>>> 100000-72169
27831
>>> b.vi(27831)
Acts 26:7 Unto which promise our twelve tribes, instantly serving God day and night, hope to come. For which hope's sake, king Agrippa, I am accused of the Jews.
>>> 610281-609247
1034
>>> 33*33
1089
>>> 33*33-1034
55
>>> 32*33
1056
>>> 32*32
1024
>>> 1034*sos
[]
>>> 1189-1034
155
>>> 1088-1034
54
>>> Genesis[1]/'very good'
Genesis 1:31 And God saw every thing that he had made, and, behold, it was very good. And the evening and the morning were the sixth day.
>>> [v for v in b if v.vc()==3]
[]
>>> [v for v in b if v.vc()==2]
[]
>>> [v for v in b.chapters() if v.vc()==2]
[Psalms 117:1 O praise the LORD, all ye nations: praise him, all ye people.
Psalms 117:2 For his merciful kindness is great toward us: and the truth of the LORD endureth for ever. Praise ye the LORD.]
>>> [v for v in b.chapters() if v.vc()==3]
[Esther 10:1-3 (3 verses), Psalms 131:1-3 (3 verses), Psalms 133:1-3 (3 verses), Psalms 134:1-3 (3 verses)]
>>> _@method.chn
[436, 609, 611, 612]
>>> b.chn(496)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: chn() takes 1 positional argument but 2 were given
>>> b.ch(496)
Psalms 18:1-50 (50 verses)
>>> 3*30
90
>>> 70
70
>>> 63
63
>>> 62
62
>>> 7
7
>>> 70*3
210
>>> 490-210
280
>>> b/'first born'
Genesis 10:15;19:37;27:19;Exodus 11:5 (4 verses)
>>> Genesis[10:15]
Genesis 10:15 And Canaan begat Sidon his first born, and Heth,
>>> Genesis/'perfect'
Genesis 6:9 These are the generations of Noah: Noah was a just man and perfect in his generations, and Noah walked with God.
Genesis 17:1 And when Abram was ninety years old and nine, the LORD appeared to Abram, and said unto him, I am the Almighty God; walk before me, and be thou perfect.
>>> b/'double'
Genesis 41:32;43:12,15;Exodus 22:4,7,9;26:9;28:16;39:9;Deuteronomy 15:18;21:17;2 Kings 2:9;1 Chronicles 12:33;Job 11:6;41:13;Psalms 12:2;Isaiah 40:2;61:7;Jeremiah 16:18;17:18;Ezekiel 21:14;Zechariah 9:12;1 Timothy 3:8;5:17;James 1:8;4:8;Revelation 18:6 (27 verses)
>>> p(_)
Genesis 41:32 And for that the dream was doubled unto Pharaoh twice; it is because the thing is established by God, and God will shortly bring it to pass.
Genesis 43:12 And take double money in your hand; and the money that was brought again in the mouth of your sacks, carry it again in your hand; peradventure it was an oversight:
Genesis 43:15 And the men took that present, and they took double money in their hand and Benjamin; and rose up, and went down to Egypt, and stood before Joseph.
Exodus 22
4 If the theft be certainly found in his hand alive, whether it be ox, or ass, or sheep; he shall restore double.
7 If a man shall deliver unto his neighbour money or stuff to keep, and it be stolen out of the man's house; if the thief be found, let him pay double.
9 For all manner of trespass, whether it be for ox, for ass, for sheep, for raiment, or for any manner of lost thing which another challengeth to be his, the cause of both parties shall come before the judges; and whom the judges shall condemn, he shall pay double unto his neighbour.
Exodus 26:9 And thou shalt couple five curtains by themselves, and six curtains by themselves, and shalt double the sixth curtain in the forefront of the tabernacle.
Exodus 28:16 Foursquare it shall be being doubled; a span shall be the length thereof, and a span shall be the breadth thereof.
Exodus 39:9 It was foursquare; they made the breastplate double: a span was the length thereof, and a span the breadth thereof, being doubled.
Deuteronomy 15:18 It shall not seem hard unto thee, when thou sendest him away free from thee; for he hath been worth a double hired servant to thee, in serving thee six years: and the LORD thy God shall bless thee in all that thou doest.
Deuteronomy 21:17 But he shall acknowledge the son of the hated for the firstborn, by giving him a double portion of all that he hath: for he is the beginning of his strength; the right of the firstborn is his.
2 Kings 2:9 And it came to pass, when they were gone over, that Elijah said unto Elisha, Ask what I shall do for thee, before I be taken away from thee. And Elisha said, I pray thee, let a double portion of thy spirit be upon me.
1 Chronicles 12:33 Of Zebulun, such as went forth to battle, expert in war, with all instruments of war, fifty thousand, which could keep rank: they were not of double heart.
Job 11:6 And that he would shew thee the secrets of wisdom, that they are double to that which is! Know therefore that God exacteth of thee less than thine iniquity deserveth.
Job 41:13 Who can discover the face of his garment? or who can come to him with his double bridle?
Psalms 12:2 They speak vanity every one with his neighbour: with flattering lips and with a double heart do they speak.
Isaiah 40:2 Speak ye comfortably to Jerusalem, and cry unto her, that her warfare is accomplished, that her iniquity is pardoned: for she hath received of the LORD's hand double for all her sins.
Isaiah 61:7 For your shame ye shall have double; and for confusion they shall rejoice in their portion: therefore in their land they shall possess the double: everlasting joy shall be unto them.
Jeremiah 16:18 And first I will recompense their iniquity and their sin double; because they have defiled my land, they have filled mine inheritance with the carcases of their detestable and abominable things.
Jeremiah 17:18 Let them be confounded that persecute me, but let not me be confounded: let them be dismayed, but let not me be dismayed: bring upon them the day of evil, and destroy them with double destruction.
Ezekiel 21:14 Thou therefore, son of man, prophesy, and smite thine hands together. and let the sword be doubled the third time, the sword of the slain: it is the sword of the great men that are slain, which entereth into their privy chambers.
Zechariah 9:12 Turn you to the strong hold, ye prisoners of hope: even to day do I declare that I will render double unto thee;
1 Timothy 3:8 Likewise must the deacons be grave, not doubletongued, not given to much wine, not greedy of filthy lucre;
1 Timothy 5:17 Let the elders that rule well be counted worthy of double honour, especially they who labour in the word and doctrine.
James 1:8 A double minded man is unstable in all his ways.
James 4:8 Draw nigh to God, and he will draw nigh to you. Cleanse your hands, ye sinners; and purify your hearts, ye double minded.
Revelation 18:6 Reward her even as she rewarded you, and double unto her double according to her works: in the cup which she hath filled fill to her double.
>>> b/'what measure'
Matthew 7:2 For with what judgment ye judge, ye shall be judged: and with what measure ye mete, it shall be measured to you again.
Mark 4:24 And he said unto them, Take heed what ye hear: with what measure ye mete, it shall be measured to you: and unto you that hear shall more be given.
>>> 496*allfactors
[1, 2, 4, 8, 16, 31, 62, 124, 248, 496]
>>> 
KeyboardInterrupt
>>> 70+70+50+40
230
>>> 70+70+50+40+200
430
>>> 70+70+50+40+200+70
500
>>> 16+16
32
>>> 27+20
47
>>> 14+16+13+16
59
>>> 14+16+13+16+20
79
>>> 14+16+13+16+14
73
>>> tells("name of Father, name of the Son, name of the Holy Spirit")
name of Father, name of the Son, name of the Holy Spirit   =
  4   2    6      4   2  3    3    4   2  3    4     6    43
 33  21    58    33  21  33  48   33  21  33  60    91    485
 96  66   310    96  66 213  210  96  66 213  798   478  2708
>>> tells("name of Father, name of the Son, name of the Holy Spirit")
name of Father, name of the Son, name of the Holy Spirit   =
  4   2    6      4   2  3    3    4   2  3    4     6    43
 33  21    58    33  21  33  48   33  21  33  60    91    485
 96  66   310    96  66 213  210  96  66 213  798   478  2708
>>> b/'baptizi'
Matthew 28:19;John 1:28,31;3:23 (4 verses)
>>> p(_)
Matthew 28:19 Go ye therefore, and teach all nations, baptizing them in the name of the Father, and of the Son, and of the Holy Ghost:
John 1:28 These things were done in Bethabara beyond Jordan, where John was baptizing.
John 1:31 And I knew him not: but that he should be made manifest to Israel, therefore am I come baptizing with water.
John 3:23 And John also was baptizing in Aenon near to Salim, because there was much water there: and they came, and were baptized.
>>> tells("name of Father, of the Son, of the Holy Ghost")
name of Father, of the Son, of the Holy Ghost   =
  4   2    6     2  3    3   2  3    4    5    34
 33  21    58   21  33  48  21  33  60    69   397
 96  66   310   66 213  210 66 213  798  375  2413
>>> tells("name of Father, of the Son, name of the Holy Ghost")
name of Father, of the Son, name of the Holy Ghost   =
  4   2    6     2  3    3    4   2  3    4    5    38
 33  21    58   21  33  48   33  21  33  60    69   430
 96  66   310   66 213  210  96  66 213  798  375  2509
>>> tells("name of Father, name of the Son, name of the Holy Ghost")
name of Father, name of the Son, name of the Holy Ghost   =
  4   2    6      4   2  3    3    4   2  3    4    5    42
 33  21    58    33  21  33  48   33  21  33  60    69   463
 96  66   310    96  66 213  210  96  66 213  798  375  2605
>>> tells("name of the Father, name of the Son, name of the Holy Ghost")
name of the Father, name of the Son, name of the Holy Ghost   =
  4   2  3     6      4   2  3    3    4   2  3    4    5    45
 33  21  33    58    33  21  33  48   33  21  33  60    69   496
 96  66 213   310    96  66 213  210  96  66 213  798  375  2818
>>> allfactors(tri(7))
[1, 2, 4, 7, 14, 28]
>>> allfactors(tri(7))@(lambda x: p(x,' * ',(496//x),' = 496))
  File "<stdin>", line 1
    allfactors(tri(7))@(lambda x: p(x,' * ',(496//x),' = 496))
                                                             ^
SyntaxError: EOL while scanning string literal
>>> allfactors(tri(7))@(lambda x: p(x,' * ',(496//x),' = 496)
  File "<stdin>", line 1
    allfactors(tri(7))@(lambda x: p(x,' * ',(496//x),' = 496)
                                                            ^
SyntaxError: EOL while scanning string literal
>>> allfactors(tri(7))@(lambda x: p(x,' * ',(496//x),' = 496'))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for @: 'list' and 'function'
>>> allfactors(tri(7))@F(lambda x: p(x,' * ',(496//x),' = 496'))
1  *  496  = 496
2  *  248  = 496
4  *  124  = 496
7  *  70  = 496
14  *  35  = 496
28  *  17  = 496
[None, None, None, None, None, None]
>>> allfactors(tri(7))@F(lambda x: p(x,'*',(496//x),'= 496'))
1 * 496 = 496
2 * 248 = 496
4 * 124 = 496
7 * 70 = 496
14 * 35 = 496
28 * 17 = 496
[None, None, None, None, None, None]
>>> tri(7)
28
>>> tri(31)
496
>>> allfactors(tri(31))@F(lambda x: p(x,'*',(496//x),'= 496'))
1 * 496 = 496
2 * 248 = 496
4 * 124 = 496
8 * 62 = 496
16 * 31 = 496
31 * 16 = 496
62 * 8 = 496
124 * 4 = 496
248 * 2 = 496
496 * 1 = 496
[None, None, None, None, None, None, None, None, None, None]
>>> 
KeyboardInterrupt
>>> b/'double'
Genesis 41:32;43:12,15;Exodus 22:4,7,9;26:9;28:16;39:9;Deuteronomy 15:18;21:17;2 Kings 2:9;1 Chronicles 12:33;Job 11:6;41:13;Psalms 12:2;Isaiah 40:2;61:7;Jeremiah 16:18;17:18;Ezekiel 21:14;Zechariah 9:12;1 Timothy 3:8;5:17;James 1:8;4:8;Revelation 18:6 (27 verses)
>>> Job/'double'
Job 11:6 And that he would shew thee the secrets of wisdom, that they are double to that which is! Know therefore that God exacteth of thee less than thine iniquity deserveth.
Job 41:13 Who can discover the face of his garment? or who can come to him with his double bridle?
>>> James/'double'
James 1:8 A double minded man is unstable in all his ways.
James 4:8 Draw nigh to God, and he will draw nigh to you. Cleanse your hands, ye sinners; and purify your hearts, ye double minded.
>>> Deuteronomy/'double'
Deuteronomy 15:18 It shall not seem hard unto thee, when thou sendest him away free from thee; for he hath been worth a double hired servant to thee, in serving thee six years: and the LORD thy God shall bless thee in all that thou doest.
Deuteronomy 21:17 But he shall acknowledge the son of the hated for the firstborn, by giving him a double portion of all that he hath: for he is the beginning of his strength; the right of the firstborn is his.
>>> b/'given unto you'
Joshua 1:3;23:16;Jeremiah 25:5;Matthew 13:11;Luke 6:38;Philemon 1:22 (6 verses)
>>> p(_
... )
Joshua 1:3 Every place that the sole of your foot shall tread upon, that have I given unto you, as I said unto Moses.
Joshua 23:16 When ye have transgressed the covenant of the LORD your God, which he commanded you, and have gone and served other gods, and bowed yourselves to them; then shall the anger of the LORD be kindled against you, and ye shall perish quickly from off the good land which he hath given unto you.
Jeremiah 25:5 They said, Turn ye again now every one from his evil way, and from the evil of your doings, and dwell in the land that the LORD hath given unto you and to your fathers for ever and ever:
Matthew 13:11 He answered and said unto them, Because it is given unto you to know the mysteries of the kingdom of heaven, but to them it is not given.
Luke 6:38 Give, and it shall be given unto you; good measure, pressed down, and shaken together, and running over, shall men give into your bosom. For with the same measure that ye mete withal it shall be measured to you again.
Philemon 1:22 But withal prepare me also a lodging: for I trust that through your prayers I shall be given unto you.
>>> b/'double portion'
Deuteronomy 21:17 But he shall acknowledge the son of the hated for the firstborn, by giving him a double portion of all that he hath: for he is the beginning of his strength; the right of the firstborn is his.
2 Kings 2:9 And it came to pass, when they were gone over, that Elijah said unto Elisha, Ask what I shall do for thee, before I be taken away from thee. And Elisha said, I pray thee, let a double portion of thy spirit be upon me.
>>> b/'more'/'given'/'hath'
Joshua 23:13;2 Chronicles 2:12;Nahum 1:14;Matthew 13:12;Romans 12:3;1 Corinthians 12:24;2 Corinthians 10:8 (7 verses)
>>> p(_)
Joshua 23:13 Know for a certainty that the LORD your God will no more drive out any of these nations from before you; but they shall be snares and traps unto you, and scourges in your sides, and thorns in your eyes, until ye perish from off this good land which the LORD your God hath given you.
2 Chronicles 2:12 Huram said moreover, Blessed be the LORD God of Israel, that made heaven and earth, who hath given to David the king a wise son, endued with prudence and understanding, that might build an house for the LORD, and an house for his kingdom.
Nahum 1:14 And the LORD hath given a commandment concerning thee, that no more of thy name be sown: out of the house of thy gods will I cut off the graven image and the molten image: I will make thy grave; for thou art vile.
Matthew 13:12 For whosoever hath, to him shall be given, and he shall have more abundance: but whosoever hath not, from him shall be taken away even that he hath.
Romans 12:3 For I say, through the grace given unto me, to every man that is among you, not to think of himself more highly than he ought to think; but to think soberly, according as God hath dealt to every man the measure of faith.
1 Corinthians 12:24 For our comely parts have no need: but God hath tempered the body together, having given more abundant honour to that part which lacked.
2 Corinthians 10:8 For though I should boast somewhat more of our authority, which the Lord hath given us for edification, and not for your destruction, I should not be ashamed:
>>> b/'gen'/'noah'
Genesis 6:9;7:1;10:1,32 (4 verses)
>>> p(_)
Genesis 6:9 These are the generations of Noah: Noah was a just man and perfect in his generations, and Noah walked with God.
Genesis 7:1 And the LORD said unto Noah, Come thou and all thy house into the ark; for thee have I seen righteous before me in this generation.
Genesis 10:1 Now these are the generations of the sons of Noah, Shem, Ham, and Japheth: and unto them were sons born after the flood.
Genesis 10:32 These are the families of the sons of Noah, after their generations, in their nations: and by these were the nations divided in the earth after the flood.
>>> tells("Shem ham Japeth")
Shem ham Japeth  =
  4   3     6    13
 45   22   60   127
 153  49   294  496
>>> tells("Shem Ham Japeth")
Shem Ham Japeth  =
  4   3     6    13
 45   22   60   127
 153  49   294  496
>>> tells("Shem Ham Japeth",ssum)
Shem Ham Japeth  =
  4   3     6    13
 45   22   60   127
 153  49   294  496
 153  49   294  496
>>> tell("Shem Ham Japeth",ssum)
Shem Ham Japeth  =
 153  49   294  496
>>> tells("name of the Father, name of the Son, name of the Holy Ghost")
name of the Father, name of the Son, name of the Holy Ghost   =
  4   2  3     6      4   2  3    3    4   2  3    4    5    45
 33  21  33    58    33  21  33  48   33  21  33  60    69   496
 96  66 213   310    96  66 213  210  96  66 213  798  375  2818
>>> tell("name of the Father, name of the Son, name of the Holy Ghost")
name of the Father, name of the Son, name of the Holy Ghost  =
 33  21  33    58    33  21  33  48   33  21  33  60    69  496
>>> tells("perfection")
 p e  r f e c  t  i  o  n  =
 1 1  1 1 1 1  1  1  1  1  10
16 5 18 6 5 3  20 9 15 14 111
70 5 90 6 5 3 200 9 60 50 498
>>> tells("perfaction")
 p e  r f a c  t  i  o  n  =
 1 1  1 1 1 1  1  1  1  1  10
16 5 18 6 1 3  20 9 15 14 107
70 5 90 6 1 3 200 9 60 50 494
>>> tells("perfecting")
 p e  r f e c  t  i  n g  =
 1 1  1 1 1 1  1  1  1 1  10
16 5 18 6 5 3  20 9 14 7 103
70 5 90 6 5 3 200 9 50 7 445
>>> tells("perfection")
 p e  r f e c  t  i  o  n  =
 1 1  1 1 1 1  1  1  1  1  10
16 5 18 6 5 3  20 9 15 14 111
70 5 90 6 5 3 200 9 60 50 498
>>> tells("surfection")
 s   u   r f e c  t  i  o  n  =
 1   1   1 1 1 1  1  1  1  1  10
 19  21 18 6 5 3  20 9 15 14 130
100 300 90 6 5 3 200 9 60 50 823
>>> tells("persect")
 p e  r  s  e c  t   =
 1 1  1  1  1 1  1   7
16 5 18  19 5 3  20  86
70 5 90 100 5 3 200 473
>>> tells("perfect ion")
perfect ion  =
   7     3   10
   73    38 111
  379   119 498
>>> tells("perfect gon")
perfect gon  =
   7     3   10
   73    36 109
  379   117 496
>>> tells("perfect on g")
perfect  on g  =
   7     2  1  10
   73    29 7 109
  379   110 7 496
>>> tells("perfect ong")
perfect ong  =
   7     3   10
   73    36 109
  379   117 496
>>> tells("perfect")+117
 p e  r f e c  t   =
 1 1  1 1 1 1  1   7
16 5 18 6 5 3  20  73
70 5 90 6 5 3 200 379
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
>>> tells("persect")+117
 p e  r  s  e c  t   =
 1 1  1  1  1 1  1   7
16 5 18  19 5 3  20  86
70 5 90 100 5 3 200 473
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
>>> tells("persect")
 p e  r  s  e c  t   =
 1 1  1  1  1 1  1   7
16 5 18  19 5 3  20  86
70 5 90 100 5 3 200 473
>>> tells("persectle")
 p e  r  s  e c  t   l e  =
 1 1  1  1  1 1  1   1 1  9
16 5 18  19 5 3  20 12 5 103
70 5 90 100 5 3 200 30 5 508
>>> tells("perect els")
perect els  =
   6    3   9
  67    36 103
  373  135 508
>>> tells("perect el")
perect el  =
   6    2  8
  67   17  84
  373  35 408
>>> tells("perect s")
perect  s   =
   6    1   7
  67    19  86
  373  100 473
>>> tells("perfect s")
perfect  s   =
   7     1   8
   73    19  92
  379   100 479
>>> tells("perfect Word")
perfect Word   =
   7      4   11
   73    60   133
  379    654 1033
>>> b/'shem, ham'
Genesis 5:32;6:10;10:1;1 Chronicles 1:4 (4 verses)
>>> p(_)
Genesis 5:32 And Noah was five hundred years old: and Noah begat Shem, Ham, and Japheth.
Genesis 6:10 And Noah begat three sons, Shem, Ham, and Japheth.
Genesis 10:1 Now these are the generations of the sons of Noah, Shem, Ham, and Japheth: and unto them were sons born after the flood.
1 Chronicles 1:4 Noah, Shem, Ham, and Japheth.
>>> Genesis[6:9]
Genesis 6:9 These are the generations of Noah: Noah was a just man and perfect in his generations, and Noah walked with God.
>>> 
KeyboardInterrupt
>>> b/'baptiz
  File "<stdin>", line 1
    b/'baptiz
            ^
SyntaxError: EOL while scanning string literal
>>> b/'baptiz'
Matthew 3:6,11,13-14,16;20:22-23;28:19;Mark 1:4-5,8-9;10:38-39;16:16;Luke 3:7,12,16,21;7:29-30;12:50;John 1:25-26,28,31,33;3:22-23,26;4:1-2;10:40;Acts 1:5;2:38,41;8:12-13,16,36,38;9:18;10:47-48;11:16;16:15,33;18:8;19:3-5;22:16;Romans 6:3;1 Corinthians 1:13-17;10:2;12:13;15:29;Galatians 3:27 (62 verses)
>>> b/'name'/'holy ghost'
Matthew 28:19;Luke 2:25;John 14:26;Acts 2:38 (4 verses)
>>> p(_)
Matthew 28:19 Go ye therefore, and teach all nations, baptizing them in the name of the Father, and of the Son, and of the Holy Ghost:
Luke 2:25 And, behold, there was a man in Jerusalem, whose name was Simeon; and the same man was just and devout, waiting for the consolation of Israel: and the Holy Ghost was upon him.
John 14:26 But the Comforter, which is the Holy Ghost, whom the Father will send in my name, he shall teach you all things, and bring all things to your remembrance, whatsoever I have said unto you.
Acts 2:38 Then Peter said unto them, Repent, and be baptized every one of you in the name of Jesus Christ for the remission of sins, and ye shall receive the gift of the Holy Ghost.
>>> Row("name of the Father", "name of the Son", "name of the Holy Ghost")*tells
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: list expected at most 1 arguments, got 3
>>> Row("name of the Father", "name of the Son", "name of the Holy Ghost")@tells
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: list expected at most 1 arguments, got 3
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")@tells
name of the Father  =
  4   2  3     6    15
 33  21  33   58   145
 96  66 213   310  685
name of the Son  =
  4   2  3   3   12
 33  21  33  48 135
 96  66 213 210 585
name of the Holy Ghost   =
  4   2  3    4    5    18
 33  21  33  60    69   216
 96  66 213  798  375  1548
(None, None, None)
>>> tell("name of the")
name of the  =
 33  21  33 87
>>> 87*3
261
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")@tell
name of the Father  =
 33  21  33   58   145
name of the Son  =
 33  21  33  48 135
name of the Holy Ghost  =
 33  21  33  60    69  216
(None, None, None)
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")@letters*tale
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 31, in __rmul__
    return Fun(left) and compose(left,self) or self(left)
  File "/home/pi/python/parle/func.py", line 24, in __call__
    return self.f(*args,**kwargs)
  File "/home/pi/python/parle/table.py", line 12, in tale
    n+=x
TypeError: unsupported operand type(s) for +=: 'int' and 'str'
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")*L@letters*tale
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 31, in __rmul__
    return Fun(left) and compose(left,self) or self(left)
  File "/home/pi/python/parle/func.py", line 24, in __call__
    return self.f(*args,**kwargs)
  File "/home/pi/python/parle/table.py", line 12, in tale
    n+=x
TypeError: unsupported operand type(s) for +=: 'int' and 'str'
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")*L@letters@Len@tale
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/pi/python/parle/func.py", line 26, in __rmatmul__
    return dmap(self,left)
  File "/home/pi/python/parle/func.py", line 291, in dmap
    return rowtype(l)(map(f,l))
  File "/home/pi/python/parle/table.py", line 11, in tale
    for x in l:
TypeError: 'int' object is not iterable
>>> ("name of the Father", "name of the Son", "name of the Holy Ghost")*L@letters@Len*tale
[15, 27, 45]
>>> b/'mighty'
Genesis 6:4;10:8-9;18:18;23:6;49:24;Exodus 1:7,20;3:19;9:28;10:19;15:10,15;32:11;Leviticus 19:15;Numbers 22:6;Deuteronomy 3:24;4:34,37;5:15;6:21;7:8,19,21,23;9:26,29;10:17;11:2;26:5,8;34:12;Joshua 1:14;4:24;6:2;8:3;10:2,7;Judges 5:13,22-23;6:12;11:1;Ruth 2:1;1 Samuel 2:4;4:8;9:1;16:18;2 Samuel 1:19,21-22,25,27;10:7;16:6;17:8,10;20:7;23:8-9,16-17,22;1 Kings 1:8,10;11:28;2 Kings 5:1;15:20;24:14-15;1 Chronicles 1:10;5:24;7:7,9,11,40;8:40;11:10-11;12:1,4,21,25,28,30;19:8;26:6,31;27:6;28:1;29:24;2 Chronicles 6:32;13:3,21;14:8;17:13-14,16-17;25:6;26:12-13;27:6;28:7;32:3,21;Ezra 4:20;7:28;Nehemiah 3:16;9:11,32;11:14;Job 5:15;6:23;9:4;12:19,21;21:7;22:8;24:22;34:20,24;35:9;36:5;41:25;Psalms 24:8;29:1;33:16;45:3;50:1;52:1;59:3;68:33;69:4;74:15;78:65;82:1;89:6,13,19,50;93:4;106:2,8;112:2;120:4;127:4;132:2,5;135:10;145:4,12;150:2;Proverbs 16:32;18:18;21:22;23:11;Ecclesiastes 7:19;Song of Solomon 4:4;Isaiah 1:24;3:2,25;5:15,22;9:6;10:21,34;11:15;13:3;17:12;21:17;22:17;28:2;30:29;31:8;42:13;43:16;49:24-26;60:16;63:1;Jeremiah 5:15-16;9:23;14:9;20:11;26:21;32:18-19;33:3;41:16;46:5-6,9,12;48:14,41;49:22;50:9,36;51:30,56-57;Lamentations 1:15;Ezekiel 17:13,17;20:33-34;31:11;32:12,21,27;38:15;39:18,20;Daniel 3:20;4:3;8:24;9:15;11:3,25;Hosea 10:13;Joel 2:7;3:9,11;Amos 2:14,16;5:12,24;Obadiah 1:9;Jonah 1:4;Nahum 2:3;Habakkuk 1:12;Zephaniah 1:14;3:17;Zechariah 9:13;10:5,7;11:2;Matthew 11:20-21,23;13:54,58;14:2;Mark 6:2,5,14;Luke 1:49,52;9:43;10:13;15:14;19:37;24:19;Acts 2:2;7:22;18:24;Romans 15:19;1 Corinthians 1:26-27;2 Corinthians 10:4;12:12;13:3;Galatians 2:8;Ephesians 1:19;2 Thessalonians 1:7;1 Peter 5:6;Revelation 6:13,15;10:1;16:18;18:10,21;19:6,18 (277 verses)
>>> b/'his word'
Genesis 37:8;Numbers 27:21;30:2;Deuteronomy 4:36;Judges 11:11;1 Samuel 1:23;3:19;2 Samuel 23:2;1 Kings 2:4;8:20;2 Kings 1:16;2 Chronicles 6:10;10:15;34:27;36:16;Job 22:22;32:12,14;34:35,37;Psalms 55:21;56:4,10;103:20;105:19,28;106:12,24;107:20;130:5;147:15,18-19;148:8;Proverbs 17:27;29:20;30:6;Ecclesiastes 9:16;Isaiah 31:2;66:5;Jeremiah 18:18;20:9;23:18;26:21;Lamentations 2:17;Daniel 9:12;10:6,9;Joel 2:11;Amos 7:10;Matthew 8:16;Mark 10:24;12:13;Luke 4:32;10:39;20:20,26;24:8;John 5:38;Acts 2:41;Titus 1:3;1 John 1:10;2:5 (63 verses)
>>> b/'his word'/'mighty'
Jeremiah 26:21 And when Jehoiakim the king, with all his mighty men, and all the princes, heard his words, the king sought to put him to death: but when Urijah heard it, he was afraid, and fled, and went into Egypt;
>>> b/'word'/'mighty'
1 Chronicles 11:10;Jeremiah 26:21;Luke 24:19;Acts 7:22 (4 verses)
>>> p(_)
1 Chronicles 11:10 These also are the chief of the mighty men whom David had, who strengthened themselves with him in his kingdom, and with all Israel, to make him king, according to the word of the LORD concerning Israel.
Jeremiah 26:21 And when Jehoiakim the king, with all his mighty men, and all the princes, heard his words, the king sought to put him to death: but when Urijah heard it, he was afraid, and fled, and went into Egypt;
Luke 24:19 And he said unto them, What things? And they said unto him, Concerning Jesus of Nazareth, which was a prophet mighty in deed and word before God and all the people:
Acts 7:22 And Moses was learned in all the wisdom of the Egyptians, and was mighty in words and in deeds.
>>> b/'pulling'/'mighty'
2 Corinthians 10:4 (For the weapons of our warfare are not carnal, but mighty through God to the pulling down of strong holds;)
>>> Genesis[2]/'seven'
Genesis 2:2 And on the seventh day God ended his work which he had made; and he rested on the seventh day from all his work which he had made.
Genesis 2:3 And God blessed the seventh day, and sanctified it: because that in it he had rested from all his work which God created and made.
>>> Genesis[2:1]
Genesis 2:1 Thus the heavens and the earth were finished, and all the host of them.
>>> Psalm[119]/'perfect'
Psalms 119:96 I have seen an end of all perfection: but thy commandment is exceeding broad.
>>> _.tells()
I have seen an end of all perfection: but thy commandment  is exceeding broad.   =
1   4    4   2  3   2  3       10      3   3       11      2      9        5    62
9  36   43  15  23 21  25     111      43  53     115      28     76      40    638
9  414  160 51  59 66  61     498     502 908     493     109    688      157  4175
>>> 

